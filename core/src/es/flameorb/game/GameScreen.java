package es.flameorb.game;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import es.flameorb.FlameOrb;
import es.flameorb.game.map.EditorMenuMapState;
import es.flameorb.game.map.Field;
import es.flameorb.game.map.MapData;
import es.flameorb.game.map.PreparationMenuDisplayedMapState;
import es.flameorb.general.GamePreferences;
import es.flameorb.gui.UIFactory;
import es.flameorb.mainmenu.MainMenuScreen;

/**
 * @author Iván Barbosa Gutiérrez
 */
public class GameScreen implements Screen {

    private final FlameOrb game;
    private Stage stage;
    private Field field;
    private Music backgroundMusic;

    public GameScreen(final FlameOrb game, MapData data, boolean editorMode) {
        this.game = game;
        
        if (Gdx.app.getType() == ApplicationType.Android)
        	UIFactory.setFontSize(20);
        else
        	UIFactory.setFontSize(16);

        field = new Field(data, this);
        StretchViewport view;
        view = new StretchViewport(GamePreferences.getWorldSize().x*32, GamePreferences.getWorldSize().y*32, field.getCamera());
        field.setViewport(view);
        stage = new Stage(view, field.getMapBatch());
        stage.addActor(field);
        stage.setKeyboardFocus(field);
        field.setInitialCameraPosition();
        field.placeBattleLog();
        if (editorMode)
        	field.setFirstMapState(new EditorMenuMapState(field));
        else {
        	if (data.getPreparationsMusicFilename() != null)
        		backgroundMusic = Gdx.audio.newMusic(Gdx.files.local("data/music/" + data.getPreparationsMusicFilename()));
        	else
        		backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("music/Video Game Soldiers.mp3"));
        	backgroundMusic.setLooping(true);
        	backgroundMusic.play();
        	field.setFirstMapState(new PreparationMenuDisplayedMapState(field));
        }
        Gdx.input.setInputProcessor(stage);
    }
    
    /**
     * Cambia la música de fondo a la indicada en el fichero. Si el nombre del fichero es nulo, se usará
     * la música por defecto.
     * @param filename - El nombre del fichero de música.
     */
    public void setBattleMusic(String filename) {
    	backgroundMusic.stop();
    	if (filename != null)
    		backgroundMusic = Gdx.audio.newMusic(Gdx.files.local("data/music/" + filename));
    	else
    		backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("music/Fall of the Solar King.mp3"));
    	backgroundMusic.setLooping(true);
    	backgroundMusic.play();
    }
    
    /**
     * Ajusta el mapa para que quede centrado si este es más pequeño que el mundo estipulado.
     */
    public void centerMap() {
    	if (field.getMapLayer().getWidth() < GamePreferences.getWorldSize().x) {
    		float xMovement = (field.getMapLayer().getWidth() - GamePreferences.getWorldSize().x)/2*32;
    		field.getCamera().translate(xMovement, 0f);
    		field.getChildren().peek().moveBy(xMovement, 0);
    		field.getCameraCentering().x = xMovement;
    	}
    	if (field.getMapLayer().getHeight() < GamePreferences.getWorldSize().y) {
    		float yMovement = (field.getMapLayer().getHeight() - GamePreferences.getWorldSize().y)/2*32;
    		field.getCamera().translate(0f, yMovement);
    		field.getChildren().peek().moveBy(0f, yMovement);
    		field.getCameraCentering().y = yMovement;
    	}
    }
    
    /**
     * Retorna al menú principal.
     */
    public void goBackToMenu() {
    	if (backgroundMusic != null)
    		backgroundMusic.stop();
    	UIFactory.setFontSize(32);
    	game.setScreen(new MainMenuScreen(game));
    	dispose();
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.5f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        if (backgroundMusic != null)
        	backgroundMusic.dispose();
    }
}
