package es.flameorb.game.calculators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import es.flameorb.game.map.Point;

/**
 * Lista de nodos cerrados para la implementación del algoritmo A*
 * que realiza el cálculo de las celdas a las que una unidad puede
 * moverse.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class ClosedList {
	
	private List<Pair<Point, Integer>> list = Collections
			.synchronizedList(new ArrayList<Pair<Point, Integer>>());
	
	/**
	 * Adds an element to the list
	 * @param element - The element to add.
	 */
	public void add(Pair<Point, Integer> element) {
		list.add(element);
	}
	
	/**
	 * Devuelve el valor del punto que se encuentra en el índice
	 * indicado.
	 * @param index - El índice de la lista
	 * @return El valor del punto.
	 */
	public int getElementValue(int index) {
		return list.get(index).second;
	}
	
	/**
	 * Busca si el nodo introducido se encuentra en la lista de nodos cerrados.
	 * Si está, comprueba si el nodo se ha alcanzado con menos pasos. Si es cierto,
	 * se devuelve su posición en la lista. Si no, o el nodo no ha sido cerrado, se
	 * devuelve el tamaño de la lista.
	 * @param element - The element to search.
	 * @return The position of the element.
	 */
	public int search(Pair<Point, Integer> element) {
		boolean found = false;
		int i = 0;
		while (!found && i<list.size()) {
			if (list.get(i).first == element.first)
				found = true;
			else
				i++;
		}
		return i;
	}
	
	/**
	 * Returns the size of the list;
	 * @return The size of the list;
	 */
	public int size() {
		return list.size();
	}
	
	/**
	 * Remove the element on the given index.
	 * @param index - The index which element is going to be removed.
	 */
	public void remove(int index) {
		list.remove(index);
	}
}
