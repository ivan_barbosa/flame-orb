package es.flameorb.game.calculators;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.utils.Logger;

import es.flameorb.game.calculators.RangeCalculator.Direction;
import es.flameorb.game.map.TerrainData;
import es.flameorb.game.map.Point;
import es.flameorb.game.map.Terrain;
import es.flameorb.items.Item;
import es.flameorb.items.Weapon;
import es.flameorb.units.Unit;
import es.flameorb.units.UnitPositionList;

/**
 * Esta clase se encarga de calcular el rango de movimiento de
 * una unidad o el rango de alcance de un arma en función de la
 * tarea que RangeCalculator le mande.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class RangeCalculatorTask implements Runnable {
	
	private TiledMapTileLayer mapLayer;
	private Unit unit;
	private CalculationType type;
	private Weapon weapon;
	private Item item;
	private Point origin;
	private List<Pair<Point, Integer>> opened;
	private Set<Point> points;
	private CyclicBarrier barrier;
	private UnitPositionList unitsPositions;
	private Direction direction;
	
	/**
	 * Construye un nuevo RangeCalculatorTask para calcular
	 * el movimiento de una unidad.
	 * @param mapLayer - El terreno de batalla.
	 * @param unit - La unidad de la que se quiere conocer
	 * su rango de movimiento.
	 * @param direction - La dirección por la que la tarea comenzará
	 * a calcular las posiciones.
	 * @param closed - La lista compartida de celdas ya visitadas.
	 * @param points - El conjunto de celdas compartido en el que se guardan
	 * las celdas a las que la unidad llega.
	 * @param unitsPositions - La lista de las posiciones de las unidades.
	 */
	public RangeCalculatorTask(TiledMapTileLayer mapLayer, Unit unit, Direction direction,
			ClosedList closed, Set<Point> points, UnitPositionList unitsPositions) {
		this.mapLayer = mapLayer;
		this.unit = unit;
		this.direction = direction;
		this.points = points;
		this.unitsPositions = unitsPositions;
		opened = new ArrayList<Pair<Point, Integer>>();
		type = CalculationType.MOVEMENT;
	}
	
	/**
	 * Construye un nuevo RangeCalculatorTask pensado para calcular
	 * el rango de alcance de un arma desde una posición.
	 * @param mapLayer - El terreno de batalla.
	 * @param origin - La celda en la que se encuentra la unidad que
	 * posee el arma.
	 * @param weapon - El arma cuyo alcance se calculará.
	 * @param direction - La direción en la que la tarea calculará el rango.
	 * @param points - El conjunto de celdas compartido en el que se guardan
	 * las celdas a las que el arma puede alcanzar.
	 * @param barrier - La barrera compartidad para sincronizar todos los
	 * hilos con el programa principal.
	 */
	public RangeCalculatorTask(TiledMapTileLayer mapLayer, Point origin, Weapon weapon,
			Direction direction, Set<Point> points, CyclicBarrier barrier) {
		this.mapLayer = mapLayer;
		this.origin = origin;
		this.direction = direction;
		this.weapon = weapon;
		this.points = points;
		this.barrier = barrier;
		opened = new ArrayList<Pair<Point, Integer>>();
		type = CalculationType.ATKRANGE;
	}
	/**
	 * Construye un nuevo RangeCalculatorTask pensado para calcular el
	 * alcance del efecto de un arma desde una posición.
	 * @param mapLayer - El terreno de batalla.
	 * @param origin - La celda en la que se encuentra la unidad que
	 * posee el arma.
	 * @param item - El arma de cuyo efecto se calculará el rango.
	 * @param unit - La unidad que usa el arma.
	 * @param direction - La direción en la que la tarea calculará el rango
	 * @param points - El conjunto de celdas compartido en el que se guardan
	 * las celdas a las que el arma puede alcanzar.
	 * @param barrier - La barrera compartidad para sincronizar todos los
	 * hilos con el programa principal.
	 */
	public RangeCalculatorTask(TiledMapTileLayer mapLayer, Point origin, Item item,
			Unit unit, Direction direction, Set<Point> points, CyclicBarrier barrier) {
		this.mapLayer = mapLayer;
		this.origin = origin;
		this.direction = direction;
		this.unit = unit;
		this.item = item;
		this.points = points;
		this.barrier = barrier;
		opened = new ArrayList<Pair<Point, Integer>>();
		type = CalculationType.USERANGE;
	}

	/**
	 * Devuelve la celda siguiente a la actual según la
	 * dirección dada, o nulo si en esa dirección se sale
	 * del borde del mapa.
	 * @param - current La celda actual.
	 * @param - direction La dirección a la que se dirige el
	 * siguiente cálculo.
	 * @return La siguiente celda en la dirección dada,
	 * si existe.
	 */
	private Point isValid(Point current, Direction direction) {
		Point next = null;
		switch(direction) {
		case UP:
			if (current.y != 0)
				next = new Point(current.x, current.y-1);
			break;
		case RIGHT:
			if (current.x != mapLayer.getWidth()-1)
				next = new Point(current.x+1, current.y);
			break;
		case DOWN:
			if (current.y != mapLayer.getHeight()-1)
				next = new Point(current.x, current.y+1);
			break;
		case LEFT:
			if (current.x != 0)
				next = new Point(current.x-1, current.y);
			break;
		}
		return next;
	}
	
	/**
	 * Calcula si la celda siguiente a la actual en la dirección dada es una celda a la
	 * que la unidad puede llegar. Si no se sale del mapa y la unidad posee movimento
	 * restante suficiente para llegar, la añade al conjunto de candidatos de movimiento.
	 * @param currentPair - El nodo actual, nulo para la primera introducción.
	 * @param origin - El punto origen del que parte el rastreo actual.
	 * @param direction - La dirección a la que se dirige el rastreo.
	 */
	private void checkMovementNextPoint(Pair<Point, Integer> currentPair, Point origin, Direction direction) {
		Point next;
		next = isValid(origin, direction);
		if (next != null) {
			Terrain cellProperties = TerrainData.getTerrain(mapLayer, next.x, next.y);
			if (currentPair == null) {
				if (isPointValid(next, cellProperties) && unit.getMovementReduction(cellProperties.getId()) <= unit.getMovement()) {
					points.add(next);
					opened.add(new Pair<Point, Integer>(next, unit.getMovementReduction(cellProperties.getId())));
				}
			}
			else {
				if (isPointValid(next, cellProperties) && currentPair.second+unit.getMovementReduction(cellProperties.getId()) <= unit.getMovement()) {
					opened.add(new Pair<Point, Integer>(next,
							currentPair.second+unit.getMovementReduction(cellProperties.getId())));
					points.add(next);
				}
			}
		}
	}
	
	/**
	 * Comprueba si una unidad puede caminar sobre la celda dada.
	 * @param next - La posición de la celda.
	 * @param cellProperties - Las propiedades de la celda.
	 * @return
	 */
	private boolean isPointValid(Point next, Terrain cellProperties) {
		return cellProperties.isCrossable() && (!unitsPositions.isPositionOccuped(next)
				|| unitsPositions.isPositionOccupedByPlayer(next) == unit.isPlayerUnit());
	}
	
	/**
	 * Función principal para el cálculo del movimiento de una unidad.
	 */
	private void movementCalculation() {
		Direction [] directions = Direction.values();
		checkMovementNextPoint(null, unit.getMapPosition(), direction);
		while(!opened.isEmpty()) {
			Pair<Point, Integer> currentPair = opened.remove(0);
			Point current = currentPair.first;
			for (int i=0; i<4; i++) {
				checkMovementNextPoint(currentPair, current, directions[i]);
			}
		}
	}
	
	/**
	 * Comprueba si el arma o su efecto puede alcanzar la próxima celda en la dirección indicada.
	 * De ser así, se introduce en el conjunto de puntos y en la lista de nodos abiertos.
	 * @param currentPair - El nodo actual, nulo para la primera llamada.
	 * @param minRange - El rango mínimo del arma o de su efecto.
	 * @param maxRange - El rango máximo del arma o de su efecto.
	 * @param origin - La posición actual.
	 * @param direction - La dirección actual.
	 */
	private void checkWeaponRangeNextPoint(Pair<Point, Integer> currentPair, int minRange, int maxRange, Point origin, Direction direction) {
		Point next = isValid(origin, direction);
		if (next != null)
			if (currentPair == null) {
				if (minRange==1 || maxRange==1)
					points.add(next);
				opened.add(new Pair<Point, Integer>(next, 1));
			}
			else {
				if (currentPair.second+1 >= minRange && currentPair.second+1 <= maxRange)
					points.add(next);
				opened.add(new Pair<Point, Integer>(next, currentPair.second+1));
			}
	}
	
	/**
	 * Función principal para el calculo del rango de un arma o su efecto.
	 * @param minRange - El rango mínimo del arma o de su efecto.
	 * @param maxRange - El rango máximo del arma o de su efecto.
	 */
	private void weaponRangeCalculation(int minRange, int maxRange) {
		checkWeaponRangeNextPoint(null, minRange, maxRange, origin, direction);
		for (int i=1; i<maxRange; i++) {
			int currentSize = opened.size();
			for (int j=0; j<currentSize; j++) {
				int twice=0;
				Direction currentDir = direction;
				Pair<Point, Integer> currentPair = opened.remove(0);
				do {
					checkWeaponRangeNextPoint(currentPair, minRange, maxRange,
							currentPair.first, currentDir);
					currentDir = Direction.values()[(currentDir.ordinal()+1)%4];
					twice++;
				} while (twice != 2);
			}
		}
		try {
			barrier.await();
		} catch (InterruptedException e) {
			new Logger("tag", Logger.ERROR).error("InterruptedException", e);
		} catch (BrokenBarrierException e) {
			new Logger("tag", Logger.ERROR).error("BrokenBarrierException", e);
		}
	}

	@Override
	public void run() {
		switch(type) {
		case MOVEMENT:
			movementCalculation();
			break;
		case ATKRANGE:
			weaponRangeCalculation(weapon.getMinRange(), weapon.getMaxRange());
			break;
		case USERANGE:
			weaponRangeCalculation(item.getEffectMinRange(), item.calculateEffectMaxRange(unit));
			break;
		}	
	}
	
	/**
	 * Enumeración que representa los tres tipos de cálculo que realiza esta clase.
	 * @author Iván Barbosa Gutiérrez
	 *
	 */
	private enum CalculationType {
		MOVEMENT, ATKRANGE, USERANGE;
	}

}
