package es.flameorb.game.calculators;

/**
 * Esta clase encapsula los datos del combate: el daño
 * causado, la probabilidad de acertar, la probabilidad
 * de crítico y la diferencia en la velocidad de ataque
 * para facilitar su guardado en memoria mediante un mapa.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class BattleData {
	
	private int damage, hit, critical, asDiference;

	/**
	 * Construye un nuevo BattleData con todos los datos.
	 * @param damage - El daño causado.
	 * @param hit - La probabilidad de acertar.
	 * @param critical - La probabilidad de crítico.
	 * @param asDiference - La diferencia de velocidad de ataque.
	 */
	public BattleData(int damage, int hit, int critical, int asDiference) {
		this.damage = damage;
		this.hit = hit;
		this.critical = critical;
		this.asDiference = asDiference;
	}

	/**
	 * Returns the damage made to the defender.
	 * @return The damage.
	 */
	public int getDamage() {
		return damage;
	}

	/**
	 * Returns the hit rate of the attacker.
	 * @return The hit rate.
	 */
	public int getHit() {
		return hit;
	}

	/**
	 * Returns the critical rate of the attacker.
	 * @return The critical rate.
	 */
	public int getCritical() {
		return critical;
	}
	
	/**
	 * Returns the attack speed diference between
	 * the attacker and the defender.
	 * @return The attack speed diference.
	 */
	public int getASDiference() {
		return asDiference;
	}
	
	/**
	 * Sets the damage made to the defender.
	 * @param damage - The new damage.
	 */
	public void setDamage(int damage) {
		this.damage = damage;
	}

	/**
	 * Sets the hit rate of the attacker.
	 * @param hit - The new hit rate.
	 */
	public void setHit(int hit) {
		this.hit = hit;
	}

	/**
	 * Sets the critical rate of the attacker.
	 * @param critical - The new critical rate.
	 */
	public void setCritical(int critical) {
		this.critical = critical;
	}

	/**
	 * Sets the attack speed diference between
	 * the attacker and the defender.
	 * @param asDiference - The new attack speed diference.
	 */
	public void setASDiference(int asDiference) {
		this.asDiference = asDiference;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + asDiference;
		result = prime * result + critical;
		result = prime * result + damage;
		result = prime * result + hit;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BattleData other = (BattleData) obj;
		if (asDiference != other.asDiference)
			return false;
		if (critical != other.critical)
			return false;
		if (damage != other.damage)
			return false;
		if (hit != other.hit)
			return false;
		return true;
	}
	
}
