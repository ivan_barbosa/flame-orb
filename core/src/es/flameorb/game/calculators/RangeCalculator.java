package es.flameorb.game.calculators;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Logger;

import es.flameorb.game.map.Point;
import es.flameorb.items.Item;
import es.flameorb.items.Weapon;
import es.flameorb.units.Unit;
import es.flameorb.units.UnitPositionList;

/**
 * Esta clase se encarga de lanzar los hilos que calcular el rango
 * del moviemento de las unidades y del rango de las armas, además
 * de guardar en memoria el rango de movimiento de todas las unidades
 * para cada turno.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class RangeCalculator {
	
	private static Map<Unit, Set<Point>> unitsMovement =
			new ConcurrentHashMap<Unit, Set<Point>>();
	
	private RangeCalculator() {
		
	}
	
	/**
	 * Calcula y devuelve el rango de ataque de un arma desde la celda dada.
	 * @param field - El terreno de batalla.
	 * @param origin - La celda en la que se encuentra la unidad.
	 * @param minRange - El rango mínimo del arma.
	 * @param maxRange - El rango máximo del arma.
	 * @return Las celdas a las que el arma puede lanzar un ataque.
	 */
	public static Set<Point> getAttackRange(TiledMapTileLayer mapLayer, Point origin, Weapon weapon) {
		ExecutorService executor = Executors.newFixedThreadPool(4);
		Set<Point> points = Collections.synchronizedSet(new HashSet<Point>());
		CyclicBarrier barrier = new CyclicBarrier(5);
		Direction [] directions = Direction.values();
		for (int i=0; i<4; i++)
			executor.execute(new RangeCalculatorTask(mapLayer, origin, weapon,
					directions[i], points, barrier));
		executor.shutdown();
		try {
			barrier.await();
		} catch (InterruptedException e) {
			new Logger("tag", Logger.ERROR).error("InterruptedException", e);
		} catch (BrokenBarrierException e) {
			new Logger("tag", Logger.ERROR).error("BrokenBarrierException", e);
		}
		return points;
	}
	
	/**
	 * Calcula y devuelve el rango de uso de un objeto desde la celda dada.
	 * @param mapLayer - El mapa.
	 * @param origin - El punto de origen.
	 * @param item - El objeto a usar.
	 * @param unit - La unidad que usa el objeto.
	 * @return El rango de uso del objeto.
	 */
	public static Set<Point> getUseRange(TiledMapTileLayer mapLayer, Point origin, Item item, Unit unit) {
		ExecutorService executor = Executors.newFixedThreadPool(4);
		Set<Point> points = Collections.synchronizedSet(new HashSet<Point>());
		CyclicBarrier barrier = new CyclicBarrier(5);
		Direction [] directions = Direction.values();
		if (item.getUseEffect().getMinRange() == 0)
			points.add(unit.getMapPosition());
		for (int i=0; i<4; i++)
			executor.execute(new RangeCalculatorTask(mapLayer, origin, item,
					unit, directions[i], points, barrier));
		executor.shutdown();
		try {
			barrier.await();
		} catch (InterruptedException e) {
			new Logger("tag", Logger.ERROR).error("InterruptedException", e);
		} catch (BrokenBarrierException e) {
			new Logger("tag", Logger.ERROR).error("BrokenBarrierException", e);
		}
		return points;
	}
	
	/**
	 * Devuelve las celdas a las cuales la unidad dada se puede mover.
	 * @param unit - La unidad de la que se quiere conocer las celdas
	 * a las que se puede mover.
	 * @return Las celdas a las que la unidad dada se puede mover.
	 */
	public static Set<Point> getUnitMoveRange(Unit unit) {
		return unitsMovement.get(unit);
	}
	
	/**
	 * Actualiza las celdas a las que la lista de unidades dadas se pueden mover.
	 * @param field - El terreno de batalla.
	 * @param units - La lista de unidades.
	 */
	public static void updateMoveRange(TiledMapTileLayer mapLayer, Array<Unit> units, UnitPositionList unitsPositions) {
		ExecutorService executor = Executors.newCachedThreadPool();
		for (Unit unit : units) {
			Set<Point> points = Collections.synchronizedSet(new HashSet<Point>());
			points.add(unit.getMapPosition());
			unitsMovement.put(unit, points);
			ClosedList closed = new ClosedList();
			Direction [] directions = Direction.values();
			for (int i=0; i<(Runtime.getRuntime().availableProcessors() < 4 ? 4 : Runtime.getRuntime().availableProcessors()); i++)
				executor.execute(new RangeCalculatorTask(mapLayer, unit, directions[i%4], closed, points, unitsPositions));
		}
		executor.shutdown();
		while (!executor.isTerminated()) {}
	}
	
	public enum Direction {
		UP, RIGHT, DOWN, LEFT;
	}
	
}
