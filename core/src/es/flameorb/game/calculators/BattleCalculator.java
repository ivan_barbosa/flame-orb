package es.flameorb.game.calculators;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.badlogic.gdx.utils.Array;

import es.flameorb.game.map.Terrain;
import es.flameorb.items.Weapon;
import es.flameorb.units.Unit;

/**
 * Esta clase se encarga de generar los hilos que calculan el daño,
 * probabilidad de golpeo, probabilidad de crítico y diferencia de
 * velocidad de ataque de todas las unidades.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class BattleCalculator {
	
	private static Map<BattleElements, BattleData> fullBattleParameters =
			new ConcurrentHashMap<BattleElements, BattleData>();
	
	private BattleCalculator() {
		
	}
	
	/**
	 * Inicializa los parámetros de combate para todas las unidades y
	 * todas las armas que posean, las puedan usar o no.
	 * @param player - La lista de unidades del jugador.
	 * @param enemy - La lista de unidades del enemigo.
	 */
	public static void initialiteBattleParameters(Array<Unit> player, Array<Unit> enemy) {
		for (Unit playerUnit : player)
			for (Unit enemyUnit : enemy) {
				for (Weapon playerUnitWeapon : playerUnit.getInventory().getWeapons())
					calculateBattleData(playerUnit, enemyUnit, playerUnitWeapon, true);
				for (Weapon enemyUnitWeapon : enemyUnit.getInventory().getWeapons())
					calculateBattleData(enemyUnit, playerUnit, enemyUnitWeapon, true);
			}
	}

	/**
	 * Actualiza los parámetros de combate cuando la unidad sube de nivel.
	 * @param unit - La unidad que subió de nivel.
	 * @param enemies - La lista de enemigos de la unidad.
	 */
	public static void unitNewLevel(Unit unit, Array<Unit> enemies) {
		for (Unit enemy : enemies) {
			for (Weapon unitWeapon : unit.getInventory().getWeapons())
				calculateBattleData(unit, enemy, unitWeapon, false);
			for (Weapon enemyWeapon : enemy.getInventory().getWeapons())
				calculateBattleData(enemy, unit, enemyWeapon, false);
		}
	}
	
	/**
	 * Actualiza los parámetros de combate cuando la unidad recibe un arma nueva.
	 * @param unit - La unidad que recibió el arma.
	 * @param weapon - El arma nueva de la unidad.
	 * @param enemies - La lista de enemigos de la unidad.
	 */
	public static void unitNewWeapon(Unit unit, Weapon weapon, Array<Unit> enemies) {
		ExecutorService executor = Executors.newCachedThreadPool();
		for (Unit enemy : enemies) {
			calculateBattleData(unit, enemy, weapon, true);
			for (Weapon enemyWeapon : enemy.getInventory().getWeapons())
				executor.execute(new BattleCalculatorTask(
						enemy, unit, enemyWeapon, weapon, fullBattleParameters, true));
		}
		executor.shutdown();
		while (!executor.isTerminated()) {}
	}
	
	/**
	 * Deveulve loa datos del combate de los combatientes y sus armas pasados por
	 * parámetro
	 * @param fighters - Los combatientes y las armas equipadas.
	 * @return Los datos del combate
	 */
	public static BattleData getInitialBattleData(BattleElements fighters) {
		return fullBattleParameters.get(fighters);
	}
	
	/**
	 * Devuelve los bonos de terreno en defensa y evasión según el terreno enviado
	 * por parámetro.
	 * @param properties - Las propiedades del terreno 
	 * @return Un par de enteros con el bono en defensa y en evasión.
	 */
	public static Pair<Integer, Integer> getTerrainBonus(Terrain properties) {
		return new Pair<Integer, Integer>(properties.getDefense(), properties.getAvoid());
	}
	
	/**
	 * Realiza la creación de las tareas para calcular los parámetros del combate.
	 * @param first - Una unidad.
	 * @param second - La otra unidad.
	 * @param firstWeapon - El arma de la primera unidad.
	 * @param newPair - Si se está creando un par de combatientes, o se actualiza uno existente.
	 */
	private static void calculateBattleData(Unit first, Unit second, Weapon firstWeapon, boolean newPair) {
		ExecutorService executor = Executors.newCachedThreadPool();
		for (Weapon secondWeapon : second.getInventory().getWeapons()) {
			executor.execute(new BattleCalculatorTask(
					first, second, firstWeapon, secondWeapon, fullBattleParameters, newPair));
		}
		executor.execute(new BattleCalculatorTask(
				first, second, firstWeapon, null, fullBattleParameters, newPair));
		executor.shutdown();
		while (!executor.isTerminated()) {}
	}
}
