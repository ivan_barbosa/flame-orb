package es.flameorb.game.calculators;

import java.util.Set;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.utils.Array;

import es.flameorb.game.map.TerrainData;
import es.flameorb.game.map.Point;
import es.flameorb.units.Unit;

/**
 * Calcula el camino que han de seguir las unidades para ir de un punto a otro
 * mediante el algoritmo A*. Actualmente no está en uso.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class PathCalculator {
	
	private PathCalculator() {
		
	}
	
	/**
	 * Devuelve el camino que ha de seguir una unidad de un punto a otro.
	 * @param mapLayer - El mapa.
	 * @param unit - La unidad
	 * @param movementRange - Los puntos por los que puede pasar la unidad.
	 * @param origin - El punto de origen.
	 * @param end - El punto de destino.
	 * @return El camino de la unidad.
	 */
	public static Array<Point> getMovementPath(TiledMapTileLayer mapLayer,
			Unit unit, Set<Point> movementRange, Point origin, Point end) {
		Array<AStarNode> nodes = createAStarNodes(movementRange);
		addAdjacentsToNodes(nodes);
		AStarNode begin = getCorrespondingNode(nodes, origin);
		AStarNode target = getCorrespondingNode(nodes, end);
		Array<AStarNode> opened = new Array<AStarNode>();
		boolean found = false;
		begin.setG(0);
		begin.setF(begin.distanceTo(target));
		opened.add(begin);
		while(!found && opened.size != 0) {
			opened.sort();
			AStarNode current = opened.first();
			if (current == target)
				found = true;
			else {
				opened.removeIndex(0);
				for (AStarNode adjacent : current.getAdjacents()) {
					int adjacentG = current.getG() +
							unit.getMovementReduction(TerrainData.getTerrain
							(mapLayer, current.getPoint().x, current.getPoint().y).getId());
					if (/*adjacentG <= unit.getMovement() &&*/ adjacentG < adjacent.getG()) {
						if (!opened.contains(adjacent, true))
							opened.add(adjacent);
						adjacent.setParent(current);
						adjacent.setG(adjacentG);
						adjacent.setF(adjacent.getG()+adjacent.distanceTo(target));
					}
				}
			}
		}
		return getSolutionPath(begin, target);
	}
	
	private static void addAdjacentsToNodes(Array<AStarNode> nodes) {
		for (int i=0; i<nodes.size; i++) {
			AStarNode node = nodes.get(i);
			for (AStarNode other : nodes) {
				if (node.getPoint().isNext(other.getPoint()))
					node.setAdjacent(other);
			}
		}
	}
	
	private static Array<AStarNode> createAStarNodes(Set<Point> points) {
		Array<AStarNode> nodes = new Array<AStarNode>();
		for (Point point : points)
			nodes.add(new AStarNode(point));
		return nodes;
	}
	
	private static AStarNode getCorrespondingNode(Array<AStarNode> nodes, Point point) {
		boolean found = false;
		int i=0;
		while (!found) {
			if (nodes.get(i).getPoint().equals(point))
				found = true;
			else
				i++;
		}
		return nodes.get(i);
	}
	
	private static Array<Point> getSolutionPath(AStarNode begin, AStarNode target) {
		Array<Point> path = new Array<Point>();
		path.insert(0, target.getPoint());
		AStarNode current = target.getParent();
		while (current != begin) {
			path.insert(0, current.getPoint());
			current = current.getParent();
		}
		return path;
	}
}
