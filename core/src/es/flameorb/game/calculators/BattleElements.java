package es.flameorb.game.calculators;

import es.flameorb.items.Weapon;
import es.flameorb.units.Unit;

/**
 * Esta clase encapsula a los participantes de un combate: la unidad
 * que ataca, la unidad que se defiende y las armas empleadas por ambos,
 * para facilitar el guardado en memoria de los datos del combate
 * mediante un mapa.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class BattleElements {
	
	private Unit attacker, defender;
	private Weapon atWeapon, dfWeapon;
	
	/**
	 * Inicia un nuevo BattleElements con todos sus datos.
	 * @param attacker - La unidad que ataca.
	 * @param defender - La unidad que se defiende.
	 * @param atWeapon - El arma del atacante.
	 * @param dfWeapon - El arma del defensor.
	 */
	public BattleElements(Unit attacker, Unit defender,
			Weapon atWeapon, Weapon dfWeapon) {
		this.attacker = attacker;
		this.defender = defender;
		this.atWeapon = atWeapon;
		this.dfWeapon = dfWeapon;
	}
	
	/**
	 * Inicia un nuevo BattleElements con las armas que
	 * las unidades llevan equipadas.
	 * @param attacker - La unidad que ataca.
	 * @param defender - La unidad que se defiende.
	 */
	public BattleElements(Unit attacker, Unit defender) {
		this.attacker = attacker;
		this.defender = defender;
		this.atWeapon = attacker.getEquipedWeapon();
		this.dfWeapon = defender.getEquipedWeapon();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((atWeapon == null) ? 0 : atWeapon.hashCode());
		result = prime * result
				+ ((attacker == null) ? 0 : attacker.hashCode());
		result = prime * result
				+ ((defender == null) ? 0 : defender.hashCode());
		result = prime * result
				+ ((dfWeapon == null) ? 0 : dfWeapon.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BattleElements other = (BattleElements) obj;
		if (atWeapon == null) {
			if (other.atWeapon != null)
				return false;
		} else if (!atWeapon.equals(other.atWeapon))
			return false;
		if (attacker == null) {
			if (other.attacker != null)
				return false;
		} else if (!attacker.equals(other.attacker))
			return false;
		if (defender == null) {
			if (other.defender != null)
				return false;
		} else if (!defender.equals(other.defender))
			return false;
		if (dfWeapon == null) {
			if (other.dfWeapon != null)
				return false;
		} else if (!dfWeapon.equals(other.dfWeapon))
			return false;
		return true;
	}

}
