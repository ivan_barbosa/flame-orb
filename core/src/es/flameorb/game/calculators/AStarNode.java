package es.flameorb.game.calculators;

import com.badlogic.gdx.utils.Array;

import es.flameorb.game.map.Point;

/**
 * Implementación de los nodos del algoritmo A*
 * @author Iván Barbosa Gutiérrez
 *
 */
public class AStarNode implements Comparable<AStarNode>{
	
	private Point point;
	private Array<AStarNode> adjacents;
	private AStarNode parent;
	private int F, G;
	
	/**
	 * Crea el nodo A* asociando a un punto.
	 * @param point - El punto asociado.
	 */
	public AStarNode(Point point) {
		this.point = point;
		adjacents = new Array<AStarNode>(4);
		G = Integer.MAX_VALUE;
	}

	public Point getPoint() {
		return point;
	}

	public void setPoint(Point point) {
		this.point = point;
	}

	public Array<AStarNode> getAdjacents() {
		return adjacents;
	}

	public void setAdjacent(AStarNode adjacent) {
		adjacents.add(adjacent);
	}

	public AStarNode getParent() {
		return parent;
	}

	public void setParent(AStarNode parent) {
		this.parent = parent;
	}

	public int getF() {
		return F;
	}

	public void setF(int f) {
		F = f;
	}

	public int getG() {
		return G;
	}

	public void setG(int g) {
		G = g;
	}

	@Override
	public int compareTo(AStarNode arg0) {
		return F - arg0.getF();
	}
	
	public int distanceTo(AStarNode other) {
		return point.distanceTo(other.getPoint());
	}
	
	
}
