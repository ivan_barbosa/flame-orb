package es.flameorb.game.calculators;

/**
 * Implementación de un par de elementos.
 * @author Iván Barbosa Gutiérrez
 *
 * @param <T1> - Un elemento
 * @param <T2> - Otro elemento.
 */
public class Pair<T1, T2> {
	
	public T1 first;
	public T2 second;
	
	/**
	 * Construye un par a partir de los elementos dados.
	 * @param first - Un elemento del primer tipo.
	 * @param second - Un elemento del segundo tipo.
	 */
	public Pair(T1 first, T2 second) {
		this.first = first;
		this.second = second;
	}
}
