package es.flameorb.game.calculators;

import java.util.Map;

import es.flameorb.items.Weapon;
import es.flameorb.units.Stats;
import es.flameorb.units.Unit;

/**
 * Esta clase se encarga del calculo del daño, probabilidad de golpeo,
 * probabilidad de crítico y diferencia de velocidad de ataque para
 * un par de unidades y armas.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class BattleCalculatorTask implements Runnable {
	
	private Unit attacker, defender;
	private Weapon atWeapon, dfWeapon;
	private boolean newPair;
	private Map<BattleElements, BattleData> fullBattleParameters;
	
	/**
	 * Construye un nuevo BattleCalculatorTask con todos sus parámetros.
	 * @param attacker - La unidad que ataca.
	 * @param defender - La unidad que defiende.
	 * @param atWeapon - El arma del atacante.
	 * @param dfWeapon - El arma del defensor.
	 * @param newPair - Si se está creando un nuevo par de combatientes, o
	 * se está actualizando uno ya existente.
	 */
	public BattleCalculatorTask(Unit attacker, Unit defender,
			Weapon atWeapon, Weapon dfWeapon, Map<BattleElements, BattleData> fullBattleParameters,
			boolean newPair) {
		this.attacker = attacker;
		this.defender = defender;
		this.atWeapon = atWeapon;
		this.dfWeapon = dfWeapon;
		this.fullBattleParameters = fullBattleParameters;
		this.newPair = newPair;
	}

	/**
	 * En función del atributo newPair, esta instancia de BattleCalculatorTask
	 * creará un nuevo BattleData o modificará uno ya existente.
	 */
	@Override
	public void run() {
		if (newPair)
			initialite();
		else 
			update();
	}
	
	/**
	 * Inicializa en GameController un BattleData a partir
	 * de los elementos recibidos en su constructor.
	 */
	private void initialite() {
		BattleElements fighters = new BattleElements(attacker, defender, atWeapon, dfWeapon);
		BattleData data = new BattleData(calculateDamage(), calculateHit(), calculateCritical(),
				calculateASDiference());
		fullBattleParameters.put(fighters, data);
	}
	
	/**
	 * Actualiza en GameController el BattleData asociado a
	 * los elementos recibidos en su constructor.
	 */
	private void update() {
		BattleData data = fullBattleParameters.get(new BattleElements(attacker, defender, atWeapon, dfWeapon));
		data.setDamage(calculateDamage());
		data.setHit(calculateHit());
		data.setCritical(calculateCritical());
		data.setASDiference(calculateASDiference());
	}
	
	/**
	 * Calucula el daño que recibe el defensor si el atacante logra impactar el ataque.
	 * @return El daño causado al defensor.
	 */
	private int calculateDamage() {
		int power = attacker.getAttackPower(atWeapon);
		if (atWeapon.makesExtraDamageTo(defender))
			power *= 3;
		if (dfWeapon != null)
			power = atWeapon.applyTrinityEffectToDamage
					(power, attacker.getWeaponSkill(atWeapon.getWeaponId()),
							dfWeapon, defender.getWeaponSkill(dfWeapon.getWeaponId()));
		if (atWeapon.isPhysical())
			return power-defender.getBattleStat(Stats.DEFENSE);
		else
			return power-defender.getBattleStat(Stats.RESISTANCE);
	}
	
	/**
	 * Calcula la probabilidad que tiene el atacante de acertar con su ataque al defensor
	 * @return La probabilidad de golpeo del atacante.
	 */
	private int calculateHit() {
		int hit = attacker.getHitRate(atWeapon);
		if (dfWeapon != null)
			hit = atWeapon.applyTrinityEffectToHit
					(hit, attacker.getWeaponSkill(atWeapon.getWeaponId()),
							dfWeapon, defender.getWeaponSkill(dfWeapon.getWeaponId()));
		return hit - defender.getAvoidRate(dfWeapon);
	}
	
	/**
	 * Calcula la probabilidad que tiene el atacante de que su ataque sea crítico.
	 * @return La probabilidad de crítico del atacante.
	 */
	private int calculateCritical() {
		return attacker.getCriticalRate(atWeapon) - defender.getBattleStat(Stats.LUCK);
	}
	
	/**
	 * Calcula la diferencia en la velocidad de ataque entre el atacante y el defensor.
	 * @return La diferencia en la velocidad de ataque.
	 */
	private int calculateASDiference() {
		return attacker.getAttackSpeed(atWeapon)-defender.getAttackSpeed(dfWeapon);
	}
	
}
