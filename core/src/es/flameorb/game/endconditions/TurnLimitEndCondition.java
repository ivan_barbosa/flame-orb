package es.flameorb.game.endconditions;

import es.flameorb.game.control.GameController;

public class TurnLimitEndCondition extends EndCondition {
	
	private int turn;
	
	protected TurnLimitEndCondition() {
		
	}

	public TurnLimitEndCondition(boolean winCondition, int turn) {
		super(winCondition, "TurnLimit");
		this.turn = turn;
	}

	@Override
	public boolean checkCondition(GameController controller) {
		return controller.getTurnController().getTurnCount() == turn;
	}

}
