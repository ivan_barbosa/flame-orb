package es.flameorb.game.endconditions;

import es.flameorb.game.control.GameController;

public class DefeatCommanderEndCondition extends EndCondition {
	
	protected DefeatCommanderEndCondition() {
		
	}

	public DefeatCommanderEndCondition(boolean winCondition) {
		super(winCondition, "DefeatCommander");
	}

	@Override
	public boolean checkCondition(GameController controller) {
		if (winCondition)
			return controller.getEnemyCommander() == null;
		else
			return controller.getPlayerCommander() == null;
	}

}
