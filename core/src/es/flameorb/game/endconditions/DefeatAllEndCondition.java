package es.flameorb.game.endconditions;

import es.flameorb.game.control.GameController;

public class DefeatAllEndCondition extends EndCondition {
	
	protected DefeatAllEndCondition() {
		
	}

	public DefeatAllEndCondition(boolean winCondition) {
		super(winCondition, "DefeatAll");
	}

	@Override
	public boolean checkCondition(GameController controller) {
		if (winCondition)
			return controller.getEnemyUnits().size == 0;
		else
			return controller.getPlayerUnits().size == 0;
	}

}
