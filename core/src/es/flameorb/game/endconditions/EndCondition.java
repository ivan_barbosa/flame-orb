package es.flameorb.game.endconditions;

import es.flameorb.game.control.GameController;
import es.flameorb.general.Language;

public abstract class EndCondition {
	
	protected boolean winCondition;
	protected String name;
	
	protected EndCondition() {
		
	}
	
	public EndCondition(boolean winCondition, String name) {
		this.winCondition = winCondition;
		this.name = name;
	}
	
	public String toString() {
		return Language.get(name);
	}
	
	public abstract boolean checkCondition(GameController controller);

}
