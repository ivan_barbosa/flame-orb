package es.flameorb.game.endconditions;

import com.badlogic.gdx.utils.Array;

import es.flameorb.game.control.GameController;
import es.flameorb.game.map.Point;
import es.flameorb.units.Unit;

public class RearchPointEndCondition extends EndCondition {
	
	private Array<Point> points;
	
	protected RearchPointEndCondition() {
		
	}

	public RearchPointEndCondition(boolean winCondition, Array<Point> points) {
		super(winCondition, "RearchPoint");
		this.points = points;
	}

	@Override
	public boolean checkCondition(GameController controller) {
		for (Point point : points) {
			Unit unit = controller.getUnitsPositions().getUnit(point);
			if (unit != null) {
				if (winCondition && unit.isPlayerUnit())
					return true;
				else if (!winCondition && !unit.isPlayerUnit())
					return true;
			}
		}
		return false;
	}

}
