package es.flameorb.game.map;

import com.badlogic.gdx.utils.Array;

import es.flameorb.game.control.GameController;
import es.flameorb.general.Language;

public class PlaceUnitsMenuDisplayedMapState extends MenuMapState {
	
	private Point position;
	private PlaceUnitsMenu menu;

	public PlaceUnitsMenuDisplayedMapState(Field field, Point position) {
		super(field);
		this.position = position;
		menu = new PlaceUnitsMenu(Language.get("PlaceUnits"), this);
		placeMenu(menu);
	}

	@Override
	public void acceptAction() {
		switch (menu.getSelected()) {
		case PLACE:
			menu.setVisible(false);
			field.removeActor(menu);
			field.setMapState(new PlaceUnitsSelectUnitMapState(field, position));
			break;
		case REMOVE:
			field.removeActor(field.unitsPositions.getUnit(position));
			GameController.getGameController().removePlayerUnit(field.unitsPositions.getUnit(position));
			refreshInfoWindows();
			cancelAction();
			break;
		case SWAP:
			menu.setVisible(false);
			field.removeActor(menu);
			field.setMapState(new PlaceUnitsSwapSelectionMapState(field, position, field.unitsPositions.getUnit(position)));
			break;
		}
	}

	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new PlaceUnitsDisplayedMapState(field));

	}
	
	private enum PlaceUnitsMenuOptions {
		PLACE("Place"), REMOVE("Remove"), SWAP("Swap");
		
		private final String name;
		
		private PlaceUnitsMenuOptions(String name) {
			this.name = name;
		}
		
		@Override
		public String toString() {
			return Language.get(name);
		}
	}
	
	private class PlaceUnitsMenu extends MapMenu<PlaceUnitsMenuOptions> {

		public PlaceUnitsMenu(String title, MapState mapState) {
			super(title, mapState);
		}

		@Override
		protected void prepareOptions() {
			Array<PlaceUnitsMenuOptions> list = new Array<PlaceUnitsMenuOptions>(PlaceUnitsMenuOptions.values());
			if (!field.unitsPositions.isPositionOccuped(position)) {
				list.removeValue(PlaceUnitsMenuOptions.SWAP, true);
				list.removeValue(PlaceUnitsMenuOptions.REMOVE, true);
			}
			options.setItems(list);
		}
		
	}

}
