package es.flameorb.game.map;

import java.io.StringWriter;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter;

import es.flameorb.game.control.GameController;
import es.flameorb.general.GamePreferences;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;
import es.flameorb.items.Item;
import es.flameorb.items.ItemFactory;
import es.flameorb.items.Weapon;
import es.flameorb.units.EnemyUnit;
import es.flameorb.units.FEClass;
import es.flameorb.units.Inventory;
import es.flameorb.units.PlayerUnit;
import es.flameorb.units.Stats;
import es.flameorb.units.aistrategy.AIStrategy;
import es.flameorb.units.aistrategy.NothingAIStrategy;
import es.flameorb.units.aistrategy.SelectionAIStrategy;

public class EditorUnitCreatorMapState extends MapState {
	
	private FEClass feclass;
	private EnemyCreatorWindow window;
	private EnemyUnit enemyUnit;
	private PlayerUnit playerUnit;
	private boolean player;
	private boolean useDefaults;

	public EditorUnitCreatorMapState(Field field, EnemyUnit enemyUnit, PlayerUnit playerUnit, boolean player, FEClass feclass, boolean useDefaults) {
		super(field);
		this.enemyUnit = enemyUnit;
		this.playerUnit = playerUnit;
		this.player = player;
		this.feclass = feclass;
		this.useDefaults = useDefaults;
		window = new EnemyCreatorWindow();
		window.setPosition(getCenteredXPosition(window.getWidth()), (CELLS_HEIGHT + 2) * 32 - window.getHeight() + field.camera.position.y - field.cameraInitialPosition.y);
		window.setVisible(true);
		field.addActor(window);
	}

	@Override
	public void acceptAction() {
		if (player)
			GameController.getGameController().addPlayerUnit(playerUnit);
		else {
			GameController.getGameController().addEnemyUnit(enemyUnit);
			field.addActorAt(0, enemyUnit);
			refreshInfoWindows();
		}
		window.setVisible(false);
		field.removeActor(window);
		if (player)
			field.setMapState(new EditorPlayerUnitsMapState(field));
		else
			field.setMapState(new EditorEnemyPlaceSelectionMapState(field));
	}

	@Override
	public void cancelAction() {
		window.setVisible(false);
		field.removeActor(window);
		field.setMapState(new EditorUnitClassSelectionMapState(field, enemyUnit, playerUnit, player));
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		return false;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		
	}
	
	private enum StrategyNameId {
		
		CLOSEST("Closest"), MORE_DAMAGE("MoreDamageGiven"), LESS_DAMAGE("LessDamageReceived"),
		LESS_HP("LessHP");
		
		private final String name;
		private String className;
		
		private StrategyNameId(String name) {
			this.name = name;
			className = "es.flameorb.units.aistrategy." + name + "AIStrategy";
		}
		
		public String getClassName() {
			return className;
		}
		
		public String toString() {
			return Language.get(name);
		}
	
	}
	
	private enum MoveType {
		YES("Yes", 0), IN_RANGE("InRange", 1), NO("No", 2);
		
		private final String name;
		private final int id;
		
		private MoveType(String name, int id) {
			this.name = name;
			this.id = id;
		}
		
		public int getId() {
			return id;
		}
		
		public String toString() {
			return Language.get(name);
		}
	}
	
	private class DeleteItemButtonListener extends ClickListener {
		
		private VerticalGroup verticalGroup;
		private HorizontalGroup horizontalGroup;
		private Array<Item> inventory;
		private Item item;
		private Array<CheckBox> dropableCheckBoxes;
		private CheckBox checkBox;
		
		public DeleteItemButtonListener(VerticalGroup verticalGroup, HorizontalGroup horizontalGroup,
				Array<Item> inventory, Item item, Array<CheckBox> dropableCheckBoxes, CheckBox checkBox) {
			super();
			this.verticalGroup = verticalGroup;
			this.horizontalGroup = horizontalGroup;
			this.inventory = inventory;
			this.item = item;
			this.dropableCheckBoxes = dropableCheckBoxes;
			this.checkBox = checkBox;
		}
		
		public DeleteItemButtonListener(VerticalGroup verticalGroup, HorizontalGroup horizontalGroup,
				Array<Item> inventory, Item item) {
			super();
			this.verticalGroup = verticalGroup;
			this.horizontalGroup = horizontalGroup;
			this.inventory = inventory;
			this.item = item;
		}

		@Override
		public void clicked(InputEvent event, float x, float y) {
			verticalGroup.removeActor(horizontalGroup);
			inventory.removeValue(item, true);
			if (!player)
				dropableCheckBoxes.removeValue(checkBox, true);
			window.pack();
		}
		
	}
	
	private class EnemyCreatorWindow extends Window {
		
		private TextField nameTextField;
		private TextField levelTextField;
		private CheckBox commanderCheckBox;
		private SelectBox<MoveType> moveSelectBox;
		private TextField hpBaseStatsTextField;
		private TextField strBaseStatsTextField;
		private TextField magBaseStatsTextField;
		private TextField sklBaseStatsTextField;
		private TextField spdBaseStatsTextField;
		private TextField lckBaseStatsTextField;
		private TextField defBaseStatsTextField;
		private TextField resBaseStatsTextField;
		private CheckBox randomBaseStatsCheckBox;
		private TextField hpTopStatsTextField;
		private TextField strTopStatsTextField;
		private TextField magTopStatsTextField;
		private TextField sklTopStatsTextField;
		private TextField spdTopStatsTextField;
		private TextField lckTopStatsTextField;
		private TextField defTopStatsTextField;
		private TextField resTopStatsTextField;
		private TextField hpGrowingTextField;
		private TextField strGrowingTextField;
		private TextField magGrowingTextField;
		private TextField sklGrowingTextField;
		private TextField spdGrowingTextField;
		private TextField lckGrowingTextField;
		private TextField defGrowingTextField;
		private TextField resGrowingTextField;
		private IntMap<SelectBox<String>> weaponLevelsSelectBoxes;
		private SelectBox<StrategyNameId> strategyAselectBox;
		private TextField strategyAWeightTextField;
		private SelectBox<StrategyNameId> strategyBselectBox;
		private TextField strategyBWeightTextField;
		private SelectBox<Item> itemsSelectBox;
		private Array<Item> inventory;
		private Array<CheckBox> dropableCheckBoxes;
		private VerticalGroup itemsGroup;
		
		public EnemyCreatorWindow() {
			super("", UIFactory.getSkin());
			nameTextField = UIFactory.newTextField();
			levelTextField = UIFactory.newTextField(2, true);
			commanderCheckBox = UIFactory.newCheckBox(Language.get("Commander"));
			if (!player)
				moveSelectBox = UIFactory.newSelectBox(MoveType.values());
			hpBaseStatsTextField = UIFactory.newTextField(2, false);
			strBaseStatsTextField = UIFactory.newTextField(2, false);
			magBaseStatsTextField = UIFactory.newTextField(2, false);
			sklBaseStatsTextField = UIFactory.newTextField(2, false);
			spdBaseStatsTextField = UIFactory.newTextField(2, false);
			lckBaseStatsTextField = UIFactory.newTextField(2, false);
			defBaseStatsTextField = UIFactory.newTextField(2, false);
			resBaseStatsTextField = UIFactory.newTextField(2, false);
			randomBaseStatsCheckBox = UIFactory.newCheckBox(Language.get("Random"));
			hpTopStatsTextField = UIFactory.newTextField(2, false);
			strTopStatsTextField = UIFactory.newTextField(2, false);
			magTopStatsTextField = UIFactory.newTextField(2, false);
			sklTopStatsTextField = UIFactory.newTextField(2, false);
			spdTopStatsTextField = UIFactory.newTextField(2, false);
			lckTopStatsTextField = UIFactory.newTextField(2, false);
			defTopStatsTextField = UIFactory.newTextField(2, false);
			resTopStatsTextField = UIFactory.newTextField(2, false);
			hpGrowingTextField = UIFactory.newTextField(3, true);
			strGrowingTextField = UIFactory.newTextField(3, true);
			magGrowingTextField = UIFactory.newTextField(3, true);
			sklGrowingTextField = UIFactory.newTextField(3, true);
			spdGrowingTextField = UIFactory.newTextField(3, true);
			lckGrowingTextField = UIFactory.newTextField(3, true);
			defGrowingTextField = UIFactory.newTextField(3, true);
			resGrowingTextField = UIFactory.newTextField(3, true);
			weaponLevelsSelectBoxes = new IntMap<SelectBox<String>>();
			for (int i=0; i<GamePreferences.getWeaponNames().size; i++) {
				if (feclass.getWeaponUsage(i)) {
					weaponLevelsSelectBoxes.put(i, UIFactory.newSelectBox(GamePreferences.getWeaponLevelsNames()));
					if (enemyUnit != null && enemyUnit.getWeaponSkill(i) != 1) {
						for (int j = 0; j < GamePreferences.getWeaponLevelsValues().size; j++) {
							if (GamePreferences.getWeaponLevelsValues().get(j) == enemyUnit.getWeaponSkill(i)-1) {
								
							}
						}
					}
				}
			}
			if (!player) {
				strategyAselectBox = UIFactory.newSelectBox(StrategyNameId.values());
				strategyAselectBox.setSelected(StrategyNameId.CLOSEST);
				strategyAWeightTextField = UIFactory.newTextField();
				strategyAWeightTextField.setText("5");
				strategyBselectBox = UIFactory.newSelectBox(StrategyNameId.values());
				strategyBselectBox.setSelected(StrategyNameId.MORE_DAMAGE);
				strategyBWeightTextField = UIFactory.newTextField();
				strategyBWeightTextField.setText("3");
			}
			itemsSelectBox = UIFactory.newSelectBox(ItemFactory.getAllItems());
			itemsSelectBox.setMaxListCount(5);
			inventory = new Array<Item>();
			dropableCheckBoxes = new Array<CheckBox>();
			itemsGroup = new VerticalGroup();
			TextButton createTextButton;
			if (enemyUnit == null && playerUnit == null) {
				hpBaseStatsTextField.setText(feclass.getBaseStat(Stats.HITPOINTS)+"");
				strBaseStatsTextField.setText(feclass.getBaseStat(Stats.STRENGHT)+"");
				magBaseStatsTextField.setText(feclass.getBaseStat(Stats.MAGIC)+"");
				sklBaseStatsTextField.setText(feclass.getBaseStat(Stats.SKILL)+"");
				spdBaseStatsTextField.setText(feclass.getBaseStat(Stats.SPEED)+"");
				lckBaseStatsTextField.setText("0");
				defBaseStatsTextField.setText(feclass.getBaseStat(Stats.DEFENSE)+"");
				resBaseStatsTextField.setText(feclass.getBaseStat(Stats.RESISTANCE)+"");
				randomBaseStatsCheckBox.setChecked(true);
				hpTopStatsTextField.setText(feclass.getTopStat(Stats.HITPOINTS)+"");
				strTopStatsTextField.setText(feclass.getTopStat(Stats.STRENGHT)+"");
				magTopStatsTextField.setText(feclass.getTopStat(Stats.MAGIC)+"");
				sklTopStatsTextField.setText(feclass.getTopStat(Stats.SKILL)+"");
				spdTopStatsTextField.setText(feclass.getTopStat(Stats.SPEED)+"");
				lckTopStatsTextField.setText(feclass.getTopStat(Stats.LUCK)+"");
				defTopStatsTextField.setText(feclass.getTopStat(Stats.DEFENSE)+"");
				resTopStatsTextField.setText(feclass.getTopStat(Stats.RESISTANCE)+"");
				hpGrowingTextField.setText(feclass.getGrowing(Stats.HITPOINTS)*2+"");
				strGrowingTextField.setText(feclass.getGrowing(Stats.STRENGHT)*2+"");
				magGrowingTextField.setText(feclass.getGrowing(Stats.MAGIC)*2+"");
				sklGrowingTextField.setText(feclass.getGrowing(Stats.SKILL)*2+"");
				spdGrowingTextField.setText(feclass.getGrowing(Stats.SPEED)*2+"");
				lckGrowingTextField.setText(50+"");
				defGrowingTextField.setText(feclass.getGrowing(Stats.DEFENSE)*2+"");
				resGrowingTextField.setText(feclass.getGrowing(Stats.RESISTANCE)*2+"");
				createTextButton = UIFactory.newTextButton(Language.get("Create"));
				if (!player && useDefaults) {
					nameTextField.setText(EnemyDefaults.getEnemyDefaults().getName());
					levelTextField.setText(Integer.toString(EnemyDefaults.getEnemyDefaults().getLevel()));
					for (SelectBox<String> selectBox : weaponLevelsSelectBoxes.values())
						selectBox.setSelected(EnemyDefaults.getEnemyDefaults().getWeaponLevel());
					addDefaultItems();
					for (Item item : inventory) {
						HorizontalGroup group = new HorizontalGroup();
						group.addActor(UIFactory.newLabel(Language.get(item.getName())));
						TextButton deleteItemTextButton = UIFactory.newTextButton(Language.get("DeleteItem"));
						if (player)
							deleteItemTextButton.addListener(new DeleteItemButtonListener(itemsGroup, group, inventory,
									item));
						else {
							dropableCheckBoxes.add(UIFactory.newCheckBox(Language.get("Dropable")));
							dropableCheckBoxes.peek().setChecked(item.isDropable());
							group.addActor(dropableCheckBoxes.peek());
							deleteItemTextButton.addListener(new DeleteItemButtonListener(itemsGroup, group, inventory,
									item, dropableCheckBoxes, dropableCheckBoxes.peek()));
						}
						group.addActor(deleteItemTextButton);
						itemsGroup.addActor(group);
						pack();
					}
				}
			}
			else if (enemyUnit != null) {
				nameTextField.setText(enemyUnit.getUnitName());
				levelTextField.setText(enemyUnit.getLevel()+"");
				commanderCheckBox.setChecked(enemyUnit.isCommander());
				setMoveType();
				hpBaseStatsTextField.setText(enemyUnit.getCurrentStat(Stats.HITPOINTS)+"");
				strBaseStatsTextField.setText(enemyUnit.getCurrentStat(Stats.STRENGHT)+"");
				magBaseStatsTextField.setText(enemyUnit.getCurrentStat(Stats.MAGIC)+"");
				sklBaseStatsTextField.setText(enemyUnit.getCurrentStat(Stats.SKILL)+"");
				spdBaseStatsTextField.setText(enemyUnit.getCurrentStat(Stats.SPEED)+"");
				lckBaseStatsTextField.setText(enemyUnit.getCurrentStat(Stats.LUCK)+"");
				defBaseStatsTextField.setText(enemyUnit.getCurrentStat(Stats.DEFENSE)+"");
				resBaseStatsTextField.setText(enemyUnit.getCurrentStat(Stats.RESISTANCE)+"");
				randomBaseStatsCheckBox.setChecked(false);
				hpGrowingTextField.setText(enemyUnit.getCurrentGrowing(Stats.HITPOINTS)+"");
				strGrowingTextField.setText(enemyUnit.getCurrentGrowing(Stats.STRENGHT)+"");
				magGrowingTextField.setText(enemyUnit.getCurrentGrowing(Stats.MAGIC)+"");
				sklGrowingTextField.setText(enemyUnit.getCurrentGrowing(Stats.SKILL)+"");
				spdGrowingTextField.setText(enemyUnit.getCurrentGrowing(Stats.SPEED)+"");
				lckGrowingTextField.setText(enemyUnit.getCurrentGrowing(Stats.LUCK)+"");
				defGrowingTextField.setText(enemyUnit.getCurrentGrowing(Stats.DEFENSE)+"");
				resGrowingTextField.setText(enemyUnit.getCurrentGrowing(Stats.RESISTANCE)+"");
				for (int i=0; i<GamePreferences.getWeaponNames().size; i++) {
					if (feclass.getWeaponUsage(i) && enemyUnit.getWeaponSkill(i) != 1) {
						for (int j = 0; j < GamePreferences.getWeaponLevelsValues().size; j++) {
							if (GamePreferences.getWeaponLevelsValues().get(j) == enemyUnit.getWeaponSkill(i)-1) {
								weaponLevelsSelectBoxes.get(i).setSelected(GamePreferences.getWeaponLevelsNames().get(j));
								continue;
							}
						}
					}
				}
				setSelectedStrategies(strategyAselectBox, enemyUnit.getStrategy());
				strategyAWeightTextField.setText(((SelectionAIStrategy)enemyUnit.getStrategy()).getWeight()+"");
				setSelectedStrategies(strategyBselectBox, ((SelectionAIStrategy)enemyUnit.getStrategy()).getFullStrategy());
				strategyBWeightTextField.setText(((SelectionAIStrategy)((SelectionAIStrategy)enemyUnit.getStrategy()).getFullStrategy()).getWeight()+"");
				inventory = enemyUnit.getInventory().getItems();
				for (Item item : inventory) {
					HorizontalGroup group = new HorizontalGroup();
					group.addActor(UIFactory.newLabel(Language.get(item.getName())));
					dropableCheckBoxes.add(UIFactory.newCheckBox(Language.get("Dropable")));
					dropableCheckBoxes.peek().setChecked(item.isDropable());
					group.addActor(dropableCheckBoxes.peek());
					TextButton deleteItemTextButton = UIFactory.newTextButton(Language.get("DeleteItem"));
					deleteItemTextButton.addListener(new DeleteItemButtonListener(itemsGroup, group, inventory,
							item, dropableCheckBoxes, dropableCheckBoxes.peek()));
					group.addActor(deleteItemTextButton);
					itemsGroup.addActor(group);
					pack();
				}
				createTextButton = UIFactory.newTextButton(Language.get("Modify"));
			}
			else {
				nameTextField.setText(playerUnit.getUnitName());
				levelTextField.setText(playerUnit.getLevel()+"");
				commanderCheckBox.setChecked(playerUnit.isCommander());
				hpBaseStatsTextField.setText(playerUnit.getCurrentStat(Stats.HITPOINTS)+"");
				strBaseStatsTextField.setText(playerUnit.getCurrentStat(Stats.STRENGHT)+"");
				magBaseStatsTextField.setText(playerUnit.getCurrentStat(Stats.MAGIC)+"");
				sklBaseStatsTextField.setText(playerUnit.getCurrentStat(Stats.SKILL)+"");
				spdBaseStatsTextField.setText(playerUnit.getCurrentStat(Stats.SPEED)+"");
				lckBaseStatsTextField.setText(playerUnit.getCurrentStat(Stats.LUCK)+"");
				defBaseStatsTextField.setText(playerUnit.getCurrentStat(Stats.DEFENSE)+"");
				resBaseStatsTextField.setText(playerUnit.getCurrentStat(Stats.RESISTANCE)+"");
				randomBaseStatsCheckBox.setChecked(false);
				hpTopStatsTextField.setText(playerUnit.getCurrentTopStat(Stats.HITPOINTS)+"");
				strTopStatsTextField.setText(playerUnit.getCurrentTopStat(Stats.STRENGHT)+"");
				magTopStatsTextField.setText(playerUnit.getCurrentTopStat(Stats.MAGIC)+"");
				sklTopStatsTextField.setText(playerUnit.getCurrentTopStat(Stats.SKILL)+"");
				spdTopStatsTextField.setText(playerUnit.getCurrentTopStat(Stats.SPEED)+"");
				lckTopStatsTextField.setText(playerUnit.getCurrentTopStat(Stats.LUCK)+"");
				defTopStatsTextField.setText(playerUnit.getCurrentTopStat(Stats.DEFENSE)+"");
				resTopStatsTextField.setText(playerUnit.getCurrentTopStat(Stats.RESISTANCE)+"");
				hpGrowingTextField.setText(playerUnit.getCurrentGrowing(Stats.HITPOINTS)+"");
				strGrowingTextField.setText(playerUnit.getCurrentGrowing(Stats.STRENGHT)+"");
				magGrowingTextField.setText(playerUnit.getCurrentGrowing(Stats.MAGIC)+"");
				sklGrowingTextField.setText(playerUnit.getCurrentGrowing(Stats.SKILL)+"");
				spdGrowingTextField.setText(playerUnit.getCurrentGrowing(Stats.SPEED)+"");
				lckGrowingTextField.setText(playerUnit.getCurrentGrowing(Stats.LUCK)+"");
				defGrowingTextField.setText(playerUnit.getCurrentGrowing(Stats.DEFENSE)+"");
				resGrowingTextField.setText(playerUnit.getCurrentGrowing(Stats.RESISTANCE)+"");
				for (int i=0; i<GamePreferences.getWeaponNames().size; i++) {
					if (feclass.getWeaponUsage(i) && playerUnit.getWeaponSkill(i) != 1) {
						for (int j = 0; j < GamePreferences.getWeaponLevelsValues().size; j++) {
							if (GamePreferences.getWeaponLevelsValues().get(j) == playerUnit.getWeaponSkill(i)-1) {
								weaponLevelsSelectBoxes.get(i).setSelected(GamePreferences.getWeaponLevelsNames().get(j));
								continue;
							}
						}
					}
				}
				inventory = playerUnit.getInventory().getItems();
				for (Item item : inventory) {
					HorizontalGroup group = new HorizontalGroup();
					group.addActor(UIFactory.newLabel(Language.get(item.getName())));
					/*dropableCheckBoxes.add(UIFactory.newCheckBox(Language.get("Dropable")));
					dropableCheckBoxes.peek().setChecked(item.isDropable());
					group.addActor(dropableCheckBoxes.peek());*/
					TextButton deleteItemTextButton = UIFactory.newTextButton(Language.get("DeleteItem"));
					deleteItemTextButton.addListener(new DeleteItemButtonListener(itemsGroup, group, inventory,
							item));
					group.addActor(deleteItemTextButton);
					itemsGroup.addActor(group);
					pack();
				}
				createTextButton = UIFactory.newTextButton(Language.get("Modify"));
			}
			add(UIFactory.newLabel(Language.get("Name")+":")).colspan(2);
			add(nameTextField).colspan(3).width(100);
			add(UIFactory.newLabel(Language.get("LevelEditor")+":")).colspan(2);
			add(levelTextField).width(30);
			add(commanderCheckBox).colspan(4);
			if (!player) {
				add(UIFactory.newLabel(Language.get("Move")+":")).colspan(2);
				add(moveSelectBox).colspan(2);
			}
			row();
			add(UIFactory.newLabel(Language.get("BaseStats"))).colspan(4).padTop(5f).padBottom(5f);
			row();
			add(UIFactory.newLabel(Language.get("HITPOINTS")+":"));
			add(hpBaseStatsTextField).width(30);
			add(UIFactory.newLabel(Language.get("StrEd")+":"));
			add(strBaseStatsTextField).width(30);
			add(UIFactory.newLabel(Language.get("MagEd")+":"));
			add(magBaseStatsTextField).width(30);
			add(UIFactory.newLabel(Language.get("SklEd")+":"));
			add(sklBaseStatsTextField).width(30);
			add(UIFactory.newLabel(Language.get("SpdEd")+":"));
			add(spdBaseStatsTextField).width(30);
			add(UIFactory.newLabel(Language.get("LckEd")+":"));
			add(lckBaseStatsTextField).width(30);
			add(UIFactory.newLabel(Language.get("DefEd")+":"));
			add(defBaseStatsTextField).width(30);
			add(UIFactory.newLabel(Language.get("ResEd")+":"));
			add(resBaseStatsTextField).width(30);
			row();
			add(randomBaseStatsCheckBox).colspan(8).padTop(5f).padBottom(5f).left();
			row();
			if (player) {
				add(UIFactory.newLabel(Language.get("TopStats"))).colspan(4).padTop(5f).padBottom(5f);
				row();
				add(UIFactory.newLabel(Language.get("HITPOINTS")+":"));
				add(hpTopStatsTextField).width(30);
				add(UIFactory.newLabel(Language.get("StrEd")+":"));
				add(strTopStatsTextField).width(30);
				add(UIFactory.newLabel(Language.get("MagEd")+":"));
				add(magTopStatsTextField).width(30);
				add(UIFactory.newLabel(Language.get("SklEd")+":"));
				add(sklTopStatsTextField).width(30);
				add(UIFactory.newLabel(Language.get("SpdEd")+":"));
				add(spdTopStatsTextField).width(30);
				add(UIFactory.newLabel(Language.get("LckEd")+":"));
				add(lckTopStatsTextField).width(30);
				add(UIFactory.newLabel(Language.get("DefEd")+":"));
				add(defTopStatsTextField).width(30);
				add(UIFactory.newLabel(Language.get("ResEd")+":"));
				add(resTopStatsTextField).width(30);
				row();
			}
			add(UIFactory.newLabel(Language.get("Growing"))).colspan(4).padTop(5f).padBottom(5f);
			row();
			add(UIFactory.newLabel(Language.get("HITPOINTS")+":"));
			add(hpGrowingTextField).width(38);
			add(UIFactory.newLabel(Language.get("StrEd")+":"));
			add(strGrowingTextField).width(38);
			add(UIFactory.newLabel(Language.get("MagEd")+":"));
			add(magGrowingTextField).width(38);
			add(UIFactory.newLabel(Language.get("SklEd")+":"));
			add(sklGrowingTextField).width(38);
			add(UIFactory.newLabel(Language.get("SpdEd")+":"));
			add(spdGrowingTextField).width(38);
			add(UIFactory.newLabel(Language.get("LckEd")+":"));
			add(lckGrowingTextField).width(38);
			add(UIFactory.newLabel(Language.get("DefEd")+":"));
			add(defGrowingTextField).width(38);
			add(UIFactory.newLabel(Language.get("ResEd")+":"));
			add(resGrowingTextField).width(38);
			row();
			add(UIFactory.newLabel(Language.get("WeaponLevels"))).colspan(4).padTop(5f).padBottom(5f);
			row();
			for (int i=0; i<GamePreferences.getWeaponNames().size; i++) {
				if (feclass.getWeaponUsage(i)) {
					add(UIFactory.newLabel(Language.get(GamePreferences.getWeaponNames().get(i))+":")).colspan(3);
					add(weaponLevelsSelectBoxes.get(i));
				}
			}
			row();
			if (!player) {
				add(UIFactory.newLabel(Language.get("AttackStrategy"))).colspan(4).padTop(5f).padBottom(5f);
				row();
				add(UIFactory.newLabel(Language.get("Strategy"))).colspan(4).padBottom(5f);
				add(UIFactory.newLabel(Language.get("Weight"))).colspan(2);
				add(UIFactory.newLabel(Language.get("Strategy"))).colspan(4);
				add(UIFactory.newLabel(Language.get("Weight"))).colspan(2);
				row();
				add(strategyAselectBox).colspan(4);
				add(strategyAWeightTextField).colspan(2).width(50);
				add(strategyBselectBox).colspan(4);
				add(strategyBWeightTextField).colspan(2).width(50);
				row();
			}
			add(UIFactory.newLabel(Language.get("Items"))).colspan(6);
			row();
			add(itemsSelectBox).colspan(6);
			TextButton addItemTextButton = UIFactory.newTextButton(Language.get("AddItem"));
			addItemTextButton.addListener(new ClickListener() {
				
				@Override
				public void clicked(InputEvent event, float x, float y) {
					inventory.add(itemsSelectBox.getSelected());
					HorizontalGroup group = new HorizontalGroup();
					group.addActor(UIFactory.newLabel(Language.get(itemsSelectBox.getSelected().getName())));
					TextButton deleteItemTextButton = UIFactory.newTextButton(Language.get("DeleteItem"));
					if (player)
						deleteItemTextButton.addListener(new DeleteItemButtonListener(itemsGroup, group, inventory,
								inventory.peek()));
					else {
						dropableCheckBoxes.add(UIFactory.newCheckBox(Language.get("Dropable")));
						group.addActor(dropableCheckBoxes.peek());
						deleteItemTextButton.addListener(new DeleteItemButtonListener(itemsGroup, group, inventory,
								inventory.peek(), dropableCheckBoxes, dropableCheckBoxes.peek()));
					}
					group.addActor(deleteItemTextButton);
					itemsGroup.addActor(group);
					pack();
				}
				
			});
			add(addItemTextButton).colspan(4);
			createTextButton.addListener(new ClickListener() {
				
				@Override
				public void clicked(InputEvent event, float x, float y) {
					int id = -1;
					if (enemyUnit != null) {
						id = enemyUnit.getBattleId();
						field.removeActor(enemyUnit);
						GameController.getGameController().removeEnemyUnit(enemyUnit);
					}
					else if (playerUnit != null) {
						id = playerUnit.getBattleId();
						GameController.getGameController().deletePlayerUnit(playerUnit);
					}
					if (player)
						playerUnit = new PlayerUnit();
					else
						enemyUnit = new EnemyUnit();
					Json json = new Json();
					StringWriter stringWriter = new StringWriter();
					JsonWriter writer = new JsonWriter(stringWriter);
					json.setWriter(writer);
					json.writeObjectStart();
					if (player) {
						json.writeType(playerUnit.getClass());
						json.writeValue("id", id == -1 ? GameController.getGameController().getAllPlayerUnits().size : id);
					}
					else {
						json.writeType(enemyUnit.getClass());
						json.writeValue("id", id == -1 ? GameController.getGameController().getEnemyUnits().size : id);
					}
					json.writeValue("name", nameTextField.getText());
					json.writeValue("shortDescription", "Mock part");
					json.writeValue("level", Integer.parseInt(levelTextField.getText()));
					json.writeArrayStart("position");
					json.writeValue(field.cursor.getXMap());
					json.writeValue(field.cursor.getYMap());
					json.writeArrayEnd();
					json.writeValue("feclass", feclass.getId());
					json.writeValue("commander", commanderCheckBox.isChecked());
					json.writeValue("playerUnit", player);
					if (player) {
						json.writeValue("exp", 0);
						json.writeArrayStart("topStats");
						json.writeValue(Integer.parseInt(hpTopStatsTextField.getText())-feclass.getTopStat(Stats.HITPOINTS));
						json.writeValue(Integer.parseInt(strTopStatsTextField.getText())-feclass.getTopStat(Stats.STRENGHT));
						json.writeValue(Integer.parseInt(magTopStatsTextField.getText())-feclass.getTopStat(Stats.MAGIC));
						json.writeValue(Integer.parseInt(sklTopStatsTextField.getText())-feclass.getTopStat(Stats.SKILL));
						json.writeValue(Integer.parseInt(spdTopStatsTextField.getText())-feclass.getTopStat(Stats.SPEED));
						json.writeValue(Integer.parseInt(lckTopStatsTextField.getText())-feclass.getTopStat(Stats.LUCK));
						json.writeValue(Integer.parseInt(defTopStatsTextField.getText())-feclass.getTopStat(Stats.DEFENSE));
						json.writeValue(Integer.parseInt(resTopStatsTextField.getText())-feclass.getTopStat(Stats.RESISTANCE));
						json.writeArrayEnd();
					}
					else
						json.writeValue("reclutable", false);
					json.writeArrayStart("baseStats");
					json.writeValue(Integer.parseInt(hpBaseStatsTextField.getText())-feclass.getBaseStat(Stats.HITPOINTS));
					json.writeValue(Integer.parseInt(strBaseStatsTextField.getText())-feclass.getBaseStat(Stats.STRENGHT));
					json.writeValue(Integer.parseInt(magBaseStatsTextField.getText())-feclass.getBaseStat(Stats.MAGIC));
					json.writeValue(Integer.parseInt(sklBaseStatsTextField.getText())-feclass.getBaseStat(Stats.SKILL));
					json.writeValue(Integer.parseInt(spdBaseStatsTextField.getText())-feclass.getBaseStat(Stats.SPEED));
					json.writeValue(Integer.parseInt(lckBaseStatsTextField.getText())-feclass.getBaseStat(Stats.LUCK));
					json.writeValue(Integer.parseInt(defBaseStatsTextField.getText())-feclass.getBaseStat(Stats.DEFENSE));
					json.writeValue(Integer.parseInt(resBaseStatsTextField.getText())-feclass.getBaseStat(Stats.RESISTANCE));
					json.writeArrayEnd();
					json.writeArrayStart("baseGrowing");
					json.writeValue(Integer.parseInt(hpGrowingTextField.getText())-feclass.getGrowing(Stats.HITPOINTS));
					json.writeValue(Integer.parseInt(strGrowingTextField.getText())-feclass.getGrowing(Stats.STRENGHT));
					json.writeValue(Integer.parseInt(magGrowingTextField.getText())-feclass.getGrowing(Stats.MAGIC));
					json.writeValue(Integer.parseInt(sklGrowingTextField.getText())-feclass.getGrowing(Stats.SKILL));
					json.writeValue(Integer.parseInt(spdGrowingTextField.getText())-feclass.getGrowing(Stats.SPEED));
					json.writeValue(Integer.parseInt(lckGrowingTextField.getText())-feclass.getGrowing(Stats.LUCK));
					json.writeValue(Integer.parseInt(defGrowingTextField.getText())-feclass.getGrowing(Stats.DEFENSE));
					json.writeValue(Integer.parseInt(resGrowingTextField.getText())-feclass.getGrowing(Stats.RESISTANCE));
					json.writeArrayEnd();
					json.writeArrayStart("weaponSkills");
					for (int i=0; i<GamePreferences.getWeaponNames().size; i++) {
						if (weaponLevelsSelectBoxes.get(i, null) == null)
							json.writeValue(1);
						else
							json.writeValue(GamePreferences.getWeaponLevelsValues()
								.get(weaponLevelsSelectBoxes.get(i).getSelectedIndex())+1);
					}
					json.writeArrayEnd();
					json.writeObjectStart("inventory");
					json.writeType(Inventory.class);
					json.writeArrayStart("newItems");
					for (Item item : inventory) {
						if (item instanceof Weapon)
							json.writeValue(((Weapon)item).getWeaponId());
						else
							json.writeValue(-1);
						json.writeValue(item.getId());
					}
					json.writeArrayEnd();
					if (!player) {
					json.writeArrayStart("dropable");
						for (CheckBox checkBox : dropableCheckBoxes)
							json.writeValue(checkBox.isChecked());
						json.writeArrayEnd();
					}
					json.writeObjectEnd();
					if (player) {
						json.writeObjectEnd();
						playerUnit = json.fromJson(PlayerUnit.class, stringWriter.toString());
						if (randomBaseStatsCheckBox.isChecked())
							playerUnit.initialLevelUp();
					}
					else {
						json.writeValue("move", moveSelectBox.getSelected().getId());
						json.writeObjectStart("strategy");
						json.writeValue("class", strategyAselectBox.getSelected().getClassName());
						json.writeObjectStart("fullStrategy");
						json.writeValue("class", strategyBselectBox.getSelected().getClassName());
						json.writeObjectStart("fullStrategy");
						json.writeType(NothingAIStrategy.class);
						json.writeObjectEnd();
						json.writeValue("weight", Float.parseFloat(strategyBWeightTextField.getText()));
						json.writeObjectEnd();
						json.writeValue("weight", Float.parseFloat(strategyAWeightTextField.getText()));
						json.writeObjectEnd();
						json.writeObjectEnd();
						enemyUnit = json.fromJson(EnemyUnit.class, stringWriter.toString());
						if (randomBaseStatsCheckBox.isChecked())
							enemyUnit.initialLevelUp();
					}
					acceptAction();
				}
				
			});
			add(createTextButton).colspan(3);
			TextButton backTextButton = UIFactory.newTextButton(Language.get("Back"));
			backTextButton.addListener(new ClickListener() {
				
				@Override
				public void clicked(InputEvent event, float x, float y) {
					cancelAction();
				}
				
			});
			add(backTextButton).colspan(3);
			row();
			add(itemsGroup).colspan(8);
			pack();
		}
		
		private void setSelectedStrategies(SelectBox<StrategyNameId> selectBox, AIStrategy strategy) {
			for (StrategyNameId strategyName : StrategyNameId.values()) {
				if (strategy.getClass().getName().equals(strategyName.getClassName())) {
					selectBox.setSelected(strategyName);
					continue;
				}
			}
		}
		
		private void setMoveType() {
			switch (enemyUnit.getMoveType()) {
			case 0:
				moveSelectBox.setSelected(MoveType.YES);
				break;
			case 1:
				moveSelectBox.setSelected(MoveType.IN_RANGE);
				break;
			case 2:
				moveSelectBox.setSelected(MoveType.NO);
				break;
			}
		}
		
		private void addDefaultItems() {
			for (int i=0; i<GamePreferences.getWeaponNames().size; i++) {
				if (feclass.getWeaponUsage(i)) {
					String level = EnemyDefaults.getEnemyDefaults().getWeaponLevel();
					for (int j=0; j<GamePreferences.getWeaponLevelsNames().size; j++) {
						if (level.equals(GamePreferences.getWeaponLevelsNames().get(j))) {
							inventory.add(ItemFactory.getItem(i, j));
						}
					}
				}
					
			}
		}
		
	}

}
