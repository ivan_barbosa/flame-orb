package es.flameorb.game.map;

import es.flameorb.general.Language;

public class EditorPlayerUnitsMapState extends MenuMapState {
	
	private EditorPlayerUnitsMenu menu;

	public EditorPlayerUnitsMapState(Field field) {
		super(field);
		menu = new EditorPlayerUnitsMenu(Language.get("Units"), this);
		placeMenu(menu);
	}

	@Override
	public void acceptAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		switch(menu.getSelected()) {
		case CREATE:
			field.setMapState(new EditorUnitClassSelectionMapState(field, null, null, true));
			break;
		case LIST:
			field.setMapState(new EditorPlayerUnitsListMapState(field));
			break;
		}

	}

	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new EditorPlayerEditionMapState(field));
	}
	
	public enum EditorPlayerUnitsOptions {
		CREATE("Create"), LIST("List");
		
		private final String name;
		
		private EditorPlayerUnitsOptions(String name) {
			this.name = name;
		}
		
		@Override
		public String toString() {
			return Language.get(name);
		}
	}
	
	public class EditorPlayerUnitsMenu extends MapMenu<EditorPlayerUnitsOptions> {

		public EditorPlayerUnitsMenu(String title, MapState mapState) {
			super(title, mapState);
			
		}
		
		@Override
		protected void prepareOptions() {
			options.setItems(EditorPlayerUnitsOptions.values());
		}
		
	}

}
