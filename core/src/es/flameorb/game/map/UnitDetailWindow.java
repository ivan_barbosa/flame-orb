package es.flameorb.game.map;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;

import es.flameorb.general.GamePreferences;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;
import es.flameorb.items.Item;
import es.flameorb.units.PlayerUnit;
import es.flameorb.units.Stats;
import es.flameorb.units.Unit;

/**
 * Ventana que muestra los detalle de una unidad.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class UnitDetailWindow extends Window {
	
	private Unit unit;
	private Label atk, mov, feclass, hit, as,
	level, exp, critical, type1, hp, avoid, type2,
	str, mag, skl, spd, lck, def, res;
	private Label atkVal, movVal, hitVal, asVal,
	criticalVal, type1Val, avoidVal, type2Val,
	strVal, magVal, sklVal, spdVal, lckVal, defVal, resVal;
	private Label [] items = new Label[5];
	private ProgressBar hpBar;
	private HorizontalGroup weaponSkills;
	private Array<WeaponSkillInfo> weaponSkill = new Array<WeaponSkillInfo>(GamePreferences.getWeaponNames().size);

	/**
	 * Construye la ventana de detalles de la unidad recibida por parámetro, inicializando
	 * todos los componenetes de la ventana.
	 * @param title - El título de la ventana.
	 * @param unit - La unidad cuyo detalles muestra la ventana.
	 */
	public UnitDetailWindow(String title, Unit unit) {
		super("", UIFactory.getSkin());
		this.unit = unit;
		atk = UIFactory.newLabel(Language.get("Atk"));
		atkVal = UIFactory.newLabel(unit.getCurrentAttackPower()+"");
		mov = UIFactory.newLabel(Language.get("Mov"));
		movVal = UIFactory.newLabel(unit.getMovement()+"");
		feclass = UIFactory.newLabel(Language.get(unit.getFeclass().getName()));
		hit = UIFactory.newLabel(Language.get("MiniHit"));
		hitVal = UIFactory.newLabel(unit.getCurrentHitRate()+"");
		as = UIFactory.newLabel(Language.get("AS"));
		asVal = UIFactory.newLabel(unit.getCurrentAttackSpeed()+"");
		level = UIFactory.newLabel(Language.format("Level", unit.getLevel()));
		if (unit.isPlayerUnit()) {
			PlayerUnit playerUnit = (PlayerUnit)unit;
			exp = UIFactory.newLabel(Language.format("Exp", playerUnit.getExp()));
		}
		else
			exp = UIFactory.newLabel(Language.format("Exp", "---"));
		critical = UIFactory.newLabel(Language.get("Cri"));
		criticalVal = UIFactory.newLabel(unit.getCurrentCriticalRate()+"");
		type1 = UIFactory.newLabel(Language.get("Type_1"));
		if (unit.getFirstType() != null)
			type1Val = UIFactory.newLabel(unit.getFirstType().getShortName());
		else
			type1Val = UIFactory.newLabel("");
		hp = UIFactory.newLabel(Language.format("HPPer", unit.getCurrentHP(), unit.getBattleStat(Stats.HITPOINTS)));
		hpBar = new ProgressBar(0, unit.getBattleStat(Stats.HITPOINTS), 1, false, UIFactory.getSkin());
		hpBar.setValue(unit.getCurrentHP());
		avoid = UIFactory.newLabel(Language.get("Avo"));
		avoidVal = UIFactory.newLabel(unit.getCurrentAvoidRate()+"");
		type2 = UIFactory.newLabel(Language.get("Type_2"));
		if (unit.getSecondType() != null)
			type2Val = UIFactory.newLabel(unit.getSecondType().getShortName());
		else
			type2Val = UIFactory.newLabel("");
		str = UIFactory.newLabel(Language.get("Str"));
		strVal = UIFactory.newLabel(unit.getBattleStat(Stats.STRENGHT)+"");
		checkStatsChanges(strVal, Stats.STRENGHT);
		mag = UIFactory.newLabel(Language.get("Mag"));
		magVal = UIFactory.newLabel(unit.getBattleStat(Stats.MAGIC)+"");
		checkStatsChanges(magVal, Stats.MAGIC);
		skl = UIFactory.newLabel(Language.get("Skl"));
		sklVal = UIFactory.newLabel(unit.getBattleStat(Stats.SKILL)+"");
		checkStatsChanges(sklVal, Stats.SKILL);
		spd = UIFactory.newLabel(Language.get("Spd"));
		spdVal = UIFactory.newLabel(unit.getBattleStat(Stats.SPEED)+"");
		checkStatsChanges(spdVal, Stats.SPEED);
		lck = UIFactory.newLabel(Language.get("Lck"));
		lckVal = UIFactory.newLabel(unit.getBattleStat(Stats.LUCK)+"");
		checkStatsChanges(lckVal, Stats.LUCK);
		def = UIFactory.newLabel(Language.get("Def"));
		defVal = UIFactory.newLabel(unit.getBattleStat(Stats.DEFENSE)+"");
		checkStatsChanges(defVal, Stats.DEFENSE);
		res = UIFactory.newLabel(Language.get("Res"));
		resVal = UIFactory.newLabel(unit.getBattleStat(Stats.RESISTANCE)+"");
		checkStatsChanges(resVal, Stats.RESISTANCE);
		for (int i=0; i<5; i++) {
			if (i < unit.getInventory().getItemsNumber()) {
				Item item = unit.getInventory().getItem(i);
				items[i] = UIFactory.newLabel(item.toString());
				if (item.isDropable())
					items[i].setColor(Color.GREEN);
			}
			else
				items[i] = UIFactory.newLabel("");
		}
		weaponSkills = new HorizontalGroup();
		for (int i=0; i<GamePreferences.getWeaponNames().size; i++) {
			if (unit.getFeclass().getWeaponUsage(i))
				weaponSkill.add(new WeaponSkillInfo(i, unit.getWeaponSkill(i)));
		}
		addElementsToTable();
		pack();
	}
	
	/**
	 * Añade todos los elementos creados previamente a la ventana de los detalles.
	 */
	private void addElementsToTable() {
		left();
		add().colspan(3);
		add(unit.getUnitName()).center().colspan(3);
		add(atk).left();
		add(atkVal).right().padRight(2f);
		add(mov).left();
		add(movVal).right().padRight(7f);
		row();
		add().colspan(3);
		add(feclass).center().colspan(3);
		add(hit).left();
		add(hitVal).right().padRight(2f);
		add(as).left();
		add(asVal).right().padRight(7f);
		row();
		add().colspan(3); 
		add(level);
		add();
		add(exp);
		add(critical).left();
		add(criticalVal).right().padRight(2f);
		add(type1).left();
		add(type1Val).right().padRight(7f);
		row();
		add().colspan(3);
		add(hp);
		add(hpBar).colspan(2).padRight(2f);
		add(avoid).left();
		add(avoidVal).right().padRight(2f);
		add(type2).left();
		add(type2Val).right().padRight(7f);
		row();
		add(str).colspan(2).left();
		add(strVal).right();
		row();
		add(mag).colspan(2).left();
		add(magVal).right();
		add();
		add(items[0]).colspan(6).left();
		row();
		add(skl).colspan(2).left();
		add(sklVal).right();
		add();
		add(items[1]).colspan(6).left();
		row();
		add(spd).colspan(2).left();
		add(spdVal).right();
		add();
		add(items[2]).colspan(6).left();
		row();
		add(lck).colspan(2).left();
		add(lckVal).right();
		add();
		add(items[3]).colspan(6).left();
		row();
		add(def).colspan(2).left();
		add(defVal).right();
		add();
		add(items[4]).colspan(6).left();
		row();
		add(res).colspan(2).left();
		add(resVal).right();
		row();
		add(weaponSkills).colspan(10);
		for (WeaponSkillInfo weaponUsage : weaponSkill) {
			weaponUsage.padRight(5f);
			weaponSkills.addActor(weaponUsage);
		}
	}
	
	/**
	 * Actualiza los detalles de la unidad tras la acción de ésta.
	 */
	public void afterActionUpdate() {
		inventoryUpdate();
		weaponExperienceUpdate();
	}
	
	/**
	 * Actualiza los parámetros derivados de la unidad.
	 */
	public void battleParametersUpdate() {
		atkVal.setText(unit.getCurrentAttackPower()+"");
		hitVal.setText(unit.getCurrentHitRate()+"");
		criticalVal.setText(unit.getCurrentCriticalRate()+"");
		avoidVal.setText(unit.getCurrentAvoidRate()+"");
		asVal.setText(unit.getCurrentAttackSpeed()+"");
		statsUpdate();
	}
	
	/**
	 * Actualiza la unidad tras el cambio de clase.
	 */
	public void changeClassUpdate() {
		feclass.setText(Language.get(unit.getFeclass().getName()));
		movVal.setText(unit.getMovement()+"");
		if (unit.getFirstType() != null)
			type1Val.setText(unit.getFirstType().getShortName());
		else
			type1Val.setText("");
		if (unit.getSecondType() != null)
			type2Val.setText(unit.getFirstType().getShortName());
		else
			type2Val.setText("");
		weaponSkills.clear();
		weaponSkill.clear();
		weaponSkills = new HorizontalGroup();
		for (int i=0; i<GamePreferences.getWeaponNames().size; i++) {
			if (unit.getFeclass().getWeaponUsage(i))
				weaponSkill.add(new WeaponSkillInfo(i, unit.getWeaponSkill(i)));
		}
		levelUpUpdate();
	}
	
	/**
	 * Actualiza los puntos de experiencia.
	 */
	public void earnedExperienceUpdate() {
		PlayerUnit playerUnit = (PlayerUnit)unit;
		exp.setText(Language.format("Exp", playerUnit.getExp()));
	}
	
	/**
	 * Actualiza los PV restantes.
	 */
	public void hpUpdate() {
		hp.setText(Language.format("HPPer", unit.getCurrentHP(), unit.getBattleStat(Stats.HITPOINTS)));
		hpBar.setValue(unit.getCurrentHP());
	}
	
	/**
	 * Actualiza el inventario de la unidad.
	 */
	public void inventoryUpdate() {
		for (int i=0; i<5; i++) {
			if (i < unit.getInventory().getItemsNumber())
				items[i].setText(unit.getInventory().getItem(i).toString());
			else
				items[i].setText("");
		}
		battleParametersUpdate();
	}
	
	/**
	 * Actualiza la unidad tras subir de nivel.
	 */
	public void levelUpUpdate() {
		level.setText(Language.format("Level", unit.getLevel()));
		statsUpdate();
		battleParametersUpdate();
		earnedExperienceUpdate();
	}
	
	public void statsUpdate() {
		hpBar.setRange(0, unit.getBattleStat(Stats.HITPOINTS));
		strVal.setText(unit.getBattleStat(Stats.STRENGHT)+"");
		checkStatsChanges(strVal, Stats.STRENGHT);
		magVal.setText(unit.getBattleStat(Stats.MAGIC)+"");
		checkStatsChanges(magVal, Stats.MAGIC);
		sklVal.setText(unit.getBattleStat(Stats.SKILL)+"");
		checkStatsChanges(sklVal, Stats.SKILL);
		spdVal.setText(unit.getBattleStat(Stats.SPEED)+"");
		checkStatsChanges(spdVal, Stats.SPEED);
		lckVal.setText(unit.getBattleStat(Stats.LUCK)+"");
		checkStatsChanges(lckVal, Stats.LUCK);
		defVal.setText(unit.getBattleStat(Stats.DEFENSE)+"");
		checkStatsChanges(defVal, Stats.DEFENSE);
		resVal.setText(unit.getBattleStat(Stats.RESISTANCE)+"");
		checkStatsChanges(resVal, Stats.RESISTANCE);
		hpUpdate();
	}
	
	private void checkStatsChanges(Label statText, Stats stat) {
		if (unit.getCurrentStat(stat) < unit.getBattleStat(stat))
			statText.setColor(Color.BLUE);
		else if (unit.getCurrentStat(stat) > unit.getBattleStat(stat))
			statText.setColor(Color.RED);
	}
	
	/**
	 * Actualiza el nivel de habilidad en las armas.
	 */
	public void weaponExperienceUpdate() {
		for (WeaponSkillInfo weaponInfo : weaponSkill)
			weaponInfo.weaponExperienceUpdate();
	}
	
	/**
	 * Clase que representa la información de habilidad en un arma.
	 * @author Iván Barbosa Gutiérrez.
	 *
	 */
	public class WeaponSkillInfo extends Table {
		
		private int weaponId, currentWeaponLevel;
		private Label weaponNameLevel;
		private ProgressBar weaponProgress;
		private IntArray weaponLevels = GamePreferences.getWeaponLevelsValues();
		
		/**
		 * Construye un WeaponSkillInfo del arma elegida.
		 * @param skin - El skin de la table.
		 * @param weaponId - El identificador del arma.
		 * @param weaponLevel - El nivel del usuario en dicha arma.
		 */
		public WeaponSkillInfo(int weaponId, int weaponLevel) {
			super(UIFactory.getSkin());
			this.weaponId = weaponId;
			currentWeaponLevel = 0;
			while (currentWeaponLevel < 7 && weaponLevels.get(currentWeaponLevel+1) <= weaponLevel)
				currentWeaponLevel++;
			weaponNameLevel = UIFactory.newLabel(Language.get(GamePreferences.getWeaponNames().get(weaponId))+": "+
				GamePreferences.getWeaponLevelsNames().get(currentWeaponLevel));
			if (currentWeaponLevel != 7 && unit.isPlayerUnit()) {
				weaponProgress = new ProgressBar(weaponLevels.get(currentWeaponLevel), weaponLevels.get(currentWeaponLevel+1), 1, false, UIFactory.getSkin());
				weaponProgress.setValue(weaponLevel);
			}
			add(weaponNameLevel).center();
			row();
			add(weaponProgress).width(66);
			pack();
		}
		
		/**
		 * Actualiza el nivel de habilidad del arma.
		 */
		public void weaponExperienceUpdate() {
			if (currentWeaponLevel != 7) {
				if (weaponLevels.get(currentWeaponLevel+1) <= unit.getWeaponSkill(weaponId)) {
					currentWeaponLevel++;
					weaponNameLevel.setText(Language.get(GamePreferences.getWeaponNames().get(weaponId))+": "+
					GamePreferences.getWeaponLevelsNames().get(currentWeaponLevel));
					weaponProgress.setRange(weaponLevels.get(currentWeaponLevel-1), weaponLevels.get(currentWeaponLevel));
				}
				weaponProgress.setValue(unit.getWeaponSkill(weaponId));
			}
		}
		
	}

}
