package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.game.control.GameController;
import es.flameorb.general.KeyBinding;
import es.flameorb.units.Unit;

public class SeeMapMapState extends MovementMapState {
	
	private boolean editorMode;

	public SeeMapMapState(Field field, boolean editorMode) {
		super(field);
		this.editorMode = editorMode;
		MapData mapData = GameController.getGameController().getMapData();
		field.setInitialPositions(mapData.getInitialPositions());
		field.illuminateInitialPositionCells();
	}

	@Override
	public void acceptAction() {
		Unit unit = field.unitsPositions.getUnit(field.cursor.getMapPosition());
		field.setMapState(new UnitDetailMapState(field, unit, this));
	}

	@Override
	public void cancelAction() {
		field.shutdownInitialPositionFlags();
		if (editorMode)
			field.setMapState(new EditorMenuMapState(field));
		else
			field.setMapState(new PreparationMenuDisplayedMapState(field));
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept() && 
			field.unitsPositions.getUnit(field.cursor.getMapPosition()) != null)
			acceptAction();
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		if (field.unitsPositions.getUnit(field.cursor.getMapPosition()) != null)
			acceptAction();
		else
			cancelAction();
	}

}
