package es.flameorb.game.map;

import java.util.Set;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.assets.loaders.resolvers.ExternalFileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.LocalFileHandleResolver;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import es.flameorb.game.GameScreen;
import es.flameorb.game.calculators.RangeCalculator;
import es.flameorb.game.control.BattleLog;
import es.flameorb.game.control.GameController;
import es.flameorb.game.map.Point;
import es.flameorb.general.GamePreferences;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;
import es.flameorb.units.Stats;
import es.flameorb.units.Unit;
import es.flameorb.units.UnitPositionList;

/**
 * Representa el terreno de juego, sobre el que se colocan todos los elementos
 * del juego.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class Field extends Group {

	GameScreen screen;
	StretchViewport view;
	Texture cursorImage, blueSquareImage,
	redSquareImage, greenSquareImage;
	OrthographicCamera camera;
	Vector2 worldSize, cameraCentering;
	Vector3 cameraInitialPosition;
	TiledMapTileLayer mapLayer;
	OrthogonalTiledMapRenderer mapRenderer;
	Cursor cursor;
	int mapWidth, mapHeight;
	boolean movementFlag, atkRangeFlag, useRangeFlag, initialPositionFlag;
	Set<Point> movementCells, atkRangeCells, useRangeCells;
	Array<Point> playerInitialPositions;
	Point movementOrigin;
	UnitPositionList unitsPositions;
	TerrainWindow terrainInfo;
	UnitInfoWindow unitInfo;
	Window battleLog;
	MapState mapState;
	
	/**
	 * Construye el terreno de batalla e inicializa todos los elementos
	 * que contiene.
	 * @param mapPath - La ruta al mapa.
	 */
	public Field(MapData mapData, GameScreen screen) {
		this.screen = screen;
		
		worldSize = GamePreferences.getWorldSize();
		
		setPosition(0, 0);
		
		TiledMap map;
		if (Gdx.app.getType() == ApplicationType.Android)
			map = new TmxMapLoader(new ExternalFileHandleResolver()).load(mapData.getFileName()+".tmx");
		else
			map = new TmxMapLoader(new LocalFileHandleResolver()).load(mapData.getFileName()+".tmx");
		
		mapLayer = (TiledMapTileLayer)map.getLayers().get(0);
		mapWidth = mapLayer.getWidth();
		mapHeight = mapLayer.getHeight();
		
		setSize(mapWidth*32f, mapHeight*32f);
		
		GameController.getGameController().loadMap(this, mapData);
	
		cursorImage = new Texture(Gdx.files.internal("images/gui/cursor.png"));
		blueSquareImage = new Texture(Gdx.files.internal("images/gui/movementLight.png"));
		redSquareImage = new Texture(Gdx.files.internal("images/gui/attackLight.png"));
		greenSquareImage = new Texture(Gdx.files.internal("images/gui/staffLight.png"));
		
		camera = new OrthographicCamera(mapWidth*32, mapHeight*32);
		camera.setToOrtho(false, camera.viewportWidth*2, camera.viewportHeight*2);
		camera.update();
		
		cameraCentering = new Vector2();
		
		cursor = new Cursor();
		
		mapRenderer = new OrthogonalTiledMapRenderer(map);
		mapRenderer.setView(camera);
		
		unitsPositions = GameController.getGameController().getUnitsPositions();
		
		for (Unit enemy : GameController.getGameController().getEnemyUnits()) {
			addActor(enemy);
		}
		
		terrainInfo = new TerrainWindow(Language.get("Terrain"));
		terrainInfo.setPosition(0.2f*32, (mapHeight >= worldSize.y ? worldSize.y*32 : mapHeight*32)
				- 0.2f*32 - terrainInfo.getHeight());
		addActor(terrainInfo);
		
		unitInfo = new UnitInfoWindow(Language.get("UnitData"));
		unitInfo.setPosition((mapWidth >= worldSize.x ? worldSize.x*32 : mapWidth*32)
			- 0.2f*32 - unitInfo.getWidth() , 0.2f*32);
		addActor(unitInfo);
				
	}
	
	/**
	 * Ajusta la camara para que no se salga del borde del mapa.
	 */
	public void adjustCamera() {
		if (mapWidth > worldSize.x)
			camera.position.x = MathUtils.clamp(camera.position.x, camera.viewportWidth/2f,
				mapWidth*32 - camera.viewportWidth/2f);
		if (mapHeight > worldSize.y)
		camera.position.y = MathUtils.clamp(camera.position.y, camera.viewportHeight/2f,
				mapHeight*32 - camera.viewportHeight/2f);
	}
	
	/**
	 * Mueve las ventanas de información al moverse la cámara para que se mantengan en la
	 * misma posición de la pantalla.
	 * @param x - El número de pixels movidos en el eje X.
	 * @param y - El número de pixels movidos en el eje Y.
	 */
	public void adjustWindows(float x, float y) {
		terrainInfo.moveBy(x, y);
		unitInfo.moveBy(x, y);
		battleLog.moveBy(x, y);
	}

	/**
	 * Verifica si el cursor se introduce en el area algunda de las ventanas que se muestran.
	 * En caso de hacerlo, mueve la ventana a la esquina opuesta de la pantalla.
	 */
	public void checkCollisionCursorWindows() {
		Rectangle terrainInfoArea = new Rectangle(terrainInfo.getX(), terrainInfo.getY(),
				terrainInfo.getWidth(), terrainInfo.getHeight());
		if (cursor.overlaps(terrainInfoArea)) {
			if (terrainInfo.left) {
				terrainInfo.setX((mapWidth >= worldSize.x ? worldSize.x*32 : mapWidth*32)
						- 0.2f*32 - terrainInfo.getWidth()+camera.position.x - cameraInitialPosition.x - cameraCentering.x);
				terrainInfo.left = false;
			}
			else {
				terrainInfo.setX(0.2f*32+camera.position.x - cameraInitialPosition.x - cameraCentering.x);
				terrainInfo.left = true;
			}
		}
		Rectangle unitInfoArea = new Rectangle(unitInfo.getX(), unitInfo.getY(),
				unitInfo.getWidth(), unitInfo.getHeight());
		if (cursor.overlaps(unitInfoArea)) {
			if (unitInfo.right) {
				unitInfo.setX(0.2f*32+camera.position.x - cameraInitialPosition.x - cameraCentering.x);
				unitInfo.right = false;
			}
			else {
				unitInfo.setX((mapWidth >= worldSize.x ? worldSize.x*32 : mapWidth*32)
						- 0.2f*32 - unitInfo.getWidth()+camera.position.x - cameraInitialPosition.x - cameraCentering.x);
				unitInfo.right = true;
			}
		}
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		batch.end();
		adjustCamera();
		camera.update();
		mapRenderer.render();
		batch.begin();
		if (initialPositionFlag) {
			for (Point point : playerInitialPositions)
				batch.draw(blueSquareImage, point.x*32f, point.y*32f);
		}
		if (movementFlag) {
			for (Point point : movementCells)
				batch.draw(blueSquareImage, point.x*32f, point.y*32f);
		}
		if (atkRangeFlag) {
			for (Point point : atkRangeCells)
				batch.draw(redSquareImage, point.x*32f, point.y*32f);
		}
		if (useRangeFlag) {
			for (Point point : useRangeCells) {
				batch.draw(greenSquareImage, point.x*32f, point.y*32f);
			}
		}
		mapState.checkMoveCursor();
		batch.draw(cursorImage, cursor.x, cursor.y, cursor.width, cursor.height);
		drawChildren(batch, parentAlpha);
	}
	
	/**
	 * Realiza las operaciones necesarias para terminar la partida y vuelve al menú
	 * principal.
	 */
	public void endGame() {
		removeListener(mapState.getKeyboardListener());
		removeListener(mapState.getTouchListener());
		GameController.getGameController().endGame();
		screen.goBackToMenu();
	}
	
	/**
	 * Returns the camera of the map.
	 * @return The camera of the map.
	 */
	public OrthographicCamera getCamera() {
		return camera;
	}
	
	/**
	 * Returns the layer of the map where the terrain cells are placed.
	 * @return The layer of the map.
	 */
	public TiledMapTileLayer getMapLayer() {
		return mapLayer;
	}
	
	/**
	 * Returns the batch of the map renderer.
	 * @return The batch of the map renderer.
	 */
	public Batch getMapBatch() {
		return mapRenderer.getBatch();
	}

	/**
	 * Returns the Terrain properties of the cell of the given position.
	 * @param position - The position where the cell is.
	 * @return The Terrain properties.
	 */
	public Terrain getCellProperties(Point position) {
		if (position.x >= mapWidth || position.y >= mapHeight || position.x < 0 || position.y < 0)
			return null;
		else
			return TerrainData.getTerrain(mapLayer, position.x, position.y);
	}
	
	/**
	 * Activa la iluminación de movimiento a las celdas dadas.
	 * @param position - La posicion origen del movimiento.
	 */
	public void illuminateMovCells(Point position) {
		movementCells = RangeCalculator.getUnitMoveRange(unitsPositions.getUnit(position));
		movementFlag = true;
	}
	
	/**
	 * Activa la iluminación de ataque a las celdas dadas.
	 * @param cells - Las celdas a iluminar.
	 */
	public void illuminateAtkCells(Set<Point> cells) {
		atkRangeCells = cells;
		atkRangeFlag = true;
	}
	
	/**
	 * Activa la iluminación de uso de bastón a las celdas dadas.
	 * @param cells - Las celdas a iluminar.
	 */
	public void illuminateUseCells(Set<Point> cells) {
		useRangeCells = cells;
		useRangeFlag = true;
	}
	
	/**
	 * Activa la iluminación de las celdas de las posiciones iniciales.
	 */
	public void illuminateInitialPositionCells() {
		initialPositionFlag = true;
	}
	
	/**
	 * Devuelve verdadero si la unidad puede moverse a la posición dada.
	 * @param unit - La unidad que se quiere mover.
	 * @param position - La posición a la que la unidad se quiere mover.
	 * @return Si la unidad puede colocarse sobre dicha posición.
	 */
	public boolean isMovementPossible(Unit unit, Point position) {
		if (position.equals(unit.getMapPosition()))
			return true;
		else
			return !unitsPositions.isPositionOccuped(position) &&
					movementCells.contains(position);
	}
	
	/**
	 * Devuelve verdadero si la unidad puede moverse a la posición indicada
	 * por el cursor del mapa.
	 * @param unit - La unidad que se quiere mover.
	 * @return Si la unidad puede colocarse sobre la posición en la que se
	 * encuentra el cursor.
	 */
	public boolean isMovementPossible(Unit unit) {
		return isMovementPossible(unit, cursor.getMapPosition());
	}
	
	/**
	 * Devuelve verdadero si es posible lanzar un ataque a la posición
	 * indicada.
	 * @param position - La posición a la que se quiere lanzar el ataque.
	 * @return Si se puede lanzar un ataque a esa posición.
	 */
	public boolean isAttackPossible(Point position) {
		return atkRangeCells.contains(position) &&
				unitsPositions.isPositionOccupedByEnemy(position);
	}
	
	/**
	 * Devuelve verdadero si es posible lanzar un ataque a la posición
	 * inicada por el cursor.
	 * @return Si se puede lanzar un ataque a la posición del cursor.
	 */
	public boolean isAttackPossible() {
		return isAttackPossible(cursor.getMapPosition());
	}
	
	/**
	 * Devuelve verdadero si es posible usar un bastón sobre la posición
	 * indicada.
	 * @param position - La posición en la que se quiere usar el bastón.
	 * @return Si se puede usar el bastón en esa posición.
	 */
	public boolean isItemUsable(Point position) {
		return useRangeCells.contains(position) &&
				unitsPositions.isPositionOccupedByPlayer(position) &&
				unitsPositions.getUnit(position).getCurrentHP() !=
				unitsPositions.getUnit(position).getCurrentStat(Stats.HITPOINTS);
	}
	
	/**
	 * Devuelve verdadero si es posible usar un bastón sobre la posición
	 * del cursor.
	 * @return Si se puede usar el bastón en la posición del cursor.
	 */
	public boolean isItemUsable() {
		return isItemUsable(cursor.getMapPosition());
	}
	
	/**
	 * Crea y añade al campo la ventana de información de combate.
	 */
	public void placeBattleLog() {
		battleLog = UIFactory.newWindow();
		BattleLog.setWindow(battleLog);
		addActor(battleLog);
	}
	
	/**
	 * Devuelve a la unidad a la posición a la que se encontraba
	 * antes de realizar el moviemiento.
	 * @param oldPosition - La posición a la que se movió la unidad.
	 * @param unit - La unidad en cuestión.
	 */
	public void revertMovement(Point oldPosition, Unit unit) {
		if (!movementOrigin.equals(oldPosition)) {
			unit.setMapPosition(movementOrigin);
			GameController.getGameController().updatePosition(unit, oldPosition);
		}
	}
	
	public void setBattleMusic(String filename) {
		screen.setBattleMusic(filename);
	}
	
	public void setInitialCameraPosition() {
		cameraInitialPosition = new Vector3(camera.position);
	}
	
	public Vector2 getCameraCentering() {
		return cameraCentering;
	}
	
	public void setInitialPositions(Array<Point> points) {
		playerInitialPositions = points;
	}
	
	public boolean isPositionAnInitialPosition(Point point) {
		if (playerInitialPositions.contains(point, false))
			return true;
		else
			return false;
	}
	
	public boolean isPositionUsed(Point point) {
		return isPositionAnInitialPosition(point) ||
				unitsPositions.isPositionOccuped(point);
	}
	
	public void setViewport(StretchViewport view) {
		this.view = view;
	}
	
	public void setMapState(MapState newMapState) {
		removeListener(mapState.getKeyboardListener());
		removeListener(mapState.getTouchListener());
		addListener(newMapState.getKeyboardListener());
		addListener(newMapState.getTouchListener());
		mapState = newMapState;
	}
	
	public void setFirstMapState(MapState mapState) {
		this.mapState = mapState;
		addListener(mapState.getKeyboardListener());
		addListener(mapState.getTouchListener());
	}
	
	/**
	 * Apaga la iluminación de movimiento de las celdas.
	 */
	public void shutdownMovCells() {
		movementFlag = false;
	}
	
	/**
	 * Apaga la iluminación de ataque de las celdas.
	 */
	public void shutdownAtkCells() {
		atkRangeFlag = false;
	}
	
	/**
	 * Apaga la iluminación de uso de objetos de las celdas.
	 */
	public void shutdownUseCells() {
		useRangeFlag = false;
	}
	
	/**
	 * Apaga la iluminación de las celdas que indican la posición inicial.
	 */
	public void shutdownInitialPositionFlags() {
		initialPositionFlag = false;
	}
	
	/**
	 * Representa la ventana de información del terreno.
	 * @author Iván Barbosa Gutiérrez
	 *
	 */
	public class TerrainWindow extends Window {
		
		private Label text;
		boolean left;
		
		/**
		 * Crea la ventana del terreno.
		 * @param title - El título de la ventana.
		 * @param skin - La skin de la ventana.
		 */
		public TerrainWindow(String title) {
			super("", UIFactory.getSkin());
			add(UIFactory.newLabel(title));
			row();
			left = true;
			text = UIFactory.newLabel(getCellProperties(cursor.getMapPosition()).getTerrainInfo());
			add(text);
			pack();
		}
		
		/**
		 * Cambia el texto que muestra la ventana del terreno.
		 * @param terrainInfo
		 */
		public void setText(String terrainInfo) {
			text.setText(terrainInfo);
			pack();
		}
	}
	/**
	 * Representa la ventana de información sobre unidad.
	 * @author Iván Barbosa Gutiérrez
	 *
	 */
	public class UnitInfoWindow extends Window {
		
		private Table table;
		boolean right;
		
		/**
		 * Crea la ventana de información de la unidad
		 * @param title - El título de la ventana.
		 * @param skin - La skin de la ventana.
		 */
		public UnitInfoWindow(String title) {
			super("", UIFactory.getSkin());
			add(UIFactory.newLabel(title));
			row();
			right = true;
			table = new Table();
			add(table);
			setVisible(false);
		}
		
		/**
		 * Cambia la información de la ventana en función de la unidad recibida, o
		 * oculta la ventana si no se recibe una unidad.
		 * @param unit - La unidad cuya información se muestra.
		 */
		public void refreshTable(Unit unit) {
			if (unit != null) {
				table.clear();
				table.add(UIFactory.newLabel(Language.format("UnitNameLevelClass",
						unit.getUnitName(), unit.getLevel(), Language.get(unit.getFeclass().getName())))).colspan(2).left();
				table.row();
				ProgressBar hpBar = new ProgressBar(0, unit.getCurrentStat(Stats.HITPOINTS), 1, false, UIFactory.getSkin());
				hpBar.setValue(unit.getCurrentHP());
				table.add(hpBar).padRight(2).left();
				table.add(UIFactory.newLabel(Integer.toString(unit.getCurrentHP())+"/"+Integer.toString(unit.getCurrentStat(Stats.HITPOINTS))));
				if (unit.getEquipedWeapon() != null) {
					table.row();
					table.add(UIFactory.newLabel(Language.get(unit.getEquipedWeapon().getName()))).left();
				}
				pack();
				setVisible(true);
			}
			else
				setVisible(false);
		}

	}

}
