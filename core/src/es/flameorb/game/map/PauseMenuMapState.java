package es.flameorb.game.map;

import es.flameorb.game.control.GameController;
import es.flameorb.general.Language;

public class PauseMenuMapState extends MenuMapState {
	
	private PauseMenu menu;

	public PauseMenuMapState(Field field) {
		super(field);
		menu = new PauseMenu(Language.get("Turn") + " "
			+ GameController.getGameController().getTurnController().getTurnCount(), this);
		placeMenu(menu);
	}

	@Override
	public void acceptAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		switch(menu.getSelected()) {
		case END_CONDITIONS:
			field.setMapState(new ShowEndConditionsMapState(field, false));
			break;
		case END_TURN:
			GameController.getGameController().getTurnController().endTurn();
			field.setMapState(new ChangeTurnMapState(field, false));
			break;
		case END_GAME:
			GameController.getGameController().clearEndActionFlags();
			field.endGame();
			break;
		}
	}

	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new InitialMapState(field));
	}
	
	private enum PauseMenuOptions {
		END_CONDITIONS("EndConditions"), END_TURN("EndTurn"), END_GAME("EndGame");
		
		private final String name;
		
		private PauseMenuOptions(String name) {
			this.name = name;
		}
		
		public String toString() {
			return Language.get(name);
		}
	}
	
	private class PauseMenu extends MapMenu<PauseMenuOptions> {

		public PauseMenu(String title, MapState mapState) {
			super(title, mapState);
		}

		@Override
		protected void prepareOptions() {
			options.setItems(PauseMenuOptions.values());
		}
		
	}

}
