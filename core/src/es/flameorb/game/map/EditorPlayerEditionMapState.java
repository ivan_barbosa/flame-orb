package es.flameorb.game.map;

import es.flameorb.game.control.GameController;
import es.flameorb.general.Language;

public class EditorPlayerEditionMapState extends MenuMapState {
	
	private EditorPlayerMenu menu;

	public EditorPlayerEditionMapState(Field field) {
		super(field);
		menu = new EditorPlayerMenu(Language.get("PlayerEdition"), this);
		placeMenu(menu);
	}

	@Override
	public void acceptAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		switch (menu.getSelected()) {
		case UNITS:
			field.setMapState(new EditorPlayerUnitsMapState(field));
			break;
		case INITIAL_POSITIONS:
			field.setMapState(new EditorInitialPositionSettingMapState(field));
			break;
		case MONEY:
			field.setMapState(new EditorPlayerMoneySetMapState(field));
			break;
		case STOCK:
			field.setMapState(new EditorShopStockItemSelectionMapState(field,
					GameController.getGameController().getPlayerStock().getAllItems(), false));
			break;
		}
	}

	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new EditorMenuMapState(field));
	}
	
	public enum EditorPlayerMenuOptions {
		UNITS("Units"), INITIAL_POSITIONS("InitialPositions"), MONEY("Money"), STOCK("Stock");
		
		
		private final String name;
		
		private EditorPlayerMenuOptions(String name) {
			this.name = name;
		}
		
		@Override
		public String toString() {
			return Language.get(name);
		}
	}
	
	public class EditorPlayerMenu extends MapMenu<EditorPlayerMenuOptions> {

		public EditorPlayerMenu(String title, MapState mapState) {
			super(title, mapState);
		}
		
		@Override
		protected void prepareOptions() {
			options.setItems(EditorPlayerMenuOptions.values());
		}
		
	}

}
