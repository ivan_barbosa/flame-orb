package es.flameorb.game.map;

import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.game.control.GameController;
import es.flameorb.general.KeyBinding;
import es.flameorb.units.Unit;

public class EditorMoveEnemyMapState extends MovementMapState {
	
	private Unit unit;

	public EditorMoveEnemyMapState(Field field, Unit unit) {
		super(field);
		this.unit = unit;
	}

	@Override
	public void acceptAction() {
		Point origin = unit.getMapPosition();
		unit.setMapPosition(field.cursor.getMapPosition());
		GameController.getGameController().updatePosition(unit, origin);
		refreshInfoWindows();
		cancelAction();
	}

	@Override
	public void cancelAction() {
		field.setMapState(new EditorEnemyPlaceSelectionMapState(field));
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept() && !field.isPositionUsed(field.cursor.getMapPosition()))
			acceptAction();
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		if (button == Buttons.LEFT && !field.isPositionUsed(field.cursor.getMapPosition()))
			acceptAction();
		else if (button == Buttons.RIGHT)
			cancelAction();
	}

}
