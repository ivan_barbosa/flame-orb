package es.flameorb.game.map;

import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.game.control.GameController;
import es.flameorb.general.KeyBinding;
import es.flameorb.units.Unit;

public class PlaceUnitsSwapSelectionMapState extends MovementMapState {
	
	private Point position;
	private Unit unit;

	public PlaceUnitsSwapSelectionMapState(Field field, Point position, Unit unit) {
		super(field);
		this.position = position;
		this.unit = unit;
	}

	@Override
	public void acceptAction() {
		Unit otherUnit = field.unitsPositions.getUnit(field.cursor.getMapPosition());
		unit.setMapPosition(field.cursor.getMapPosition());
		GameController.getGameController().updatePosition(unit, position);
		field.addActor(unit);
		if (otherUnit != null) {
			otherUnit.setMapPosition(position);
			field.unitsPositions.putUnit(otherUnit);
		}
		refreshInfoWindows();
		field.setMapState(new PlaceUnitsDisplayedMapState(field));
	}

	@Override
	public void cancelAction() {
		field.setMapState(new PlaceUnitsMenuDisplayedMapState(field, position));
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept() && !(position.equals(field.cursor.getMapPosition())) &&
				field.playerInitialPositions.contains(field.cursor.getMapPosition(), false))
			acceptAction();
		else if (keycode == KeyBinding.getCancel()) 
			cancelAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		if (button == Buttons.LEFT && !(position.equals(field.cursor.getMapPosition())) &&
				field.playerInitialPositions.contains(field.cursor.getMapPosition(), false))
			acceptAction();
		else
			cancelAction();
	}

}
