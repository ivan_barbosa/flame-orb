package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import es.flameorb.game.control.GameController;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;

public class EditorPlayerMoneySetMapState extends MapState {
	
	private Window window;
	private TextField moneyTextField;

	public EditorPlayerMoneySetMapState(Field field) {
		super(field);
		window = UIFactory.newWindow(Language.get("Money"), 2);
		moneyTextField = UIFactory.newTextField(5, true);
		moneyTextField.setText(GameController.getGameController().getPlayerMoney()+"");
		window.add(moneyTextField).colspan(2).padBottom(5f);
		window.row();
		TextButton accept = UIFactory.newTextButton(Language.get("Accept"));
		accept.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				acceptAction();
			}
			
		});
		TextButton cancel = UIFactory.newTextButton(Language.get("Cancel"));
		cancel.addListener(new ClickListener() {
			
			@Override
			public void clicked(InputEvent event, float x, float y) {
				cancelAction();
			}
			
		});
		window.add(accept).padRight(5f);
		window.add(cancel);
		window.pack();
		window.setPosition(getCenteredXPosition(window.getWidth()), getCenteredYPosition(window.getHeight()));
		window.setVisible(true);
		field.addActor(window);
	}

	@Override
	public void acceptAction() {
		GameController.getGameController().setPlayerMoney(Integer.parseInt(moneyTextField.getText()));
		cancelAction();
	}

	@Override
	public void cancelAction() {
		window.setVisible(false);
		field.removeActor(window);
		field.setMapState(new EditorPlayerEditionMapState(field));
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		return false;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		
	}

}
