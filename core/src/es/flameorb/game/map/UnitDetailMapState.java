package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.units.Unit;

/**
 * Muestra la ventana de detalle de la unidad.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class UnitDetailMapState extends MapState {
	
	private Unit unit;
	private MapState previousMapState;

	/**
	 * Construye este estado del mapa.
	 * @param field - El mapa.
	 * @param unit - La unidad cuyos detalle se van a mostrar.
	 * @param previousMapState - El estado del mapa desde el que se llamó a este.
	 */
	public UnitDetailMapState(Field field, Unit unit, MapState previousMapState) {
		super(field);
		this.unit = unit;
		this.previousMapState = previousMapState;
		unit.getUnitDetails().setPosition(getCenteredXPosition(unit.getUnitDetails().getWidth()), getCenteredYPosition(unit.getUnitDetails().getHeight()));
		unit.getUnitDetails().setVisible(true);
		field.addActor(unit.getUnitDetails());
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int count, int button) {
		cancelAction();
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		cancelAction();
		return true;
	}

	@Override
	public void acceptAction() {
		//No action needed.
	}

	/**
	 * Vuelve al estado desde el que se llamó éste.
	 */
	@Override
	public void cancelAction() {
		unit.getUnitDetails().setVisible(false);
		field.removeActor(unit.getUnitDetails());
		field.setMapState(previousMapState);
	}

}
