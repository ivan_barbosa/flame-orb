package es.flameorb.game.map;

import com.badlogic.gdx.utils.Array;

import es.flameorb.game.control.GameController;
import es.flameorb.general.Language;
import es.flameorb.items.Item;
import es.flameorb.units.Unit;

/**
 * Muestra el menú con las opciones de un objeto al ser elegido.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class ItemExchangeItemSelectedMapState extends MenuMapState {
	
	private Unit user, other;
	private Item selected;
	private ItemExchangeWindow window;
	private MapState previousMapState;
	private ItemExchangeMenu menu;

	public ItemExchangeItemSelectedMapState(Field field, Unit user, Unit other, Item selected,
			ItemExchangeWindow window, MapState previousMapState) {
		super(field);
		this.user = user;
		this.other = other;
		this.selected = selected;
		this.window = window;
		this.previousMapState = previousMapState;
		menu = new ItemExchangeMenu(Language.get(selected.getName()), this);
		placeMenu(menu);
	}

	@Override
	public void acceptAction() {
		switch(menu.getSelected()) {
		case EXCHANGE:
			menu.setVisible(false);
			field.removeActor(menu);
			field.setMapState(new ItemExchangeExchangeActionMapState(field, user, other, selected, window, previousMapState));
			break;
		case GIVE:
			window.giveItem(selected);
			cancelAction();
			break;
		}
	}

	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(previousMapState);
	}
	
	private enum ItemExchangeOptions {
		EXCHANGE("ExchangeVerb"), GIVE("Give");
		
		private final String name;
		
		private ItemExchangeOptions(String name) {
			this.name = name;
		}
		
		@Override
		public String toString() {
			return Language.get(name);
		}
		
	}
	
	private class ItemExchangeMenu extends MapMenu<ItemExchangeOptions> {

		public ItemExchangeMenu(String title, MapState mapState) {
			super(title, mapState);
		}

		@Override
		protected void prepareOptions() {
			Array<ItemExchangeOptions> list = new Array<ItemExchangeOptions>(ItemExchangeOptions.values());
			if (user.getInventory().getItemsNumber() == 0 ||
					((other != null && other.getInventory().getItemsNumber() == 0) ||
							GameController.getGameController().getPlayerStock().getItemsNumber() == 0))
				list.removeValue(ItemExchangeOptions.EXCHANGE, true);
			options.setItems(list);
		}
		
	}

}
