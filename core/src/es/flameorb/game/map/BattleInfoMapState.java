package es.flameorb.game.map;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import es.flameorb.game.calculators.BattleData;
import es.flameorb.game.control.ActionResult;
import es.flameorb.game.control.GameController;
import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;
import es.flameorb.units.Unit;

/**
 * Muestra la información relativa al combate entre las unidades seleccionadas.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class BattleInfoMapState extends MapState {

	private Unit attacker, defender;
	private BattleInfoWindow window;

	/**
	 * Constructor de este estado del mapa.
	 * @param field - El mapa.
	 * @param attacker - La unidad atacante.
	 * @param defender - La unidad defensora.
	 */
	public BattleInfoMapState(Field field, Unit attacker, Unit defender) {
		super(field);
		this.attacker = attacker;
		this.defender = defender;
		window = new BattleInfoWindow(Language.get("BattleData"));
		window.setPosition(getCenteredXPosition(window.getWidth()), getCenteredYPosition(window.getHeight()));
		window.setVisible(true);
		field.addActor(window);
	}

	/**
	 * Realiza el ataque de la unidad seleccionada sobre la unidad enemiga, y muestra
	 * el BattleLog con la información del combate.
	 */
	@Override
	public void acceptAction() {
		window.setVisible(false);
		field.removeActor(window);
		ActionResult attackResults = GameController.getGameController().beginAttack(attacker, defender);
		field.setMapState(new AfterPlayerActionMapState(field, attackResults));
	}

	/**
	 * Vuelve al estado de selección de la unidad enemiga.
	 */
	@Override
	public void cancelAction() {
		window.setVisible(false);
		field.removeActor(window);
		field.setMapState(new EnemySelectionMapState(field, attacker));
	}
	
	
	
	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept())
			acceptAction();
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int count, int button) {
		//No action needed.
	}
	
	/**
	 * Ventana que contiene toda la información relativa a un combate
	 * entre dos unidades.
	 * @author Iván Barbosa Gutiérrez.
	 *
	 */
	public class BattleInfoWindow extends Window {

		private TextButton attack, retreat;

		public BattleInfoWindow(String title) {
			super("", UIFactory.getSkin());
			add(UIFactory.newLabel(title)).colspan(3);
	        row();
			attack = UIFactory.newTextButton(Language.get("AttackBat"));
			attack.addListener(new ClickListener() {

				@Override
				public void clicked(InputEvent event, float x, float y) {
					acceptAction();
				}
				
			});
			retreat = UIFactory.newTextButton(Language.get("Retreat"));
			retreat.addListener(new ClickListener() {
				
				@Override
				public void clicked(InputEvent event, float x, float y) {
					cancelAction();
				}
				
			});
			prepareTable();
			row();
			add(attack).center();
			add();
			add(retreat).center();
			pack();
		}
		
		private void prepareTable() {
			boolean enemyInRange = attacker.canEnemyCounterattack(defender);
			BattleData attackerData = GameController.getCurrentBattleData(attacker, defender),
					defenderData = GameController.getCurrentBattleData(defender, attacker);
			Label attackerWeaponLabel = UIFactory.newLabel(Language.get(attacker.getEquipedWeapon().getName()));
			if (attacker.getEquipedWeapon().hasAdvantageTo(defender.getEquipedWeapon().getWeaponId()))
				attackerWeaponLabel.setColor(Color.GREEN);
			else if (defender.getEquipedWeapon().hasAdvantageTo(attacker.getEquipedWeapon().getWeaponId()))
				attackerWeaponLabel.setColor(Color.RED);
			add(attackerWeaponLabel).center();
			add(UIFactory.newLabel(Language.get("Weapon"))).center();
			Label defenderWeaponLabel = UIFactory.newLabel(Language.get(defender.getEquipedWeapon().getName()));
			if (defender.getEquipedWeapon().hasAdvantageTo(attacker.getEquipedWeapon().getWeaponId()))
				defenderWeaponLabel.setColor(Color.GREEN);
			else if (attacker.getEquipedWeapon().hasAdvantageTo(defender.getEquipedWeapon().getWeaponId()))
				defenderWeaponLabel.setColor(Color.RED);
			add(defenderWeaponLabel).center();
			row();
			add(UIFactory.newLabel(attacker.getUnitName())).center();
			add(UIFactory.newLabel(Language.get("Unit"))).center();
			add(UIFactory.newLabel(defender.getUnitName())).center();
			row();
			add(UIFactory.newLabel(Integer.toString(attacker.getCurrentHP()))).center();
			add(UIFactory.newLabel(Language.get("HP"))).center();
			add(UIFactory.newLabel(Integer.toString(defender.getCurrentHP()))).center();
			row();
			Label attackerDamageLabel = UIFactory.newLabel(displayMultiplier(attackerData.getDamage(),
					attacker.getEquipedWeapon().isDoubleAttack(), attackerData.getASDiference()));
			if (attacker.getEquipedWeapon().makesExtraDamageTo(defender))
				attackerDamageLabel.setColor(Color.GREEN);
			add(attackerDamageLabel).center();
			add(UIFactory.newLabel(Language.get("Damage"))).center();
			if (enemyInRange) {
				Label defenderDamageLabel = UIFactory.newLabel(displayMultiplier(defenderData.getDamage(),
						defender.getEquipedWeapon().isDoubleAttack(), defenderData.getASDiference()));
				if (defender.getEquipedWeapon().makesExtraDamageTo(attacker))
					defenderDamageLabel.setColor(Color.GREEN);
				add(defenderDamageLabel).center();
			}
			else
				add(UIFactory.newLabel("---")).center();
			row();
			add(UIFactory.newLabel(adjustValue(attackerData.getHit())+"%")).center();
			add(UIFactory.newLabel(Language.get("Hit"))).center();
			if (enemyInRange)
				add(UIFactory.newLabel(adjustValue(defenderData.getHit())+"%")).center();
			else
				add(UIFactory.newLabel("---")).center();
			row();
			add(UIFactory.newLabel(adjustValue(attackerData.getCritical())+"%")).center();
			add(UIFactory.newLabel(Language.get("Critical"))).center();
			if (enemyInRange)
				add(UIFactory.newLabel(adjustValue(defenderData.getCritical())+"%")).center();
			else
				add(UIFactory.newLabel("---")).center();
		}
		
		private String displayMultiplier(int damage, boolean doubleAttack, int diference) {
			if (damage < 0)
				damage = 0;
			int multiplier = 0;
			if (doubleAttack)
				multiplier+=2;
			if (diference >= 5)
				multiplier+=2;
			if (multiplier != 0)
				return damage+"x"+multiplier;
			return Integer.toString(damage);
		}
		
		private String adjustValue(int value) {
			if (value <= 0)
				return Integer.toString(0);
			if (value >= 100)
				return Integer.toString(100);
			return Integer.toString(value);
		}

	}

}
