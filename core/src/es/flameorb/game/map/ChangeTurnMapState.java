package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.game.control.BattleLog;

public class ChangeTurnMapState extends MapState {
	
	private boolean player;

	public ChangeTurnMapState(Field field, boolean player) {
		super(field);
		this.player = player;
		BattleLog.addTurnBanner(player);
		BattleLog.touchOrPressToContinueText();
		BattleLog.showWindow(this);
	}

	@Override
	public void acceptAction() {
		BattleLog.removeWindow();
		if (player)
			field.setMapState(new InitialMapState(field));
		else
			field.setMapState(new EnemyTurnMapState(field, 0));
	}

	@Override
	public void cancelAction() {
		
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		acceptAction();
		return false;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		acceptAction();
	}

}
