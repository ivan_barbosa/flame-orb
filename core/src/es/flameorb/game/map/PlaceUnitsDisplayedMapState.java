package es.flameorb.game.map;

import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.general.KeyBinding;

public class PlaceUnitsDisplayedMapState extends MovementMapState {

	public PlaceUnitsDisplayedMapState(Field field) {
		super(field);
		field.illuminateInitialPositionCells();
	}

	@Override
	public void acceptAction() {
		field.setMapState(new PlaceUnitsMenuDisplayedMapState(field, new Point(field.cursor.getMapPosition())));
	}

	@Override
	public void cancelAction() {
		field.shutdownInitialPositionFlags();
		field.setMapState(new PreparationMenuDisplayedMapState(field));
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept() &&
				field.playerInitialPositions.contains(field.cursor.getMapPosition(), false))
			acceptAction();
		else if (keycode == KeyBinding.getCancel()) 
			cancelAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		if (button == Buttons.LEFT &&
				field.playerInitialPositions.contains(field.cursor.getMapPosition(), false))
			acceptAction();
		else
			cancelAction();
	}

}
