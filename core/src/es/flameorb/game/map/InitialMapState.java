package es.flameorb.game.map;

import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.general.KeyBinding;

/**
 * Estado inicial del mapa para el jugador, desde el cual puede seleccionar cualquiera de
 * sus unidades para iniciar una acción, o observar el estado y el movimiento de las unidades
 * enemigas.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class InitialMapState extends MovementMapState {
	
	/**
	 * Construye el estado inicial del mapa.
	 * @param field - El mapa.
	 */
	public InitialMapState(Field field) {
		super(field);
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int count, int button) {
		if (button == Buttons.LEFT) {
			if (field.unitsPositions.isPositionOccuped(field.cursor.getMapPosition()))
				acceptAction();
		}
		else if (button == Buttons.RIGHT)
			cancelAction();
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept()) {
			if (field.unitsPositions.isPositionOccuped(field.cursor.getMapPosition()))
				acceptAction();
		}
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	/**
	 * Activa el movimiento para la unidad seleccionada si es del jugador, o muestra
	 * el menú de información del enemigo.
	 */
	@Override
	public void acceptAction() {
		if (field.unitsPositions.isPositionOccupedByPlayer(field.cursor.getMapPosition()) &&
				 !field.unitsPositions.getUnit(field.cursor.getMapPosition()).hasEndedAction())
			field.setMapState(new MovementOnMapState(field, new Point(field.cursor.getMapPosition()),
					field.unitsPositions.getUnit(field.cursor.getMapPosition())));
		else
			field.setMapState(new EnemyMenuMapState(field, field.unitsPositions.getUnit(field.cursor.getMapPosition())));
	}

	@Override
	public void cancelAction() {
		field.setMapState(new PauseMenuMapState(field));
	}
	
}
