package es.flameorb.game.map;

public class EnemyDefaults {
	
	private static EnemyDefaults enemyDefaults = new EnemyDefaults();
	private String name;
	private int level;
	private String weaponLevel;
	private boolean defaultWeapons;
	
	private EnemyDefaults() {
		name = "";
		level = 1;
		weaponLevel = "E";
		defaultWeapons = true;
	}
	
	public static EnemyDefaults getEnemyDefaults() {
		return enemyDefaults;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public String getWeaponLevel() {
		return weaponLevel;
	}

	public void setWeaponLevel(String weaponLevel) {
		this.weaponLevel = weaponLevel;
	}

	public boolean isDefaultWeapons() {
		return defaultWeapons;
	}

	public void setDefaultWeapons(boolean defaultWeapons) {
		this.defaultWeapons = defaultWeapons;
	}

}
