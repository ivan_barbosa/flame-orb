package es.flameorb.game.map;

import es.flameorb.general.Language;
import es.flameorb.items.Item;
import es.flameorb.units.PlayerUnit;
import es.flameorb.units.Unit;

/**
 * Muestra el menú de gestión de los objetos.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class ItemManagementMapState extends MenuMapState {

	private Unit unit;
	private ItemSelectionMenu menu;

	/**
	 * Construye este estado del mapa.
	 * @param field - El mapa.
	 * @param unit - La unidad elegida.
	 */
	public ItemManagementMapState(Field field, Unit unit) {
		super(field);
		this.unit = unit;
		menu = new ItemSelectionMenu(Language.get("ItemManagement"), this);
		placeMenu(menu);
	}

	/**
	 * Muestra el menu de gestión del arma elegida.
	 */
	@Override
	public void acceptAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new ItemSelectedMapState(field, (PlayerUnit)unit, menu.getSelected()));
	}

	/**
	 * Vuelve al menú de la unidad.
	 */
	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new UnitMenuDisplayedMapState(field, field.movementOrigin, unit));
	}
	
	class ItemSelectionMenu extends MapMenu<Item> {

		public ItemSelectionMenu(String title, MapState mapState) {
			super(title, mapState);
		}

		@Override
		protected void prepareOptions() {
			options.setItems(unit.getInventory().getItems());
		}
		
	}

}
