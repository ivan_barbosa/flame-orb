package es.flameorb.game.map;

import com.badlogic.gdx.utils.Array;

import es.flameorb.general.Language;
import es.flameorb.items.Staff;
import es.flameorb.items.Weapon;
import es.flameorb.units.Unit;

/**
 * Muestra los bastones que la unidad elegida puede usar.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class StaffSelectionMapState extends MenuMapState {
	
	private StaffSelectionMenu menu;
	private Unit unit;

	/**
	 * Construye este estado del mapa.
	 * @param field - El mapa.
	 * @param unit - La unidad elegida.
	 */
	public StaffSelectionMapState(Field field, Unit unit) {
		super(field);
		this.unit = unit;
		menu = new StaffSelectionMenu(Language.get("ChooseAWeapon"), this);
		placeMenu(menu);
	}

	/**
	 * Muestra el rango de alcance del bastón elegido.
	 */
	@Override
	public void acceptAction() {
		unit.equipWeapon(menu.getSelected());
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new ItemUseTargetSelectMapState(field, unit, unit.getEquipedWeapon()));
	}

	/**
	 * Vuelve a mostrar el menú de la unidad.
	 */
	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new UnitMenuDisplayedMapState(field, field.movementOrigin, unit));
	}
	
	class StaffSelectionMenu extends MapMenu<Staff> {

		public StaffSelectionMenu(String title, MapState mapState) {
			super(title, mapState);
		}

		@Override
		protected void prepareOptions() {
			Array<Weapon> weapons = unit.getInventory().getWeapons();
			Array<Staff> usableStaffs = new Array<Staff>();
			for (Weapon weapon : weapons) {
				if (weapon instanceof Staff && unit.canUseWeapon(weapon))
					usableStaffs.add((Staff)weapon);
			}
			options.setItems(usableStaffs);
		}
		
	}

}
