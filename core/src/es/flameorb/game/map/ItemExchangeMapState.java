package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;
import es.flameorb.items.Item;
import es.flameorb.units.Unit;

/**
 * Muestra la ventana con los objetos de ambas unidades.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class ItemExchangeMapState extends MapState {
	
	private ItemExchangeWindow window;
	private Unit user, other;

	public ItemExchangeMapState(Field field, Unit user, Unit other) {
		super(field);
		this.user = user;
		this.other = other;
		window = new ItemExchangeWindow(Language.get("Exchange"), this,
				user, other);
		window.setPosition(getCenteredXPosition(window.getWidth()), getCenteredYPosition(window.getHeight()));
		window.setVisible(true);
		field.addActor(window);
	}

	@Override
	public void acceptAction() {
		Item selected = window.getSelected();
		if (selected != null) {
			field.setMapState(new ItemExchangeItemSelectedMapState(field, user, other, selected, window, this));
		}
	}

	@Override
	public void cancelAction() {
		window.setVisible(false);
		user.equipWeapon();
		other.equipWeapon();
		field.removeActor(window);
		field.setMapState(new ItemExchangeUnitSelectionMapState(field, user));
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		window.moveSelection(keycode);
		if (keycode == KeyBinding.getAccept())
			acceptAction();
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int count, int button) {
		window.setClickedList();
		acceptAction();
	}

}
