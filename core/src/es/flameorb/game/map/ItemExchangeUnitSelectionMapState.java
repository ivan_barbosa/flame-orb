package es.flameorb.game.map;

import java.util.HashSet;
import java.util.Set;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.general.KeyBinding;
import es.flameorb.units.Unit;

/**
 * Elige la unidad con la que intercambiar objetos.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class ItemExchangeUnitSelectionMapState extends MovementMapState {

	private Unit unit;

	public ItemExchangeUnitSelectionMapState(Field field, Unit unit) {
		super(field);
		this.unit = unit;
		Set<Point> points = new HashSet<Point>();
		points.add(new Point(unit.getXMap(), unit.getYMap()+1));
		points.add(new Point(unit.getXMap()+1, unit.getYMap()));
		points.add(new Point(unit.getXMap(), unit.getYMap()-1));
		points.add(new Point(unit.getXMap()-1, unit.getYMap()));
		field.illuminateUseCells(points);
	}

	@Override
	public void acceptAction() {
		Unit other = field.unitsPositions.getUnit(field.cursor.getMapPosition());
		if (other != null && unit.distanceTo(other) == 1) {
			field.shutdownUseCells();
			field.setMapState(new ItemExchangeMapState(field, unit, other));
		}
		else
			cancelAction();
	}

	@Override
	public void cancelAction() {
		field.shutdownUseCells();
		field.setMapState(new UnitMenuDisplayedMapState(field, field.movementOrigin, unit));
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept())
			acceptAction();
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int count, int button) {
		acceptAction();
	}

}
