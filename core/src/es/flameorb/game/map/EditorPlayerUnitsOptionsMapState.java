package es.flameorb.game.map;

import es.flameorb.game.control.GameController;
import es.flameorb.general.Language;
import es.flameorb.units.PlayerUnit;
import es.flameorb.units.Unit;

public class EditorPlayerUnitsOptionsMapState extends MenuMapState {
	
	private Unit unit;
	private UnitOptionsMenu menu;

	public EditorPlayerUnitsOptionsMapState(Field field, Unit unit) {
		super(field);
		this.unit = unit;
		menu = new UnitOptionsMenu(unit.getUnitName(), this);
		placeMenu(menu);
	}

	@Override
	public void acceptAction() {
		switch(menu.getSelected()) {
		case MODIFY:
			menu.setVisible(false);
			field.removeActor(menu);
			field.setMapState(new EditorUnitClassSelectionMapState(field, null, (PlayerUnit)unit, true));
			break;
		case DELETE:
			GameController.getGameController().deletePlayerUnit(unit);
			cancelAction();
			break;
		case DETAILS:
			field.setMapState(new UnitDetailMapState(field, unit, this));
			break;
		}
	}

	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new EditorPlayerUnitsListMapState(field));
	}
	
	private enum UnitOptionsMenuOptions {
		MODIFY("Modify"), DELETE("Delete"), DETAILS("Details");
		
		private final String name;
		
		private UnitOptionsMenuOptions(final String name) {
			this.name = name;
		}
		
		@Override
		public String toString() {
			return Language.get(name);
		}
	}
	
	private class UnitOptionsMenu extends MapMenu<UnitOptionsMenuOptions> {

		public UnitOptionsMenu(String title, MapState mapState) {
			super(title, mapState);
		}

		@Override
		protected void prepareOptions() {
			options.setItems(UnitOptionsMenuOptions.values());
		}
		
	}

}
