package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Window;

import es.flameorb.game.control.GameController;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;

public class ShowEndConditionsMapState extends MapState {
	
	private boolean preparations;
	private Window window;

	public ShowEndConditionsMapState(Field field, boolean preparations) {
		super(field);
		this.preparations = preparations;
		window = UIFactory.newWindow(Language.get("EndConditions"), 2);
		window.add(UIFactory.newLabel(Language.get("VictoryCondition")+": "));
		window.add(UIFactory.newLabel(GameController.getGameController().getMapData().getVictoryConditions().first().toString()));
		window.row();
		window.add(UIFactory.newLabel(Language.get("DefeatCondition")+": "));
		window.add(UIFactory.newLabel(GameController.getGameController().getMapData().getDefeatConditions().first().toString()));
		window.pack();
		window.setPosition(getCenteredXPosition(window.getWidth()), getCenteredYPosition(window.getHeight()));
		window.setVisible(true);
		field.addActor(window);
	}

	@Override
	public void acceptAction() {
		window.setVisible(false);
		field.removeActor(window);
		if (preparations)
			field.setMapState(new PreparationMenuDisplayedMapState(field));
		else
			field.setMapState(new PauseMenuMapState(field));
	}

	@Override
	public void cancelAction() {
		
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		acceptAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		acceptAction();
	}

}
