package es.flameorb.game.map;

import es.flameorb.general.Language;
import es.flameorb.units.FEClass;
import es.flameorb.units.PlayerUnit;

public class PromotionSelectionMapState extends MenuMapState {
	
	private PlayerUnit unit;
	private boolean onGame;
	private PromotionMenu menu;

	public PromotionSelectionMapState(Field field, PlayerUnit unit, boolean onGame) {
		super(field);
		this.unit = unit;
		this.onGame = onGame;
		menu = new PromotionMenu(Language.get("ChoosePromotion"), this);
		placeMenu(menu);
	}

	@Override
	public void acceptAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new PromotionChosenMapState(field, unit, onGame, menu.getSelected()));
	}

	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		if (onGame)
			field.setMapState(new UnitMenuDisplayedMapState(field, field.movementOrigin, unit));
		else
			field.setMapState(new PreparationMenuDisplayedMapState(field));
	}
	
	private class PromotionMenu extends MapMenu<FEClass> {

		public PromotionMenu(String title, MapState mapState) {
			super(title, mapState);
		}

		@Override
		protected void prepareOptions() {
			options.setItems(unit.getFeclass().getPromotions());
		}
		
	}

}
