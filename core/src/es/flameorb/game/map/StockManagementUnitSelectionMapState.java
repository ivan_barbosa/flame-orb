package es.flameorb.game.map;

import es.flameorb.game.control.GameController;
import es.flameorb.general.Language;
import es.flameorb.units.Unit;

public class StockManagementUnitSelectionMapState extends MenuMapState {
	
	private UnitMenu menu;

	public StockManagementUnitSelectionMapState(Field field) {
		super(field);
		menu = new UnitMenu(Language.get("ChooseUnit"), this);
		placeMenu(menu);
	}

	@Override
	public void acceptAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new StockManagementMapState(field, menu.getSelected(), true));
	}

	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new PreparationMenuDisplayedMapState(field));
	}
	
	private class UnitMenu extends MapMenu<Unit> {

		public UnitMenu(String title, MapState mapState) {
			super(title, mapState);
		}

		@Override
		protected void prepareOptions() {
			options.setItems(GameController.getGameController().getAllPlayerUnits());
		}
		
	}

}
