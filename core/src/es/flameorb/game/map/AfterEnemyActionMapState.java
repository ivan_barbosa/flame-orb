package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.game.control.ActionResult;
import es.flameorb.game.control.BattleLog;

/**
 * Muestra el log si el enemigo ha realizado un ataque.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class AfterEnemyActionMapState extends MapState {
	
	private ActionResult actionResult;
	private int nextUnit;
	private boolean battle;

	public AfterEnemyActionMapState(Field field, ActionResult actionResult, int nextUnit, boolean battle) {
 		super(field);
		this.actionResult = actionResult;
		this.nextUnit = nextUnit;
		this.battle = battle;
		if (battle) {
			BattleLog.touchOrPressToContinueText();
			BattleLog.showWindow(this);
			field.setMapState(this);
		}
		else
			acceptAction();
	}

	@Override
	public void acceptAction() {
		if (battle) {
			refreshInfoWindows();
			BattleLog.removeWindow();
		}
		if (actionResult.hasGameEnd()) {
			if (actionResult.isVictory())
				field.setMapState(new GameEndMapState(field, true));
			else
				field.setMapState(new GameEndMapState(field, false));
		}
		else if (actionResult.hasTurnEnd()) {
			field.setMapState(new ChangeTurnMapState(field, true));
		}
		else
			field.setMapState(new EnemyTurnMapState(field, nextUnit));		
	}

	@Override
	public void cancelAction() {
		// No actions required
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (battle)
			acceptAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int count, int button) {
		if (battle)
			acceptAction();
	}

}
