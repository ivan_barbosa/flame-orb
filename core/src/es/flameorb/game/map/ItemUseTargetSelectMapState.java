package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.general.KeyBinding;
import es.flameorb.items.Item;
import es.flameorb.items.Staff;
import es.flameorb.units.PlayerUnit;
import es.flameorb.units.Unit;

/**
 * Muestra el alcance del objeto elegido.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class ItemUseTargetSelectMapState extends MovementMapState {
	
	private Unit unit;
	private Item item;

	/**
	 * Construye este estado del mapa.
	 * @param field - El mapa.
	 * @param unit - La unidad elegida.
	 * @param item - El objeto que va a ser usado.
	 */
	public ItemUseTargetSelectMapState(Field field, Unit unit, Item item) {
		super(field);
		this.unit = unit;
		this.item = item;
		field.illuminateUseCells(item.getUseRange());
	}
	
	@Override
	public void touchAction(InputEvent event, float x, float y, int count, int button) {
		if (field.isItemUsable())
			acceptAction();
		else
			cancelAction();
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept() && field.isItemUsable())
			acceptAction();
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	/**
	 * Muestra la ventana con el efecto de usar el objeto sobre la unidad elegida.
	 */
	@Override
	public void acceptAction() {
		field.shutdownUseCells();
		field.setMapState(new ItemUseEffectInfoMapState(field, unit, field.unitsPositions.getUnit(field.cursor.getMapPosition()), item));
	}

	/**
	 * Vuelve a la selección de objeto.
	 */
	@Override
	public void cancelAction() {
		field.shutdownUseCells();
		if (item instanceof Staff)
			field.setMapState(new StaffSelectionMapState(field, unit));
		else
			field.setMapState(new ItemSelectedMapState(field, (PlayerUnit)unit, item));
	}

}
