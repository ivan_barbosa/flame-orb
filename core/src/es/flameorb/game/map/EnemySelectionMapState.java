package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.general.KeyBinding;
import es.flameorb.units.Unit;

/**
 * Estado del mapa en el que el jugador puede elegir a la unidad enemiga que va
 * a atacar.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class EnemySelectionMapState extends MovementMapState {
	
	private Unit unit;

	/**
	 * Construye este estado del mapa.
	 * @param field - El mapa.
	 * @param unit - La unidad del jugador.
	 */
	public EnemySelectionMapState(Field field, Unit unit) {
		super(field);
		this.unit = unit;
		field.illuminateAtkCells(unit.getEquipedWeapon().getAttackRange());
	}
	
	@Override
	public void touchAction(InputEvent event, float x, float y, int count, int button) {
		if (field.isAttackPossible())
			acceptAction();
		else
			cancelAction();
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept()) {
			if (field.isAttackPossible())
				acceptAction();
		}
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	/**
	 * Pasa a mostrar la información sobre el combate entre la unidad del jugador
	 * y la unidad elegida para atacarla.
	 */
	@Override
	public void acceptAction() {
		field.shutdownAtkCells();
		field.setMapState(new BattleInfoMapState(field, unit,
				field.unitsPositions.getUnit(field.cursor.getMapPosition())));
	}

	/**
	 * Vuelve al estado de selección del arma.
	 */
	@Override
	public void cancelAction() {
		field.shutdownAtkCells();
		field.setMapState(new WeaponSelectionMapState(field, unit));
	}

}
