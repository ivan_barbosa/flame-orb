package es.flameorb.game.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import es.flameorb.general.Language;
import es.flameorb.gui.Menu;
import es.flameorb.gui.UIFactory;

/**
 * Clase base para todos los menús dentro del campo de batalla.
 * @author Iván Barbosa Gutiérrez.
 *
 * @param <T> - El tipo de los objetos que representan las distintas
 * opciones del menú.
 */
public abstract class MapMenu<T> extends Menu<T> {

    protected TextButton backButton;

    /**
     * Crea un nuevo menú, inicializa la lista de opciones y carga todas las opciones.
     *
     * @param title    - El título de la ventana.
     * @param mapState - El estado del mapa.
     */
    public MapMenu(String title, final MapState mapState) {
        super(title);
        if (Gdx.app.getType() == ApplicationType.Android) {
            backButton = UIFactory.newTextButton(Language.get("Back"));
            backButton.addListener(new ClickListener() {

                @Override
                public void clicked(InputEvent event, float x, float y) {
                    mapState.cancelAction();
                }

            });
            row();
            add(backButton).center();
        }
        pack();
        setPosition(mapState.getCenteredXPosition(getWidth()), mapState.getCenteredYPosition(getHeight()));
    }
}
