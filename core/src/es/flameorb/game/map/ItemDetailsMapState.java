package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import es.flameorb.gui.UIFactory;
import es.flameorb.items.Item;
import es.flameorb.units.Unit;

/**
 * Muestra los detalles de un objeto en pantalla.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class ItemDetailsMapState extends MapState {
	
	private Item item;
	private ItemSelectedMapState previousState;
	private ItemDetailsWindow window;

	public ItemDetailsMapState(Field field, Unit unit, Item item, ItemSelectedMapState previousState) {
		super(field);
		this.item = item;
		this.previousState = previousState;
		window = new ItemDetailsWindow();
		window.setPosition(getCenteredXPosition(window.getWidth()), getCenteredYPosition(window.getHeight()));
		window.setVisible(true);
		field.addActor(window);
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int count, int button) {
		cancelAction();
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		cancelAction();
		return true;
	}

	@Override
	public void acceptAction() {
		// Not needed
	}

	@Override
	public void cancelAction() {
		window.setVisible(false);
		previousState.enableButton();
		field.removeActor(window);
		field.setMapState(previousState);
	}
	
	public class ItemDetailsWindow extends Window {

		public ItemDetailsWindow() {
			super("", UIFactory.getSkin());
			add(UIFactory.newLabel(item.displayFullData()));
			pack();
		}
	}

}
