package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.ui.Window;

import es.flameorb.game.control.GameController;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;

public class EditorMenuMapState extends MenuMapState {
	
	private EditorMenu menu;
	private Window savedWindow;

	public EditorMenuMapState(Field field) {
		super(field);
		this.menu = new EditorMenu(Language.get("EditorMenu"), this);
		placeMenu(menu);
	}
	
	private void showSaveWindow() {
		savedWindow = UIFactory.newWindow(Language.get("Saved"), 1);
		savedWindow.setVisible(false);
		savedWindow.pack();
        savedWindow.setPosition(getCenteredXPosition(savedWindow.getWidth()), getCenteredYPosition(savedWindow.getHeight()));
		field.addActor(savedWindow);
		savedWindow.setVisible(true);
	}

	@Override
	public void acceptAction() {
		if (savedWindow != null && savedWindow.isVisible())
			savedWindow.setVisible(false);
		else {
			switch (menu.getSelected()) {
			case SEE_MAP:
				menu.setVisible(false);
				field.removeActor(menu);
				field.removeActor(savedWindow);
				field.setMapState(new SeeMapMapState(field, true));
				break;
			case NAME:
				menu.setVisible(false);
				field.removeActor(menu);
				field.removeActor(savedWindow);
				field.setMapState(new EditorMapNameMapState(field));
				break;
			case MUSIC:
				menu.setVisible(false);
				field.removeActor(menu);
				field.removeActor(savedWindow);
				field.setMapState(new EditorMusicSelectionMapState(field));
				break;
			case ENEMIES:
				menu.setVisible(false);
				field.removeActor(menu);
				field.removeActor(savedWindow);
				field.setMapState(new EditorEnemyPlaceSelectionMapState(field));
				break;
			case PLAYER:
				menu.setVisible(false);
				field.removeActor(menu);
				field.removeActor(savedWindow);
				field.setMapState(new EditorPlayerEditionMapState(field));
				break;
			case END_CONDITIONS:
				menu.setVisible(false);
				field.removeActor(menu);
				field.removeActor(savedWindow);
				field.setMapState(new EditorEndConditionsSettingMapState(field));
				break;
			case SHOP:
				menu.setVisible(false);
				field.removeActor(menu);
				field.removeActor(savedWindow);
				field.setMapState(new EditorShopStockItemSelectionMapState(field, GameController.getGameController().getMapData().getShop(), true));
				break;
			case SAVE:
				GameController.getGameController().saveMapData();
				showSaveWindow();
				break;
			}
		}
	}

	@Override
	public void cancelAction() {
		if (savedWindow != null && savedWindow.isVisible()) {
			savedWindow.setVisible(false);
			field.removeActor(savedWindow);
		}
		else {
			menu.setVisible(false);
			field.removeActor(menu);
			field.removeActor(savedWindow);
			field.endGame();
		}
	}
	
	public enum EditorMenuOptions {
		SEE_MAP("SeeMap"), NAME("Name"), MUSIC("Music"), ENEMIES("Enemies"),
		PLAYER("Player"), END_CONDITIONS("EndConditions"), SHOP("Shop"), SAVE("Save");
		
		private final String name;
		
		private EditorMenuOptions(String name) {
			this.name = name;
		}
		
		@Override
		public String toString() {
			return Language.get(name);
		}
	}
	
	public class EditorMenu extends MapMenu<EditorMenuOptions> {

		public EditorMenu(String title, MapState mapState) {
			super(title, mapState);
		}

		@Override
		protected void prepareOptions() {
			options.setItems(EditorMenuOptions.values());
		}
		
	}

}
