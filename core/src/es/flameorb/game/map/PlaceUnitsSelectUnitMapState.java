package es.flameorb.game.map;

import com.badlogic.gdx.utils.Array;

import es.flameorb.game.control.GameController;
import es.flameorb.general.Language;
import es.flameorb.units.Unit;

public class PlaceUnitsSelectUnitMapState extends MenuMapState {
	
	private Point position;
	private UnitsMenu menu;

	public PlaceUnitsSelectUnitMapState(Field field, Point position) {
		super(field);
		this.position = position;
		menu = new UnitsMenu(Language.get("Units"), this);
		placeMenu(menu);
	}

	@Override
	public void acceptAction() {
		Unit unit = menu.getSelected().getUnit();
		if (field.unitsPositions.getUnit(position) != null) {
			Unit otherUnit = field.unitsPositions.getUnit(position);
			GameController.getGameController().removePlayerUnit(otherUnit);
			field.removeActor(otherUnit);
		}
		unit.setMapPosition(position);
		GameController.getGameController().placePlayerUnit(unit);
		field.addActorAt(0, unit);
		refreshInfoWindows();
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new PlaceUnitsDisplayedMapState(field));
	}

	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new PlaceUnitsMenuDisplayedMapState(field, position));
	}
	
	private class UnitShow {
		
		private Unit unit;
		
		public UnitShow(Unit unit) {
			this.unit = unit;
		}
		
		public Unit getUnit() {
			return unit;
		}
		
		public String toString() {
			if (unit.isCommander())
				return unit.getUnitName() + "(" + Language.get("CommanderFirstLetter") +") - " + Language.get(unit.getFeclass().getName());
			else
				return unit.getUnitName() + " - " + Language.get(unit.getFeclass().getName());
		}
	}
	
	private class UnitsMenu extends MapMenu<UnitShow> {

		public UnitsMenu(String title, MapState mapState) {
			super(title, mapState);
		}

		@Override
		protected void prepareOptions() {
			Array<UnitShow> items = new Array<UnitShow>();
			for (Unit unit : GameController.getGameController().getNotPlacedUnits())
				items.add(new UnitShow(unit));
			options.setItems(items);
		}
		
	}

}
