package es.flameorb.game.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.utils.Array;

import es.flameorb.general.Language;
import es.flameorb.units.Unit;
import es.flameorb.units.EnemyUnit;

public class EditorEnemyCreatorMenuMapState extends MenuMapState {
	
	private Unit unit;
	private EnemyCreatorMenu menu;

	public EditorEnemyCreatorMenuMapState(Field field, Unit unit) {
		super(field);
		this.unit = unit;
		this.menu = new EnemyCreatorMenu(Language.get("EnemyEditor"), this);
		placeMenu(menu);
	}

	@Override
	public void acceptAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		switch(menu.getSelected()) {
		case NEW_ENEMY:
			field.setMapState(new EditorUnitClassSelectionMapState(field, null, null, false));
			break;
		case MODIFY_ENEMY:
			field.setMapState(new EditorUnitClassSelectionMapState(field, (EnemyUnit)unit, null, false));
			break;
		case DELETE_ENEMY:
			field.setMapState(new EditorEnemyDeleteMapState(field, unit));
			break;
		case MOVE_ENEMY:
			field.setMapState(new EditorMoveEnemyMapState(field, unit));
			break;
		case SET_DEFAULTS:
			field.setMapState(new EditorEnemyDefaultsMapState(field, unit));
			break;
		case GO_TO_MENU:
			field.setMapState(new EditorMenuMapState(field));
			break;
		}
	}

	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new EditorEnemyPlaceSelectionMapState(field));
	}
	
	private enum EnemyCreatorMenuOptions {
		NEW_ENEMY("NewEnemy"), MODIFY_ENEMY("ModifyEnemy"), DELETE_ENEMY("DeleteEnemy"),
		MOVE_ENEMY("MoveEnemy"), SET_DEFAULTS("SetDefaults"), GO_TO_MENU("GoToMenu");
		
		private final String name;
		
		private EnemyCreatorMenuOptions(String name) {
			this.name = name;
		}
		
		@Override
		public String toString() {
			return Language.get(name);
		}
	}
	
	private class EnemyCreatorMenu extends MapMenu<EnemyCreatorMenuOptions> {

		public EnemyCreatorMenu(String title, MapState mapState) {
			super(title, mapState);
		}

		@Override
		protected void prepareOptions() {
			Array<EnemyCreatorMenuOptions> list = new Array<EnemyCreatorMenuOptions>(EnemyCreatorMenuOptions.values());
			if (Gdx.app.getType() != ApplicationType.Android)
				list.removeValue(EnemyCreatorMenuOptions.GO_TO_MENU, true);
			if (unit == null) {
				list.removeValue(EnemyCreatorMenuOptions.MODIFY_ENEMY, true);
				list.removeValue(EnemyCreatorMenuOptions.DELETE_ENEMY, true);
				list.removeValue(EnemyCreatorMenuOptions.MOVE_ENEMY, true);
			}
			options.setItems(list);
		}
		
	}

}
