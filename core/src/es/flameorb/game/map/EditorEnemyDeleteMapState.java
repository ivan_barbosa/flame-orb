package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import es.flameorb.game.control.GameController;
import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;
import es.flameorb.units.Unit;

public class EditorEnemyDeleteMapState extends MapState {
	
	private Unit unit;
	private Window window;

	public EditorEnemyDeleteMapState(Field field, Unit unit) {
		super(field);
		this.unit = unit;
		window = UIFactory.newWindow(Language.get("AreYouSure"), 2);
		TextButton acceptButton = UIFactory.newTextButton(Language.get("Accept"));
		acceptButton.addListener(new ClickListener() {
			
			@Override
			public void clicked(InputEvent event, float x, float y) {
				acceptAction();
			}
			
		});
		window.add(acceptButton);
		TextButton cancelButton = UIFactory.newTextButton(Language.get("Cancel"));
		cancelButton.addListener(new ClickListener() {
			
			@Override
			public void clicked(InputEvent event, float x, float y) {
				cancelAction();
			}
			
		});
		window.add(cancelButton);
		window.pack();
		window.setPosition(getCenteredXPosition(window.getWidth()), getCenteredYPosition(window.getHeight()));
		window.setVisible(true);
		field.addActor(window);
	}

	@Override
	public void acceptAction() {
		field.removeActor(unit);
		GameController.getGameController().removeEnemyUnit(unit);
		refreshInfoWindows();
		window.setVisible(true);
		field.removeActor(window);
		field.setMapState(new EditorEnemyCreatorMenuMapState(field, null));
	}

	@Override
	public void cancelAction() {
		window.setVisible(true);
		field.removeActor(window);
		field.setMapState(new EditorEnemyCreatorMenuMapState(field, unit));
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept())
			acceptAction();
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub

	}

}
