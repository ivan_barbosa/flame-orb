package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.general.KeyBinding;
import es.flameorb.units.Unit;

/**
 * Muestra las celdas a las que el enemigo puede moverse.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class ShowEnemyMovementMapState extends MovementMapState {
	
	private Unit enemy;

	/**
	 * Construye este estado del mapa.
	 * @param field - El mapa
	 * @param enemy - La unidad enemiga.
	 */
	public ShowEnemyMovementMapState(Field field, Unit enemy) {
		super(field);
		this.enemy = enemy;
		field.illuminateMovCells(enemy.getMapPosition());
	}
	
	@Override
	public void touchAction(InputEvent event, float x, float y, int count, int button) {
		if (field.cursor.getMapPosition().equals(enemy.getMapPosition()))
			cancelAction();
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept())
			acceptAction();
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}



	@Override
	public void acceptAction() {
		//No action needed.
	}

	/**
	 * Vuelve al menú del enemigo.
	 */
	@Override
	public void cancelAction() {
		field.shutdownMovCells();
		field.setMapState(new EnemyMenuMapState(field, enemy));
	}

}
