package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.game.control.BattleLog;
import es.flameorb.units.PlayerUnit;

public class ShowPromotionResultsMapState extends MapState {
	
	private boolean onGame;
	private PlayerUnit unit;

	public ShowPromotionResultsMapState(Field field, PlayerUnit unit, boolean onGame) {
		super(field);
		this.unit = unit;
		this.onGame = onGame;
		BattleLog.showWindow(this);
	}

	@Override
	public void acceptAction() {
		BattleLog.removeWindow();
		refreshInfoWindows();
		if (onGame)
			field.setMapState(new UnitMenuDisplayedMapState(field, field.movementOrigin, unit));
		else
			field.setMapState(new PreparationMenuDisplayedMapState(field));
	}

	@Override
	public void cancelAction() {
		
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		acceptAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		acceptAction();
	}

}
