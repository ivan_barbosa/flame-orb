package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Window;

import es.flameorb.game.control.GameController;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;

public class ShopAfterItemBuyMapState extends MapState {
	
	private Window window;
	private MapState previousMapState;

	public ShopAfterItemBuyMapState(Field field, MapState previousMapState, boolean bought, Label moneyLabel) {
		super(field);
		this.previousMapState = previousMapState;
		window = UIFactory.newWindow(Language.get("BuyInfo"), 1);
		if (bought) {
			window.add(UIFactory.newLabel(Language.get("ItemBought")));
			moneyLabel.setText(GameController.getGameController().getPlayerMoney()+"");
		}
		else
			window.add(UIFactory.newLabel(Language.get("NotEnoughtMoney")));
		window.pack();
		window.setPosition(getCenteredXPosition(window.getWidth()), getCenteredYPosition(window.getHeight()));
		window.setVisible(true);
		field.addActor(window);
	}

	@Override
	public void acceptAction() {
		window.setVisible(false);
		field.removeActor(window);
		field.setMapState(previousMapState);
	}

	@Override
	public void cancelAction() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		acceptAction();
		return false;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		acceptAction();
	}

}
