package es.flameorb.game.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter.OutputType;

/**
 * Encapsula todos los datos relativos al terreno de una celda.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class TerrainData {
	
	private static IntMap<Terrain> terrains;
	
	/**
	 * Construye las propiedades de la celda a partir de los datos sacados
	 * de las propiedades del mapa.
	 * @param properties - Las propiedades del mapa con los datos del terreno.
	 */
	public static void loadTerrains() {
		terrains = new IntMap<Terrain>();
		Json jsonParser = new Json();
		JsonValue json = new JsonReader().parse(Gdx.files.internal("data/terrains.json")).get("terrains");
		for (JsonValue terrain : json)
			terrains.put(terrain.getInt("id"), jsonParser.fromJson(Terrain.class, terrain.toJson(OutputType.json)));
	}
	
	public static Terrain getTerrain(TiledMapTileLayer mapLayer, int x, int y) {
		MapProperties properties = mapLayer.getCell(x, y).getTile().getProperties();
		return terrains.get(properties.get("id", Integer.class));
	}
	
}
