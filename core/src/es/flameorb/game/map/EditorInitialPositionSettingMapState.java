package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.game.control.GameController;
import es.flameorb.general.KeyBinding;

public class EditorInitialPositionSettingMapState extends MovementMapState {
	
	private MapData mapData;

	public EditorInitialPositionSettingMapState(Field field) {
		super(field);
		mapData = GameController.getGameController().getMapData();
		field.setInitialPositions(mapData.getInitialPositions());
		field.illuminateInitialPositionCells();
	}

	@Override
	public void acceptAction() {
		if (!field.isPositionAnInitialPosition(field.cursor.getMapPosition()) &&
				!field.unitsPositions.isPositionOccuped(field.cursor.getMapPosition()))
			mapData.addInitialPosition(new Point(field.cursor.getMapPosition()));
		else
			mapData.removeInitialPosition(field.cursor.getMapPosition());
	}

	@Override
	public void cancelAction() {
		field.shutdownInitialPositionFlags();
		field.setMapState(new EditorPlayerEditionMapState(field));
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept())
			acceptAction();
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		acceptAction();
	}

}
