package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.game.control.ActionResult;
import es.flameorb.game.control.BattleLog;

/**
 * Muestra el log tras un ataque o el uso de un bastón del jugador.
 * @author Iván Barbosa Gutiérrez.
 *
 */
public class AfterPlayerActionMapState extends MapState {
	
	private ActionResult actionResult;

	public AfterPlayerActionMapState(Field field, ActionResult actionResult) {
		super(field);
		this.actionResult = actionResult;
		BattleLog.touchOrPressToContinueText();
		BattleLog.showWindow(this);
	}

	/**
	 * Vuelve al estado inicial del mapa.
	 */
	@Override
	public void acceptAction() {
		refreshInfoWindows();
		BattleLog.removeWindow();
		if (actionResult.hasGameEnd()) {
			if (actionResult.isVictory())
				field.setMapState(new GameEndMapState(field, true));
			else
				field.setMapState(new GameEndMapState(field, false));
		}
		else if (actionResult.hasTurnEnd())
			field.setMapState(new ChangeTurnMapState(field, false));
		else
			field.setMapState(new InitialMapState(field));
	}

	@Override
	public void cancelAction() {
		//No action required
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		acceptAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int count, int button) {
		acceptAction();
	}

}
