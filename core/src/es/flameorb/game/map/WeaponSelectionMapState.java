package es.flameorb.game.map;

import com.badlogic.gdx.utils.Array;

import es.flameorb.general.Language;
import es.flameorb.items.Weapon;
import es.flameorb.units.Unit;

/**
 * Muestra la elección del arma para atacar.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class WeaponSelectionMapState extends MenuMapState {
	
	private WeaponSelectionMenu menu;
	private Unit unit;

	/**
	 * Construye este estado del mapa.
	 * @param field - El mapa.
	 * @param unit - La unidad elegida.
	 */
	public WeaponSelectionMapState(Field field, Unit unit) {
		super(field);
		this.unit = unit;
		menu = new WeaponSelectionMenu(Language.get("ChooseAWeapon"), this);
		placeMenu(menu);
	}

	/**
	 * Muestra el alcance del arma elegida.
	 */
	@Override
	public void acceptAction() {
		unit.equipWeapon(menu.getSelected());
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new EnemySelectionMapState(field, unit));
	}

	/**
	 * Vuelve al menú de la unidad.
	 */
	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new UnitMenuDisplayedMapState(field, field.movementOrigin, unit));
	}
	
	class WeaponSelectionMenu extends MapMenu<Weapon> {

		public WeaponSelectionMenu(String title, MapState mapState) {
			super(title, mapState);
		}

		@Override
		protected void prepareOptions() {
			Array<Weapon> weapons = unit.getInventory().getWeapons();
			Array<Weapon> usableWeapons = new Array<Weapon>();
			for (Weapon weapon : weapons) {
				if (unit.canUseWeapon(weapon))
					usableWeapons.add(weapon);
			}
			options.setItems(usableWeapons);
		}
		
	}

}
