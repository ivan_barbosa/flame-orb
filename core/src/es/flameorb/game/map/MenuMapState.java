package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.general.KeyBinding;

public abstract class MenuMapState extends MapState {
	
	private MapMenu<?> menu;

	public MenuMapState(Field field) {
		super(field);
	}
	
	protected void placeMenu(MapMenu<?> menu) {
		this.menu = menu;
		menu.setVisible(true);
		field.addActor(menu);
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		menu.moveSelection(keycode);
		if (keycode == KeyBinding.getAccept() && menu.getSelected() != null)
			acceptAction();
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		if (menu.getSelected() != null)
			acceptAction();
	}

}
