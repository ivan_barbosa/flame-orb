package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.utils.Array;

import es.flameorb.game.control.GameController;
import es.flameorb.general.Language;
import es.flameorb.items.Item;

public class ShopItemSelectionMapState extends MenuMapState {
	
	private ShopItemsMenu menu;
	private Window moneyWindow;
	private Label moneyLabel;

	public ShopItemSelectionMapState(Field field, Window moneyWindow, Label moneyLabel) {
		super(field);
		this.moneyWindow = moneyWindow;
		this.moneyLabel = moneyLabel;
		menu = new ShopItemsMenu(Language.get("Shop"), this);
		placeMenu(menu);
		moneyWindow.setPosition(getCenteredXPosition(moneyWindow.getWidth()), menu.getY()+menu.getHeight()+16);
	}

	@Override
	public void acceptAction() {
		field.setMapState(new ShopAfterItemBuyMapState(field, this,
			GameController.getGameController().buyItem(menu.getSelected().getItem()), moneyLabel));
	}

	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new ShopMenuDisplayedMapState(field, moneyWindow, moneyLabel));
	}
	
	private class ShopItem {
		private Item item;
		private String listDisplay;
		
		public ShopItem(Item item) {
			this.item = item;
			listDisplay = Language.get(item.getName()) + "  " + item.getPrice();
		}
		
		public Item getItem() {
			return item;
		}
		
		public String toString() {
			return listDisplay;
		}
	}
	
	private class ShopItemsMenu extends MapMenu<ShopItem> {

		public ShopItemsMenu(String title, MapState mapState) {
			super(title, mapState);
		}

		@Override
		protected void prepareOptions() {
			Array<ShopItem> items = new Array<ShopItem>();
			for (Item item : GameController.getGameController().getMapData().getShop())
				items.add(new ShopItem(item));
			options.setItems(items);
		}
		
	}

}
