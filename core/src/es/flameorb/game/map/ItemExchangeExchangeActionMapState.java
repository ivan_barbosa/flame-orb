package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.general.KeyBinding;
import es.flameorb.items.Item;
import es.flameorb.units.Unit;

/**
 * Permite elegir el objeto a intercambiar por el seleccionado.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class ItemExchangeExchangeActionMapState extends MapState {
	
	private Unit user, other;
	private Item selected;
	private ItemExchangeWindow window;
	private MapState previousMapState;

	public ItemExchangeExchangeActionMapState(Field field, Unit user, Unit other, Item selected,
			ItemExchangeWindow window, MapState previousMapState) {
		super(field);
		this.user = user;
		this.other = other;
		this.selected = selected;
		this.window = window;
		this.previousMapState = previousMapState;
		window.onExchangeAction(this);
	}

	@Override
	public void acceptAction() {
		Item otherSelected = window.getSelected();
		if (otherSelected != null) {
			window.exchangeItem(selected, otherSelected);
			window.exchangeActionEnd(this);
			field.setMapState(previousMapState);
		}
	}

	@Override
	public void cancelAction() {
		field.setMapState(new ItemExchangeItemSelectedMapState(field, user, other, selected, window, previousMapState));
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		window.moveSelection(keycode);
		if (keycode == KeyBinding.getAccept())
			acceptAction();
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int count, int button) {
		acceptAction();
	}

}
