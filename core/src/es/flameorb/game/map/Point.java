package es.flameorb.game.map;

/**
 * Clase que representa un punto en las coordenadas del mapa.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class Point {
	
	public int x;
	public int y;
	
	/**
	 * Construye un nuevo Punto con las coordenadas dadas.
	 * @param x - La coordenada X.
	 * @param y - La coordenada Y.
	 */
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	/**
	 * Construye un nuevo Punto a partir de otro.
	 * @param point - El otro punto.
	 */
	public Point(Point point) {
		x = point.x;
		y = point.y;
	}
	
	public int distanceTo(Point other) {
		return Math.abs(x-other.x) + Math.abs(y-other.y);
	}
	
	/**
	 * Devuelve true si el punto se encuentra junto al recibido.
	 * @param other - El otro punto
	 * @return true si los dos puntos están pegados, false si no lo están.
	 */
	public boolean isNext(Point other) {
		return (Math.abs(x-other.x) == 1 && y == other.y) ||
				(Math.abs(y-other.y) == 1 && x == other.x);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
	
	

}
