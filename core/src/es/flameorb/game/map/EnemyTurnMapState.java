package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.game.calculators.Pair;
import es.flameorb.game.control.ActionResult;
import es.flameorb.game.control.GameController;
import es.flameorb.units.EnemyUnit;
import es.flameorb.units.Unit;

/**
 * Estado durante el cual el enemigo realiza sus movimientos.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class EnemyTurnMapState extends MapState {
	
	private EnemyUnit unit;
	private Unit target;
	private Pair<Point, Boolean> destiny;
	private int listPosition;

	public EnemyTurnMapState(Field field, int listPosition) {
		super(field);
		this.listPosition = listPosition;
		unit = (EnemyUnit) GameController.getGameController().getEnemyUnits().get(listPosition);
		target = unit.selectEnemy();
		switch (unit.getMoveType()) {
		case 0:
			destiny = unit.moveToEnemy(target);
			break;
		case 1:
			destiny = new Pair<Point, Boolean>(unit.getMapPosition(), unit.isEnemyInRange(target));
			break;
		case 2:
			destiny = new Pair<Point, Boolean>(unit.getMapPosition(), unit.isEnemyInRange(target));
			break;
		}	
		new MovementDisplayer(field, unit, unit.getMapPosition(), destiny.first, this);
	}
	
	protected void afterMovement() {
		unit.chooseWeapon(target);
		ActionResult actionResult;
		if (destiny.second) {
			actionResult = GameController.getGameController().beginAttack(unit, target);
			if (unit.getCurrentHP() == 0)
				listPosition--;
		}
		else
			actionResult = new ActionResult(false, false, GameController.getGameController().unitEndAction(unit));
		if (!actionResult.hasTurnEnd())
			field.setMapState(new AfterEnemyActionMapState(field, actionResult, listPosition+1, destiny.second));
		else
			new AfterEnemyActionMapState(field, actionResult, listPosition+1, destiny.second);
	}

	@Override
	public void acceptAction() {
		// No action required

	}

	@Override
	public void cancelAction() {
		// No action required

	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		return false;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int count, int button) {
		//No action
	}

}
