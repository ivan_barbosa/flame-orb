package es.flameorb.game.map;

import com.badlogic.gdx.utils.Array;

import es.flameorb.general.GamePreferences;
import es.flameorb.general.Language;
import es.flameorb.items.Item;
import es.flameorb.items.Staff;
import es.flameorb.items.Weapon;
import es.flameorb.units.PlayerUnit;

/**
 * Muestra el menú de gestión del objeto elegida.
 * @author Iván Barbosa Gutiérrez.
 *
 */
public class ItemSelectedMapState extends MenuMapState {

	private PlayerUnit unit;
	private Item item;
	private ItemOptionsMenu menu;

	/**
	 * Construye este estado del mapa.
	 * @param field - El mapa
	 * @param unit - La unidad elegida.
	 * @param weapon - El arma elegida.
	 */
	public ItemSelectedMapState(Field field, PlayerUnit unit, Item item) {
		super(field);
		this.unit = unit;
		this.item = item;
		menu = new ItemOptionsMenu(Language.get(item.getName()), this);
		placeMenu(menu);
	}

	/**
	 * Actúa en función de la acción elegida.
	 */
	@Override
	public void acceptAction() {
		switch(menu.getSelected()) {
		case EQUIP:
			unit.equipWeapon((Weapon)item);
			cancelAction();
			break;
		case USE:
			menu.setVisible(false);
			field.removeActor(menu);
			field.setMapState(new ItemUseTargetSelectMapState(field, unit, item));
			break;
		case TOSS:
			unit.tossItem(item);
			cancelAction();
			break;
		case DETAILS:
			if (menu.backButton != null)
				menu.backButton.setDisabled(true);
			field.setMapState(new ItemDetailsMapState(field, unit, item, this));
			break;
		}
	}

	/**
	 * Vuelve al menú de gestión de objetos.
	 */
	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new ItemManagementMapState(field, unit));
	}
	
	public void enableButton() {
		if (menu.backButton != null)
			menu.backButton.setDisabled(false);
	}
	
	/**
	 * Las opciones del menú del objeto.
	 * @author Iván Barbosa Gutiérrez.
	 *
	 */
	public enum ItemOptions {
		
		EQUIP("Equip"), USE("Use"), TOSS("Toss"), DETAILS("Details");
		
		private final String name;
		
		private ItemOptions(String name) {
			this.name = name;
		}
		
		@Override
		public String toString() {
			return Language.get(name);
		}
	}
	
	/**
	 * El menú de selección de la acción del objeto elegido.
	 * @author Iván Barbosa Gutiérrez
	 *
	 */
	public class ItemOptionsMenu extends MapMenu<ItemOptions> {

		public ItemOptionsMenu(String name, MapState mapState) {
			super(name, mapState);
		}

		@Override
		protected void prepareOptions() {
			Array<ItemOptions> list = new Array<ItemOptions>(ItemOptions.values());
			if (!(item instanceof Weapon) || item == unit.getEquipedWeapon()
					|| GamePreferences.getWeaponLevelsValues().get(((Weapon)item).getLevel()) > unit.getWeaponSkill(((Weapon)item).getWeaponId()))
				list.removeValue(ItemOptions.EQUIP, true);
			if (item instanceof Staff || item.getUseEffect() == null)
				list.removeValue(ItemOptions.USE, true);
			options.setItems(list);
		}

	}

}
