package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import es.flameorb.effects.HealEffect;
import es.flameorb.game.control.ActionResult;
import es.flameorb.game.control.GameController;
import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;
import es.flameorb.items.Item;
import es.flameorb.items.Staff;
import es.flameorb.units.Stats;
import es.flameorb.units.Unit;

/**
 * Muestra la información relativa al usar el efecto del objeto sobre la unidad elegida.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class ItemUseEffectInfoMapState extends MapState {

	private Unit user, receiver;
	private Item item;
	private EffectInfoWindow window;

	/**
	 * Construye este estado del mapa.
	 * @param field - El mapa.
	 * @param user - El usuario del bastón.
	 * @param receiver - El receptor del efecto.
	 */
	public ItemUseEffectInfoMapState(Field field, Unit user, Unit receiver, Item item) {
		super(field);
		this.user = user;
		this.receiver = receiver;
		this.item = item;
		/*if (item instanceof Staff)
			window = new EffectInfoWindow(Language.get("StaffEffect"));
		else
			window = new EffectInfoWindow(Language.get("ItemUseEffect"));*/
		window = new EffectInfoWindow(Language.get("Healing"));
		window.setPosition(getCenteredXPosition(window.getWidth()), getCenteredYPosition(window.getHeight()));
		window.setVisible(true);
		field.addActor(window);
	}
	
	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept())
			acceptAction();
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}
	
	@Override
	public void touchAction(InputEvent event, float x, float y, int count, int button) {
		//No action needed.
	}

	/**
	 * Emplea el objeto sobre la unidad elegida y muestra los resultados
	 * de su uso en el log.
	 */
	@Override
	public void acceptAction() {
		window.setVisible(false);
		field.removeActor(window);
		GameController.getGameController().useItem(user, receiver, item);
		ActionResult actionResult = new ActionResult(false, false, GameController.getGameController().unitEndAction(user));
		field.setMapState(new AfterPlayerActionMapState(field, actionResult));
	}

	/**
	 * Vuelve al estado de selección de unidad.
	 */
	@Override
	public void cancelAction() {
		window.setVisible(false);
		field.removeActor(window);
		field.setMapState(new ItemUseTargetSelectMapState(field, user, item));
	}

	/**
	 * Ventana que muestra el efecto de usar el objeto.
	 * @author Iván Barbosa Gutiérrez.
	 *
	 */
	private class EffectInfoWindow extends Window {

		public EffectInfoWindow(String title) {
			super("", UIFactory.getSkin());
			add(UIFactory.newLabel(title)).colspan(3);
			row();
			HealEffect effect = (HealEffect)item.getUseEffect();
			add(UIFactory.newLabel(Language.get("HPBefore"))).center();
			add();
			add(UIFactory.newLabel(Language.get("HPAfter"))).center();
			row();
			add(UIFactory.newLabel(Integer.toString(receiver.getCurrentHP()))).center();
			add(UIFactory.newLabel("--->")).center();
			int recovering = effect.previewEffect(user.getCurrentStat(Stats.MAGIC), receiver.getCurrentHP(), item instanceof Staff);
			if (recovering > receiver.getCurrentStat(Stats.HITPOINTS))
				recovering = receiver.getCurrentStat(Stats.HITPOINTS);
			add(UIFactory.newLabel(Integer.toString(recovering))).center();
			row();
			TextButton accept = UIFactory.newTextButton(Language.get("Heal"));
			accept.addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					acceptAction();
				}
			});
			add(accept).center();
			add();
			TextButton cancel = UIFactory.newTextButton(Language.get("Back"));
			cancel.addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					cancelAction();
				}
			});
			add(cancel).center();
			pack();
		}

	}
	
}
