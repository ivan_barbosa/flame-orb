package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;
import es.flameorb.units.FEClass;
import es.flameorb.units.PlayerUnit;
import es.flameorb.units.Stats;

public class PromotionChosenMapState extends MapState {
	
	private PlayerUnit unit;
	private boolean onGame;
	private FEClass promotion;
	private PromotionWindow window;

	public PromotionChosenMapState(Field field, PlayerUnit unit, boolean onGame, FEClass promotion) {
		super(field);
		this.unit = unit;
		this.onGame = onGame;
		this.promotion = promotion;
		window = new PromotionWindow(Language.get("PromotionInfo"));
		window.setPosition(getCenteredXPosition(window.getWidth()), getCenteredYPosition(window.getHeight()));
		window.setVisible(true);
		field.addActor(window);
	}

	@Override
	public void acceptAction() {
		unit.changeClass(promotion, true);
		window.setVisible(false);
		field.removeActor(window);
		field.setMapState(new ShowPromotionResultsMapState(field, unit, onGame));
	}

	@Override
	public void cancelAction() {
		window.setVisible(false);
		field.removeActor(window);
		field.setMapState(new PromotionSelectionMapState(field, unit, onGame));
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept())
			acceptAction();
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub

	}
	
	private class PromotionWindow extends Window {
		
		private TextButton accept, cancel;
		
		public PromotionWindow(String title) {
			super("", UIFactory.getSkin());
			add(UIFactory.newLabel(title)).colspan(4).center();
			row();
			add(UIFactory.newLabel(Language.get("Stat"))).center();
			add(UIFactory.newLabel(unit.getFeclass().toString())).center();
			add();
			add(UIFactory.newLabel(promotion.toString())).center();
			row();
			add(UIFactory.newLabel(Language.get("HP"))).center();
			add(UIFactory.newLabel(unit.getCurrentStat(Stats.HITPOINTS)+"")).center();
			add();
			add(UIFactory.newLabel(displayFutureStat(Stats.HITPOINTS)+"")).center();
			row();
			add(UIFactory.newLabel(Language.get("STRENGHT"))).center();
			add(UIFactory.newLabel(unit.getCurrentStat(Stats.STRENGHT)+"")).center();
			add();
			add(UIFactory.newLabel(displayFutureStat(Stats.STRENGHT)+"")).center();
			row();
			add(UIFactory.newLabel(Language.get("MAGIC"))).center();
			add(UIFactory.newLabel(unit.getCurrentStat(Stats.MAGIC)+"")).center();
			add();
			add(UIFactory.newLabel(displayFutureStat(Stats.MAGIC)+"")).center();
			row();
			add(UIFactory.newLabel(Language.get("SKILL"))).center();
			add(UIFactory.newLabel(unit.getCurrentStat(Stats.SKILL)+"")).center();
			add();
			add(UIFactory.newLabel(displayFutureStat(Stats.SKILL)+"")).center();
			row();
			add(UIFactory.newLabel(Language.get("SPEED"))).center();
			add(UIFactory.newLabel(unit.getCurrentStat(Stats.SPEED)+"")).center();
			add();
			add(UIFactory.newLabel(displayFutureStat(Stats.SPEED)+"")).center();
			row();
			add(UIFactory.newLabel(Language.get("LUCK"))).center();
			add(UIFactory.newLabel(unit.getCurrentStat(Stats.LUCK)+"")).center();
			add();
			add(UIFactory.newLabel(displayFutureStat(Stats.LUCK)+"")).center();
			row();
			add(UIFactory.newLabel(Language.get("DEFENSE"))).center();
			add(UIFactory.newLabel(unit.getCurrentStat(Stats.DEFENSE)+"")).center();
			add();
			add(UIFactory.newLabel(displayFutureStat(Stats.DEFENSE)+"")).center();
			row();
			add(UIFactory.newLabel(Language.get("RESISTANCE"))).center();
			add(UIFactory.newLabel(unit.getCurrentStat(Stats.RESISTANCE)+"")).center();
			add();
			add(UIFactory.newLabel(displayFutureStat(Stats.RESISTANCE)+"")).center();
			row();
			add(UIFactory.newLabel(Language.get("Movement"))).center();
			add(UIFactory.newLabel(unit.getMovement()+"")).center();
			add();
			add(UIFactory.newLabel(promotion.getMovement()+"")).center();
			row();
			accept = UIFactory.newTextButton(Language.get("Accept"));
			accept.addListener(new ClickListener() {

				@Override
				public void clicked(InputEvent event, float x, float y) {
					acceptAction();
				}
				
			});
			cancel = UIFactory.newTextButton(Language.get("Cancel"));
			cancel.addListener(new ClickListener() {
				
				@Override
				public void clicked(InputEvent event, float x, float y) {
					cancelAction();
				}
				
			});
			add(accept).colspan(2).center();
			add(cancel).colspan(2).center();
			pack();
		}
		
		private int displayFutureStat(Stats stat) {
			return unit.getBaseStat(stat) + promotion.getBaseStat(stat);
		}
	}

}
