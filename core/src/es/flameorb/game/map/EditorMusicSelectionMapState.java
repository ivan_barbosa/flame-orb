package es.flameorb.game.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;

import es.flameorb.game.control.GameController;
import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;

public class EditorMusicSelectionMapState extends MapState {
	
	private Window window;
	private MapData mapData;
	private SelectBox<String> preparationsSelectBox, battleSelectBox;

	public EditorMusicSelectionMapState(Field field) {
		super(field);
		mapData = GameController.getGameController().getMapData();
		window = UIFactory.newWindow(Language.get("Music"), 2);
		Array<String> options = new Array<String>();
		options.add(Language.get("Default"));
		FileHandle [] files = Gdx.files.local("data/music").list();
		for (FileHandle file : files)
			options.add(file.name());
		preparationsSelectBox = UIFactory.newSelectBox(options);
		if (mapData.getPreparationsMusicFilename() != null)
			preparationsSelectBox.setSelected(mapData.getPreparationsMusicFilename());
		battleSelectBox = UIFactory.newSelectBox(options);
		if (mapData.getBattleMusicFilename() != null)
			battleSelectBox.setSelected(mapData.getBattleMusicFilename());
		TextButton accept = UIFactory.newTextButton(Language.get("Accept"));
		accept.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				acceptAction();
			}
			
		});
		TextButton cancel = UIFactory.newTextButton(Language.get("Cancel"));
		cancel.addListener(new ClickListener() {
			
			@Override
			public void clicked(InputEvent event, float x, float y) {
				cancelAction();
			}
			
		});
		window.add(UIFactory.newLabel(Language.get("Preparations")));
		window.add(UIFactory.newLabel(Language.get("Battle")));
		window.row();
		window.add(preparationsSelectBox);
		window.add(battleSelectBox);
		window.row();
		window.add(accept);
		window.add(cancel);
		window.pack();
		window.setPosition(getCenteredXPosition(window.getWidth()), getCenteredYPosition(window.getHeight()));
		window.setVisible(true);
		field.addActor(window);
	}

	@Override
	public void acceptAction() {
		if (preparationsSelectBox.getSelected().equals(Language.get("Default")))
			mapData.setPreparationsMusicFilepath(null);
		else
			mapData.setPreparationsMusicFilepath(preparationsSelectBox.getSelected());
		if (battleSelectBox.getSelected().equals(Language.get("Default")))
			mapData.setBattleMusicFilepath(null);
		else
			mapData.setBattleMusicFilepath(battleSelectBox.getSelected());
		cancelAction();
	}

	@Override
	public void cancelAction() {
		window.setVisible(false);
		field.removeActor(window);
		field.setMapState(new EditorMenuMapState(field));
		
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept())
			acceptAction();
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		
	}

}
