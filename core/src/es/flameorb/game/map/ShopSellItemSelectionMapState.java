package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.utils.Array;

import es.flameorb.game.control.GameController;
import es.flameorb.general.Language;
import es.flameorb.items.Item;

public class ShopSellItemSelectionMapState extends MenuMapState {
	
	private SellItemsMenu menu;
	private Window moneyWindow;
	private Label moneyLabel;

	public ShopSellItemSelectionMapState(Field field, Window moneyWindow, Label moneyLabel) {
		super(field);
		this.moneyWindow = moneyWindow;
		this.moneyLabel = moneyLabel;
		menu = new SellItemsMenu(Language.get("Sell"), this);
		placeMenu(menu);
		moneyWindow.setPosition(getCenteredXPosition(moneyWindow.getWidth()), menu.getY()+menu.getHeight()+16);
	}

	@Override
	public void acceptAction() {
		GameController.getGameController().sellItem(menu.getSelected().getItem());
		moneyLabel.setText(GameController.getGameController().getPlayerMoney()+"");
		menu.reloadTable();
		menu.setPosition(getCenteredXPosition(menu.getWidth()), getCenteredYPosition(menu.getHeight()));
		moneyWindow.setPosition(getCenteredXPosition(moneyWindow.getWidth()), menu.getY()+menu.getHeight()+16);
	}

	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new ShopMenuDisplayedMapState(field, moneyWindow, moneyLabel));
	}
	
	private class SellItem {
		private Item item;
		private String listDisplay;
		
		public SellItem(Item item) {
			this.item = item;
			listDisplay = Language.get(item.getName()) + "  " + item.getPrice()/2;
		}
		
		public Item getItem() {
			return item;
		}
		
		public String toString() {
			return listDisplay;
		}
	}
	
	private class SellItemsMenu extends MapMenu<SellItem> {

		public SellItemsMenu(String title, MapState mapState) {
			super(title, mapState);
		}

		@Override
		protected void prepareOptions() {
			Array<SellItem> items = new Array<SellItem>();
			for (Item item : GameController.getGameController().getPlayerStock().getAllItems())
				if (item.getPrice() > 0)
					items.add(new SellItem(item));
			options.setItems(items);
		}
		
		public void reloadTable() {
			prepareOptions();
			pack();
		}
		
	}

}
