package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;

import es.flameorb.game.control.GameController;
import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;
import es.flameorb.items.Item;
import es.flameorb.items.ItemFactory;

public class EditorShopStockItemSelectionMapState extends MapState {
	
	private Window window;
	private boolean shop;
	private SelectBox<Item> itemsSelectBox;
	private VerticalGroup itemsGroup;
	private Array<Item> items;

	public EditorShopStockItemSelectionMapState(Field field, Array<Item> previousItems, boolean shop) {
		super(field);
		this.shop = shop;
		items = new Array<Item>(previousItems);
		window = UIFactory.newWindow(shop ? Language.get("Shop") : Language.get("Stock"), 4);
		itemsGroup = new VerticalGroup();
		for (Item item : items)
			addItemToGroup(item);
		if (shop)
			itemsSelectBox = UIFactory.newSelectBox(ItemFactory.getBuyableItems());
		else
			itemsSelectBox = UIFactory.newSelectBox(ItemFactory.getAllItems());
		itemsSelectBox.setMaxListCount(12);
		window.add(itemsSelectBox).padRight(5f);
		TextButton addTextButton = UIFactory.newTextButton(Language.get("AddItem"));
		addTextButton.addListener(new ClickListener() {
			
			@Override
			public void clicked(InputEvent event, float x, float y) {
				items.add(itemsSelectBox.getSelected());
				addItemToGroup(itemsSelectBox.getSelected());
			}
			
		});
		window.add(addTextButton).padRight(5f);
		TextButton accept = UIFactory.newTextButton(Language.get("Accept"));
		accept.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				acceptAction();
			}
		});
		TextButton cancel = UIFactory.newTextButton(Language.get("Cancel"));
		cancel.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				cancelAction();
			}
		});
		window.add(accept).padRight(5f);
		window.add(cancel);
		window.row();
		window.add(itemsGroup).colspan(4).padTop(5f);
		window.pack();
		window.setPosition(getCenteredXPosition(window.getWidth()), getCenteredYPosition(window.getHeight()));
		window.setVisible(true);
		field.addActor(window);
	}
	
	private void addItemToGroup(Item item) {
		HorizontalGroup group = new HorizontalGroup();
		group.addActor(UIFactory.newLabel(Language.get(item.getName())));
		TextButton deleteItemTextButton = UIFactory.newTextButton(Language.get("DeleteItem"));
		deleteItemTextButton.addListener(new DeleteItemButtonListener(itemsGroup, group, items, item));
		group.addActor(deleteItemTextButton);
		itemsGroup.addActor(group);
		window.pack();
		window.setPosition(getCenteredXPosition(window.getWidth()), getCenteredYPosition(window.getHeight()));
	}

	@Override
	public void acceptAction() {
		if (shop)
			GameController.getGameController().getMapData().setShop(items);
		else
			GameController.getGameController().setPlayerStock(items);
		cancelAction();
	}

	@Override
	public void cancelAction() {
		window.setVisible(true);
		field.removeActor(window);
		if (shop)
			field.setMapState(new EditorMenuMapState(field));
		else
			field.setMapState(new EditorPlayerEditionMapState(field));
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept())
			acceptAction();
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub

	}
	
private class DeleteItemButtonListener extends ClickListener {
		
		private VerticalGroup verticalGroup;
		private HorizontalGroup horizontalGroup;
		private Array<Item> items;
		private Item item;
		
		public DeleteItemButtonListener(VerticalGroup verticalGroup, HorizontalGroup horizontalGroup,
				Array<Item> items, Item item) {
			super();
			this.verticalGroup = verticalGroup;
			this.horizontalGroup = horizontalGroup;
			this.items = items;
			this.item = item;
		}

		@Override
		public void clicked(InputEvent event, float x, float y) {
			verticalGroup.removeActor(horizontalGroup);
			items.removeValue(item, true);
			window.pack();
		}
		
	}

}
