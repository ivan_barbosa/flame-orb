package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.game.control.BattleLog;
import es.flameorb.game.control.GameController;

public class GameEndMapState extends MapState {
	
	public GameEndMapState(Field field, boolean victory) {
		super(field);
		if (victory)
			BattleLog.victoryText();
		else
			BattleLog.defeatText();
		BattleLog.showWindow(this);
	}

	@Override
	public void acceptAction() {
		BattleLog.removeWindow();
		field.endGame();
	}

	@Override
	public void cancelAction() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		acceptAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		acceptAction();
	}

}
