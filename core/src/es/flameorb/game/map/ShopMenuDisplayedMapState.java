package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Window;

import es.flameorb.game.control.GameController;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;

public class ShopMenuDisplayedMapState extends MenuMapState {
	
	private ShopMenu menu;
	private Window moneyWindow;
	private Label moneyLabel;

	public ShopMenuDisplayedMapState(Field field, Window window, Label label) {
		super(field);
		menu = new ShopMenu(Language.get("Shop"), this);
		placeMenu(menu);
		if (window == null) {
			moneyWindow = UIFactory.newWindow(Language.get("Money"), 1);
			moneyLabel = UIFactory.newLabel(GameController.getGameController().getPlayerMoney()+"");
			moneyWindow.add(moneyLabel);
			moneyWindow.pack();
			moneyWindow.setPosition(getCenteredXPosition(moneyWindow.getWidth()), menu.getY()+menu.getHeight()+16);
			moneyWindow.setVisible(true);
			field.addActor(moneyWindow);
		}
		else {
			moneyWindow = window;
			moneyLabel = label;
			moneyWindow.setPosition(getCenteredXPosition(moneyWindow.getWidth()), menu.getY()+menu.getHeight()+16);
		}
	}

	@Override
	public void acceptAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		switch(menu.getSelected()) {
		case BUY:
			field.setMapState(new ShopItemSelectionMapState(field, moneyWindow, moneyLabel));
			break;
		case SELL:
			field.setMapState(new ShopSellItemSelectionMapState(field, moneyWindow, moneyLabel));
			break;
		}
	}

	@Override
	public void cancelAction() {
		menu.setVisible(false);
		moneyWindow.setVisible(false);
		field.removeActor(menu);
		field.removeActor(moneyWindow);
		field.setMapState(new PreparationMenuDisplayedMapState(field));
	}
	
	private enum ShopMenuOptions {
		BUY("Buy"), SELL("Sell");
		
		private final String name;
		
		private ShopMenuOptions(String name) {
			this.name = name;
		}
		
		public String toString() {
			return Language.get(name);
		}
	}
	
	private class ShopMenu extends MapMenu<ShopMenuOptions> {

		public ShopMenu(String title, MapState mapState) {
			super(title, mapState);
		}

		@Override
		protected void prepareOptions() {
			options.setItems(ShopMenuOptions.values());
		}
		
	}

}
