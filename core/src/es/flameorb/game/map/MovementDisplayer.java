package es.flameorb.game.map;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

import com.badlogic.gdx.utils.Array;

import es.flameorb.game.calculators.PathCalculator;
import es.flameorb.game.calculators.RangeCalculator;
import es.flameorb.game.control.GameController;
import es.flameorb.units.Unit;

/**
 * Muestra el camino que hace una unidad para moverse por el mapa.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class MovementDisplayer extends Thread {
	
	private Field field;
	private Unit unit;
	private Point origin, destiny;
	private EnemyTurnMapState enemyTurn;
	
	public MovementDisplayer(Field field, Unit unit, Point origin, Point destiny, EnemyTurnMapState enemyTurn) {
		this.field = field;
		this.unit = unit;
		this.origin = origin;
		this.destiny = destiny;
		this.enemyTurn = enemyTurn;
		start();
	}

	@Override
	public void run() {
		if (!destiny.equals(unit.getMapPosition())) {
			if (unit.isPlayerUnit()) {
				unit.addAction(moveTo((int)field.cursor.x, (int)field.cursor.y, 0.5f));
				while (unit.hasActions()) {
					System.out.print("");
				}
				unit.setMapPosition(destiny);
				GameController.getGameController().updatePosition(unit, origin);
				field.setMapState(new UnitMenuDisplayedMapState(field, origin, unit));
			}
			else {
				unit.addAction(moveTo(destiny.x*32, destiny.y*32, 0.5f));
				while (unit.hasActions()) {
					System.out.print("");
				}
				unit.setMapPosition(destiny);
				GameController.getGameController().updatePosition(unit, origin);
				enemyTurn.afterMovement();
			}
			/*Array<Point> path = PathCalculator.getMovementPath(field.mapLayer, unit, RangeCalculator.getUnitMoveRange(unit),
					origin, destiny);
			for (int i=0; i<path.size; i++) {
				if (i == path.size-1) {
					if (unit.isPlayerUnit()) {
						unit.addAction(moveTo((int)field.cursor.x, (int)field.cursor.y, 0.2f));
						while (unit.hasActions())
							System.out.print("");
						unit.setMapPosition(destiny);
						GameController.getGameController().updatePosition(unit, origin);
						field.setMapState(new UnitMenuDisplayedMapState(field, origin, unit));
					}
					else {
						unit.addAction(moveTo(destiny.x*32, destiny.y*32, 0.2f));
						while (unit.hasActions())
							System.out.print("");
						unit.setMapPosition(destiny);
						GameController.getGameController().updatePosition(unit, origin);
						enemyTurn.afterMovement();
					}
				}
				else {
					unit.addAction(moveTo(path.get(i).x*32, path.get(i).y*32, 0.2f));
					while (unit.hasActions())
						System.out.print("");
				}
			}*/
		}
		else if (unit.isPlayerUnit())
					field.setMapState(new UnitMenuDisplayedMapState(field, origin, unit));
		else
			enemyTurn.afterMovement();
	}

}
