package es.flameorb.game.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;
import es.flameorb.general.Stock;
import es.flameorb.gui.UIFactory;
import es.flameorb.items.Item;
import es.flameorb.units.Unit;

/**
 * Ventana en la que se muestran las dos listas de objetos, la de la unidad
 * que inicia el intercambio, y la de la otra unidad o el carro, en función de la
 * opción.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class ItemExchangeWindow extends Window {
	
	private Unit unit, other;
	private Stock stock;
	private List<Item> unitList, otherList;
	private boolean onUnitList = true, itemSelected = false;
	private TextButton backButton, exchangeBackButton;

	public ItemExchangeWindow(String title, final MapState mapState, Unit unit, Unit other) {
		super("", UIFactory.getSkin());
		add(UIFactory.newLabel(title)).colspan(2);
		row();
		this.unit = unit;
		this.other = other;
		createWindow(mapState);
	}
	
	public ItemExchangeWindow(String title, final MapState mapState, Unit unit, Stock stock) {
		super("", UIFactory.getSkin());
		add(UIFactory.newLabel(title)).colspan(2);
		row();
		this.unit = unit;
		this.stock = stock;
		createWindow(mapState);
	}
	
	private void createWindow(final MapState mapState) {
		unitList = UIFactory.newList();
		unitList.setItems(unit.getInventory().getItems());
		add(unitList);
		if (unit.getInventory().getItemsNumber() == 0)
			onUnitList = false;
		otherList = UIFactory.newList();
		if (stock == null)
			otherList.setItems(other.getInventory().getItems());
		else
			otherList.setItems(stock.getAllItems());
		add(otherList);
		row();
		if (Gdx.app.getType() == ApplicationType.Android) {
			unitList.setSelectedIndex(-1);
			otherList.setSelectedIndex(-1);
			backButton = UIFactory.newTextButton(Language.get("Back"));
			backButton.addListener(new ClickListener() {
			
				@Override
				public void clicked(InputEvent event, float x, float y) {
					mapState.cancelAction();
				}
				
			});
			add(backButton);
			exchangeBackButton = UIFactory.newTextButton(Language.get("Back"));
		}
		pack();
	}
	
	public void moveSelection(int keycode) {
		int index;
		if (onUnitList) {
			index = unitList.getSelectedIndex();
			if (keycode == KeyBinding.getUp()) {
				if (index==0)
					index = unitList.getItems().size-1;
				else
					index--;
				unitList.setSelectedIndex(index);
			}
			else if (keycode == KeyBinding.getDown()) {
				if (index == unitList.getItems().size-1)
					index = 0;
				else
					index++;
				unitList.setSelectedIndex(index);
			}
			else if (keycode == KeyBinding.getLeft()) {
				if (stock == null) {
					if (!itemSelected && other.getInventory().getItemsNumber() != 0)
						onUnitList = false;
				}
				else {
					if (!itemSelected && stock.getItemsNumber() != 0)
						onUnitList = false;
				}
			}
			else if (keycode == KeyBinding.getRight()) {
				if (stock == null) {
					if (!itemSelected && other.getInventory().getItemsNumber() != 0)
						onUnitList = false;
				}
				else {
					if (!itemSelected && stock.getItemsNumber() != 0)
						onUnitList = false;
				}
			}
		}
		else {
			index = otherList.getSelectedIndex();
			if (keycode == KeyBinding.getUp()) {
				if (index==0)
					index = otherList.getItems().size-1;
				else
					index--;
				otherList.setSelectedIndex(index);
			}
			else if (keycode == KeyBinding.getDown()) {
				if (index == otherList.getItems().size-1)
					index = 0;
				else
					index++;
				otherList.setSelectedIndex(index);
			}
			else if (keycode == KeyBinding.getLeft()) {
				if (!itemSelected && unit.getInventory().getItemsNumber() != 0)
					onUnitList = true;
			}
			else if (keycode == KeyBinding.getRight()) {
				if (!itemSelected && unit.getInventory().getItemsNumber() != 0)
					onUnitList = true;
			}
		}
	}
	
	public Item getSelected() {
		if (onUnitList)
			return unitList.getSelected();
		else
			return otherList.getSelected();
	}
	
	/**
	 * Cambia la lista sobre la que se tiene el foco.
	 */
	public void setClickedList() {
		if (unitList.getSelected() != null)
			onUnitList = true;
		else
			onUnitList = false;
	}
	
	/**
	 * Realiza la acción de dar un objeto.
	 * @param selected - El objeto a dar.
	 */
	public void giveItem(Item selected) {
		if (onUnitList) {
			unit.getInventory().tossItem(selected);
			if (stock == null) 
				other.getInventory().insertItem(selected);
			else
				stock.insertItem(selected);
			if (unit.getInventory().getItemsNumber() == 0)
				onUnitList = false;
		}
		else {
			if (stock == null)
				other.getInventory().tossItem(selected);
			else
				stock.takeOutItem(selected);
			unit.getInventory().insertItem(selected);
			if ((stock == null && other.getInventory().getItemsNumber() == 0) ||
					(stock != null && stock.getItemsNumber() == 0))
				onUnitList = true;
		}
		unitList.setItems(unit.getInventory().getItems());
		unit.getUnitDetails().inventoryUpdate();
		if (stock == null) {
			otherList.setItems(other.getInventory().getItems());
			other.getUnitDetails().inventoryUpdate();
		}
		else
			otherList.setItems(stock.getAllItems());
		pack();
	}
	
	/**
	 * Realiza el intercambio de objetos.
	 * @param firstSelected - El primer objeto seleccionando.
	 * @param lastSelected - El segundo objeto seleccionado.
	 */
	public void exchangeItem(Item firstSelected, Item lastSelected) {
		if (onUnitList) {
			unit.getInventory().tossItem(lastSelected);
			unit.getInventory().insertItem(firstSelected);
			if (stock == null) {
				other.getInventory().tossItem(firstSelected);
				other.getInventory().insertItem(lastSelected);
			}
			else {
				stock.takeOutItem(firstSelected);
				stock.insertItem(lastSelected);
			}
			
		}
		else {
			unit.getInventory().tossItem(firstSelected);
			unit.getInventory().insertItem(lastSelected);
			if (stock == null) {
				other.getInventory().tossItem(lastSelected);
				other.getInventory().insertItem(firstSelected);
			}
			else {
				stock.takeOutItem(lastSelected);
				stock.insertItem(firstSelected);
			}
		}
		unitList.setItems(unit.getInventory().getItems());
		unit.getUnitDetails().inventoryUpdate();
		if (stock == null) {
			otherList.setItems(other.getInventory().getItems());
			other.getUnitDetails().inventoryUpdate();
		}
		else
			otherList.setItems(stock.getAllItems());
		pack();
	}
	
	/**
	 * Cambia la lista seleccionada durante la acción de intercambio.
	 * @param mapState - El estado del mapa.
	 */
	public void onExchangeAction(final MapState mapState) {
		itemSelected = true;
		if (onUnitList)
			onUnitList = false;
		else
			onUnitList = true;
		if (Gdx.app.getType() == ApplicationType.Android) {
			removeActor(backButton);
			exchangeBackButton.addListener(new ClickListener() {
				
				@Override
				public void clicked(InputEvent event, float x, float y) {
					mapState.cancelAction();
				}
				
			});
			add(exchangeBackButton);
			pack();
		}
	}
	
	/**
	 * Finaliza la acción de intercambio.
	 * @param mapState - El estado del mapa.
	 */
	public void exchangeActionEnd(MapState mapState) {
		itemSelected = false;
		if (Gdx.app.getType() == ApplicationType.Android) {
			removeActor(exchangeBackButton);
			exchangeBackButton.clearListeners();
			add(backButton);
			pack();
		}
	}

}
