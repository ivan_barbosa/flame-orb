package es.flameorb.game.map;

import com.badlogic.gdx.math.Rectangle;

/**
 * Representa al cursor del juego, que es un triángulo al que se le
 * han añadido la posición relativa dentro de la ventana, y la posición
 * en función de las celdas del mapa.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class Cursor extends Rectangle {
	
	private static final long serialVersionUID = -8308092557469400880L;
	private Point relPosition;
	private Point mapPosition;
	
	/**
	 * Construye un nuevo cursor posicionado en la esquina inferior
	 * izquierda del mapa y de la ventana.
	 */
	public Cursor() {
		relPosition = new Point(0, 0);
		mapPosition = new Point(0, 0);
		x = 0;
		y = 0;
		width = 32;
		height = 32;
	}
	
	/**
	 * Returns the position on the map coordinates.
	 * @return The position on the map coordinates.
	 */
	public Point getMapPosition() {
		return mapPosition;
	}
	
	/**
	 * Returns the X position on the map coordinates.
	 * @return The X position on the map coordinates.
	 */
	public int getXMap() {
		return mapPosition.x;
	}
	
	/**
	 * Returns the Y position on the map coordinates.
	 * @return The Y position on the map coordinates.
	 */
	public int getYMap() {
		return mapPosition.y;
	}
	
	/**
	 * Sets the position on the map coordinates, and updates its position on the screen.
	 * @param position - The new map position.
	 */
	public void setMapPosition(Point position) {
		mapPosition = position;
		setPosition(mapPosition.x*32f, mapPosition.y*32f);
	}
	
	/**
	 * Sets the X position on the map coordinates, and updates its position on the screen.
	 * @param x - The new X map position.
	 */
	public void setXMap(int x) {
		mapPosition.x = x;
		setX(x*32f);
	}
	
	/**
	 * Sets the Y position on the map coordinates, and updates its position on the screen.
	 * @param y - The new Y map position.
	 */
	public void setYMap(int y) {
		mapPosition.y = y;
		setY(y*32f);
	}
	
	/**
	 * Returns the position on the window.
	 * @return The position on the window.
	 */
	public Point getRelPosition() {
		return relPosition;
	}
	
	/**
	 * Returns the X position on the window.
	 * @return The X position on the window.
	 */
	public int getXRel() {
		return relPosition.x;
	}
	
	/**
	 * Returns the Y position on the window.
	 * @return The Y position on the window.
	 */
	public int getYRel() {
		return relPosition.y;
	}
	
	/**
	 * Adds to the X position on the window the given value.
	 * @param value - The number of cells to move.
	 */
	public void addXRel(int value) {
		relPosition.x += value;
	}
	
	/**
	 * Adds to the Y position on the window the given value.
	 * @param value - The number of cells to move.
	 */
	public void addYRel(int value) {
		relPosition.y += value;
	}
	
	/**
	 * Sets the position on the window.
	 * @param position - The new position.
	 */
	public void setRelPosition(Point position) {
		relPosition = position;
	}
	
	/**
	 * Sets the X position on the window.
	 * @param x - The new X position.
	 */
	public void setXRel(int x) {
		relPosition.x = x;
	}
	
	/**
	 * Sets the Y position on the window.
	 * @param y - The new Y position.
	 */
	public void setYRel(int y) {
		relPosition.y = y;
	}
	
	
}
