package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.utils.Array;

import es.flameorb.game.control.GameController;
import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;
import es.flameorb.units.Unit;

public class EditorPlayerUnitsListMapState extends MapState {
	
	private UnitMenu menu;

	public EditorPlayerUnitsListMapState(Field field) {
		super(field);
		menu = new UnitMenu(Language.get("Units"), this);
		menu.setVisible(true);
		field.addActor(menu);
	}

	@Override
	public void acceptAction() {
		if (menu.getSelected() != null) {
			menu.setVisible(false);
			field.removeActor(menu);
			field.setMapState(new EditorPlayerUnitsOptionsMapState(field, menu.getSelected().getUnit()));
		}
	}

	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new EditorPlayerUnitsMapState(field));
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		menu.moveSelection(keycode);
		if (keycode == KeyBinding.getAccept())
			acceptAction();
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		acceptAction();
	}
	
private class UnitShow {
		
		private Unit unit;
		
		public UnitShow(Unit unit) {
			this.unit = unit;
		}
		
		public Unit getUnit() {
			return unit;
		}
		
		public String toString() {
			if (unit.isCommander())
				return unit.getUnitName() + "(" + Language.get("CommanderFirstLetter") +") - " + Language.get(unit.getFeclass().getName());
			else
				return unit.getUnitName() + " - " + Language.get(unit.getFeclass().getName());
		}
	}
	
	private class UnitMenu extends MapMenu<UnitShow> {

		public UnitMenu(String title, MapState mapState) {
			super(title, mapState);
		}

		@Override
		protected void prepareOptions() {
			Array<UnitShow> items = new Array<UnitShow>();
			for (Unit unit : GameController.getGameController().getAllPlayerUnits())
				items.add(new UnitShow(unit));
			options.setItems(items);
		}
		
	}

}
