package es.flameorb.game.map;

import es.flameorb.general.Language;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

public class Terrain implements Json.Serializable {
	
	private String name;
	private int id, avoid, defense, healRate;
	private boolean crossable, damage;
	
	
	/**
	 * Returns the id of the terrain of the cell.
	 * @return The id of the terrain.
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Returns the name of the terrain of the cell.
	 * @return The name of the terrain.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Devuelve el aumento de la probabilidad de esquivar del terreno.
	 * @return El aumento de la probabilidad de esquivar.
	 */
	public int getAvoid() {
		return avoid;
	}

	/**
	 * Devuelve el aumento de defensa del terreno.
	 * @return El aumento de defensa.
	 */
	public int getDefense() {
		return defense;
	}

	/**
	 * Devuelve el porcentaje de PV que restaura el terreno.
	 * @return El porcentaje de PV que restaura.
	 */
	public int getHealRate() {
		return healRate;
	}

	/**
	 * Devuelve si se puede pasar por el terreno.
	 * @return Si se puede pasar.
	 */
	public boolean isCrossable() {
		return crossable;
	}

	/**
	 * Devuelve si el terreno causa daño al estar sobre él.
	 * @return Si causa daño.
	 */
	public boolean isDamage() {
		return damage;
	}
	
	/**
	 * Devuelve el texto asociado al terreno.
	 * @return El texto asociado.
	 */
	public String getTerrainInfo() {
		return Language.get(name)+"\n"+Language.format("Properties", defense, avoid, healRate);
	}

	@Override
	public void write(Json json) {
		// Not needed
		
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		id = jsonData.getInt("id");
		name = jsonData.getString("name");
		avoid = jsonData.getInt("avoid");
		defense = jsonData.getInt("defense");
		crossable = jsonData.getBoolean("crossable");
		damage = jsonData.getBoolean("damage");
		healRate = jsonData.getInt("healRate");
	}

}
