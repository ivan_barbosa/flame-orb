package es.flameorb.game.map;

import com.badlogic.gdx.utils.Array;

import es.flameorb.game.control.GameController;
import es.flameorb.general.Language;
import es.flameorb.units.PlayerUnit;
import es.flameorb.units.Unit;

public class PromotionUnitsListMapState extends MenuMapState {
	
	private PromotionUnitsMenu menu;

	public PromotionUnitsListMapState(Field field) {
		super(field);
		menu = new PromotionUnitsMenu(Language.get("ChooseUnit"), this);
		placeMenu(menu);
	}

	@Override
	public void acceptAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new PromotionSelectionMapState(field, (PlayerUnit)menu.getSelected(), false));
	}

	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new PreparationMenuDisplayedMapState(field));
	}
	
	private class PromotionUnitsMenu extends MapMenu<Unit> {

		public PromotionUnitsMenu(String title, MapState mapState) {
			super(title, mapState);
		}

		@Override
		protected void prepareOptions() {
			Array<Unit> list = new Array<Unit>();
			for (Unit unit : GameController.getGameController().getAllPlayerUnits())
				if (((PlayerUnit)unit).canPromote())
					list.add(unit);
			options.setItems(list);
		}
		
	}

}
