package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import es.flameorb.general.GamePreferences;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;
import es.flameorb.units.Unit;

public class EditorEnemyDefaultsMapState extends MapState {
	
	private Unit unit;
	private Window window;
	private TextField nameTextField;
	private TextField levelTextField;
	private SelectBox<String> weaponLevelSelectBox;
	private CheckBox defaultWeaponsCheckBox;

	public EditorEnemyDefaultsMapState(Field field, Unit unit) {
		super(field);
		this.unit = unit;
		window = UIFactory.newWindow(Language.get("EnemyDefaults"), 2);
		window.add(UIFactory.newLabel(Language.get("Name")+":"));
		nameTextField = UIFactory.newTextField();
		nameTextField.setText(EnemyDefaults.getEnemyDefaults().getName());
		window.add(nameTextField);
		window.row();
		window.add(UIFactory.newLabel(Language.get("LevelEditor")+":"));
		levelTextField = UIFactory.newTextField(2, true);
		levelTextField.setText(Integer.toString(EnemyDefaults.getEnemyDefaults().getLevel()));
		window.add(levelTextField);
		window.row();
		window.add(UIFactory.newLabel(Language.get("WeaponLevel")+":"));
		weaponLevelSelectBox = UIFactory.newSelectBox(GamePreferences.getWeaponLevelsNames());
		weaponLevelSelectBox.setSelected(EnemyDefaults.getEnemyDefaults().getWeaponLevel());
		window.add(weaponLevelSelectBox);
		window.row();
		defaultWeaponsCheckBox = UIFactory.newCheckBox(Language.get("DefaultWeapons"));
		defaultWeaponsCheckBox.setChecked(EnemyDefaults.getEnemyDefaults().isDefaultWeapons());
		window.add(defaultWeaponsCheckBox).colspan(2);
		window.row();
		TextButton acceptButton = UIFactory.newTextButton(Language.get("Accept"));
		acceptButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				acceptAction();
			}
		});
		window.add(acceptButton);
		TextButton cancelButton = UIFactory.newTextButton(Language.get("Cancel"));
		cancelButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				cancelAction();
			}
		});
		window.add(cancelButton);
		window.pack();
		window.setPosition(getCenteredXPosition(window.getWidth()), getCenteredYPosition(window.getHeight()));
		window.setVisible(true);
		field.addActor(window);
	}

	@Override
	public void acceptAction() {
		EnemyDefaults.getEnemyDefaults().setName(nameTextField.getText());
		EnemyDefaults.getEnemyDefaults().setLevel(Integer.parseInt(levelTextField.getText()));
		EnemyDefaults.getEnemyDefaults().setWeaponLevel(weaponLevelSelectBox.getSelected());
		EnemyDefaults.getEnemyDefaults().setDefaultWeapons(defaultWeaponsCheckBox.isChecked());
		cancelAction();
	}

	@Override
	public void cancelAction() {
		window.setVisible(false);
		field.removeActor(window);
		field.setMapState(new EditorEnemyCreatorMenuMapState(field, unit));
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		return false;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {

	}

}
