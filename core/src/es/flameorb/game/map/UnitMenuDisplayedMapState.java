package es.flameorb.game.map;

import com.badlogic.gdx.utils.Array;

import es.flameorb.game.control.GameController;
import es.flameorb.general.Language;
import es.flameorb.items.Staff;
import es.flameorb.items.Weapon;
import es.flameorb.units.PlayerUnit;
import es.flameorb.units.Stats;
import es.flameorb.units.Unit;

/**
 * Estado del mapa en el que se muestra el menú de acción de una unidad.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class UnitMenuDisplayedMapState extends MenuMapState {
	
	private Unit unit;
	private Point origin;
	private UnitMenu menu;

	/**
	 * Constructor del estado del mapa.
	 * @param field - El mapa.
	 * @param origin - La posición anterior de la unidad.
	 * @param unit - La unidad.
	 */
	public UnitMenuDisplayedMapState(Field field, Point origin, Unit unit) {
		super(field);
		this.origin = origin;
		this.unit = unit;
		refreshInfoWindows();
		menu = new UnitMenu(Language.get("Menu"), this);
		placeMenu(menu);
	}

	/**
	 * Cambia el estado de mapa en función de la acción elegida.
	 */
	@Override
	public void acceptAction() {
		switch(menu.getSelected()) {
		case ATTACK:
			menu.setVisible(false);
			field.removeActor(menu);
			field.setMapState(new WeaponSelectionMapState(field, unit));
			break;
		case STAFF:
			menu.setVisible(false);
			field.removeActor(menu);
			field.setMapState(new StaffSelectionMapState(field, unit));
			break;
		case ITEMS:
			menu.setVisible(false);
			field.removeActor(menu);
			field.setMapState(new ItemManagementMapState(field, unit));
			break;
		case EXCHANGE:
			menu.setVisible(false);
			field.removeActor(menu);
			field.setMapState(new ItemExchangeUnitSelectionMapState(field, unit));
			break;
		case STOCK:
			menu.setVisible(false);
			field.removeActor(menu);
			field.setMapState(new StockManagementMapState(field, unit, false));
			break;
		case PROMOTE:
			menu.setVisible(false);
			field.removeActor(menu);
			field.setMapState(new PromotionSelectionMapState(field, (PlayerUnit)unit, true));
			break;
		case DETAILS:
			field.setMapState(new UnitDetailMapState(field, unit, this));
			break;
		case WAIT:
			boolean turnEnd = GameController.getGameController().unitEndAction(unit);
			menu.setVisible(false);
			field.removeActor(menu);
			if (turnEnd)
				field.setMapState(new ChangeTurnMapState(field, false));
			else
				field.setMapState(new InitialMapState(field));
			break;
		}
	}

	/**
	 * Vuelve al MovementOnMapState.
	 */
	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.revertMovement(unit.getMapPosition(), unit);
		refreshInfoWindows();
		field.removeActor(menu);
		field.setMapState(new MovementOnMapState(field, origin, unit));
	}
	
	/**
	 * Enumerado que representa las diferentes opciones del menú de la unidad.
	 * @author Iván Barbosa Gutiérrez.
	 *
	 */
	private enum UnitMenuOptions {
		
		ATTACK("Attack"), STAFF("Staff"), ITEMS("Items"), EXCHANGE("Exchange"),
		STOCK("Stock"), PROMOTE("Promote"), DETAILS("Details"), WAIT("Wait");
		
		private final String name;
		
		private UnitMenuOptions(String name) {
			this.name = name;
		}
		
		@Override
		public String toString() {
			return Language.get(name);
		}
	}
	
	/**
	 * Clase que representa el menú de la unidad.
	 * @author Iván Barbosa Gutiérrez
	 *
	 */
	private class UnitMenu extends MapMenu<UnitMenuOptions> {

		public UnitMenu(String name, MapState mapState) {
			super(name, mapState);
		}

		@Override
		protected void prepareOptions() {
			Array<UnitMenuOptions> list = new Array<UnitMenuOptions>(UnitMenuOptions.values());
			enableAttackButton(list);
			if (unit.getWeaponSkill(Staff.ID) >= 0)
				enableStaffButton(list);
			else
				list.removeValue(UnitMenuOptions.STAFF, true);
			if (!unit.isAllyNext() || (unit.getInventory().getItemsNumber() == 0
					&& !checkNextAlliesInventory()))
				list.removeValue(UnitMenuOptions.EXCHANGE, true);
			if (unit.getBattleId() != 0 && !unit.isNearCommander())
				list.removeValue(UnitMenuOptions.STOCK, true);
			if (!((PlayerUnit)unit).canPromote())
				list.removeValue(UnitMenuOptions.PROMOTE, true);
			options.setItems(list);
			pack();
		}
		
		private void enableAttackButton(Array<UnitMenuOptions> list) {
			int i=0;
			boolean enable = false;
			Array<Weapon> unitWeapons = unit.getInventory().getWeapons();
			while(!enable && i<unitWeapons.size) {
				Weapon weapon = unitWeapons.get(i);
				if (weapon.getAttackRange() != null)
					for (Point point : weapon.getAttackRange())
						if (field.unitsPositions.isPositionOccupedByEnemy(point))
							enable = true;
				i++;
			}
			if (!enable)
				list.removeValue(UnitMenuOptions.ATTACK, true);
		}
		
		private void enableStaffButton(Array<UnitMenuOptions> list) {
			int i=0;
			boolean enable = false;
			Array<Weapon> unitWeapons = unit.getInventory().getWeapons();
			while(!enable && i<unitWeapons.size) {
				Weapon weapon = unitWeapons.get(i);
				if (weapon instanceof Staff)
					enable = checkAllyInRange(weapon);
				i++;
			}
			if (!enable)
				list.removeValue(UnitMenuOptions.STAFF, true);
		}
		
		private boolean checkAllyInRange(Weapon weapon) {
			for (Point point : weapon.getUseRange())
				if (field.unitsPositions.isPositionOccupedByPlayer(point) &&
						field.unitsPositions.getUnit(point).getCurrentHP() !=
						field.unitsPositions.getUnit(point).getCurrentStat(Stats.HITPOINTS))
					return true;
			return false;
		}
		
		private boolean checkNextAlliesInventory() {
			for (Unit other : GameController.getGameController().getPlayerUnits())
				if (unit.distanceTo(other) == 1 && other.getInventory().getItemsNumber() != 0)
					return true;
			return false;
		}

	}

}
