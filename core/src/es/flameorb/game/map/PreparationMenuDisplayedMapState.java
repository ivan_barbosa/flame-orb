package es.flameorb.game.map;

import com.badlogic.gdx.utils.Array;

import es.flameorb.game.control.GameController;
import es.flameorb.general.Language;

public class PreparationMenuDisplayedMapState extends MenuMapState {
	
	private PreparationMenu menu;

	public PreparationMenuDisplayedMapState(Field field) {
		super(field);
		menu = new PreparationMenu(Language.get("PreparationMenu"), this);
		placeMenu(menu);
	}

	@Override
	public void acceptAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		switch(menu.getSelected()) {
		case SEE_MAP:
			field.setMapState(new SeeMapMapState(field, false));
			break;
		case END_CONDITIONS:
			field.setMapState(new ShowEndConditionsMapState(field, true));
			break;
		case UNIT_LIST:
			field.setMapState(new PlayerUnitsListMapState(field));
			break;
		case PLACE_UNITS:
			field.setMapState(new PlaceUnitsDisplayedMapState(field));
			break;
		case PROMOTE:
			field.setMapState(new PromotionUnitsListMapState(field));
			break;
		case SHOP:
			field.setMapState(new ShopMenuDisplayedMapState(field, null, null));
			break;
		case STOCK:
			field.setMapState(new StockManagementUnitSelectionMapState(field));
			break;
		case START:
			GameController.getGameController().startGame();
			field.setMapState(new ChangeTurnMapState(field, true));
			break;
		}
	}

	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.endGame();
	}
	
	private enum PreparationMenuOptions{
		SEE_MAP("SeeMap"), END_CONDITIONS("EndConditions"), UNIT_LIST("UnitList"),
		PLACE_UNITS("PlaceUnits"), PROMOTE("Promote"), SHOP("Shop"),
		STOCK("Stock"), START("Start");
		
		private final String name;
		
		private PreparationMenuOptions(String name) {
			this.name = name;
		}
		
		@Override
		public String toString() {
			return Language.get(name);
		}
	}
	
	private class PreparationMenu extends MapMenu<PreparationMenuOptions> {

		public PreparationMenu(String title, MapState mapState) {
			super(title, mapState);
		}

		@Override
		protected void prepareOptions() {
			Array<PreparationMenuOptions> items = new Array<PreparationMenuOptions>(PreparationMenuOptions.values());
			if (!GameController.getGameController().arePromotionablePlayerUnits())
				items.removeValue(PreparationMenuOptions.PROMOTE, true);
			if (GameController.getGameController().getPlayerCommander() == null)
				items.removeValue(PreparationMenuOptions.START, true);
			options.setItems(items);
		}
		
	}

}
