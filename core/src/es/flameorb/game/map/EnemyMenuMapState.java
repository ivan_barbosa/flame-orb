package es.flameorb.game.map;

import es.flameorb.general.Language;
import es.flameorb.units.Unit;

/**
 * Muestra el menú del enemigo, que permite ver sus detalles o su movimiento.
 * @author Iván Barbosa Gutiérrez.
 *
 */
public class EnemyMenuMapState extends MenuMapState {
	
	private Unit enemy;
	private EnemyMenu menu;

	/**
	 * Construye el estado del mapa.
	 * @param field - El mapa
	 * @param enemy - La unidad enemiga. 
	 */
	public EnemyMenuMapState(Field field, Unit enemy) {
		super(field);
		this.enemy = enemy;
		menu = new EnemyMenu(Language.get("Menu"), this);
		placeMenu(menu);
	}

	/**
	 * Actúa en función de la acción elegida.
	 */
	@Override
	public void acceptAction() {
		switch(menu.getSelected()) {
		case DETAILS:
			field.setMapState(new UnitDetailMapState(field, enemy, this));
			break;
		case MOVEMENT:
			menu.setVisible(false);
			field.removeActor(menu);
			field.setMapState(new ShowEnemyMovementMapState(field, enemy));
			break;
		}
	}

	/**
	 * Vuelve al estado inicial del mapa.
	 */
	@Override
	public void cancelAction() {
		menu.setVisible(false);
		field.removeActor(menu);
		field.setMapState(new InitialMapState(field));
	}
	
	/**
	 * Las opciones del menú del enemigo.
	 * @author Iván Barbosa Gutiérrez.
	 *
	 */
	public enum EnemyMenuOptions {
		DETAILS("Details"), MOVEMENT("Movement");
		
		private final String name;
		
		private EnemyMenuOptions(String name) {
			this.name = name;
		}
		
		@Override
		public String toString() {
			return Language.get(name);
		}
	}
	
	/**
	 * Clase que representa al menú del enemigo.
	 * @author Iván Barbosa Gutiérrez
	 *
	 */
	class EnemyMenu extends MapMenu<EnemyMenuOptions> {
		
		public EnemyMenu(String title, MapState mapState) {
			super(title, mapState);
		}

		@Override
		protected void prepareOptions() {
			options.setItems(EnemyMenuOptions.values());
		}
	}

}
