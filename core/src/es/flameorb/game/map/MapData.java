package es.flameorb.game.map;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter.OutputType;

import es.flameorb.game.endconditions.DefeatAllEndCondition;
import es.flameorb.game.endconditions.DefeatCommanderEndCondition;
import es.flameorb.game.endconditions.EndCondition;
import es.flameorb.general.Player;
import es.flameorb.items.Item;
import es.flameorb.items.ItemFactory;
import es.flameorb.items.Weapon;
import es.flameorb.units.Barracks;
import es.flameorb.units.Unit;

/**
 * Encapsula los datos relativos a un mapa.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class MapData implements Json.Serializable {
	
	private String fileName;
	private boolean finished;
	private String name;
	private Player player;
	private String preparationsMusicFilename, battleMusicFilename;
	private Array<EndCondition> victoryConditions, defeatConditions;
	private Array<Unit> enemies;
	private Array<Point> initialPositions;
	private Array<Item> shop;
	
	public MapData() {
		player = new Player();
		victoryConditions = new Array<EndCondition>();
		defeatConditions = new Array<EndCondition>();
		enemies = new Array<Unit>();
		initialPositions = new Array<Point>();
		shop = new Array<Item>();
	}

	public MapData(String fileName, boolean finished, String name, Player player, String preparationsMusicFilename,
			String battleMusicFilename, Array<EndCondition> victoryConditions, Array<EndCondition> defeatConditions,
			Array<Unit> enemies, Array<Point> initialPositions, Array<Item> shop) {
		super();
		this.fileName = fileName;
		this.finished = finished;
		this.name = name;
		this.player = player;
		this.preparationsMusicFilename = preparationsMusicFilename;
		this.battleMusicFilename = battleMusicFilename;
		this.victoryConditions = victoryConditions;
		this.defeatConditions = defeatConditions;
		this.enemies = enemies;
		this.initialPositions = initialPositions;
		this.shop = shop;
	}
	
	public String getFileName() {
		return fileName;
	}
	
	public boolean isFinished() {
		return finished;
	}
	
	public String getName() {
		return name;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public String getPreparationsMusicFilename() {
		return preparationsMusicFilename;
	}
	
	public String getBattleMusicFilename() {
		return battleMusicFilename;
	}

	public Array<EndCondition> getVictoryConditions() {
		return victoryConditions;
	}

	public Array<EndCondition> getDefeatConditions() {
		return defeatConditions;
	}

	public Array<Unit> getEnemies() {
		return enemies;
	}

	public Array<Point> getInitialPositions() {
		return initialPositions;
	}

	public Array<Item> getShop() {
		return shop;
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public void setFinished(boolean finished) {
		this.finished = finished;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setPlayer(Player player) {
		this.player = player;
	}
	
	public void setPreparationsMusicFilepath(String preparationsMusicFilepath) {
		this.preparationsMusicFilename = preparationsMusicFilepath;
	}

	public void setBattleMusicFilepath(String battleMusicFilepath) {
		this.battleMusicFilename = battleMusicFilepath;
	}

	public void setShop(Array<Item> shop) {
		this.shop = shop;
	}
	
	public void addInitialPosition(Point position) {
		initialPositions.add(position);
	}
	
	public void removeInitialPosition(Point position) {
		initialPositions.removeValue(position, false);
	}
	
	public void addVictoryCondition(String name) {
		victoryConditions.clear();
		victoryConditions.add(createEndCondition(name, true));
	}
	
	public void addDefeatCondition(String name) {
		defeatConditions.clear();
		defeatConditions.add(createEndCondition(name, false));
	}
	
	private EndCondition createEndCondition(String name, boolean winCondition) {
		switch(name) {
		case "DefeatAll":
			return new DefeatAllEndCondition(winCondition);
		case "DefeatCommander":
			return new DefeatCommanderEndCondition(winCondition);
		default:
			return null;
		}
	}
	
	public String toString() {
		return name;
	}

	@Override
	public void write(Json json) {
		json.writeValue("fileName", fileName);
		json.writeValue("finished", finished);
		json.writeValue("name", name);
		json.writeValue("player", player);
		json.writeValue("preparationsMusic", preparationsMusicFilename);
		json.writeValue("battleMusic", battleMusicFilename);
		json.writeArrayStart("victoryConditions");
		for (EndCondition condition : victoryConditions)
			json.writeValue(condition, EndCondition.class);
		json.writeArrayEnd();
		json.writeArrayStart("defeatConditions");
		for (EndCondition condition : defeatConditions)
			json.writeValue(condition, EndCondition.class);
		json.writeArrayEnd();
		json.writeArrayStart("enemies");
		for (Unit enemy : enemies)
			json.writeValue(enemy);
		json.writeArrayEnd();
		json.writeArrayStart("initialPositions");
		for (Point position : initialPositions) {
			json.writeArrayStart();
			json.writeValue(position.x);
			json.writeValue(position.y);
			json.writeArrayEnd();
		}
		json.writeArrayEnd();
		json.writeArrayStart("shop");
		for (Item item : shop) {
			json.writeArrayStart();
			if (item instanceof Weapon)
				json.writeValue(((Weapon)item).getWeaponId());
			else
				json.writeValue(-1);
			json.writeValue(item.getId());
			json.writeArrayEnd();
		}
		json.writeArrayEnd();
	}
	
	private void loadConditions(Json json, JsonValue conditions, Array<EndCondition> conditionsArray) {
		int i = 0;
		JsonValue condition = conditions.get(i);
		while (condition != null) {
			conditionsArray.add(json.readValue(EndCondition.class, condition));
			i++;
			condition = conditions.get(i);
		}
	}
	
	private void loadInitialPositions(JsonValue json) {
		int i=0;
		JsonValue point = json.get(0);
		while (point != null) {
			initialPositions.add(new Point(point.getInt(0), point.getInt(1)));
			i++;
			point = json.get(i);
		}
	}
	
	private void loadShop(JsonValue json) {
		JsonValue item = json.get(0);
		int i = 0;
		while (item != null) {
			shop.add(ItemFactory.getItem(item.getInt(0), item.getInt(1)));
			i++;
			item = json.get(i);
		}
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		fileName = jsonData.getString("fileName");
		finished = jsonData.getBoolean("finished");
		name = jsonData.getString("name");
		player = json.fromJson(Player.class, jsonData.toJson(OutputType.json));
		preparationsMusicFilename = jsonData.getString("preparationsMusic", null);
		battleMusicFilename = jsonData.getString("battleMusic", null);
		loadConditions(json, jsonData.get("victoryConditions"), victoryConditions);
		loadConditions(json, jsonData.get("defeatConditions"), defeatConditions);
		enemies = Barracks.getMapEnemies(jsonData.get("enemies"));
		loadInitialPositions(jsonData.get("initialPositions"));
		loadShop(jsonData.get("shop"));
	}
	

}
