package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.game.control.GameController;
import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;
import es.flameorb.items.Item;
import es.flameorb.units.Unit;

/**
 * Muestra la ventana de intercambio de objetos con el carro.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class StockManagementMapState extends MapState {
	
	private ItemExchangeWindow window;
	private Unit user;
	private boolean fromPreparationMenu;

	public StockManagementMapState(Field field, Unit user, boolean fromPreparationMenu) {
		super(field);
		this.user = user;
		this.fromPreparationMenu = fromPreparationMenu;
		window = new ItemExchangeWindow(Language.get("Stock"), this,
				user, GameController.getGameController().getPlayerStock());
		window.setPosition(getCenteredXPosition(window.getWidth()), getCenteredYPosition(window.getHeight()));
		window.setVisible(true);
		field.addActor(window);
	}

	@Override
	public void acceptAction() {
		Item selected = window.getSelected();
		if (selected != null) {
			field.setMapState(new ItemExchangeItemSelectedMapState(field, user, null, selected, window, this));
		}
	}

	@Override
	public void cancelAction() {
		user.equipWeapon();
		window.setVisible(false);
		field.removeActor(window);
		if (fromPreparationMenu)
			field.setMapState(new StockManagementUnitSelectionMapState(field));
		else
			field.setMapState(new UnitMenuDisplayedMapState(field, field.movementOrigin, user));
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		window.moveSelection(keycode);
		if (keycode == KeyBinding.getAccept())
			acceptAction();
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		window.setClickedList();
		acceptAction();
	}

}
