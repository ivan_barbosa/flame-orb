package es.flameorb.game.map;

import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.scenes.scene2d.InputEvent;

import es.flameorb.game.map.Point;
import es.flameorb.general.KeyBinding;
import es.flameorb.units.Unit;

/**
 * Estado del mapa en el que se muestra las celdas a las que la unidad seleccionada
 * puede moverse, pudiendo elegir aquella a la que jugador quiere que se desplace.
 * @author ivan
 *
 */
public class MovementOnMapState extends MovementMapState {
	
	private Point origin;
	private Unit unit;

	/**
	 * Construye este estado del mapa.
	 * @param field - El mapa.
	 * @param origin - La posición inicial de la unidad.
	 * @param unit - La unidad.
	 */
	public MovementOnMapState(Field field, Point origin, Unit unit) {
		super(field);
		this.origin = origin;
		this.unit = unit;
		field.illuminateMovCells(origin);
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int count, int button) {
		if (button == Buttons.LEFT) {
			if (field.isMovementPossible(unit))
				acceptAction();
			else
				cancelAction();
		}
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept()) {
			if (field.isMovementPossible(unit))
				acceptAction();
		}
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	/**
	 * Mueve la unidad seleccionada a la posición indicada.
	 */
	@Override
	public void acceptAction() {
		cursorDirection = -1;
		field.removeListener(keyboardListener);
		field.removeListener(touchListener);
		field.shutdownMovCells();
		field.movementOrigin = origin;
		new MovementDisplayer(field, unit, origin, field.cursor.getMapPosition(), null);
	}

	/**
	 * Vuelve al InitialMapState.
	 */
	@Override
	public void cancelAction() {
		field.shutdownMovCells();
		field.setMapState(new InitialMapState(field));
	}
}
