package es.flameorb.game.map;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;

import es.flameorb.general.GamePreferences;
import es.flameorb.general.KeyBinding;
import es.flameorb.units.Unit;

/**
 * Clase base de la que heredan todos los estados del mapa, que encapsula los métodos
 * comunes empleados por varios de los diversos estados. Todos deben definir los métodos
 * acceptAction and cancelAction, que hacen avanzar al estado siguiente o retroceder al
 * anterior, respectivamente.
 * @author Iván Barbosa Gutiérrez
 *
 */
public abstract class MapState {
	
	protected Field field;
	protected KeyboardListener keyboardListener;
	protected TouchListener touchListener;
	protected boolean moveable;
	protected long keyDownStartTime;
	protected int cursorDirection;
	protected static int CELLS_WIDTH = (int) (GamePreferences.getWorldSize().x-2);
	protected static int CELLS_HEIGHT = (int) (GamePreferences.getWorldSize().y-2);
	//protected static int ZOOM_LEVEL = 3;
	
	/**
	 * Constructor base de MapState, que recibe el mapa cuyo estado representa.
	 * @param field - El mapa al que pertenece este estado.
	 */
	public MapState(Field field) {
		this.field = field;
		keyboardListener = new KeyboardListener();
		touchListener = new TouchListener();
	}
	
	public KeyboardListener getKeyboardListener() {
		return keyboardListener;
	}
	
	public TouchListener getTouchListener() {
		return touchListener;
	}

	/**
	 * Cambia el estado del mapa actual por el que muestra los detalles de la
	 * unidad.
	 * @param unit - La unidad cuyos detalles se van a mostrar.
	 */
	public void displayUnitDetails(Unit unit) {
		field.setMapState(new UnitDetailMapState(field, unit, this));
	}
	
	public float getCenteredXPosition(float windowWidth) {
		return Math.round(((CELLS_WIDTH + 2) * 32 - windowWidth) / 2) + field.camera.position.x - field.cameraInitialPosition.x;
	}
	
	public float getCenteredYPosition(float windowHeight) {
		return Math.round(((CELLS_HEIGHT + 2) * 32 - windowHeight) / 2) + field.camera.position.y - field.cameraInitialPosition.y;
	}
	
	/**
	 * Mueve la cámara tantos píxeles como los indicados.
	 * @param x - Los píxeles a mover en dirección x.
	 * @param y - Los píxeles a mover en dirección y.
	 */
	protected void moveCamera(int x, int y) {
		moveCamera((float)x, (float)y);
	}
	
	/**
	 * Mueve la cámara tantos píxeles como los indicados.
	 * @param x - Los píxeles a mover en dirección x.
	 * @param y - Los píxeles a mover en dirección y.
	 */
	protected void moveCamera(float x, float y) {
		field.camera.translate(x, y);
		field.adjustWindows(x, y);
	}
	
	/**
	 * Refresca la información de la ventanas de información del terreno y de unidad al
	 * mover el cursor de casilla.
	 */
	public void refreshInfoWindows() {
		Terrain properties = field.getCellProperties(field.cursor.getMapPosition());
		if (properties != null)
			field.terrainInfo.setText(properties.getTerrainInfo());
		else
			field.terrainInfo.setText(null);
		field.unitInfo.refreshTable(field.unitsPositions.getUnit(field.cursor.getMapPosition()));
	}
	
	/*
	 * Realiza un zoom sobre el mapa, acercándola o alejándola.
	 * @param keycode - El botón pulsado.
	 *
	private void zoom(int keycode) {
		if (keycode == KeyBinding.getZoomIn()) {
			//zoom in
			if (ZOOM_LEVEL != 5) {
				ZOOM_LEVEL++;
				changeZoom();
				field.view.apply(false);
				adjustCameraZoom(-32, -16f);
			}
		}
		else if (keycode == KeyBinding.getZoomOut()) {
			//zoom out
			if (ZOOM_LEVEL != 1) {
				ZOOM_LEVEL--;
				changeZoom();
				field.view.apply(false);
				adjustCameraZoom(32f, 16f);
			}
		}
	}
	
	/**
	 * Cambia el zoom sobre el mapa.
	 *
	private void changeZoom() {
		switch (ZOOM_LEVEL) {
		case 1:
			field.view.setWorldSize(20*32, 11*32);
			CELLS_WIDTH = 18;
			CELLS_HEIGHT = 9;
			break;
		case 2:
			field.view.setWorldSize(18*32, 10*32);
			CELLS_WIDTH = 16;
			CELLS_HEIGHT = 8;
			break;
		case 3:
			field.view.setWorldSize(16*32, 9*32);
			CELLS_WIDTH = 14;
			CELLS_HEIGHT = 7;
			break;
		case 4:
			field.view.setWorldSize(14*32, 8*32);
			CELLS_WIDTH = 12;
			CELLS_HEIGHT = 6;
			break;
		case 5:
			field.view.setWorldSize(12*32, 7*32);
			CELLS_WIDTH = 10;
			CELLS_HEIGHT = 5;
			break;
		}
	}
	
	/*
	 * Ajusta las ventanas tras el zoom.
	 * @param x - Los píxeles movidos en el eje X.
	 * @param y - Los píxeles movidos en el eje Y.
	 *
	private void adjustCameraZoom(float x, float y) {
		field.camera.translate(x, y);
		field.adjustWindows(0f, y*2);
	}*/
	
	/**
	 * Mueve la cámara en la dirección de la tecla que se ha pulsado si el cursor
	 * llega a cierto punto de la ventana.
	 * @param direction - La tecla de dirección pulsada.
	 */
	private void adjustCameraKeyboard(int direction) {
		if (direction == KeyBinding.getUp()) {
			if (field.cursor.getYRel() == CELLS_HEIGHT && field.cursor.getYMap() <= field.mapHeight-3)
				moveCamera(0, 32);
			else if (field.cursor.getYMap() != field.mapHeight-1)
				field.cursor.addYRel(1);
		}
		else if (direction == KeyBinding.getDown()) {
			if (field.cursor.getYRel() == 1 && field.cursor.getYMap() >= 2)
				moveCamera(0, -32);
			else if (field.cursor.getYMap() != 0)
				field.cursor.addYRel(-1);
		}
		else if (direction == KeyBinding.getLeft()) {
			if (field.cursor.getXRel() == 1 && field.cursor.getXMap() >= 2)
				moveCamera(-32, 0);
			else if (field.cursor.getXMap() != 0)
				field.cursor.addXRel(-1);
		}
		else if (direction == KeyBinding.getRight()) {
			if (field.cursor.getXRel() == CELLS_WIDTH && field.cursor.getXMap() <= field.mapWidth-3)
				moveCamera(32, 0);
			else if (field.cursor.getXMap() != field.mapWidth-1)
				field.cursor.addXRel(1);
		}
	}
	
	/**
	 * Recoloca el cursor en caso de que se salga del mapa.
	 */
	private void adjustCursorBorder() {
		if (field.cursor.getXMap() < 0)
			field.cursor.setXMap(0);
		if (field.cursor.getYMap() < 0)
			field.cursor.setYMap(0);
		if (field.cursor.getXMap() >= field.mapWidth)
			field.cursor.setXMap(field.mapWidth - 1);
		if (field.cursor.getYMap() >= field.mapHeight)
			field.cursor.setYMap(field.mapHeight - 1);
	}
	
	private void setCursorDirection(int keycode) {
		if (keycode == KeyBinding.getUp())
			cursorDirection = KeyBinding.getUp();
		else if (keycode == KeyBinding.getDown())
			cursorDirection = KeyBinding.getDown();
		else if (keycode == KeyBinding.getLeft())
			cursorDirection = KeyBinding.getLeft();
		else if (keycode == KeyBinding.getRight())
			cursorDirection = KeyBinding.getRight();
	}
	
	/**
	 * Mueve el cursor en la dirección pulsada en el teclado.
	 * @param keycode - La tecla pulsada.
	 */
	public void moveCursorKeyboard(int keycode) {
		adjustCameraKeyboard(keycode);
		if (keycode == KeyBinding.getUp())
			moveCursor(field.cursor.getXMap(), field.cursor.getYMap()+1);
		else if (keycode == KeyBinding.getDown())
			moveCursor(field.cursor.getXMap(), field.cursor.getYMap()-1);
		else if (keycode == KeyBinding.getLeft())
			moveCursor(field.cursor.getXMap()-1, field.cursor.getYMap());
		else if (keycode == KeyBinding.getRight())
			moveCursor(field.cursor.getXMap()+1, field.cursor.getYMap());
	}
	
	private void moveCursorMouse(int x, int y) {
		int diferenceX = x - field.cursor.getXMap();
		int diferenceY = y - field.cursor.getYMap();
		field.cursor.addXRel(diferenceX);
		field.cursor.addYRel(diferenceY);
		if (field.cursor.getYRel() == CELLS_HEIGHT+1 && field.cursor.getYMap() <= field.mapHeight-3) {
			moveCamera(0, 32);
			field.cursor.setYRel(CELLS_HEIGHT);
		}
		else if (field.cursor.getYRel() == 0 && field.cursor.getYMap() >= 2) {
			moveCamera(0, -32);
			field.cursor.setYRel(1);
		}
		if (field.cursor.getXRel() == 0 && field.cursor.getXMap() >= 2) {
			moveCamera(-32, 0);
			field.cursor.setXRel(1);
		}
		else if (field.cursor.getXRel() == CELLS_WIDTH+1 && field.cursor.getXMap() <= field.mapWidth-3) {
			moveCamera(32, 0);
			field.cursor.setXRel(CELLS_WIDTH);
		}
		moveCursor(x, y);
	}
	
	/**
	 * Mueve el cursor a la posición del mapa indicada.
	 * @param x - La posición X del mapa.
	 * @param y - La posición Y del mapa.
	 */
	public void moveCursor(int x, int y) {
		field.cursor.setXMap(x);
		field.cursor.setYMap(y);
		adjustCursorBorder();
		field.checkCollisionCursorWindows();
		refreshInfoWindows();
	}
	
	public void checkMoveCursor() {
		if (cursorDirection != -1 && System.currentTimeMillis() - keyDownStartTime >= 100) {
			moveCursorKeyboard(cursorDirection);
			keyDownStartTime = System.currentTimeMillis();
		}
	}
	
	/**
	 * La acción a realizar cuando se continúa la acción.
	 */
	public abstract void acceptAction();
	
	/**
	 * La acción a realizar cuando se cancela la acción.
	 */
	public abstract void cancelAction();
	
	public abstract boolean keyboardAction(InputEvent event, int keycode);
	
	public abstract void touchAction(InputEvent event, float x, float y, int pointer, int button);
	
	private class KeyboardListener extends InputListener {
		
		@Override
		public boolean keyDown(InputEvent event, int keycode) {
			if (moveable) {
				//zoom(keycode);
				keyDownStartTime = System.currentTimeMillis();
				setCursorDirection(keycode);
				moveCursorKeyboard(keycode);
			}
			return keyboardAction(event, keycode);
		}

		@Override
		public boolean keyUp(InputEvent event, int keycode) {
			cursorDirection = -1;
			return true;
		}

	}
	
	private class TouchListener extends ActorGestureListener {
		
		@Override
		public void tap(InputEvent event, float x, float y, int count, int button) {
			if (button == Buttons.LEFT)
				touchAction(event, x, y, count, button);
			else if (button == Buttons.RIGHT)
				cancelAction();
		}

		@Override
		public void touchDown(InputEvent event, float x, float y, int pointer, int button) {
			if (moveable) {
				moveCursorMouse((int)x/32, (int)y/32);
			}
		}

		@Override
		public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
			if (moveable)
				moveCursorMouse((int)x/32, (int)y/32);
		}

		@Override
		public void fling(InputEvent event, float velocityX, float velocityY, int button) {
			if (Gdx.app.getType() == ApplicationType.Android && moveable)
				moveCamera(-velocityX, -velocityY);
		}
		
	}

}
