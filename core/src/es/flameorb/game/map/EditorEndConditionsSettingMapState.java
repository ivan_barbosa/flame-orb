package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import es.flameorb.game.control.GameController;
import es.flameorb.game.endconditions.EndCondition;
import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;

public class EditorEndConditionsSettingMapState extends MapState {
	
	private Window window;
	private SelectBox<Conditions> winConditionSelectBox;
	private SelectBox<Conditions> loseConditionSelectBox;

	public EditorEndConditionsSettingMapState(Field field) {
		super(field);
		window = UIFactory.newWindow(Language.get("EndConditions"), 2);
		window.add(UIFactory.newLabel(Language.get("VictoryCondition")));
		window.add(UIFactory.newLabel(Language.get("DefeatCondition")));
		window.row();
		winConditionSelectBox = UIFactory.newSelectBox(Conditions.values());
		if (GameController.getGameController().getMapData().getVictoryConditions().size != 0)
			setSelectedCondition(GameController.getGameController().getMapData().getVictoryConditions().first(), winConditionSelectBox);
		loseConditionSelectBox = UIFactory.newSelectBox(Conditions.values());
		if (GameController.getGameController().getMapData().getDefeatConditions().size != 0)
			setSelectedCondition(GameController.getGameController().getMapData().getDefeatConditions().first(), winConditionSelectBox);
		window.add(winConditionSelectBox).padRight(4f);
		window.add(loseConditionSelectBox);
		window.row();
		TextButton accept = UIFactory.newTextButton(Language.get("Accept"));
		accept.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				acceptAction();
			}
		});
		TextButton cancel = UIFactory.newTextButton(Language.get("Cancel"));
		cancel.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				cancelAction();
			}
		});
		window.add(accept);
		window.add(cancel);
		window.pack();
		window.setPosition(getCenteredXPosition(window.getWidth()), getCenteredYPosition(window.getHeight()));
		window.setVisible(true);
		field.addActor(window);
	}
	
	private void setSelectedCondition(EndCondition condition, SelectBox<Conditions> selectBox) {
		for (Conditions endCondition : Conditions.values()) {
			if (condition.getClass().getName().equals(endCondition.getClassName())) {
				selectBox.setSelected(endCondition);
				continue;
			}
		}
	}

	@Override
	public void acceptAction() {
		GameController.getGameController().getMapData().addVictoryCondition(winConditionSelectBox.getSelected().getName());
		GameController.getGameController().getMapData().addDefeatCondition(loseConditionSelectBox.getSelected().getName());
		cancelAction();
	}

	@Override
	public void cancelAction() {
		window.setVisible(false);
		field.removeActor(window);
		field.setMapState(new EditorMenuMapState(field));
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept())
			acceptAction();
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub

	}
	
	private enum Conditions {
		DEFEAT_ALL("DefeatAll"), DEFEAT_COMMANDER("DefeatCommander");
		
		private final String name;
		private String className;
		
		private Conditions(String name) {
			this.name = name;
			className = "es.flameorb.game.endconditions." + name + "EndCondition";
		}
		
		public String getName() {
			return name;
		}
		
		public String getClassName() {
			return className;
		}
		
		public String toString() {
			return Language.get(name);
		}
	}

}
