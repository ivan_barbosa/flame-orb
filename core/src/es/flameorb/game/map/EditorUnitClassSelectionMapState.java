package es.flameorb.game.map;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;
import es.flameorb.units.Barracks;
import es.flameorb.units.EnemyUnit;
import es.flameorb.units.FEClass;
import es.flameorb.units.PlayerUnit;

public class EditorUnitClassSelectionMapState extends MapState {
	
	private EnemyUnit enemyUnit;
	private PlayerUnit playerUnit;
	private boolean player;
	private Window window;
	private SelectBox<FEClass> selectBox;
	private CheckBox checkBox;

	public EditorUnitClassSelectionMapState(Field field, EnemyUnit enemyUnit, PlayerUnit playerUnit, boolean player) {
		super(field);
		this.enemyUnit = enemyUnit;
		this.playerUnit = playerUnit;
		this.player = player;
		window = UIFactory.newWindow(Language.get("ChooseAClass"), 2);
		window.add(UIFactory.newLabel(Language.get("Class")+":"));
		selectBox = UIFactory.newSelectBox(Barracks.getAllFEClasses());
		if (enemyUnit != null)
			selectBox.setSelected(enemyUnit.getFeclass());
		if (playerUnit != null)
			selectBox.setSelected(playerUnit.getFeclass());
		selectBox.setMaxListCount(13);
		window.add(selectBox);
		window.row();
		checkBox = UIFactory.newCheckBox(Language.get("UseDefaults"));
		if (!player) {
			checkBox.setChecked(true);
			window.add(checkBox).colspan(2);
			window.row();
		}
		TextButton acceptButton = UIFactory.newTextButton(Language.get("Accept"));
		acceptButton.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				acceptAction();
			}
			
		});
		window.add(acceptButton);
		TextButton cancelButton = UIFactory.newTextButton(Language.get("Back"));
		cancelButton.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				cancelAction();
			}
			
		});
		window.add(cancelButton);
		window.pack();
		window.setPosition(getCenteredXPosition(window.getWidth()), getCenteredYPosition(window.getHeight()));
		window.setVisible(true);
		field.addActor(window);
	}

	@Override
	public void acceptAction() {
		if (selectBox.getSelected() != null) {
			window.setVisible(false);
			field.removeActor(window);
			field.setMapState(new EditorUnitCreatorMapState(field, enemyUnit, playerUnit,
				player, selectBox.getSelected(), checkBox.isChecked()));
		}
	}

	@Override
	public void cancelAction() {
		window.setVisible(false);
		field.removeActor(window);
		if (player) {
			if (playerUnit == null)
				field.setMapState(new EditorPlayerUnitsMapState(field));
			else
				field.setMapState(new EditorPlayerUnitsOptionsMapState(field, playerUnit));
		}
		else
			field.setMapState(new EditorEnemyCreatorMenuMapState(field, enemyUnit));
	}

	@Override
	public boolean keyboardAction(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept())
			acceptAction();
		else if (keycode == KeyBinding.getCancel())
			cancelAction();
		return true;
	}

	@Override
	public void touchAction(InputEvent event, float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
	}

}
