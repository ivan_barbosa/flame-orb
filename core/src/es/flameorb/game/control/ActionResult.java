package es.flameorb.game.control;

import es.flameorb.game.calculators.Pair;

/**
 * Esta clase encapsula los resultados de la acción de una unidad..
 * @author Iván Barbosa Gutiérrez
 *
 */
public class ActionResult {
	
	private boolean gameEnd, victory, turnEnd;
	
	public ActionResult(Pair<Boolean, Boolean> gameEnd, boolean turnEnd) {
		this.gameEnd = gameEnd.first;
		this.victory = gameEnd.second;
		this.turnEnd = turnEnd;
	}
	
	public ActionResult(boolean gameEnd, boolean victory, boolean turnEnd) {
		this.gameEnd = gameEnd;
		this.victory = victory;
		this.turnEnd = turnEnd;
	}
	
	public boolean hasGameEnd() {
		return gameEnd;
	}
	
	public boolean isVictory() {
		return victory;
	}
	
	public boolean hasTurnEnd() {
		return turnEnd;
	}

}
