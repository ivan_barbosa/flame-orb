package es.flameorb.game.control;

import com.badlogic.gdx.utils.Array;

import es.flameorb.units.Unit;

/**
 * Esta clase lleva el control de los turnos del juego, llevando la cuenta,
 * cambiando el turno del jugador a la IA y viceversa o
 * realizando las acciones pertinentes cuando finaliza un turno.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class TurnController {
	
	private GameController controller;
	private int turnCount, remainUnits;
	private boolean playerTurn;
	
	/**
	 * Construye un nuevo TurnController asociado al 
	 * GameController dado.
	 * @param controller - El GameController asociado.
	 */
	public TurnController(GameController controller) {
		this.controller = controller;
		turnCount = 1;
		playerTurn = true;
		remainUnits = controller.getPlayerUnits().size;
	}
	
	/**
	 * Retira de las unidades que acaban de finalizar su turno la bandera
	 * de fin de acción para que recuperen su color original.
	 * @param units - La lista de unidades que finalizó su turno.
	 */
	private void clearEndedActionFlags(Array<Unit> units) {
		for (Unit unit : units)
			unit.setEndedAction(false);
	}
	
	/**
	 * Actualiza los cambios en las estadísticas de las unidades.
	 * @param units - La lista de unidades.
	 */
	private void updateTemporaryStatChanges(Array<Unit> units) {
		for (Unit unit : units)
			unit.getStatsChanges().updateTemporaryChanges();
	}
	
	/**
	 * Finaliza el turno actual del jugador o la IA y le pasa el
	 * turno al contrario.
	 */
	public void endTurn() {
		if (playerTurn) {
			remainUnits = controller.getEnemyUnits().size;
			playerTurn = false;
			controller.updateMovementRange();
			controller.applyTerrainEffects(false);
			updateTemporaryStatChanges(controller.getEnemyUnits());
			clearEndedActionFlags(controller.getPlayerUnits());
		}
		else {
			remainUnits = controller.getPlayerUnits().size;
			turnCount++;
			playerTurn = true;
			controller.updateMovementRange();
			controller.applyTerrainEffects(true);
			updateTemporaryStatChanges(controller.getPlayerUnits());
			clearEndedActionFlags(controller.getEnemyUnits());
		}
	}
	
	/**
	 * Returns the number of turns of the game.
	 * @return The number of turns.
	 */
	public int getTurnCount() {
		return turnCount;
	}
	
	/**
	 * Returns the number of units which hasn't done any
	 * action yet.
	 * @return The number of remain units.
	 */
	public int getRemainUnits() {
		return remainUnits;
	}
	
	/**
	 * Returns if now it's the player turn.
	 * @return If it's the player turn.
	 */
	public boolean isPlayerTurn() {
		return playerTurn;
	}
	
	/**
	 * Se llama cuando una unidad termina su acción, reduciendo
	 * el número de unidades que faltan por actuar. Si el número
	 * de unidades restantes llega a cero, llama a endTurn().
	 */
	public boolean unitEndAction() {
		remainUnits--;
		if (remainUnits == 0) {
			endTurn();
			return true;
		}
		return false;
	}

}
