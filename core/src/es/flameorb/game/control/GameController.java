package es.flameorb.game.control;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import es.flameorb.game.calculators.BattleCalculator;
import es.flameorb.game.calculators.BattleData;
import es.flameorb.game.calculators.BattleElements;
import es.flameorb.game.calculators.Pair;
import es.flameorb.game.calculators.RangeCalculator;
import es.flameorb.game.endconditions.DefeatAllEndCondition;
import es.flameorb.game.endconditions.DefeatCommanderEndCondition;
import es.flameorb.game.endconditions.EndCondition;
import es.flameorb.game.map.TerrainData;
import es.flameorb.game.map.Field;
import es.flameorb.game.map.MapData;
import es.flameorb.game.map.Point;
import es.flameorb.game.map.Terrain;
import es.flameorb.general.Player;
import es.flameorb.general.Stock;
import es.flameorb.items.Item;
import es.flameorb.items.Staff;
import es.flameorb.items.Weapon;
import es.flameorb.units.EnemyUnit;
import es.flameorb.units.Inventory;
import es.flameorb.units.PlayerUnit;
import es.flameorb.units.Stats;
import es.flameorb.units.Unit;
import es.flameorb.units.UnitPositionList;

/**
 * Esta clase se encarga del control de todas las acciones del juego,
 * encapsulando el jugador, el terreno de combate, las listas de unidades
 * y el mapa con todos los BattleData de todos los emparejamientos posibles
 * de unidades.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class GameController {
	
	private static GameController controller = new GameController();
	private FileHandle mapDataFile;
	private MapData mapData;
	private Field field;
	private TurnController turnController;
	private Array<Unit> playerUnits, enemy;
	private Unit playerCommander, enemyCommander;
	private UnitPositionList unitsPositions;
	private static Random rng = new Random();
	private boolean testMode;
	
	private GameController() {}
	
	public static GameController getGameController() {
		return controller;
	}
	
	public void onTestMode() {
		testMode = true;
	}
	
	public String getMapName() {
		return mapData.getName();
	}
	
	public void setMapName(String name) {
		mapData.setName(name);
	}
	
	public Array<Unit> getAllPlayerUnits() {
		return mapData.getPlayer().getUnits();
	}
	
	public Stock getPlayerStock() {
		return mapData.getPlayer().getStock();
	}
	
	public void setPlayerStock(Array<Item> items) {
		mapData.getPlayer().setStock(items);
	}
	
	public int getPlayerMoney() {
		return mapData.getPlayer().getMoney();
	}
	
	public void setPlayerMoney(int money) {
		mapData.getPlayer().setMoney(money);
	}
	
	public boolean buyItem(Item item) {
		return mapData.getPlayer().buyItem(item);
	}
	
	public void sellItem(Item item) {
		mapData.getPlayer().sellItem(item);
	}
	
	public void setPlayer(Player player) {
		mapData.setPlayer(player);
	}
	
	public Unit getPlayerCommander() {
		return playerCommander;
	}
	
	public Unit getEnemyCommander() {
		return enemyCommander;
	}
	
	public MapData getMapData() {
		return mapData;
	}
	
	public void addPlayerUnit(Unit unit) {
		mapData.getPlayer().addUnit(unit);
	}
	
	public void placePlayerUnit(Unit unit) {
		playerUnits.add(unit);
		unitsPositions.putUnit(unit);
		if (unit.isCommander())
			playerCommander = unit;
	}
	
	public void addEnemyUnit(Unit unit) {
		enemy.add(unit);
		unitsPositions.putUnit(unit);
	}
	
	public void deletePlayerUnit(Unit unit) {
		mapData.getPlayer().removeUnit(unit);
	}
	
	public void removePlayerUnit(Unit unit) {
		playerUnits.removeValue(unit, true);
		unitsPositions.deleteUnit(unit);
		if (unit.isCommander())
			playerCommander = null;
	}
	
	public void removeEnemyUnit(Unit unit) {
		enemy.removeValue(unit, true);
		unitsPositions.deleteUnit(unit);
	}
	
	public void createPlayer(Player player) {
		setPlayer(player);
	}
	
	public boolean arePromotionablePlayerUnits() {
		for (Unit unit : mapData.getPlayer().getUnits()) {
			if (((PlayerUnit)unit).canPromote())
				return true;
		}
		return false;
	}
	
	/**
	 * Crea un GameController que controlará el campo recibido.
	 * @param field - El campo de batalla.
	 * @param mapData - Los datos del mapa.
	 */
	public void loadMap(Field field, MapData mapData) {
		this.field = field;
		this.mapData = mapData;
		if (Gdx.app.getType() == ApplicationType.Android)
			mapDataFile = Gdx.files.external(mapData.getFileName()+".json");
		else
			mapDataFile = Gdx.files.local(mapData.getFileName()+".json");
		enemy = mapData.getEnemies();
		unitsPositions = new UnitPositionList(enemy, field.getMapLayer());
		field.setInitialPositions(mapData.getInitialPositions());
		playerUnits = new Array<Unit>();
	}
	
	/**
	 * Aplica la curación y la pérdida de PV según el terreno donde se
	 * encuentren las unidades. En función del turno, se aplicarán a las
	 * unidades del jugador o del enemigo.
	 * @param playerTurn - Si es el turno del jugador.
	 */
	public void applyTerrainEffects(boolean playerTurn) {
		Array<Unit> units;
		if (playerTurn)
			units = playerUnits;
		else
			units = enemy;
		for (Unit unit : units) {
			Terrain terrain = field.getCellProperties(unit.getMapPosition());
			if (terrain.getHealRate() > 0 && unit.getCurrentHP() != unit.getCurrentStat(Stats.HITPOINTS))
				unit.restoreHP((int)(unit.getCurrentStat(Stats.HITPOINTS) * (terrain.getHealRate()) / 100f));
			else if (terrain.isDamage() && unit.getCurrentHP() > 1)
				unit.loseHP((int)(unit.getCurrentStat(Stats.HITPOINTS) * 0.1), false);
		}
	}
	
	/**
	 * Inicia los parámetros que se necesitan para llevar a cabo la
	 * secuencia de ataque.
	 * @param attacker - La unidad que inicia el ataque.
	 * @param defender - La unidad que se defiende al inicio.
	 */
	public ActionResult beginAttack(Unit attacker, Unit defender) {
		Pair<Boolean, Boolean> gameEnded;
		int currentEnemyHP;
		int diference = getCurrentBattleData(attacker, defender).getASDiference();
		if (attacker.isPlayerUnit()) {
			currentEnemyHP = defender.getCurrentHP();
			gameEnded = endAttack((PlayerUnit)attacker, (EnemyUnit)defender, attackSecuence(attacker, defender, attacker.canEnemyCounterattack(defender), diference), currentEnemyHP);
		}
		else {
			currentEnemyHP = attacker.getCurrentHP();
			gameEnded = endAttack((PlayerUnit)defender, (EnemyUnit)attacker, attackSecuence(attacker, defender, attacker.canEnemyCounterattack(defender), diference), currentEnemyHP);
		}
		return new ActionResult(gameEnded, unitEndAction(attacker));
	}
	
	/**
	 * Este método representa a la secuencia de ataque, y se encarga de hacer la primera llamada
	 * al método recursivo que realmente realiza la acción.
	 * @param attacker - La unidad que inicia el ataque
	 * @param defender - La unidad que se defiende al inicio.
	 * @param inEnemyRange - Si el defensor puede responder al ataque recibido.
	 * @param asDiference - La diferencia en velocidad de ataque, que determina si hay ataque dobles.
	 * @return Si una unidad muere consecuencia de la batalla.
	 */
	public boolean attackSecuence(Unit attacker, Unit defender, boolean inEnemyRange, int asDiference) {
		return recursiveAttackSecuence(attacker, defender, inEnemyRange, asDiference, true, false);
	}
	
	/**
	 * Realiza la secuencia de combate, la cual es la siguiente:
	 * El atacante ataca, si su arma es de doble golpe, vuelve a atacar.
	 * Ahora el defensor pasa al ataque. Tras esto, se ve quién posee
	 * una velociadad de ataque 5 unidades mayor a la del otro, volviendo a
	 * atacar en caso de ello. La secuencia se interrumpe si una de las unidades
	 * muere durante el proceso, devolviendo true. Si nadie muere, se devuelve false.
	 * @param attacker - La unidad que inicia el ataque.
	 * @param defender - La unidad que se defiende al inicio.
	 * @param inEnemyRange - Si el enemigo puede contestar al ataque rival.
	 * @param asDiference - La diferencia en la velocidad de ataque de las unidades. 
	 * @param firstTime - Si el primer atacante ha realizado ya el ataque.
	 * @param attackSpeedChecked - Si ya se ha realizado la comprobación para determinar
	 * si la unidad con mayor velocidad de ataque vuelve a actuar.
	 * @return Si una unidad muere durante el combate.
	 */
	private boolean recursiveAttackSecuence(Unit attacker, Unit defender, boolean inEnemyRange, int asDiference, boolean firstTime, boolean attackSpeedChecked) {
		boolean defeated = tryAttack(attacker, defender);
		if (!defeated && attacker.getEquipedWeapon().isDoubleAttack())
			defeated = tryAttack(attacker, defender);
		if (!defeated) {
			if (firstTime && inEnemyRange)
				 defeated = recursiveAttackSecuence(defender, attacker, inEnemyRange, -asDiference, false, false);
			else {
				if (!attackSpeedChecked)
					if (asDiference >= 5)
						defeated = recursiveAttackSecuence(attacker, defender, inEnemyRange, asDiference, false, true);
					else if (asDiference <= -5 && inEnemyRange)
						defeated = recursiveAttackSecuence(defender, attacker, inEnemyRange, -asDiference, false, true);
			}	
		}
		return defeated;
	}
	
	/**
	 * Este método determina si el atacante llega a golpear al defensor,
	 * su ataque es crítico y/o el arma se rompe tras usarla.
	 * @param attacker - La unidad que ataca.
	 * @param defender - La unidad que se defiende
	 * @return Si el atacante logra derrotar al defensor.
	 */
	private boolean tryAttack(Unit attacker, Unit defender) {
		BattleLog.beginAttackText(attacker, defender);
		Weapon atWeapon = attacker.getEquipedWeapon();
		BattleData data = getCurrentBattleData(attacker, defender);
		if (rng.nextInt(100)+1 <= data.getHit()) {
			int damage = data.getDamage();
			if (damage < 0)
				damage = 0;
			if (rng.nextInt(100)+1 <= data.getCritical()) {
				BattleLog.criticalHitText();
				damage *= 3;
			}
			boolean killed = attacker.attack(defender, damage);
			boolean broken = atWeapon.waste();
			if (broken) {
				BattleLog.brokenWeaponText(atWeapon);
				attacker.usedUpItem();
			}
			if (attacker.isPlayerUnit()) {
				PlayerUnit playerUnit = (PlayerUnit)attacker;
				playerUnit.addWeaponExperience(atWeapon.getWeaponExp(), atWeapon.getWeaponId());
			}
			return killed;
		}
		else
			BattleLog.avoidAttackText(defender);
		return false;
	}
	
	/**
	 * Realiza las acciones posteriores al ataque según el resultado de la batalla.
	 * @param playerUnit - La unidad del jugador.
	 * @param enemyUnit - La unidad del enemigo. 
	 * @param result - El resultado de la batalla.
	 * @param initialEnemyHP - Los PV del enemigo al comienzo.
	 */
	public Pair<Boolean, Boolean> endAttack(PlayerUnit playerUnit, EnemyUnit enemyUnit, boolean result, int initialEnemyHP) {
		boolean endConditionRearched = false;
		if (playerUnit.getCurrentHP() == 0) {
			BattleLog.diedUnitText(playerUnit);
			if (playerUnit == playerCommander)
				playerCommander = null;
			playerUnits.removeValue(playerUnit, true);
			unitsPositions.deleteUnit(playerUnit);
			field.removeActor(playerUnit);
			updateEnemyMovementRange();
			for (EndCondition condition : mapData.getDefeatConditions())
				if (condition instanceof DefeatAllEndCondition || condition instanceof DefeatCommanderEndCondition) {
					endConditionRearched = condition.checkCondition(this);
					if (endConditionRearched)
						return new Pair<Boolean, Boolean>(true, false);
					else if (playerCommander == null)
						playerCommander = playerUnits.first();
				}
		}
		else {
			if (enemyUnit.getCurrentHP() == 0) {
				BattleLog.diedUnitText(enemyUnit);
				checkDropableItems(playerUnit, enemyUnit.getInventory());
				if (enemyUnit == enemyCommander)
					enemyCommander = null;
				enemy.removeValue(enemyUnit, true);
				unitsPositions.deleteUnit(enemyUnit);
				field.removeActor(enemyUnit);
				updatePlayerMovementRange();
			}
			if (playerUnit.getLevel() != 20 && initialEnemyHP != enemyUnit.getCurrentHP())
				playerUnit.addExperience(enemyUnit.getInternalLevel(), result);
			for (EndCondition condition : mapData.getVictoryConditions())
				if (condition instanceof DefeatAllEndCondition || condition instanceof DefeatCommanderEndCondition) {
					endConditionRearched = condition.checkCondition(this);
					if (endConditionRearched)
						return new Pair<Boolean, Boolean>(true, true);
				}
		}
		return new Pair<Boolean, Boolean>(false, false);
	}
	
	/**
	 * Comprueba si alguno de los objetos del enemigo puede ser recogido al ser derrotado
	 * y se le da al jugador
	 * @param unit - La unidad que derrotó al enemigo.
	 * @param enemyInventory - El inventario del enemigo.
	 */
	private void checkDropableItems(Unit unit, Inventory enemyInventory) {
		for (Item item : enemyInventory.getItems()) {
			if (item.isDropable())
				if (unit.getInventory().insertItem(item)) {
					BattleLog.obtainedItemText(unit, item);
					unit.getUnitDetails().inventoryUpdate();
				}
				else
					mapData.getPlayer().getStock().insertItem(item);
		}
	}
	
	/**
	 * Limpia las banderas de fin de turno de las unidades
	 */
	public void clearEndActionFlags() {
		for (Unit unit : mapData.getPlayer().getUnits())
			unit.setEndedAction(false);
	}
	
	public Field getField() {
		return field;
	}
	
	/**
	 * Devuelve el BattleData con todos los parámetros del combate de las
	 * unidades recibidas y las armas que usarán.
	 * @param attacker - La unidad que ataca.
	 * @param defender - La unidad que se defiende.
	 * @param atWeapon - El arma del atacante.
 	 * @param dfWeapon - El arma del defensor.
	 * @return El BattleData con los parámetros del combate.
	 */
	public static BattleData getBattleData(Unit attacker, Unit defender,
			Weapon atWeapon, Weapon dfWeapon) {
		BattleData initialData = BattleCalculator.getInitialBattleData(new BattleElements(attacker, defender, atWeapon, dfWeapon));
		BattleData variableData = getVariableBattleData(attacker, defender);
		return new BattleData(initialData.getDamage()+variableData.getDamage(),
				initialData.getHit()+variableData.getHit(),
				initialData.getCritical()+variableData.getCritical(),
				initialData.getASDiference()+variableData.getASDiference());
	}
	
	/**
	 * Devuelve el BattleData con todos los parámetros del combate de las
	 * unidades recibidas y las armas que llevan equipadas.
	 * @param attacker - La unidad que ataca.
	 * @param defender - La unidad que se defiende.
	 * @return El BattleData con los parámetros del combate.
	 */
	public static BattleData getCurrentBattleData(Unit attacker, Unit defender) {
		return getBattleData(attacker, defender, attacker.getEquipedWeapon(), defender.getEquipedWeapon());
	}
	
	/**
	 * Devuelve los datos variables del combate de las unidades recibidas
	 * @param attacker - La unidad que ataque.
	 * @param defender - La unidad que se defiende.
	 * @return El BattleData con los parámetros variables del combate.
	 */
	private static BattleData getVariableBattleData(Unit attacker, Unit defender) {
		if (!defender.isFlyer() && !controller.testMode) {
			Pair<Integer, Integer> terrainBonus = BattleCalculator
					.getTerrainBonus(TerrainData.getTerrain(getGameController().getField().getMapLayer(), defender.getXMap(), defender.getYMap()));
			return new BattleData(-terrainBonus.first, -terrainBonus.second, 0, 0);
		}
		else
			return new BattleData(0, 0, 0, 0);
	}
	
	/**
	 * Returns the list of player units that
	 * participate on this game.
	 * @return The list of player units.
	 */
	public Array<Unit> getPlayerUnits() {
		return playerUnits;
	}
	
	public Array<Unit> getNotPlacedUnits() {
		Array<Unit> units = new Array<Unit>(mapData.getPlayer().getUnits());
		for (Unit unit : playerUnits)
			units.removeValue(unit, true);
		return units;
	}
	
	/**
	 * Returns the list of enemy units of
	 * this scenario.
	 * @return The list of enemies.
	 */
	public Array<Unit> getEnemyUnits() {
		return enemy;
	}
	
	/**
	 * Returns the TurnController of this
	 * GameController
	 * @return The turn controller.
	 */
	public TurnController getTurnController() {
		return turnController;
	}
	
	public UnitPositionList getUnitsPositions() {
		return unitsPositions;
	}
	
	/**
	 * Da inicio al juego una vez que el jugador ha decidido sus unidades,
	 * calculando todos los datos de los emparejamientos entre las unidades,
	 * el movimiento de todas las unidades e inicializando el controlador de
	 * turnos y la lista de las posiciones de las unidades.
	 */
	public void startGame() {
		BattleCalculator.initialiteBattleParameters(playerUnits, enemy);
		for (Unit playerUnit : playerUnits)
			playerUnit.setEnemies(enemy);
		for (Unit enemyUnit : enemy) {
			enemyUnit.setEnemies(playerUnits);
			if (enemyUnit.isCommander())
				enemyCommander = enemyUnit;
		}
		turnController = new TurnController(this);
		updatePlayerMovementRange();
		updateEnemyMovementRange();
		field.setBattleMusic(mapData.getBattleMusicFilename());
	}
	
	/**
	 * Limpia todos los campos al término de una partida.
	 */
	public void endGame() {
		mapDataFile = null;
		mapData = null;
		field = null;
		turnController = null;
		playerUnits = null;
		enemy = null;
		playerCommander = null;
		enemyCommander = null;
		unitsPositions = null;
	}
	
	/**
	 * Guarda los datos del mapa al fichero y añade a este si el
	 * mapa es jugable.
	 */
	public void saveMapData() {
		boolean enemyCommander = false, allyCommander = false;
		for (Unit unit : mapData.getEnemies()) {
			if (unit.isCommander()) {
				enemyCommander = true;
				break;
			}
		}
		for (Unit unit : mapData.getPlayer().getUnits()) {
			if (unit.isCommander()) {
				allyCommander = true;
				break;
			}
		}
		if (enemyCommander && allyCommander && mapData.getInitialPositions().size > 0
				&& mapData.getInitialPositions().size > 0 && mapData.getDefeatConditions().size > 0)
			mapData.setFinished(true);
		else
			mapData.setFinished(false);
		mapDataFile.writeString(new Json().prettyPrint(mapData), false);
	}
	
	/**
	 * Activa la bandera de la unidad que indica que ya ha actuado y avisa al
	 * controlador del turno de que una unidad ha finalizado su acción.
	 * @param unit - La unidad que ha finalizado su acción.
	 */
	public boolean unitEndAction(Unit unit) {
		unit.setEndedAction(true);
		if (unit.isPlayerUnit())
			updateEnemyMovementRange();
		return turnController.unitEndAction();
	}
	
	/**
	 * Actualiza la posición de la unidad en la lista de posiciones y
	 * el rango de las armas de dicha unidad.
	 * @param unit - La unidad que se ha movido.
	 * @param origin - Su posición anterior.
	 */
	public void updatePosition(Unit unit, Point origin) {
		unitsPositions.updatePosition(unit, origin);
		unit.updateItemsRange(field.getMapLayer());
	}
	
	/**
	 * Actualiza el rango de movimiento de todas las unidades.
	 */
	public void updateMovementRange() {
		updatePlayerMovementRange();
		updateEnemyMovementRange();
	}
	
	/**
	 * Actualiza el rango de movimiento de las unidades del jugador.
	 */
	public void updatePlayerMovementRange() {
		RangeCalculator.updateMoveRange(field.getMapLayer(), playerUnits, unitsPositions);
	}
	
	/**
	 * Actualiza el rango de movimiento de las unidades del enemigo.
	 */
	public void updateEnemyMovementRange() {
		RangeCalculator.updateMoveRange(field.getMapLayer(), enemy, unitsPositions);
	}
	
	/**
	 * Realiza la secuencia de uso de un arma.
	 * @param user - El usuario del arma.
	 * @param receiver - El receptor del efecto.
	 */
	public void useItem(Unit user, Unit receiver, Item item) {
		user.useItem(receiver, item);
		if (user.isPlayerUnit() && item instanceof Staff) {
			PlayerUnit playerUnit = (PlayerUnit)user;
			playerUnit.addWeaponExperience(user.getEquipedWeapon().getWeaponExp(), user.getEquipedWeapon().getWeaponId());
		}
	}
}
