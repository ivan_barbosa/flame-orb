package es.flameorb.game.control;

import com.badlogic.gdx.scenes.scene2d.ui.Window;

import es.flameorb.game.map.MapState;
import es.flameorb.general.Language;
import es.flameorb.items.Item;
import es.flameorb.items.Weapon;
import es.flameorb.units.Stats;
import es.flameorb.units.Unit;

/**
 * Esta clase se encarga de colocar en el battle log los diversos
 * textos que va produciendo para mostrarlos.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class BattleLog {

	private static Window logWindow;
	
	private BattleLog() {
		
	}
	
	/**
	 * Adds the text given to the log window.
	 * @param text - The text to add.
	 */
	private static void addInfoToLog(String text) {
		logWindow.add(text);
		logWindow.row();
	}
	
	/**
	 * Crea y añade el texto que informa de la cantidad de experiencia
	 * que ha ganado una unidad.
	 * @param unit - La unidad que ganó experiencia.
	 * @param experience - La cantidad de experiencia recibida.
	 */
	public static void addExperienceText(Unit unit, int experience) {
		addInfoToLog(Language.format("addExperience", unit.getUnitName(), experience));
	}
	
	/**
	 * añada el texto que informa si es el turno del jugador o del enemigo
	 * en función del parámetro pasado.
	 * @param player - Si el el turno del jugador.
	 */
	public static void addTurnBanner(boolean player) {
		if (player)
			addInfoToLog(Language.get("PlayerTurn"));
		else
			addInfoToLog(Language.get("EnemyTurn"));
	}
	
	/**
	 * Añade el texto que informa que la unidad que ha sido
	 * atacada ha esquivado el ataque.
	 * @param defender - La unidad que ha esquivado el ataque.
	 */
	public static void avoidAttackText(Unit defender) {
		addInfoToLog(Language.format("avoidAttack", defender.getUnitName()));
	}
	
	/**
	 * Añade el texto que informa que unidad ataca a cual otra.
	 * @param attacker - La unidad que ataca.
	 * @param defender - La unidad que se defiende.
	 */
	public static void beginAttackText(Unit attacker, Unit defender) {
		addInfoToLog(Language.format("beginAttack", attacker.getUnitName(), defender.getUnitName()));
	}
	
	/**
	 * Añade el texto que informa que el arma que se ha usado
	 * se ha roto tras el ataque.
	 * @param weapon - El arma que se ha roto.
	 */
	public static void brokenWeaponText(Weapon weapon) {
		addInfoToLog(Language.format("brokenWeapon", Language.get(weapon.getName())));
	}
	
	/**
	 * Añade el texto de toca o pulsa para continuar.
	 */
	public static void touchOrPressToContinueText() {
		addInfoToLog(Language.get("continue"));
	}
	
	/**
	 * Añade el texto que informa que se ha realizado un
	 * golpe crítico.
	 */
	public static void criticalHitText() {
		addInfoToLog(Language.get("criticalHit"));
	}
	
	/**
	 * Añade el texto que informa de la derrota del jugador
	 * en la partida
	 */
	public static void defeatText() {
		addInfoToLog(Language.get("defeat"));
	}
	
	/**
	 * Añade el texto que informa que la unidad recibida
	 * se ha quedado sin PV y ha muerto en consecuencia.
	 * @param corpse - La unidad muerta.
	 */
	public static void diedUnitText(Unit corpse) {
		addInfoToLog(Language.format("diedUnit", corpse.getUnitName()));
	}
	
	/**
	 * Añade el texto que informa que la unidad recibida
	 * ha alcanzado un nuevo nivel.
	 * @param unit - El nombre de la unidad.
	 */
	public static void levelUpText(String unit) {
		addInfoToLog(Language.format("levelUp", unit));
	}
	
	/**
	 * Añade el texto que informa que la unidad recibida
	 * ha perdido la cantidad de PV indicada.
	 * @param defender - La unidad que perdió PV.
	 * @param damage - La cantidad de PV perdidos.
	 */
	public static void loseHPText(Unit defender, int damage) {
		addInfoToLog(Language.format("loseHP", defender.getUnitName(), damage));
	}
	
	/**
	 * Añade el texto que informa que una unidad ha promocionado a otra.
	 * @param unit - El nombre de la unidad
	 * @param promotion - El nombre de la clase a la que ha promocionado.
	 */
	public static void promotionText(String unit, String promotion) {
		addInfoToLog(Language.format("promotionDone", unit, Language.get(promotion)));
	}
	
	/**
	 * Añade el texto que informa de la obtención de un objeto
	 * @param player - La unidad que ha recibido el objeto.
	 * @param item - El objeto que se ha recibido.
	 */
	public static void obtainedItemText(Unit player, Item item) {
		addInfoToLog(Language.format("getItem", player.getUnitName(), Language.get(item.getName())));
	}
	
	/**
	 * Añade el texto que informa que la unidad recibida
	 * ha recuperado la cantidad de PV indicada.
	 * @param receiver
	 * @param restored
	 */
	public static void restoreHPText(Unit receiver, int restored) {
		addInfoToLog(Language.format("recoverHP", receiver.getUnitName(), restored));
	}
	
	/**
	 * Añade el texto que informa que el stat recibido ha subido
	 * la cantidada recibida.
	 * @param stat - El stat que ha subido.
	 * @param amount - La cantidad subida.
	 */
	public static void statUpText(Stats stat, int amount) {
		addInfoToLog(Language.format("statUp", Language.get(stat.name()), amount));
	}
	
	/**
	 * Añade el texto que informa de la victoria del jugador en la partida.
	 */
	public static void victoryText() {
		addInfoToLog(Language.get("victory"));
	}
	
	/**
	 * Sets the window where all the text will be written.
	 * @param window - The log window
	 */
	public static void setWindow(Window window) {
		logWindow = window;
		logWindow.left();
		logWindow.setVisible(false);
	}
	
	/**
	 * Shows the log window.
	 */
	public static void showWindow(MapState mapState) {
		logWindow.pack();
		logWindow.setPosition(mapState.getCenteredXPosition(logWindow.getWidth()), mapState.getCenteredYPosition(logWindow.getHeight()));
		logWindow.setVisible(true);
	}
	
	/**
	 * Hides the log window and removes all the text.
	 */
	public static void removeWindow() {
		logWindow.setVisible(false);
		logWindow.clear();
	}
	
}
