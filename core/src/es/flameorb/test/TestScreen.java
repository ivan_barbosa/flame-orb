package es.flameorb.test;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import es.flameorb.FlameOrbTest;
import es.flameorb.game.control.GameController;
import es.flameorb.gui.UIFactory;

public class TestScreen implements Screen {
	
	private Stage stage;
	
	public TestScreen(final FlameOrbTest game) {
		stage = new Stage(new ScreenViewport());
		UIFactory.setFontSize(32);
		GameController.getGameController().onTestMode();
		TestMenu menu = new TestMenu("Menú de test");
		stage.addActor(menu);
		stage.addActor(TestLog.getWindow());
		stage.addListener(new TestListener(menu));
		Gdx.input.setInputProcessor(stage);
		TestSuite.initializerTest();
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0.5f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		stage.dispose();
	}


}
