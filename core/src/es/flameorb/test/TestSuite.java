package es.flameorb.test;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter.OutputType;

import es.flameorb.game.calculators.BattleCalculator;
import es.flameorb.game.calculators.BattleData;
import es.flameorb.game.calculators.BattleElements;
import es.flameorb.game.control.BattleLog;
import es.flameorb.game.control.GameController;
import es.flameorb.general.GamePreferences;
import es.flameorb.gui.UIFactory;
import es.flameorb.items.Axe;
import es.flameorb.items.Bow;
import es.flameorb.items.Cannon;
import es.flameorb.items.Dark;
import es.flameorb.items.Fire;
import es.flameorb.items.Fist;
import es.flameorb.items.Gun;
import es.flameorb.items.ItemFactory;
import es.flameorb.items.Light;
import es.flameorb.items.Spear;
import es.flameorb.items.Staff;
import es.flameorb.items.Sword;
import es.flameorb.items.Thunder;
import es.flameorb.items.Wind;
import es.flameorb.units.EnemyUnit;
import es.flameorb.units.PlayerUnit;
import es.flameorb.units.Stats;
import es.flameorb.units.Unit;

public class TestSuite {
	
	public static void initializerTest() {
		JsonValue json = new JsonReader().parse(Gdx.files.internal("test/initializerTest.json"));
		TestLog.addTitle("Pruebas de carga de datos iniciales");
		if (!GamePreferences.getWeaponNames()
				.equals(new Array<String>(json.get("weaponNames").asStringArray())))
			TestLog.addText("Error en nombres de armas");
		if (!GamePreferences.getWeaponLevelsNames()
				.equals(new Array<String>(json.get("weaponLevelsNames").asStringArray())))
			TestLog.addText("Error en el nombre de los niveles de armas");
		if (!GamePreferences.getWeaponLevelsValues()
				.equals(new IntArray(json.get("weaponLevelsValues").asIntArray())))
			TestLog.addText("Error en el valor de los niveles de armas");
		if (GamePreferences.getClassStates() != json.getInt("classStates"))
			TestLog.addText("Error en el número de promociones");
		if (GamePreferences.getMaxPromotionOptions() != json.getInt("maxPromotionOptions"))
			TestLog.addText("Error en el número de opciones de promoción");
		if (GamePreferences.getInventoryMaxCapacity() != json.getInt("inventoryMaxCapacity"))
			TestLog.addText("Error en el número máximo de objetos del inventario");
		if (GamePreferences.getWorldSize().x != json.getInt("worldX"))
			TestLog.addText("Error en la anchura de casillas del mundo");
		if (GamePreferences.getWorldSize().y != json.getInt("worldY"))
			TestLog.addText("Error en la altura de casillas del mundo");
		if (ItemFactory.getItems(-1).size != json.getInt("itemNumber"))
			TestLog.addText("Error en la carga de objetos: ");
		if (ItemFactory.getItems(Sword.ID).size != json.getInt("swordNumber"))
			TestLog.addText("Error en la carga de espadas");
		if (ItemFactory.getItems(Spear.ID).size != json.getInt("spearNumber"))
			TestLog.addText("Error en la carga de lanzas");
		if (ItemFactory.getItems(Axe.ID).size != json.getInt("axeNumber"))
			TestLog.addText("Error en la carga de hachas");
		if (ItemFactory.getItems(Bow.ID).size != json.getInt("bowNumber"))
			TestLog.addText("Error en la carga de arcos");
		if (ItemFactory.getItems(Wind.ID).size != json.getInt("windNumber"))
			TestLog.addText("Error en la carga de vientos");
		if (ItemFactory.getItems(Fire.ID).size != json.getInt("fireNumber"))
			TestLog.addText("Error en la carga de fuegos");
		if (ItemFactory.getItems(Thunder.ID).size != json.getInt("thunderNumber"))
			TestLog.addText("Error en la carga de truenos");
		if (ItemFactory.getItems(Light.ID).size != json.getInt("lightNumber"))
			TestLog.addText("Error en la carga de luz");
		if (ItemFactory.getItems(Dark.ID).size != json.getInt("darkNumber"))
			TestLog.addText("Error en la carga de oscuridad");
		if (ItemFactory.getItems(Staff.ID).size != json.getInt("staffNumber"))
			TestLog.addText("Error en la carga de bastones");
		if (ItemFactory.getItems(Fist.ID).size != json.getInt("fistNumber"))
			TestLog.addText("Error en la carga de puños");
		if (ItemFactory.getItems(Gun.ID).size != json.getInt("gunNumber"))
			TestLog.addText("Error en la carga de pistolas");
		if (ItemFactory.getItems(Cannon.ID).size != json.getInt("cannonNumber"))
			TestLog.addText("Error en la carga de cañones");
		TestLog.showErrors();
	}
	
	public static void battleTest() {
		JsonValue json = new JsonReader().parse(Gdx.files.internal("test/battleTest.json"));
		TestLog.addTitle("Pruebas de combate");
		Json parser = new Json();
		BattleLog.setWindow(UIFactory.newWindow());
		BattleData playerData, enemyData;
		Unit playerUnit, enemyUnit;
		int index = 0;
		Array<Unit> playerUnits = new Array<Unit>();
		Array<Unit> enemyUnits = new Array<Unit>();
		Array<String> casesName = new Array<String>(json.get("cases").asStringArray());
		for (JsonValue player : json.get("playerUnits"))
			playerUnits.add(parser.fromJson(PlayerUnit.class, player.toJson(OutputType.json)));
		for (JsonValue enemy : json.get("enemyUnits"))
			enemyUnits.add(parser.fromJson(EnemyUnit.class, enemy.toJson(OutputType.json)));
		BattleCalculator.initialiteBattleParameters(playerUnits, enemyUnits);
		for (JsonValue results : json.get("battleResults")) {
			TestLog.addText("Caso " + (index+1) + ": " + casesName.get(index));
			playerUnit = playerUnits.get(index);
			enemyUnit = enemyUnits.get(index);
			playerData = BattleCalculator.getInitialBattleData(new BattleElements(playerUnit, enemyUnit,
					playerUnit.getEquipedWeapon(), enemyUnit.getEquipedWeapon()));
			if (playerData.getDamage() != results.getInt("damagePlayer"))
				TestLog.addText("Error en el daño de la unidad atacante: " + playerData.getDamage() + " != " + results.getInt("damagePlayer"));
			if (playerData.getHit() != results.getInt("hitPlayer"))
				TestLog.addText("Error en el golpeo de la unidad atacante: " + playerData.getHit() + " != " + results.getInt("hitPlayer"));
			if (playerData.getCritical() != results.getInt("critPlayer"))
				TestLog.addText("Error en el crítico de la unidad atacante: " + playerData.getCritical() + " != " + results.getInt("critPlayer"));
			if (playerData.getASDiference() != results.getInt("asPlayer"))
				TestLog.addText("Error en la diferencia de VA de la unidad atacante: " + playerData.getASDiference() + " != " + results.getInt("asPlayer"));
			enemyData = BattleCalculator.getInitialBattleData(new BattleElements(enemyUnit, playerUnit,
					enemyUnit.getEquipedWeapon(), playerUnit.getEquipedWeapon()));
			if (enemyData.getDamage() != results.getInt("damageEnemy"))
				TestLog.addText("Error en el daño de la unidad defensora: " + enemyData.getDamage() + " != " + results.getInt("damageEnemy"));
			if (enemyData.getHit() != results.getInt("hitEnemy"))
				TestLog.addText("Error en el golpeo de la unidad defensora: " + enemyData.getHit() + " != " + results.getInt("hitEnemy"));
			if (enemyData.getCritical() != results.getInt("critEnemy"))
				TestLog.addText("Error en el crítico de la unidad defensora: " + enemyData.getCritical() + " != " + results.getInt("critEnemy"));
			if (enemyData.getASDiference() != results.getInt("asEnemy"))
				TestLog.addText("Error en la diferencia de VA de la unidad defensora: " + enemyData.getASDiference() + " != " + results.getInt("asEnemy"));
			GameController.getGameController().attackSecuence(playerUnit, enemyUnit, true, playerData.getASDiference());
			checkBattle(index, results, playerUnit, enemyUnit, playerData, enemyData);
			index++;
		}
		TestLog.showErrors();
	}
	
	private static void checkBattle(int index, JsonValue results, Unit playerUnit, Unit enemyUnit, BattleData playerData, BattleData enemyData) {
		int playerUnitHPLost = playerUnit.getCurrentStat(Stats.HITPOINTS) - playerUnit.getCurrentHP();
		int enemyUnitHPLost = enemyUnit.getCurrentStat(Stats.HITPOINTS) - enemyUnit.getCurrentHP();
		boolean playerHPError = false, enemyHPError = false;
		switch (index) {
		case 0:
			if (playerUnitHPLost != 0 && playerUnitHPLost != results.getInt("damageEnemy"))
				playerHPError = true;
			if (enemyUnitHPLost != 0 && enemyUnitHPLost != results.getInt("damagePlayer"))
				enemyHPError = true;
			break;
		case 1:
			if (playerUnitHPLost != 0 && playerUnitHPLost != results.getInt("damageEnemy")
					&& playerUnitHPLost != playerUnit.getCurrentStat(Stats.HITPOINTS))
				playerHPError = true;
			if (enemyUnitHPLost != results.getInt("damagePlayer") && enemyUnitHPLost != enemyUnit.getCurrentStat(Stats.HITPOINTS))
				enemyHPError = true;
			break;
		case 2:
			if (playerUnitHPLost != 0 && playerUnitHPLost != results.getInt("damageEnemy"))
				playerHPError = true;
			if (enemyUnitHPLost != enemyUnit.getCurrentStat(Stats.HITPOINTS))
				enemyHPError = true;
			break;
		case 3:
			if (playerUnitHPLost != 0 && playerUnitHPLost != results.getInt("damageEnemy")
				&& playerUnitHPLost != playerUnit.getCurrentStat(Stats.HITPOINTS))
				playerHPError = true;
			if (enemyUnitHPLost != 0 && enemyUnitHPLost != results.getInt("damagePlayer"))
				enemyHPError = true;
			break;
		case 4:
			if (playerUnitHPLost != 0 && playerUnitHPLost != results.getInt("damageEnemy"))
				playerHPError = true;
			if (enemyUnitHPLost != 0)
				enemyHPError = true;
			break;
		case 5:
			if (playerUnitHPLost != 0)
				playerHPError = true;
			if (enemyUnitHPLost != results.getInt("damagePlayer") && enemyUnitHPLost != enemyUnit.getCurrentStat(Stats.HITPOINTS))
				enemyHPError = true;
			break;
		case 6:
			if (playerUnitHPLost != 0)
				playerHPError = true;
			if (enemyUnitHPLost != enemyUnit.getCurrentStat(Stats.HITPOINTS))
				enemyHPError = true;
			break;
		case 7:
			if (playerUnitHPLost != 0)
				playerHPError = true;
			if (enemyUnitHPLost != enemyUnit.getCurrentStat(Stats.HITPOINTS))
				enemyHPError = true;
			break;
		case 8:
			if (playerUnitHPLost != 0)
				playerHPError = true;
			if (enemyUnitHPLost != enemyUnit.getCurrentStat(Stats.HITPOINTS))
				enemyHPError = true;
			break;
		case 9:
			if (playerUnitHPLost != results.getInt("damageEnemy"))
				playerHPError = true;
			if (enemyUnitHPLost != results.getInt("damagePlayer") * 4)
				enemyHPError = true;
			break;
		}
		if (playerHPError)
			TestLog.addText("Error en los PV restantes de la unidad atacante: " +
					playerUnit.getCurrentStat(Stats.HITPOINTS) + " -> " + playerUnit.getCurrentHP());
		if (enemyHPError)
			TestLog.addText("Error en los PV restantes de la unidad defensora: " +
					enemyUnit.getCurrentStat(Stats.HITPOINTS) + " -> " + enemyUnit.getCurrentHP());
	}
	
	public static void healTest() {
		JsonValue json = new JsonReader().parse(Gdx.files.internal("test/healTest.json"));
		TestLog.addTitle("Pruebas de curación");
		Json parser = new Json();
		BattleLog.setWindow(UIFactory.newWindow());
		PlayerUnit healer = parser.fromJson(PlayerUnit.class, json.get("healer").toJson(OutputType.json));
		PlayerUnit receiver = parser.fromJson(PlayerUnit.class, json.get("receiver").toJson(OutputType.json));
		int [] results = json.get("healResults").asIntArray();
		int [] expResults = json.get("expResults").asIntArray();
		String [] cases = json.get("cases").asStringArray();
		int index = 0;
		for (int result : results) {
			TestLog.addText("Caso " + (index+1) + ": " + cases[index]);
			checkHealing(index, healer, receiver);
			if (receiver.getCurrentHP() != result)
				TestLog.addText("Error en la curación: " + receiver.getCurrentHP() + " != " + result);
			if (healer.getExp() != expResults[index])
				TestLog.addText ("Error en la experiencia del curador: " + healer.getExp() + " != " + expResults[index]);
			receiver.restoreHP(55);
			index++;
		}
		TestLog.showErrors();
	}
	
	private static void checkHealing(int index, PlayerUnit healer, PlayerUnit receiver) {
		switch(index) {
		case 0:
			receiver.loseHP(11, false);
			healer.useItem(receiver, healer.getInventory().getItem(4));
			break;
		case 1:
			receiver.loseHP(41, false);
			healer.useItem(receiver, healer.getInventory().getItem(1));
			break;
		}
			
	}
}
