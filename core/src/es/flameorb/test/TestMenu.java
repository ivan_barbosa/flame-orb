package es.flameorb.test;

import com.badlogic.gdx.Gdx;

import es.flameorb.gui.Menu;

public class TestMenu extends Menu<TestMenuOptions> {

	public TestMenu(String title) {
		super(title);
		setPosition(Math.round((Gdx.graphics.getWidth() - getWidth()) / 2), 
				Math.round((Gdx.graphics.getHeight() - getHeight()) / 2));
	}

	@Override
	protected void prepareOptions() {
		options.setItems(TestMenuOptions.values());
	}
	

}
