package es.flameorb.test;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Window;

import es.flameorb.gui.UIFactory;

public class TestLog {
	
	private static Window log = UIFactory.newWindow();
	private static boolean error;
	
	private TestLog() {
		
	}
	
	public static void addTitle(String title) {
		log.add(UIFactory.newLabel(title)).center();
		log.row();
	}
	
	public static void addText(String text) {
		log.add(UIFactory.newLabel(text));
		log.row();
		error = true;
	}
	
	public static void showErrors() {
		if (!error)
			addText("No se han detectado errores");
		log.pack();
		log.setPosition(Math.round((Gdx.graphics.getWidth() - log.getWidth()) / 2),
				Math.round((Gdx.graphics.getHeight() - log.getHeight()) / 2));
		log.setVisible(true);
	}
	
	public static void hideLog() {
		error = false;
		log.setVisible(false);
		log.clear();
	}
	
	public static Window getWindow() {
		return log;
	}
}
