package es.flameorb.test;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

import es.flameorb.general.KeyBinding;

public class TestListener extends InputListener {
	
	private TestMenu menu;
	private boolean logShowed = true;
	
	public TestListener(TestMenu menu) {
		this.menu = menu;
	}

	@Override
	public boolean keyDown(InputEvent event, int keycode) {
		if (logShowed) {
			TestLog.hideLog();
			logShowed = false;
		}
		else {
			if (keycode == KeyBinding.getAccept())
				acceptAction();
			else
				menu.moveSelection(keycode);
		}
		return true;
	}
	
	private void acceptAction() {
		switch(menu.getSelected()) {
		case BATTLE_TEST:
			TestSuite.battleTest();
			logShowed = true;
			break;
		case HEAL_TEST:
			TestSuite.healTest();
			logShowed = true;
			break;
		case EXIT:
			Gdx.app.exit();
			break;
		}
	}

}
