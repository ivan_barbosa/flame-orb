package es.flameorb.test;

public enum TestMenuOptions {
	BATTLE_TEST("Test de combate"), HEAL_TEST("Test de curación"), EXIT("Salir");
	
	private final String name;
	
	private TestMenuOptions(String name) {
		this.name = name;
	}
	
	public String toString() {
		return name;
	}
}
