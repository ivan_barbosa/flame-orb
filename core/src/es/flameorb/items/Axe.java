package es.flameorb.items;

public class Axe extends Weapon {
	
	public static final int ID = 2;
	
	protected Axe() {
		super();
	}
	
	protected Axe(Axe axe) {
		super(axe);
	}
	
	@Override
	public int getWeaponId() {
		return ID;
	}

	@Override
	public boolean hasAdvantageTo(int enemyWeaponId) {
		return enemyWeaponId == Spear.ID || enemyWeaponId == Fist.ID;
	}

}
