package es.flameorb.items;

public class Spear extends Weapon {
	
	public static final int ID = 1;

	protected Spear() {
		super();
	}
	
	protected Spear(Spear spear) {
		super(spear);
	}

	@Override
	public int getWeaponId() {
		return ID;
	}

	@Override
	public boolean hasAdvantageTo(int enemyWeaponId) {
		return enemyWeaponId == Sword.ID || enemyWeaponId == Fist.ID;
	}

}
