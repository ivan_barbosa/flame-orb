package es.flameorb.items;

public class Dark extends Weapon {
	
	public static final int ID = 8;
	
	protected Dark() {
		super();
	}
	
	protected Dark(Dark dark) {
		super(dark);
	}

	@Override
	public int getWeaponId() {
		return ID;
	}

	@Override
	public boolean hasAdvantageTo(int enemyWeaponId) {
		return enemyWeaponId == Wind.ID || enemyWeaponId == Fire.ID
				|| enemyWeaponId == Thunder.ID;
	}

}
