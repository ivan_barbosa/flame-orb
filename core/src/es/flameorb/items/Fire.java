package es.flameorb.items;

public class Fire extends Weapon {
	
	public static final int ID = 5;
	
	protected Fire() {
		
	}
	
	protected Fire(Fire fire) {
		super(fire);
	}

	@Override
	public int getWeaponId() {
		return ID;
	}

	@Override
	public boolean hasAdvantageTo(int enemyWeaponId) {
		return enemyWeaponId == Wind.ID || enemyWeaponId == Light.ID;
	}

}
