package es.flameorb.items;

import java.util.Set;

import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

import es.flameorb.effects.Effect;
import es.flameorb.game.map.Point;
import es.flameorb.general.Language;
import es.flameorb.units.Stats;
import es.flameorb.units.Unit;

/**
 * Representa a todos los objetos del juego.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class Item implements Json.Serializable {
	protected String name, description;
	protected int id, totalUses, remainUses, price;
	protected Effect useEffect;
	protected Set<Point> useRange;
	protected boolean dropable;
	
	protected Item() {
		
	}
	
	/**
	 * Constructor de copia.
	 * @param item - El objeto a copiar.
	 */
	protected Item(Item item) {
		id = item.getId();
		name = item.getName();
		description = item.getDescription();
		remainUses = totalUses = item.getTotalUses();
		price = item.getPrice();
		useEffect = item.getUseEffect();
	}
	
	/**
	 * Devuelve el identificador del objeto dentro de su clase.
	 * @return El identificador del objeto dentro de su clase.
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Devuelve el nombre del objeto.
	 * @return El nombre del objeto.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Devuelve la descripción del objeto.
	 * @return La descripción del objeto.
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Devuelve los usos totales del objeto.
	 * @return Los usos totales del objeto.
	 */
	public int getTotalUses() {
		return totalUses;
	}
	
	/**
	 * Devuelve los usos restantes del objeto.
	 * @return Los usos restantes del objeto.
	 */
	public int getRemainUses() {
		return remainUses;
	}
	
	/**
	 * Devuelve el precio del objeto.
	 * @return El precio del objeto.
	 */
	public int getPrice() {
		return price;
	}
	
	/**
	 * Devuelve el efecto que tiene el objeto al usarlo.
	 * @return El efecto al usar el objeto.
	 */
	public Effect getUseEffect() {
		return useEffect;
	}
	
	/**
	 * Devuelve el rango mínimo al que alcanza el efecto de
	 * usar el objeto.
	 * @return El rango mínimo del efecto del objeto.
	 */
	public int getEffectMinRange() {
		return useEffect.getMinRange();
	}
	
	/**
	 * Devuelve las celdas a las que el efecto del objeto alcanza.
	 * @return - Las posiciones de las celdas.
	 */
	public Set<Point> getUseRange() {
		return useRange;
	}
	
	public boolean isDropable() {
		return dropable;
	}
	
	public void setDropable(boolean dropable) {
		this.dropable = dropable;
	}
	
	/**
	 * Calcula el rango máximo de usar el arma, por si éste es
	 * diferente en función de alguna variable o alcanza a todos
	 * por igual.
	 * @param user - El usuario del arma.
	 * @return El rango máximo del efecto del arma.
	 */
	public int calculateEffectMaxRange(Unit user) {
		if (useEffect.getMaxRange() > 0)
			return useEffect.getMaxRange();
		else {
			return user.getCurrentStat(Stats.MAGIC)/2;
		}
	}
	
	/**
	 * Devuelve la descripción del objeto.
	 * @return La descripción del objeto.
	 */
	public String displayFullData() {
		return Language.get(description);
	}
	
	/**
	 * Muestra la cantidad de datos mínimos que se muestran a primera
	 * vista en el menú.
	 * @return Un string con los datos para mostrar en el menú.
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(Language.get(name));
		if (totalUses > 0)
			builder.append("   "+remainUses+"/"+totalUses);
		return builder.toString();
	}
	
	/**
	 * Actualiza las celdas que el efecto del arma alcanza.
	 * @param useRange - Las nuevas celdas.
	 */
	public void updateUseRange(Set<Point> useRange) {
		this.useRange = useRange;
	}
	
	/**
	 * Resta un uso al arma tras atacar, y devuelve si se quedó
	 * sin usos.
	 * @return Si el arma se quedó sin usos.
	 */
	public boolean waste() {
		return --remainUses == 0;
	}
	
	protected static void writeArray(Json json, IntArray array, String fieldName) {
		json.writeArrayStart(fieldName);
		for (int i=0; i<array.size; i++)
			json.writeValue(array.get(i));
		json.writeArrayEnd();
	}
	

	@Override
	public void write(Json json) {
		json.writeType(getClass());
		json.writeValue("id", id);
		json.writeValue("name", name);
		json.writeValue("description", description);
		json.writeValue("totalUses", totalUses);
		json.writeValue("remainUses", remainUses);
		json.writeValue("price", price);
		json.writeValue("dropable", dropable);
		if (useEffect != null)
			json.writeValue("useEffect", useEffect, null);
	}
	
	@Override
	public void read(Json json, JsonValue jsonData) {
		id = jsonData.getInt("id");
		name = jsonData.getString("name");
		description = jsonData.getString("description");
		totalUses = jsonData.getInt("totalUses");
		remainUses = jsonData.getInt("remainUses", totalUses);
		price = jsonData.getInt("price");
		dropable = jsonData.getBoolean("dropable", false);
		useEffect = json.readValue(null, jsonData.get("useEffect"));
	}
}
