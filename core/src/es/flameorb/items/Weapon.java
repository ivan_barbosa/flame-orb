package es.flameorb.items;

import java.util.Set;

import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

import es.flameorb.game.map.Point;
import es.flameorb.general.GamePreferences;
import es.flameorb.general.Language;
import es.flameorb.units.FEClassType;
import es.flameorb.units.Unit;

/**
 * Esta clase es la base para la representación de todas las
 * armas usadas en el juego. Para crear nuevas armas, heredar
 * de esta clase.
 * @author Iván Barbosa Gutiérrez
 */
public abstract class Weapon extends Item {
	
	private int power, hit, critical, weight, minRange,
	maxRange, level, weaponExp;
	private boolean physical, doubleAttack, onlyPower;
	private FEClassType extraDamage;
	private IntArray statsUp;
	private Set<Point> attackRange;
	protected static IntArray weaponLevels = GamePreferences.getWeaponLevelsValues();
	
	/**
	 * Constructor vacío para poder crear armas a partir de JSON.
	 */
	protected Weapon() {
		
	}
	
	/**
	 * Constructor de copia.
	 * @param weapon - La arma a copiar.
	 */
	protected Weapon(Weapon weapon) {
		super(weapon);
		power = weapon.getPower();
		hit = weapon.getHit();
		critical = weapon.getCritical();
		weight = weapon.getWeight();
		minRange = weapon.getMinRange();
		maxRange = weapon.getMaxRange();
		level = weapon.getLevel();
		weaponExp = weapon.getWeaponExp();
		physical = weapon.isPhysical();
		doubleAttack = weapon.isDoubleAttack();
		onlyPower = weapon.isOnlyPower();
		extraDamage = weapon.getExtraDamage();
		statsUp = weapon.getStatsUp();
	}
	
	/**
	 * Devuelve el poder del arma.
	 * @return El poder del arma.
	 */
	public int getPower() {
		return power;
	}
	
	/**
	 * Devuelve la probabilidad de golpeo del arma.
	 * @return La probabilidad de golpeo del arma.
	 */
	public int getHit() {
		return hit;
	}
	
	/**
	 * Devuelve la probabilidad de golpe crítico del arma.
	 * @return La probabilidad de golpe crítico del arma.
	 */
	public int getCritical() {
		return critical;
	}
	
	/**
	 * Devuelve el peso del arma.
	 * @return El peso del arma.
	 */
	public int getWeight() {
		return weight;
	}
	
	/**
	 * Devuelve el alcance mínimo del arma.
	 * @return El alcance mínimo del arma.
	 */
	public int getMinRange() {
		return minRange;
	}
	
	/**
	 * Devuelve el alcance máximo del arma.
	 * @return El alcance máximo del arma.
	 */
	public int getMaxRange() {
		return maxRange;
	}
	
	/**
	 * Devuelve el nivel mínimo necesario para usar el arma.
	 * @return El nivel del arma.
	 */
	public int getLevel() {
		return level;
	}
	
	/**
	 * Devuelve la experiencia que proporciona el arma para
	 * el rango de uso de su tipo.
	 * @return La experiencia del arma.
	 */
	public int getWeaponExp() {
		return weaponExp;
	}
	
	/**
	 * Devuelve verdadero si el arma emplea la fuerza del usuario,
	 * y falso si emplea la magia.
	 * @return Si el arma hace daño físico o mágico.
	 */
	public boolean isPhysical() {
		return physical;
	}
	
	/**
	 * Devuelve verdadero si el arma ataca 2 veces consecutivas en
	 * un ataque.
	 * @return Si el arma realiza ataques consecutivos.
	 */
	public boolean isDoubleAttack() {
		return doubleAttack;
	}
	
	/**
	 * Devuelve verdadero si el arma no usa la fuerza o magia
	 * del usuario para calcular el daño.
	 * @return Si el arma depende de la fuerza o magia del usuario.
	 */
	public boolean isOnlyPower() {
		return onlyPower;
	}
	
	/**
	 * Devuelve el tipo de la clase a la que hace daño extra. Si no,
	 * devuelve null.
	 * @return El tipo de la clase a la que hace daño extra, o null
	 * si no causa daño extra.
	 */
	public FEClassType getExtraDamage() {
		return extraDamage;
	}
	
	/**
	 * Devuelve el stat que sube si se lleva el arma equipada, o
	 * null si no lo hace.
	 * @return El stat que sube, o null si no lo hace.
	 */
	public IntArray getStatsUp() {
		return statsUp;
	}
	
	/**
	 * Devuelve las celdas a las que el arma puede atacar.
	 * @return - Las posiciones de las celdas.
	 */
	public Set<Point> getAttackRange() {
		return attackRange;
	}
	
	/**
	 * Muestra todos los datos del arma que no son mostrados en el menú.
	 * @return Un string con el resto de los datos del arma.
	 */
	@Override
	public String displayFullData() {
		StringBuilder builder = new StringBuilder();
		builder.append(Language.get(getClass().getSimpleName())+" ");
		builder.append(GamePreferences.getWeaponLevelsNames().get(level)+"  ");
		if (minRange == maxRange)
			builder.append(Language.format("singleRange", minRange));
		else
			builder.append(Language.format("multiRange", minRange, maxRange));
		builder.append("  "+Language.format("otherParameters", power, hit, critical, weight)+"\n");
		builder.append(Language.get(getDescription()));
		return builder.toString();
	}
	
	/**
	 * Actualiza las celdas a las que el arma puede atacar.
	 * @param attackRange - Las nuevas celdas.
	 */
	public void updateAttackRange(Set<Point> attackRange) {
		this.attackRange = attackRange;
	}
	
	/**
	 * Aplica el efecto del triángulo de las armas, si procede,
	 * sobre el daño que esta arma realiza al enemigo.
	 * @param damage - El daño inicial
	 * @param userWeaponLevel - El nivel del rango del arma del usuario.
	 * @param enemyWeaponId - El tipo de arma del enemigo.
	 * @param enemyWeaponLevel - El nivel del rango del arma del enemigo.
	 * @return El daño posterior a aplicar el efecto.
	 */
	public int applyTrinityEffectToDamage(int damage, int userWeaponLevel, Weapon enemyWeapon, int enemyWeaponLevel) {
		int newDamage = damage;
		if (hasAdvantageTo(enemyWeapon.getWeaponId())) {
			if (userWeaponLevel >= weaponLevels.get(4) && userWeaponLevel < weaponLevels.get(6))
				newDamage += 1;
			if (userWeaponLevel >= weaponLevels.get(6) && userWeaponLevel < weaponLevels.get(7))
				newDamage += 2;
			if (userWeaponLevel >= weaponLevels.get(7))
				newDamage += 5;
		}
		else if (enemyWeapon.hasAdvantageTo(getWeaponId())) {
			if (enemyWeaponLevel >= weaponLevels.get(4) && enemyWeaponLevel < weaponLevels.get(6))
				newDamage -= 1;
			if (enemyWeaponLevel >= weaponLevels.get(6) && enemyWeaponLevel < weaponLevels.get(7))
				newDamage -= 2;
			if (enemyWeaponLevel >= weaponLevels.get(7))
				newDamage -= 5;
		}
		return newDamage;
	}
	
	/**
	 * Aplica el efecto del triángulo de las armas, si procede,
	 * a la probabilidad de golpear con el arma al enemigo.
	 * @param hit - La probabilidad de golpeo inicial
	 * @param userWeaponLevel - El nivel del rango del arma del usuario.
	 * @param enemyWeaponId - El tipo de arma del enemigo.
	 * @param enemyWeaponLevel - El nivel del rango del arma del enemigo.
	 * @return La probabilidad de golpeo posterior a aplicar el efecto.
	 */
	public int applyTrinityEffectToHit(int hit, int userWeaponLevel, Weapon enemyWeapon, int enemyWeaponLevel) {
		int newHit = hit;
		if (hasAdvantageTo(enemyWeapon.getWeaponId())) {
			if (userWeaponLevel < weaponLevels.get(3))
				newHit += 5;
			if (userWeaponLevel >= weaponLevels.get(3) && userWeaponLevel < weaponLevels.get(5))
				newHit += 10;
			if (userWeaponLevel >= weaponLevels.get(5) && userWeaponLevel < weaponLevels.get(7))
				newHit += 15;
			if (userWeaponLevel >= weaponLevels.get(7))
				newHit += 30;
		}
		else if (enemyWeapon.hasAdvantageTo(getWeaponId())) {
			if (enemyWeaponLevel < weaponLevels.get(3))
				newHit -= 5;
			if (enemyWeaponLevel >= weaponLevels.get(3) && enemyWeaponLevel < weaponLevels.get(5))
				newHit -= 10;
			if (enemyWeaponLevel >= weaponLevels.get(5) && enemyWeaponLevel < weaponLevels.get(7))
				newHit -= 15;
			if (enemyWeaponLevel >= weaponLevels.get(7))
				newHit -= 30;
		}
		return newHit;
	}
	
	public boolean makesExtraDamageTo(Unit unit) {
		return (unit.getFirstType() != null && unit.getFirstType().equals(extraDamage)) ||
				(unit.getSecondType() != null && unit.getSecondType().equals(extraDamage));
	}
	
	protected static void writeArray(Json json, IntArray array, String fieldName) {
		json.writeArrayStart(fieldName);
		for (int i=0; i<array.size; i++)
			json.writeValue(array.get(i));
		json.writeArrayEnd();
	}
	
	private FEClassType readExtraDamage(String name) {
		if (name != null)
			for (FEClassType type : FEClassType.values())
				if (type.name().equals(name))
					return type;
		return null;
	}
	
	@Override
	public void write(Json json) {
		json.writeType(getClass());
		json.writeValue("id", id);
		json.writeValue("name", name);
		json.writeValue("description", description);
		json.writeValue("totalUses", totalUses);
		json.writeValue("remainUses", remainUses);
		json.writeValue("price", price);
		if (useEffect != null)
			json.writeValue("useEffect", useEffect, null);
		json.writeValue("power", power);
		json.writeValue("hit", hit);
		json.writeValue("critical", critical);
		json.writeValue("weight", weight);
		json.writeValue("minRange", minRange);
		json.writeValue("maxRange", maxRange);
		json.writeValue("level", level);
		json.writeValue("weaponExp", weaponExp);
		json.writeValue("physical", physical);
		json.writeValue("doubleAttack", doubleAttack);
		json.writeValue("onlyPower", onlyPower);
		if (extraDamage != null) 
			json.writeValue("extraDamage", extraDamage.name());
		if (statsUp != null)
			writeArray(json, statsUp, "statsUp");
			
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		super.read(json, jsonData);
		power = jsonData.getInt("power");
		hit = jsonData.getInt("hit");
		critical = jsonData.getInt("critical");
		weight = jsonData.getInt("weight");
		minRange = jsonData.getInt("minRange");
		maxRange = jsonData.getInt("maxRange");
		level = jsonData.getInt("level");
		weaponExp = jsonData.getInt("weaponExp");
		physical = jsonData.getBoolean("physical");
		doubleAttack = jsonData.getBoolean("doubleAttack");
		onlyPower = jsonData.getBoolean("onlyPower");
		extraDamage = readExtraDamage(jsonData.getString("extraDamage", null));
		statsUp = jsonData.get("statsUp") == null ? null : new IntArray(jsonData.get("statsUp").asIntArray());
	}
	
	public abstract boolean hasAdvantageTo(int enemyWeaponId);

	/**
	 * Devuelve el identificador común a un tipo de arma.
	 * @return El identificador común del arma.
	 */
	public abstract int getWeaponId();

}
