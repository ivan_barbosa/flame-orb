package es.flameorb.items;

public class Staff extends Weapon {
	
	public static final int ID = 9;
	
	protected Staff() {
		super();
	}
	
	protected Staff(Staff staff) {
		super(staff);
	}

	@Override
	public int getWeaponId() {
		return ID;
	}

	@Override
	public boolean hasAdvantageTo(int enemyWeaponId) {
		return false;
	}

}
