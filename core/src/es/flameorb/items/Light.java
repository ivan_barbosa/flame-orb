package es.flameorb.items;

public class Light extends Weapon {
	
	public static final int ID = 7;
	
	protected Light() {
		super();
	}
	
	protected Light(Light light) {
		super(light);
	}

	@Override
	public int getWeaponId() {
		return ID;
	}

	@Override
	public boolean hasAdvantageTo(int enemyWeaponId) {
		return enemyWeaponId == Dark.ID;
	}

}
