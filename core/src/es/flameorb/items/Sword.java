package es.flameorb.items;

public class Sword extends Weapon {
	
	public static final int ID = 0;
	
	protected Sword() {
		super();
	}
	
	protected Sword(Sword sword) {
		super(sword);
	}

	@Override
	public int getWeaponId() {
		return ID;
	}

	@Override
	public boolean hasAdvantageTo(int enemyWeaponId) {
		return enemyWeaponId == Axe.ID || enemyWeaponId == Fist.ID;
	}

}
