package es.flameorb.items;

public class Gun extends Weapon {
	
	public static final int ID = 11;
	
	protected Gun() {
		super();
	}
	
	protected Gun(Gun gun) {
		super(gun);
	}

	@Override
	public int getWeaponId() {
		return ID;
	}

	@Override
	public boolean hasAdvantageTo(int enemyWeaponId) {
		return false;
	}

}
