package es.flameorb.items;

public class Fist extends Weapon {
	
	public static final int ID = 10;
	
	protected Fist() {
		super();
	}
	
	protected Fist(Fist fist) {
		super(fist);
	}

	@Override
	public int getWeaponId() {
		return ID;
	}

	@Override
	public boolean hasAdvantageTo(int enemyWeaponId) {
		return enemyWeaponId == Wind.ID || enemyWeaponId == Fire.ID
				|| enemyWeaponId == Thunder.ID || enemyWeaponId == Light.ID
				|| enemyWeaponId == Dark.ID;
	}

}
