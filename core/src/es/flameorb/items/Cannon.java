package es.flameorb.items;

public class Cannon extends Weapon {
	
	public static final int ID = 12;
	
	protected Cannon() {
		super();
	}
	
	protected Cannon(Cannon cannon) {
		super(cannon);
	}

	@Override
	public int getWeaponId() {
		return ID;
	}

	@Override
	public boolean hasAdvantageTo(int enemyWeaponId) {
		return false;
	}

}
