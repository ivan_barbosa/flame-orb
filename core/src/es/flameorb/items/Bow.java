package es.flameorb.items;

public class Bow extends Weapon {
	
	public static final int ID = 3;
	
	protected Bow() {
		super();
	}

	protected Bow(Bow bow) {
		super(bow);
	}

	@Override
	public int getWeaponId() {
		return ID;
	}

	@Override
	public boolean hasAdvantageTo(int enemyWeaponId) {
		return false;
	}

}
