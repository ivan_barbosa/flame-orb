package es.flameorb.items;

public class Thunder extends Weapon {
	
	public static final int ID = 6;
	
	protected Thunder() {
		super();
	}
	
	protected Thunder(Thunder thunder) {
		super(thunder);
	}

	@Override
	public int getWeaponId() {
		return ID;
	}

	@Override
	public boolean hasAdvantageTo(int enemyWeaponId) {
		return enemyWeaponId == Fire.ID || enemyWeaponId == Light.ID;
	}

}
