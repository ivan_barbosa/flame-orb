package es.flameorb.items;

public class Wind extends Weapon {
	
	public static final int ID = 4;
	
	protected Wind() {
		super();
	}
	
	protected Wind(Wind wind) {
		super(wind);
	}

	@Override
	public int getWeaponId() {
		return ID;
	}

	@Override
	public boolean hasAdvantageTo(int enemyWeaponId) {
		return enemyWeaponId == Thunder.ID || enemyWeaponId == Light.ID;
	}

}
