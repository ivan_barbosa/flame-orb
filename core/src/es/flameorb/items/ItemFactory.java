package es.flameorb.items;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter.OutputType;

/**
 * Esta clase se emplear para guardar en memoria todas las armas del juego y
 * construir nuevas armas. Es la única forma de crear nuevas armas dentro
 * del juego.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class ItemFactory {
	
	private static Map<Integer, Array<Item>> matrix = new ConcurrentHashMap<Integer, Array<Item>>();
	
	private ItemFactory() {
		
	}
	
	/**
	 * Carga a partir del objeto JSON todos los objetos de un tipo en memoria, para
	 * su posterior uso.
	 * @param json El objeto JSON con todas los objetos de un tipo
	 * @param itemId El tipo de arma que se va a insertar
	 */
	public static void loadItem(JsonValue json, int itemId) {
		matrix.put(itemId, new Array<Item>());
		Json jsonParser = new Json();
		for (JsonValue item : json)
			matrix.get(itemId).insert(item.getInt("id"), jsonParser.fromJson(Item.class, item.toJson(OutputType.json)));
	}
	
	/**
	 * Crea un arma nueva a partir de las que se encuentran en la armería.
	 * @param itemTypeId - El identificador del tipo del arma.
	 * @param itemId - El identificador del arma dentro de su tipo.
	 * @return El arma pedida.
	 */
	public static Item getItem(int itemTypeId, int itemId) {
		switch(itemTypeId) {
		case -1:
			return getUsableItem(itemId);
		case Sword.ID:
			return getSword(itemId);
		case Spear.ID:
			return getSpear(itemId);
		case Axe.ID:
			return getAxe(itemId);
		case Bow.ID:
			return getBow(itemId);
		case Wind.ID:
			return getWind(itemId);
		case Fire.ID:
			return getFire(itemId);
		case Thunder.ID:
			return getThunder(itemId);
		case Light.ID:
			return getLight(itemId);
		case Dark.ID:
			return getDark(itemId);
		case Staff.ID:
			return getStaff(itemId);
		case Fist.ID:
			return getFist(itemId);
		case Gun.ID:
			return getGun(itemId);
		case Cannon.ID:
			return getCannon(itemId);
		default:
			return null;
		}
	}
	
	public static Array<Item> getItems(int id) {
		return matrix.get(id);
	}
	
	public static Array<Item> getAllItems() {
		Array<Item> items = new Array<Item>();
		for (Array<Item> itemsArray : matrix.values())
			items.addAll(itemsArray);
		return items;
	}
	
	public static Array<Item> getBuyableItems() {
		Array<Item> items = new Array<Item>();
		for (Array<Item> itemsArray : matrix.values())
			for (Item item : itemsArray)
				if (item.getPrice() > 0)
					items.add(item);
		return items;
	}
	
	private static Item getUsableItem(int itemId) {
		return new Item(matrix.get(-1).get(itemId));
	}
	
	private static Sword getSword(int weaponId) {
		return new Sword((Sword) matrix.get(Sword.ID).get(weaponId));
	}
	
	private static Spear getSpear(int weaponId) {
		return new Spear((Spear) matrix.get(Spear.ID).get(weaponId));
	}
	
	private static Axe getAxe(int weaponId) {
		return new Axe((Axe) matrix.get(Axe.ID).get(weaponId));
	}
	
	private static Bow getBow(int weaponId) {
		return new Bow((Bow) matrix.get(Bow.ID).get(weaponId));
	}
	
	private static Wind getWind(int weaponId) {
		return new Wind((Wind) matrix.get(Wind.ID).get(weaponId));
	}
	
	private static Fire getFire(int weaponId) {
		return new Fire((Fire) matrix.get(Fire.ID).get(weaponId));
	}
	
	private static Thunder getThunder(int weaponId) {
		return new Thunder((Thunder) matrix.get(Thunder.ID).get(weaponId));
	}
	
	private static Light getLight(int weaponId) {
		return new Light((Light) matrix.get(Light.ID).get(weaponId));
	}
	
	private static Dark getDark(int weaponId) {
		return new Dark((Dark) matrix.get(Dark.ID).get(weaponId));
	}
	
	private static Staff getStaff(int weaponId) {
		return new Staff((Staff) matrix.get(Staff.ID).get(weaponId));
	}
	
	private static Fist getFist(int weaponId) {
		return new Fist((Fist) matrix.get(Fist.ID).get(weaponId));
	}
	
	private static Gun getGun(int weaponId) {
		return new Gun((Gun) matrix.get(Gun.ID).get(weaponId));
	}
	
	private static Cannon getCannon(int weaponId) {
		return new Cannon((Cannon) matrix.get(Cannon.ID).get(weaponId));
	}
}

