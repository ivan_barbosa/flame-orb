package es.flameorb.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox.CheckBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.List.ListStyle;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox.SelectBoxStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldFilter;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.utils.Array;

/**
 * Crea diversos elementos de la interfaz gráfica.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class UIFactory {
	private static UIFactory uiFactory = new UIFactory();
	private Skin skin; 
	private BitmapFont font;
	
	private UIFactory () {
		skin = new Skin(Gdx.files.internal("images/gui/skin.json"));
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/font.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 16;
		font = generator.generateFont(parameter);
	}
	
	public static Skin getSkin() {
		return uiFactory.skin;
	}
	
	public static BitmapFont getFont() {
		return uiFactory.font;
	}
	
	public static void setFontSize(int size) {
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/font.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = size;
		uiFactory.font = generator.generateFont(parameter);
	}
	
	public static CheckBox newCheckBox(String labelText) {
		CheckBox checkBox = new CheckBox(labelText, uiFactory.skin);
		CheckBoxStyle style = checkBox.getStyle();
		style.font = uiFactory.font;
		checkBox.setStyle(style);
		return checkBox;
	}
	
	public static Label newLabel(String text) {
		Label label = new Label(text, uiFactory.skin);
		LabelStyle style = label.getStyle();
		style.font = uiFactory.font;
		label.setStyle(style);
		return label;
	}
	
	public static <T> List<T> newList() {
		List<T> list = new List<T>(uiFactory.skin);
		ListStyle style = list.getStyle();
		style.font = uiFactory.font;
		list.setStyle(style);
		return list;
	}
	
	public static <T> SelectBox<T> newSelectBox(Array<T> items) {
		SelectBox<T> selectBox = new SelectBox<T>(uiFactory.skin);
		SelectBoxStyle style = selectBox.getStyle();
		style.font = uiFactory.font;
		selectBox.setStyle(style);
		ListStyle listStyle = selectBox.getList().getStyle();
		listStyle.font = uiFactory.font;
		selectBox.getList().setStyle(listStyle);
		selectBox.setItems(items);
		return selectBox;
	}
	
	public static <T> SelectBox<T> newSelectBox(T[] items) {
		return UIFactory.newSelectBox(new Array<T>(items));
	}
	
	public static TextButton newTextButton(String text) {
		TextButton textButton = new TextButton(text, uiFactory.skin);
		TextButtonStyle style = textButton.getStyle();
		style.font = uiFactory.font;
		textButton.setStyle(style);
		return textButton;
	}
	
	public static TextField newTextField() {
		return UIFactory.newTextField(0, false);
	}
	
	public static TextField newTextField(int maxLength, boolean digitsOnly) {
		TextField textField = new TextField("", uiFactory.skin);
		TextFieldStyle style = textField.getStyle();
		style.font = uiFactory.font;
		textField.setStyle(style);
		textField.setMaxLength(maxLength);
		if (digitsOnly)
			textField.setTextFieldFilter(new TextFieldFilter.DigitsOnlyFilter());
		return textField;
	}
	
	public static Window newWindow() {
		return newWindow(null, 0);
	}
	
	public static Window newWindow(String title, int colspan) {
		Window window = new Window("", uiFactory.skin);
		if (title != null) {
			window.add(UIFactory.newLabel(title)).colspan(colspan);
			window.row();
		}
		return window;
	}
}
