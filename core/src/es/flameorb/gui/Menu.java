package es.flameorb.gui;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.Window;

import es.flameorb.general.KeyBinding;

/**
 * Clase base para todos los menús del juego.
 * @author Iván Barbosa Gutiérrez
 */
public abstract class Menu<T> extends Window {

    protected List<T> options;

    public Menu(String title) {
        super("", UIFactory.getSkin());
        add(UIFactory.newLabel(title));
        row();
        options = UIFactory.newList();
        prepareOptions();
        if (Gdx.app.getType() == Application.ApplicationType.Android)
            options.setSelectedIndex(-1);
        add(options).center();
        pack();
    }

    /**
     * Mueve la opción elegida en función de la tecla pulsada.
     * @param keycode - La tecla pulsada.
     */
    public void moveSelection(int keycode) {
        int index = options.getSelectedIndex();
        if (keycode == KeyBinding.getUp()) {
            if (index==0)
                index = options.getItems().size-1;
            else
                index--;
            options.setSelectedIndex(index);
        }
        else if (keycode == KeyBinding.getDown()) {
            if (index == options.getItems().size-1)
                index = 0;
            else
                index++;
            options.setSelectedIndex(index);
        }
    }

    /**
     * Devuelve la opción elegida.
     * @return La opción elegida.
     */
    public T getSelected() {
        return options.getSelected();
    }

    /**
     * Prepara todas las opciones a mostrar por este menú.
     */
    protected abstract void prepareOptions();


}
