package es.flameorb.general;

import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Array;

/**
 * Esta clase se encarga de llamar a los hilos que cargan
 * en memoria los datos básicos del juego como las armas o
 * las clases.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class Initializer {
	
	private Initializer() {
		
	}
	
	/**
	 * Carga en memoria todos los datos necesarios para el juego.
	 */
	public static void initializeGame() {
		GamePreferences.loadParameters();
		Preferences userPrefs = getPlayerPreferences();
		KeyBinding.loadKeyBindigs(userPrefs);
		Language.setLanguage(new Locale(userPrefs.getString("language"), userPrefs.getString("country")));
		ExecutorService executor = Executors.newCachedThreadPool();
		Array<String> weaponNames = GamePreferences.getWeaponNames();
		executor.execute(new InitializerTask("Item", -1));
		for (int i=0; i<weaponNames.size; i++)
			executor.execute(new InitializerTask(weaponNames.get(i), i));
		executor.execute(new InitializerTask(TaskType.CLASSESLOAD));
		executor.execute(new InitializerTask(TaskType.TERRAINLOAD));
		executor.shutdown();
		while (!executor.isTerminated());
	}
	
	/**
	 * Carga las opciones que puede elegir el jugador, como el idioma o la resolución de
	 * pantalla.
	 * @return Las opciones del jugador.
	 */
	private static Preferences getPlayerPreferences() {
		Preferences prefs = Gdx.app.getPreferences("FlameOrbPrefs");
		if (prefs.get().isEmpty()) {
			prefs.putString("language", "es");
			prefs.putString("country", "ES");
			if (Gdx.app.getType() == ApplicationType.Android)
				prefs.putBoolean("keyboardMode", false);
			else
				prefs.putBoolean("keyboardMode", true);
			prefs.flush();
			
		}
		return prefs;
	}
	
	public enum TaskType {
		ITEMLOAD, CLASSESLOAD, TERRAINLOAD;
	}

}
