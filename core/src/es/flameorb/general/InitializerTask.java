package es.flameorb.general;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.JsonReader;

import es.flameorb.game.map.TerrainData;
import es.flameorb.general.Initializer.TaskType;
import es.flameorb.items.ItemFactory;
import es.flameorb.units.Barracks;

/**
 * Esta clase realiza la carga en memoria inicial de
 * los datos necesarios para el juego, en función de
 * la tarea recibida
 * @author Iván Barbosa Gutiérrez
 *
 */
public class InitializerTask implements Runnable {
	private String itemTypeName;
	private int itemTypeId;
	private TaskType taskType;
	
	/**
	 * Construye una nueva InitializerTask que realizará la tarea recibida.
	 * @param taskType - La tarea a realizar.
	 */
	public InitializerTask(TaskType taskType) {
		this.taskType = taskType;
	}
	
	
	/**
	 * Construye una nueva InitializerTask para crear las armas del juegoo
	 * @param itemTypeName - El nombre de las armas a cargar.
	 * @param itemTypeId - El tipo de arma que se va a cargar.
	 */
	public InitializerTask(String itemTypeName, int itemTypeId) {
		this.itemTypeName = itemTypeName;
		this.itemTypeId = itemTypeId;
		taskType = TaskType.ITEMLOAD;
	}
	
	@Override
	public void run() {
		switch (taskType) {
		case ITEMLOAD:
			ItemFactory.loadItem(new JsonReader().parse(Gdx.files.internal("data/"+itemTypeName+".json")).get("items"), itemTypeId);
			break;
		case CLASSESLOAD:
			Barracks.addClasses(new JsonReader().parse(Gdx.files.internal("data/feclasses.json")).get("classes"));
			break;
		case TERRAINLOAD:
			TerrainData.loadTerrains();
			break;
		}
	}

}
