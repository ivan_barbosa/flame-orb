package es.flameorb.general;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

/**
 * Esta clase carga en memoria todas las opciones constantes del juego, para facilitar el
 * desarrollo de otras aplicaciones.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class GamePreferences {
	
	private static Array<String> weaponNames;
	private static Array<String> weaponLevelsNames;
	private static IntArray weaponLevelsValues;
	private static int classStates;
	private static int maxPromotionOptions;
	private static int inventoryMaxCapacity;
	private static Vector2 worldSize;
	
	
	private GamePreferences() {
		
	}
	
	/**
	 * Carga en memoria todas las opciones del juego.
	 */
	public static void loadParameters() {
		JsonValue preferences = new JsonReader().parse(Gdx.files.internal("data/preferences.json"));
		weaponNames = new Array<String>(preferences.get("weaponNames").asStringArray());
		weaponLevelsNames = new Array<String>(preferences.get("weaponLevelsNames").asStringArray());
		weaponLevelsValues = new IntArray(preferences.get("weaponLevelsValues").asIntArray());
		classStates = preferences.getInt("classStates");
		maxPromotionOptions = preferences.getInt("maxPromotionOptions");
		inventoryMaxCapacity = preferences.getInt("inventoryMaxCapacity");
		worldSize = new Vector2(preferences.getFloat("worldX"), preferences.getFloat("worldY"));
	}

	/**
	 * Returs the list with the names of all weapons.
	 * @return The name of the weapons.
	 */
	public static Array<String> getWeaponNames() {
		return weaponNames;
	}

	/**
	 * Returns the name which each weapon skill level has.
	 * @return The name of the weapon levels.
	 */
	public static Array<String> getWeaponLevelsNames() {
		return weaponLevelsNames;
	}

	/**
	 * Returns the accumulated value a unit needs to rearch
	 * a new weapon skill level.
	 * @return The accumulated value of the weapon levels.
	 */
	public static IntArray getWeaponLevelsValues() {
		return weaponLevelsValues;
	}

	/**
	 * Returns the number of states a class can have, typically 2,
	 * basic and promotioned.
	 * @return The number of class states.
	 */
	public static int getClassStates() {
		return classStates;
	}

	/**
	 * Returns the maximum number of classes a unit can choose when it
	 * promotions.
	 * @return The number of promotion options.
	 */
	public static int getMaxPromotionOptions() {
		return maxPromotionOptions;
	}

	/**
	 * Returns the maximum number of weapon a unit can have in its
	 * inventory.
	 * @return The maximum capacity of the inventory.
	 */
	public static int getInventoryMaxCapacity() {
		return inventoryMaxCapacity;
	}
	
	/**
	 * Devuelve el tamaño del mundo que será mostrado por pantalla.
	 * @return El tamaño del mundo.
	 */
	public static Vector2 getWorldSize() {
		return worldSize;
	}
	
	
}
