package es.flameorb.general;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Preferences;

/**
 * Esta clase muestra y cambia las asociaciones de teclas.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class KeyBinding {
	
	private static Preferences prefs;
	
	public static void loadKeyBindigs(Preferences preferences) {
		prefs = preferences;
	}
	
	public static int getUp() {
		return prefs.getInteger("key_up", Keys.UP);
	}
	
	public static void setUp(int key) {
		prefs.putInteger("key_up", key);
		prefs.flush();
	}
	
	public static int getDown() {
		return prefs.getInteger("key_down", Keys.DOWN);
	}
	
	public static void setDown(int key) {
		prefs.putInteger("key_down", key);
		prefs.flush();
	}
	
	public static int getLeft() {
		return prefs.getInteger("key_left", Keys.LEFT);
	}
	
	public static void setLeft(int key) {
		prefs.putInteger("key_left", key);
		prefs.flush();
	}
	
	public static int getRight() {
		return prefs.getInteger("key_right", Keys.RIGHT);
	}
	
	public static void setRight(int key) {
		prefs.putInteger("key_right", key);
		prefs.flush();
	}
	
	public static int getAccept() {
		return prefs.getInteger("key_accept", Keys.Z);
	}
	
	public static void setAccept(int key) {
		prefs.putInteger("key_accept", key);
		prefs.flush();
	}
	
	public static int getCancel() {
		return prefs.getInteger("key_cancel", Keys.X);
	}
	
	public static void setCancel(int key) {
		prefs.putInteger("key_cancel", key);
		prefs.flush();
	}
	
	public static void set(String control, int key) {
		switch(control) {
		case "Up":
			KeyBinding.setUp(key);
			break;
		case "Down":
			KeyBinding.setDown(key);
			break;
		case "Left":
			KeyBinding.setLeft(key);
			break;
		case "Right":
			KeyBinding.setRight(key);
			break;
		case "Accept":
			KeyBinding.setAccept(key);
			break;
		case "Cancel":
			KeyBinding.setCancel(key);
			break;
		}
	}

}
