package es.flameorb.general;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter.OutputType;

import es.flameorb.items.Item;
import es.flameorb.units.Unit;

/**
 * Representa al jugador humano del juego, guardando nombre, dinero,
 * unidades y carro.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class Player implements Json.Serializable {
	
	private int money;
	private Array<Unit> units;
	private Stock stock;
	
	public Player() {
		stock = new Stock();
		units = new Array<Unit>();
	}
	
	public int getMoney() {
		return money;
	}
	
	public void setMoney(int money) {
		this.money = money;
	}
	
	public Array<Unit> getUnits() {
		return units;
	}
	
	public Stock getStock() {
		return stock;
	}
	
	public void setStock(Array<Item> items) {
		stock.reloadStock(items);
	}
	
	public void addUnit(Unit unit) {
		units.add(unit);
	}
	
	public void removeUnit(Unit unit) {
		units.removeValue(unit, true);
	}
	
	public boolean buyItem(Item item) {
		if (item.getPrice() > money)
			return false;
		stock.insertItem(item);
		money -= item.getPrice();
		return true;
	}
	
	public void sellItem(Item item) {
		stock.takeOutItem(item);
		money += item.getPrice()/2;
	}

	@Override
	public void write(Json json) {
		json.writeValue("money", money);
		json.writeArrayStart("units");
		for (Unit unit : units)
			json.writeValue(unit);
		json.writeArrayEnd();
		json.writeValue("stock", stock);
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		jsonData = jsonData.get("player");
		money = jsonData.getInt("money");
		units = new Array<Unit>();
		JsonValue unitsJson = jsonData.get("units");
		int index = 0;
		JsonValue unitJson = unitsJson.get(index);
		while (unitJson != null) {
			units.add(json.fromJson(Unit.class, unitJson.prettyPrint(OutputType.json, 1)));
			index++;
			unitJson = unitsJson.get(index);
		}
		stock = json.fromJson(Stock.class, jsonData.get("Stock").prettyPrint(OutputType.json, 1));
	}

}
