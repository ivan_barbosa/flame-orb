package es.flameorb.general;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter.OutputType;

import es.flameorb.items.Item;
import es.flameorb.items.Weapon;

/**
 * Representa al carro del jugador, dónde se guardan todos
 * los objetos que se obtienen en el juego.
 * @author ivan
 *
 */
public class Stock implements Json.Serializable {
	
	private IntMap<Array<Item>> items;
	
	public Stock() {
		items = new IntMap<Array<Item>>();
		for (int i = -1; i<GamePreferences.getWeaponNames().size+1; i++)
			items.put(i, new Array<Item>());
	}
	
	/**
	 * Introduce un objeto en el carro.
	 * @param item - El objeto a introducir.
	 */
	public void insertItem(Item item) {
		if (item instanceof Weapon) {
			Weapon weapon = (Weapon)item;
			items.get(weapon.getWeaponId()).add(weapon);
		}
		else
			items.get(-1).add(item);
	}
	
	/**
	 * Saca un objeto del carro.
	 * @param item - El objeto a sacar.
	 */
	public void takeOutItem(Item item) {
		if (item instanceof Weapon)
			items.get(((Weapon)item).getWeaponId()).removeValue(item, true);
		else
			items.get(-1).removeValue(item, true);
	}
	
	/**
	 * Devuelve el array de un tipo de objeto o arma.
	 * @param id - El id de la clase de objeto o arma.
	 * @return El array de este tipo.
	 */
	public Array<Item> getItemTypeArray(int id) {
		return items.get(id);
	}
	
	/**
	 * Devuelve todos los objetos del carro.
	 * @return Todos los objetos del carro.
	 */
	public Array<Item> getAllItems() {
		Array<Item> allItems = new Array<Item>();
		for (int i = -1; i<GamePreferences.getWeaponNames().size+1; i++)
			allItems.addAll(getItemTypeArray(i));
		return allItems;
	}
	
	public void reloadStock(Array<Item> newItems) {
		for (int i = -1; i<GamePreferences.getWeaponNames().size+1; i++)
			items.get(i).clear();
		for (Item item : newItems)
			insertItem(item);
		
	}
	
	/**
	 * Devuelve la cantidad de objetos que hay en el carro.
	 * @return
	 */
	public int getItemsNumber() {
		int totalSize = 0;
		for (Array<Item> itemArray : items.values())
			totalSize += itemArray.size;
		return totalSize;
	}

	@Override
	public void write(Json json) {
		json.writeArrayStart("items");
		for (int i = -1; i<GamePreferences.getWeaponNames().size+1; i++) {
			Array<Item> itemArray = items.get(i);
			if (itemArray != null) {
				for (Item item : itemArray)
					json.writeValue(item);
			}
		}
		json.writeArrayEnd();
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		for (int i = -1; i<GamePreferences.getWeaponNames().size+1; i++)
			items.put(i, new Array<Item>());
		JsonValue itemsJson = jsonData.get("items");
		for (JsonValue itemJson : itemsJson)
			 insertItem(json.fromJson(Item.class, itemJson.toJson(OutputType.json)));
	}

}
