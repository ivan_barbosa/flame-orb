package es.flameorb.general;

import java.util.Locale;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.I18NBundle;

/**
 * Esta clase guarda el idioma actual del juego y cada uno de los textos
 * en el susodicho idioma.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class Language {
	
	private static Locale locale;
	private static I18NBundle texts;
	
	private Language() {
		
	}
	
	/**
	 * Sets the game language and loads all texts.
	 * @param locale - The locale of the language.
	 */
	public static void setLanguage(Locale loc) {
		locale = loc;
		texts = I18NBundle.createBundle(Gdx.files.local("data/locales/FlameOrb"), locale);
	}
	
	/**
	 * Devuelve el texto asociado a una clave
	 * @param key - La clave.
	 * @return El texto asociado.
	 */
	public static String get(String key) {
		return texts.get(key);
	}
	
	/**
	 * Devuelve el texto formateado asociado a una clave
	 * @param key - La clave
	 * @param args - Los parámetros del formateo.
	 * @return El texto formateado asociado.
	 */
	public static String format(String key, Object... args) {
		return texts.format(key, args);
	}

	/**
	 * Returns the locale of the language.
	 * @return The locale.
	 */
	public static Locale getLocale() {
		return locale;
	}

}
