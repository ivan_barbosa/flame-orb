package es.flameorb;

import com.badlogic.gdx.Game;
import es.flameorb.general.Initializer;
import es.flameorb.mainmenu.MainMenuScreen;

public class FlameOrb extends Game {
	
	@Override
	public void create () {
		Initializer.initializeGame();
		setScreen(new MainMenuScreen(this));
		
	}

	@Override
	public void render () {
        super.render();
    }
	
}
