package es.flameorb.mainmenu;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;

import es.flameorb.game.map.MapData;
import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;

public class MapSelectionListener extends MainMenuListener {
	
	private MapSelectionMenu menu;
	
	public MapSelectionListener(MainMenuScreen screen) {
		super(screen);
		menu = new MapSelectionMenu(Language.get("ChooseAMap"), this);
	    menu.addListener(this);
	    menu.setVisible(true);
	    screen.addActor(menu);
	}
	
	@Override
	protected void optionPressed() {
		if (menu.getSelected() != null) {
			screen.removeActor(menu);
			screen.startGame(menu.getSelected());
		}
	}
	
	protected void goBack() {
		screen.removeActor(menu);
		new InitialMenuListener(screen);
	}
	
	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
		return true;
	}

	@Override
	public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
		if (button == Buttons.LEFT)
			optionPressed();
		else if (button == Buttons.RIGHT)
			goBack();
	}

	@Override
	public boolean keyDown(InputEvent event, int keycode) {
		menu.moveSelection(keycode);
		if (keycode == KeyBinding.getAccept())
			optionPressed();
		else if (keycode == KeyBinding.getCancel())
			goBack();
		return true;
	}
	
	private class MapSelectionMenu extends MainMenuMenu<MapData> {

		public MapSelectionMenu(String title, MainMenuListener listener) {
			super(title, listener);
		}

		@Override
		protected void prepareOptions() {
			FileHandle [] files;
			Array<MapData> list = new Array<MapData>();
			if (Gdx.app.getType() == ApplicationType.Android) {
				files = Gdx.files.external("flameorb/maps/").list("json");
				UIFactory.setFontSize(20);
			}
			else {
				files = Gdx.files.local("data/maps/").list("json");
				UIFactory.setFontSize(16);
			}
			for (FileHandle file : files) {
				MapData mapData = new Json().fromJson(MapData.class, file);
				if (mapData.isFinished())
					list.add(mapData);
			}
			if (Gdx.app.getType() == ApplicationType.Android)
				UIFactory.setFontSize(40);
			else
				UIFactory.setFontSize(32);
			options.setItems(list);
		}
		
	}

}
