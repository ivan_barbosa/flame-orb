package es.flameorb.mainmenu;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import es.flameorb.FlameOrb;
import es.flameorb.game.GameScreen;
import es.flameorb.game.map.MapData;
import es.flameorb.gui.UIFactory;

/**
 * @author Iván Barbosa Gutiérrez
 */
public class MainMenuScreen implements Screen {

    private final FlameOrb game;
    private Stage stage;
    private OrthographicCamera camera;
    private Texture backgroundImageTexture;
    private Image backgroundImage;
    private Music backgroundMusic;

    public MainMenuScreen(final FlameOrb game) {
        this.game = game;

        backgroundImageTexture = new Texture(Gdx.files.internal("images/background/mainmenu.png"));
        backgroundImage = new Image(backgroundImageTexture);
        backgroundImage.setPosition(0, 0);
        backgroundImage.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("music/Intro Screen.mp3"));
        backgroundMusic.setVolume(0.6f);
        backgroundMusic.setLooping(true);
        stage = new Stage(new ScreenViewport());
        stage.addActor(backgroundImage);
        camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.setToOrtho(false, camera.viewportWidth, camera.viewportHeight);
        camera.update();
        
        if (Gdx.app.getType() == ApplicationType.Android)
        	UIFactory.setFontSize(40);
        else
        	UIFactory.setFontSize(32);
        
        backgroundMusic.play();
        new InitialMenuListener(this);
        
        Gdx.input.setInputProcessor(stage);
    }
    
    public void addActor(Actor actor) {
    	stage.addActor(actor);
    }
    
    public void removeActor(Actor actor) {
    	stage.getRoot().removeActor(actor);
    }
    
    public float getCenteredXPosition(float windowWidth) {
		return Math.round((Gdx.graphics.getWidth() - windowWidth) / 2);
	}
	
	public float getCenteredYPosition(float windowHeight) {
		return Math.round((Gdx.graphics.getHeight() - windowHeight) / 2);
	}
    
    public void startGame(MapData mapData) {
    	backgroundMusic.stop();
    	GameScreen screen = new GameScreen(game, mapData, false);
    	game.setScreen(screen);
    	screen.centerMap();
    	dispose();
    }
    
    public void startEditor(MapData mapData) {
    	backgroundMusic.stop();
    	GameScreen screen = new GameScreen(game, mapData, true);
    	game.setScreen(screen);
    	screen.centerMap();
    	dispose();
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.5f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
    	backgroundImage.setSize(width, height);
        stage.getViewport().update(width, height, true);
        stage.getActors().get(1).setPosition(Math.round((width - stage.getActors().get(1).getWidth()) / 2),
        		Math.round((height - stage.getActors().get(1).getHeight()) / 2));
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        backgroundImageTexture.dispose();
        backgroundMusic.dispose();
    }
}
