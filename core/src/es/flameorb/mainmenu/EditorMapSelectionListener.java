package es.flameorb.mainmenu;

import java.io.IOException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonWriter;

import es.flameorb.game.map.MapData;
import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;
import es.flameorb.general.Player;
import es.flameorb.gui.UIFactory;

public class EditorMapSelectionListener extends MainMenuListener {
	
	private EditorMapSelectionMenu menu;

	public EditorMapSelectionListener(MainMenuScreen screen) {
		super(screen);
		menu = new EditorMapSelectionMenu(Language.get("ChooseAMap"), this);
	    menu.addListener(this);
	    menu.setVisible(true);
	    screen.addActor(menu);
	}

	@Override
	protected void optionPressed() {
		if (menu.getSelected() != null) {
			screen.removeActor(menu);
			screen.startEditor(menu.getSelected());
		}
	}

	@Override
	protected void goBack() {
		screen.removeActor(menu);
		new InitialMenuListener(screen);
	}
	
	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
		return true;
	}

	@Override
	public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
		if (button == Buttons.LEFT)
			optionPressed();
		else if (button == Buttons.RIGHT)
			goBack();
	}

	@Override
	public boolean keyDown(InputEvent event, int keycode) {
		menu.moveSelection(keycode);
		if (keycode == KeyBinding.getAccept())
			optionPressed();
		else if (keycode == KeyBinding.getCancel())
			goBack();
		return true;
	}
	
	private class EditorMapSelectionMenu extends MainMenuMenu<MapData> {

		public EditorMapSelectionMenu(String title, MainMenuListener listener) {
			super(title, listener);
		}

		@Override
		protected void prepareOptions() {
			FileHandle [] files;
			FileHandle jsonFile;
			Array<MapData> list = new Array<MapData>();
			if (Gdx.app.getType() == ApplicationType.Android) {
				files = Gdx.files.external("flameorb/maps/").list("tmx");
				UIFactory.setFontSize(20);
			}
			else {
				files = Gdx.files.local("data/maps/").list("tmx");
				UIFactory.setFontSize(16);
			}
			for (FileHandle file : files) {
				if (Gdx.app.getType() == ApplicationType.Android)
					jsonFile = Gdx.files.external("flameorb/maps/"+file.nameWithoutExtension()+".json");
				else
					jsonFile = Gdx.files.local("data/maps/"+file.nameWithoutExtension()+".json");
				if (jsonFile.exists())
					list.add(new Json().fromJson(MapData.class, jsonFile));
				else {
					createMapJson(jsonFile);
					list.add(new Json().fromJson(MapData.class, jsonFile));
				}
			}
			if (Gdx.app.getType() == ApplicationType.Android)
				UIFactory.setFontSize(40);
			else
				UIFactory.setFontSize(32);
			options.setItems(list);
		}
		
		private void createMapJson(FileHandle mapDataFile) {
			Json json = new Json();
			JsonWriter writer = new JsonWriter(mapDataFile.writer(true));
			json.setWriter(writer);
			json.writeObjectStart();
			if (Gdx.app.getType() == ApplicationType.Android)
				json.writeValue("fileName", "flameorb/maps/"+mapDataFile.nameWithoutExtension());
			else
				json.writeValue("fileName", "data/maps/"+mapDataFile.nameWithoutExtension());
			json.writeValue("finished", false);
			json.writeValue("name", mapDataFile.nameWithoutExtension());
			json.writeValue("player", new Player(), Player.class);
			json.writeArrayStart("victoryConditions");
			json.writeArrayEnd();
			json.writeArrayStart("defeatConditions");
			json.writeArrayEnd();
			json.writeArrayStart("enemies");
			json.writeArrayEnd();
			json.writeArrayStart("initialPositions");
			json.writeArrayEnd();
			json.writeArrayStart("shop");
			json.writeArrayEnd();
			json.writeObjectEnd();
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

}
