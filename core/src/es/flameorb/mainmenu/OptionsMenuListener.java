package es.flameorb.mainmenu;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;

import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;

public class OptionsMenuListener extends MainMenuListener {
	
	private OptionsMenu menu;

	public OptionsMenuListener(MainMenuScreen screen) {
		super(screen);
		menu = new OptionsMenu(Language.get("Options"), this);
	    menu.addListener(this);
	    menu.setVisible(true);
	    screen.addActor(menu);
	}

	@Override
	protected void optionPressed() {
		if (menu.getSelected() != null) {
			switch(menu.getSelected()) {
			case CONTROLS:
				screen.removeActor(menu);
				new ControlsSettingListener(screen);
				break;
			case LANGUAGE:
				screen.removeActor(menu);
				new LanguageSelectionListener(screen);
				break;
			}
		}
	}

	@Override
	protected void goBack() {
		screen.removeActor(menu);
		new InitialMenuListener(screen);
	}
	
	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
		return true;
	}

	@Override
	public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
		if (button == Buttons.LEFT)
			optionPressed();
		else if (button == Buttons.RIGHT)
			goBack();
	}

	@Override
	public boolean keyDown(InputEvent event, int keycode) {
		menu.moveSelection(keycode);
		if (keycode == KeyBinding.getAccept())
			optionPressed();
		else if (keycode == KeyBinding.getCancel())
			goBack();
		return true;
	}
	
	private enum OptionsMenuOptions {
		LANGUAGE("Language"), CONTROLS("Controls");
		
		private final String name;
		
		private OptionsMenuOptions(String name) {
			this.name = name;
		}
		
		@Override
	    public String toString() {
	        return Language.get(name);
	    }
	}
	
	private class OptionsMenu extends MainMenuMenu<OptionsMenuOptions> {

		public OptionsMenu(String title, MainMenuListener listener) {
			super(title, listener);
		}

		@Override
		protected void prepareOptions() {
			Array<OptionsMenuOptions> list = new Array<OptionsMenuOptions>(OptionsMenuOptions.values());
			if (Gdx.app.getType() == ApplicationType.Android)
				list.removeValue(OptionsMenuOptions.CONTROLS, true);
			options.setItems(list);
		}
		
	}

}
