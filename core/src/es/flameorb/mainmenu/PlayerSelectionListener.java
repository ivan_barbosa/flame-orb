package es.flameorb.mainmenu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;

import es.flameorb.game.control.GameController;
import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;
import es.flameorb.general.Player;

public class PlayerSelectionListener extends MainMenuListener {
	
	private PlayerSelectionMenu menu;
	
	public PlayerSelectionListener(MainMenuScreen screen) {
		super(screen);
		menu = new PlayerSelectionMenu(Language.get("ChooseAPlayer"), this);
	    menu.addListener(this);
	    menu.setVisible(true);
	    screen.addActor(menu);
	}
	
	@Override
	protected void optionPressed() {
		if (menu.getSelected() != null) {
			GameController.getGameController().setPlayer(menu.getSelected());
			screen.removeActor(menu);
			new MapSelectionListener(screen);
		}
	}
	
	@Override
	protected void goBack() {
		screen.removeActor(menu);
		new PlayerLoaderListener(screen);
	}
	
	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
		return true;
	}

	@Override
	public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
		optionPressed();
	}

	@Override
	public boolean keyDown(InputEvent event, int keycode) {
		menu.moveSelection(keycode);
		if (keycode == KeyBinding.getAccept())
			optionPressed();
		else if (keycode == KeyBinding.getCancel())
			goBack();
		return true;
	}

	private class PlayerSelectionMenu extends MainMenuMenu<Player> {

		public PlayerSelectionMenu(String title, MainMenuListener listener) {
			super(title, listener);
		}

		@Override
		protected void prepareOptions() {
			FileHandle [] files;
			Array<Player> players = new Array<Player>();
			files = Gdx.files.local("players/").list();
			for (FileHandle file : files)
				players.add(new Json().fromJson(Player.class, file));
			options.setItems(players);
		}
		
	}
	
}
