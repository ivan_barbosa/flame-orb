package es.flameorb.mainmenu;

import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.utils.Array;

import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;

public class ControlsSettingListener extends MainMenuListener {
	
	private ControlsSettingMenu menu;

	public ControlsSettingListener(MainMenuScreen screen) {
		super(screen);
		menu = new ControlsSettingMenu(Language.get("Controls"), this);
	    menu.addListener(this);
	    menu.setVisible(true);
	    screen.addActor(menu);
	}

	@Override
	protected void optionPressed() {
		if (menu.getSelected() != null) {
			screen.removeActor(menu);
			new ControlsSetListener(screen, menu.getSelected());
		}
	}

	@Override
	protected void goBack() {
		screen.removeActor(menu);
		new OptionsMenuListener(screen);
	}
	
	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
		return true;
	}

	@Override
	public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
		if (button == Buttons.LEFT)
			optionPressed();
		else if (button == Buttons.RIGHT)
			goBack();
	}

	@Override
	public boolean keyDown(InputEvent event, int keycode) {
		menu.moveSelection(keycode);
		if (keycode == KeyBinding.getAccept())
			optionPressed();
		else if (keycode == KeyBinding.getCancel())
			goBack();
		return true;
	}
	
	protected class PairControlKey {
		
		private String control;
		private int key;
		
		public PairControlKey(String control, int key) {
			this.control = control;
			this.key = key;
		}
		
		public String getControl() {
			return control;
		}
		
		public void setKey(int key) {
			this.key = key;
		}
		
		public String toString() {
			return Language.get(control) + ":    " + Keys.toString(key);
		}
	}
	
	private class ControlsSettingMenu extends MainMenuMenu<PairControlKey> {

		public ControlsSettingMenu(String title, MainMenuListener listener) {
			super(title, listener);
		}

		@Override
		protected void prepareOptions() {
			Array<PairControlKey> list = new Array<PairControlKey>();
			list.add(new PairControlKey("Up", KeyBinding.getUp()));
			list.add(new PairControlKey("Down", KeyBinding.getDown()));
			list.add(new PairControlKey("Left", KeyBinding.getLeft()));
			list.add(new PairControlKey("Right", KeyBinding.getRight()));
			list.add(new PairControlKey("Accept", KeyBinding.getAccept()));
			list.add(new PairControlKey("Cancel", KeyBinding.getCancel()));
			options.setItems(list);
		}
		
	}

}
