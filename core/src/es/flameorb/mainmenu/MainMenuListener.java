package es.flameorb.mainmenu;

import com.badlogic.gdx.scenes.scene2d.InputListener;

public abstract class MainMenuListener extends InputListener {
	
	protected MainMenuScreen screen;
	
	public MainMenuListener(MainMenuScreen screen) {
		this.screen = screen;
	}
	
	protected abstract void optionPressed();
	
	protected abstract void goBack();
}
