package es.flameorb.mainmenu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;
import es.flameorb.gui.Menu;

/**
 * @author Iván Barbosa Gutiérrez
 */
public class InitialMenuListener extends MainMenuListener {
	
	private InitialMenu menu;
	
	public InitialMenuListener(MainMenuScreen screen) {
		super(screen);
		menu = new InitialMenu(Language.get("MainMenu"));
		menu.setPosition(screen.getCenteredXPosition(menu.getWidth()), screen.getCenteredYPosition(menu.getHeight()));
	    menu.addListener(this);
	    menu.setVisible(true);
	    screen.addActor(menu);
	}
	
	@Override
	protected void optionPressed() {
    	if (menu.getSelected() != null) {
    		switch(menu.getSelected()) {
    		case START:
    			screen.removeActor(menu);
    			new MapSelectionListener(screen);
    			break;
    		case EDITOR:
    			screen.removeActor(menu);
    			new EditorMapSelectionListener(screen);
    			break;
    		case OPTIONS:
    			screen.removeActor(menu);
    			new OptionsMenuListener(screen);
    			break;
    		case EXIT:
    			Gdx.app.exit();
    			break;
    		}
    	}
    }
	
	@Override
	protected void goBack() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
		return true;
	}

	@Override
	public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
		if (button == Buttons.LEFT)
			optionPressed();
		else if (button == Buttons.RIGHT)
			Gdx.app.exit();
	}

	@Override
	public boolean keyDown(InputEvent event, int keycode) {
		menu.moveSelection(keycode);
		if (keycode == KeyBinding.getAccept())
			optionPressed();
		return true;
	}

	private enum InitialMenuOptions {
	    START("Start"), EDITOR("Editor"), OPTIONS("Options"), EXIT("Exit");
	
	    private final String name;
	
	    private InitialMenuOptions(String name) {
	        this.name = name;
	    }
	
	    @Override
	    public String toString() {
	        return Language.get(name);
	    }
	}
	
	private class InitialMenu extends Menu<InitialMenuOptions> {
	
	    public InitialMenu(String title) {
	        super(title);
	    }
	
	    @Override
	    protected void prepareOptions() {
	        options.setItems(InitialMenuOptions.values());
	    }
	}

}

