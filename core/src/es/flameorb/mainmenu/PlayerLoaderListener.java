package es.flameorb.mainmenu;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;

public class PlayerLoaderListener extends MainMenuListener {
	
	private PlayerLoaderMenu menu;
	
	public PlayerLoaderListener(MainMenuScreen screen) {
		super(screen);
		menu = new PlayerLoaderMenu(Language.get("PlayerLoader"), this);
	    menu.addListener(this);
	    menu.setVisible(true);
	    screen.addActor(menu);
	}
	
	@Override
	protected void optionPressed() {
		if (menu.getSelected() != null) {
			switch(menu.getSelected()) {
			case NEW:
				screen.removeActor(menu);
				new PlayerCreationListener(screen);
				break;
			case SELECT:
				screen.removeActor(menu);
				new PlayerSelectionListener(screen);
				break;
			}
		}
	}
	
	@Override
	protected void goBack() {
		screen.removeActor(menu);
		new InitialMenuListener(screen);
	}
	
	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
		return true;
	}

	@Override
	public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
		optionPressed();
	}

	@Override
	public boolean keyDown(InputEvent event, int keycode) {
		menu.moveSelection(keycode);
		if (keycode == KeyBinding.getAccept())
			optionPressed();
		else if (keycode == KeyBinding.getCancel())
			goBack();
		return true;
	}
	
	private enum PlayerLoaderMenuOptions {
		NEW("NewPlayer"), SELECT("SelectPlayer");
		
		private final String name;
		
		private PlayerLoaderMenuOptions(String name) {
			this.name = name;
		}
		
		public String toString() {
			return Language.get(name);
		}
	}
	
	private class PlayerLoaderMenu extends MainMenuMenu<PlayerLoaderMenuOptions> {

		public PlayerLoaderMenu(String title, MainMenuListener listener) {
			super(title, listener);
		}

		@Override
		protected void prepareOptions() {
			options.setItems(PlayerLoaderMenuOptions.values());
		}
		
	}

}
