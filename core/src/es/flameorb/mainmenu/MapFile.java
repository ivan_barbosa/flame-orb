package es.flameorb.mainmenu;

import com.badlogic.gdx.files.FileHandle;

public class MapFile {
	
	private FileHandle file;
	
	public MapFile(FileHandle file) {
		this.file = file;
	}
	
	public String getMapPath() {
		return file.pathWithoutExtension();
	}
	
	public String toString() {
		return file.nameWithoutExtension();
	}
}
