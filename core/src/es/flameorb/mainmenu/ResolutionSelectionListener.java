package es.flameorb.mainmenu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;

public class ResolutionSelectionListener extends InputListener {
	
	private MainMenuScreen screen;
	private Window window;
	private TextField heightTextField, widthTextField;

	public ResolutionSelectionListener(MainMenuScreen screen) {
		this.screen = screen;
		Preferences prefs = Gdx.app.getPreferences("FlameOrbPrefs");
		window = UIFactory.newWindow(Language.get("Resolution"), 3);
		window.add(Language.get("Width")+":");
		widthTextField = UIFactory.newTextField();
		widthTextField.setText(prefs.getString("width"));
		window.add(widthTextField);
		window.row();
		window.add(Language.get("Height")+":");
		heightTextField = UIFactory.newTextField();
		heightTextField.setText(prefs.getString("height"));
		window.add(heightTextField);
		window.row();
		TextButton accept = UIFactory.newTextButton(Language.get("Accept"));
		accept.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				changeResolution();
			}
		});
		TextButton cancel = UIFactory.newTextButton(Language.get("Cancel"));
		cancel.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				goBack();
			}
		});
		window.add(accept);
		window.add();
		window.add(cancel);
		window.pack();
		window.setPosition(screen.getCenteredXPosition(window.getWidth()), screen.getCenteredYPosition(window.getHeight()));
		window.addListener(this);
		window.setVisible(true);
		screen.addActor(window);
	}
	
	private void changeResolution() {
		int width = Integer.parseInt(widthTextField.getText());
		int height = Integer.parseInt(heightTextField.getText());
		Gdx.graphics.setWindowedMode(width, height);
		Preferences prefs = Gdx.app.getPreferences("FlameOrbPrefs");
		prefs.putInteger("width", width);
		prefs.putInteger("height", height);
		prefs.flush();
		goBack();
	}

	private void goBack() {
		screen.removeActor(window);
		new OptionsMenuListener(screen);
	}
	
	@Override
	public boolean keyDown(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept())
			changeResolution();
		else if (keycode == KeyBinding.getCancel())
			goBack();
		return true;
	}

}
