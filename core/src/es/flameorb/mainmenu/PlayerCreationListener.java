package es.flameorb.mainmenu;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import es.flameorb.game.control.GameController;
import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;
import es.flameorb.general.Player;
import es.flameorb.gui.UIFactory;

public class PlayerCreationListener extends InputListener {
	
	private MainMenuScreen screen;
	private Window window;
	private TextField textField;
	
	public PlayerCreationListener(MainMenuScreen screen) {
		this.screen = screen;
		window = UIFactory.newWindow(Language.get("NewPlayer"), 3);
		window.add(UIFactory.newLabel(Language.get("Name")+":"));
		textField = UIFactory.newTextField();
		window.add();
		window.add(textField);
		window.row();
		TextButton accept = UIFactory.newTextButton(Language.get("Accept"));
		accept.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				createPlayer();
			}
		});
		TextButton cancel = UIFactory.newTextButton(Language.get("Cancel"));
		cancel.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				goBack();
			}
		});
		window.add(accept);
		window.add();
		window.add(cancel);
		window.pack();
		window.setPosition(screen.getCenteredXPosition(window.getWidth()), screen.getCenteredYPosition(window.getHeight()));
		window.addListener(this);
		window.setVisible(true);
		screen.addActor(window);
	}
	
	private void createPlayer() {
		GameController.getGameController().createPlayer(new Player());
		window.setVisible(false);
		screen.removeActor(window);
		new MapSelectionListener(screen);
	}
	
	private void goBack() {
		window.setVisible(false);
		screen.removeActor(window);
		new PlayerLoaderListener(screen);
	}

	@Override
	public boolean keyDown(InputEvent event, int keycode) {
		if (keycode == KeyBinding.getAccept())
			createPlayer();
		else if (keycode == KeyBinding.getCancel())
			goBack();
		return true;
	}
}
