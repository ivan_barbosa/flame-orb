package es.flameorb.mainmenu;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Window;

import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;
import es.flameorb.gui.UIFactory;
import es.flameorb.mainmenu.ControlsSettingListener.PairControlKey;

public class ControlsSetListener extends InputListener {
	
	private MainMenuScreen screen;
	private Window window;
	private PairControlKey key;
	
	public ControlsSetListener(MainMenuScreen screen, PairControlKey key) {
		this.screen = screen;
		this.key = key;
		this.window = UIFactory.newWindow();
		window.add(Language.format("PressNewKey", Language.get(key.getControl())));
		window.pack();
		window.setPosition(screen.getCenteredXPosition(window.getWidth()), screen.getCenteredYPosition(window.getHeight()));
		window.addListener(this);
		window.setVisible(true);
		screen.addActor(window);
	}
	
	@Override
	public boolean keyDown(InputEvent event, int keycode) {
		KeyBinding.set(key.getControl(), keycode);
		screen.removeActor(window);
		new ControlsSettingListener(screen);
		return true;
	}
	
}
