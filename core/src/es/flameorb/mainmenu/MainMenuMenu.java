package es.flameorb.mainmenu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.Application.ApplicationType;

import es.flameorb.general.Language;
import es.flameorb.gui.Menu;
import es.flameorb.gui.UIFactory;

public abstract class MainMenuMenu<T> extends Menu<T> {

	public MainMenuMenu(String title, final MainMenuListener listener) {
		super(title);
		if (Gdx.app.getType() == ApplicationType.Android) {
			TextButton backButton = UIFactory.newTextButton(Language.get("Back"));
			backButton.addListener(new ClickListener() {
				
				@Override
                public void clicked(InputEvent event, float x, float y) {
                    listener.goBack();
                }
				
			});
			row();
			add(backButton.center());
			pack();
		}
		setPosition(listener.screen.getCenteredXPosition(getWidth()), listener.screen.getCenteredYPosition(getHeight()));
	}

}
