package es.flameorb.mainmenu;

import java.util.Locale;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

import es.flameorb.general.KeyBinding;
import es.flameorb.general.Language;

public class LanguageSelectionListener extends MainMenuListener {
	
	private LanguageSelectionMenu menu;

	public LanguageSelectionListener(MainMenuScreen screen) {
		super(screen);
		menu = new LanguageSelectionMenu(Language.get("Language"), this);
	    menu.addListener(this);
	    menu.setVisible(true);
	    screen.addActor(menu);
	}

	@Override
	protected void optionPressed() {
		if (menu.getSelected() != null) {
			LanguageLocale locale = menu.getSelected();
			if (locale.getCountry() != null)
				Language.setLanguage(new Locale(locale.getLanguage(), locale.getCountry()));
			else
				Language.setLanguage(new Locale(locale.getLanguage()));
			Preferences prefs = Gdx.app.getPreferences("FlameOrbPrefs");
			prefs.putString("language", locale.getLanguage());
			if (locale.getCountry() != null)
				prefs.putString("country", locale.getCountry());
			else
				prefs.remove("country");
			prefs.flush();
			goBack();
		}
	}

	@Override
	protected void goBack() {
		screen.removeActor(menu);
		new OptionsMenuListener(screen);
	}
	
	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
		return true;
	}

	@Override
	public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
		if (button == Buttons.LEFT)
			optionPressed();
		else if (button == Buttons.RIGHT)
			goBack();
	}

	@Override
	public boolean keyDown(InputEvent event, int keycode) {
		menu.moveSelection(keycode);
		if (keycode == KeyBinding.getAccept())
			optionPressed();
		else if (keycode == KeyBinding.getCancel())
			goBack();
		return true;
	}
	
	private class LanguageLocale {
		
		private String name;
		private String language;
		private String country;
		
		public LanguageLocale(String name, String language, String country) {
			this.name = name;
			this.language = language;
			this.country = country;
		}

		public String getName() {
			return name;
		}

		public String getLanguage() {
			return language;
		}

		public String getCountry() {
			return country;
		}
		
		@Override
		public String toString() {
			return Language.get(getName());
		}
		
	}
	
	private class LanguageSelectionMenu extends MainMenuMenu<LanguageLocale> {

		public LanguageSelectionMenu(String title, MainMenuListener listener) {
			super(title, listener);
		}

		@Override
		protected void prepareOptions() {
			JsonValue json = new JsonReader().parse(Gdx.files.local("data/locales/availableLanguages.json")).get("availableLanguages");
			Array<LanguageLocale> list = new Array<LanguageLocale>();
			for (JsonValue locale : json)
				list.add(new LanguageLocale(locale.getString("name"), locale.getString("language"), locale.getString("country", null)));
			options.setItems(list);
		}
		
	}

}
