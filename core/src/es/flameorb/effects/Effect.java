package es.flameorb.effects;

import es.flameorb.items.Item;
import es.flameorb.units.Unit;

/**
 * Interface class for all effect applied when using objects, such as healing, etc.
 * @author Iván Barbosa Gutiérrez
 *
 */
public interface Effect {
	
	/**
	 * Applies the effect of the item used by the user on the receiver
	 * @param user - The unit using the item.
	 * @param receiver - The unit receiving the effect of the item.
	 * @param itemUsing - The item used.
	 */
	public void applyEffect(Unit user, Unit receiver, Item itemUsing);
	
	/**
	 * Returns the min range of use of the item.
	 * @return The min range.
	 */
	public int getMinRange();
	
	/**
	 * Returns the max range of use of the item.
	 * @return The max range.
	 */
	public int getMaxRange();

}
