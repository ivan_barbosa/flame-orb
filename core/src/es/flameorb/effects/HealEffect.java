package es.flameorb.effects;

import es.flameorb.items.Item;
import es.flameorb.items.Staff;
import es.flameorb.units.PlayerUnit;
import es.flameorb.units.Stats;
import es.flameorb.units.Unit;

/**
 * Allows using a item to heal an ally or the unit itself.
 * @author Iván Barbosa Gutiérreez
 *
 */
public class HealEffect implements Effect {
	
	private int power, minRange, maxRange, exp;
	
	/**
	 * Returns the HP that the unit will recover if the item is used.
	 * @param userMagic - The magic parameter of the user, when a staff is used.
	 * @param allyHP - The HP that the unit has now.
	 * @return The HP will have the unit after the healing.
	 */
	public int previewEffect(int userMagic, int allyHP, boolean isStaff) {
		if (isStaff)
			return allyHP+power+(userMagic/2);
		else
			return allyHP+power;
	}

	@Override
	public void applyEffect(Unit user, Unit ally, Item itemUsing) {
		if (itemUsing instanceof Staff)
			ally.restoreHP(power + user.getCurrentStat(Stats.MAGIC)/2);
		else
			ally.restoreHP(power);
		if (user.isPlayerUnit()) {
			PlayerUnit playerUnit = (PlayerUnit)user;
			playerUnit.addExperience(exp);
		}
		if (itemUsing.waste())
			user.usedUpItem(itemUsing);
	}

	@Override
	public int getMinRange() {
		return minRange;
	}

	@Override
	public int getMaxRange() {
		return maxRange;
	}

}
