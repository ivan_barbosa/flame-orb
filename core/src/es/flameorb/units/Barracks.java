package es.flameorb.units;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter.OutputType;

/**
 * Esta clase se encarga de cargar y mantener en memoria los datos de todas las clases
 * del juego.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class Barracks {
	
	private static IntMap<FEClass> classes = new IntMap<FEClass>();
	
	private Barracks() {
		
	}
	
	/**
	 * Carga en memoria las clases del juego.
	 * @param json - El objeto JSON con todas las clases.
	 */
	public static void addClasses(JsonValue json) {
		Json jsonParser = new Json();
		for (JsonValue feclass : json)
			classes.put(feclass.getInt("id"), jsonParser.fromJson(FEClass.class, feclass.toJson(OutputType.json)));
	}
	
	/**
	 * Returns the FEClass with the identifier given.
	 * @param id - The id of the FEClass.
	 * @return The FEClass with the identifier given.
	 */
	public static FEClass getFEClass(int id) {
		return classes.get(id);
	}
	
	public static Array<FEClass> getAllFEClasses() {
		return classes.values().toArray();
	}
	
	/**
	 * Returns the list of enemies of a map.
	 * @param json - The JSONObject with the enemies of the map
	 * @return The list of enemies of the map.
	 */
	public static Array<Unit> getMapEnemies(JsonValue json) {
		Array<Unit> enemies = new Array<Unit>();
		Json jsonParser = new Json();
		for (JsonValue enemy : json)
			enemies.add(jsonParser.fromJson(Unit.class, enemy.toJson(OutputType.json)));
		return enemies;
	}

}
