package es.flameorb.units;

import es.flameorb.general.Language;

/**
 * Esta enumeración representa los tipos a los que pueden
 * pertenecer las clases, para el calculo del movimiento
 * según el terreno, o para las armas eficaces contra un
 * conjunto de unidades con características similares.
 * @author Iván Barbosa Gutiérrez
 *
 */
public enum FEClassType {
	
	RIDER("Rider"), FLYER("Flyer"), ARMOURED("Armoured"),
	DRAGON("Dragon"), FIRE("FireType"), ICE("IceType"),
	LIGHT("LightType"), DARK("DarkType");
	
	private final String name, shortName, description;
	
	private FEClassType(String name) {
		this.name = name;
		this.shortName = name+"Short";
		this.description = name+"Des";
	}

	public String getName() {
		return Language.get(name);
	}

	public String getShortName() {
		return Language.get(shortName);
	}

	public String getDescription() {
		return Language.get(description);
	}

}
