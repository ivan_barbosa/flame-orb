package es.flameorb.units.aistrategy;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;

import es.flameorb.units.Unit;

/**
 * No elige a ninguna unidad como objetivo.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class NothingAIStrategy implements AIStrategy {

	@Override
	public FloatArray givePunctuation(Unit attacker, Array<Unit> enemiesInRange) {
		FloatArray punctuation = new FloatArray(enemiesInRange.size);
		for (int i=0; i<enemiesInRange.size; i++)
			punctuation.add(0f);
		return punctuation;
	}

}
