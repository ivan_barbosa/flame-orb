package es.flameorb.units.aistrategy;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;

import es.flameorb.game.control.GameController;
import es.flameorb.units.Unit;

/**
 * Elige como objetivo a la unidad a la que hace más daño.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class MoreDamageGivenAIStrategy extends SelectionAIStrategy {
	
	protected MoreDamageGivenAIStrategy() {
		
	}

	public MoreDamageGivenAIStrategy(AIStrategy strategy, float weight) {
		super(strategy, weight);
	}
	
	public FloatArray givePunctuation(Unit attacker, Array<Unit> enemiesInRange) {
		FloatArray partial = super.givePunctuation(attacker, enemiesInRange);
		int [] damageArray = new int[enemiesInRange.size];
		int moreDamage = 0;
		for (int i=0; i<enemiesInRange.size; i++) {
			damageArray[i] = GameController.getCurrentBattleData(attacker, enemiesInRange.get(i)).getDamage();
			if (damageArray[i] > moreDamage)
				moreDamage = damageArray[i];
		}
		for (int i=0; i<enemiesInRange.size; i++) {
			partial.set(i, partial.get(i)+((float)damageArray[i]/moreDamage)*weight);
		}
		return partial;
	}

}
