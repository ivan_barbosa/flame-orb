package es.flameorb.units.aistrategy;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;

import es.flameorb.units.Unit;

/**
 * Elige como objetivo a la unidad más cercana.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class ClosestAIStrategy extends SelectionAIStrategy {
	
	protected ClosestAIStrategy() {
		
	}

	public ClosestAIStrategy(AIStrategy strategy, float weight) {
		super(strategy, weight);
	}
	
	public FloatArray givePunctuation(Unit attacker, Array<Unit> enemiesInRange) {
		FloatArray partial = super.givePunctuation(attacker, enemiesInRange);
		for (int i=0; i<enemiesInRange.size; i++)
			partial.set(i, partial.get(i)+(1f/attacker.distanceTo(enemiesInRange.get(i)))*weight);
		return partial;
	}

}
