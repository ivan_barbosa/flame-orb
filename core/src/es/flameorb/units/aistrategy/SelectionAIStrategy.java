package es.flameorb.units.aistrategy;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;

import es.flameorb.units.Unit;

/**
 * Clase base para las estrategias que eligen objetivo.
 * @author Iván Barbosa Gutiérrez
 *
 */
public abstract class SelectionAIStrategy implements AIStrategy {
	
	protected AIStrategy fullStrategy;
	protected float weight;
	
	protected SelectionAIStrategy() {
		
	}
	
	public SelectionAIStrategy(AIStrategy strategy, float weight) {
		this.fullStrategy = strategy;
		this.weight = weight;
	}
	
	public AIStrategy getFullStrategy() {
		return fullStrategy;
	}
	
	public float getWeight() {
		return weight;
	}

	@Override
	public FloatArray givePunctuation(Unit attacker, Array<Unit> enemiesInRange) {
		return fullStrategy.givePunctuation(attacker, enemiesInRange);
	}

}
