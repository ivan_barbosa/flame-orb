package es.flameorb.units.aistrategy;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;

import es.flameorb.units.Unit;

/**
 * Elige como objetivo a la unidad con menos PV.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class LessHPAIStrategy extends SelectionAIStrategy {
	
	protected LessHPAIStrategy() {
		
	}

	public LessHPAIStrategy(AIStrategy strategy, float weight) {
		super(strategy, weight);
	}
	
	public FloatArray givePunctuation(Unit attacker, Array<Unit> enemiesInRange) {
		FloatArray partial = super.givePunctuation(attacker, enemiesInRange);
		for (int i=0; i<enemiesInRange.size; i++)
			partial.set(i, partial.get(i)+(1f/enemiesInRange.get(i).getCurrentHP())*weight);
		return partial;
	}

}
