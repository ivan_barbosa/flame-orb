package es.flameorb.units.aistrategy;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;

import es.flameorb.units.Unit;

/**
 * Interfaz básica para las estrategias de la consola.
 * @author Iván Barbosa Gutiérrez
 *
 */
public interface AIStrategy {
	
	public FloatArray givePunctuation(Unit attacker, Array<Unit> enemiesInRange);

}
