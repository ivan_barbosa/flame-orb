package es.flameorb.units.aistrategy;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;

import es.flameorb.game.control.GameController;
import es.flameorb.units.Unit;

/**
 * Elige como objetivo a la unidad que le hará menos daño.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class LessDamageReceivedAIStrategy extends SelectionAIStrategy {
	
	protected LessDamageReceivedAIStrategy() {
		
	}

	public LessDamageReceivedAIStrategy(AIStrategy strategy, float weight) {
		super(strategy, weight);
	}
	
	public FloatArray givePunctuation(Unit attacker, Array<Unit> enemiesInRange) {
		FloatArray partial = super.givePunctuation(attacker, enemiesInRange);
		int [] damageArray = new int[enemiesInRange.size];
		int lessDamage = Integer.MAX_VALUE;
		for (int i=0; i<enemiesInRange.size; i++) {
			damageArray[i] = GameController.getCurrentBattleData(enemiesInRange.get(i), attacker).getDamage();
			if (damageArray[i] < lessDamage)
				lessDamage = damageArray[i];
		}
		for (int i=0; i<enemiesInRange.size; i++) {
			partial.set(i, partial.get(i)+(lessDamage/(float)damageArray[i])*weight);
		}
		return partial;
	}

}
