package es.flameorb.units;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

import es.flameorb.game.map.Point;

/**
 * Lista que contiene las posiciones de todas las unidades.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class UnitPositionList {
	
	private ObjectMap<Point, Unit> units;
	private TiledMapTileLayer mapLayer;
	
	/**
	 * Construye la lista de posiciones de las unidades con todas las unidades.
	 * @param player - La lista de unidades del jugador.
	 * @param enemy - La lista de unidades del enemigo.
	 * @param mapLayer - El mapa de juego.
	 */
	public UnitPositionList(Array<Unit> enemy, TiledMapTileLayer mapLayer) {
		this.mapLayer = mapLayer;
		units = new ObjectMap<Point, Unit>();
		for (Unit unit : enemy) {
			units.put(unit.getMapPosition(), unit);
			unit.updateItemsRange(mapLayer);
		}
	}
	
	/**
	 * Elimina la unidad de la lista.
	 * @param unit - La unidad.
	 */
	public void deleteUnit(Unit unit) {
		units.remove(unit.getMapPosition());
	}
	
	/**
	 * Obtiene la unidad en la posición indicada.
	 * @param position - La posición requerida.
	 * @return La unidad en dicha posición, o nulo si no hay ninguna unidad.
	 */
	public Unit getUnit(Point position) {
		return units.get(position);
	}
	
	public void putUnit(Unit unit) {
		units.put(unit.getMapPosition(), unit);
		unit.updateItemsRange(mapLayer);
	}
	
	/**
	 * Actualiza la posición de la unidad.
	 * @param unit - La unidad.
	 * @param oldPosition - La posición anterior.
	 */
	public void updatePosition(Unit unit, Point oldPosition) {
		units.remove(oldPosition);
		units.put(unit.getMapPosition(), unit);
	}
	
	/**
	 * Devuelve todas las unidades de la lista.
	 * @return Todas las unidades de la lista.
	 */
	public ObjectMap.Values<Unit> getAllUnits() {
		return units.values();
	}
	
	/**
	 * Devuelve si hay una unidad en la posición recibida.
	 * @param position - La posición
	 * @return Si hay una unidad en la posición recibida.
	 */
	public boolean isPositionOccuped(Point position) {
		return units.containsKey(position);
	}
	
	/**
	 * Devuelve si en la posición recibida hay una unidad del jugador.
	 * @param position - La posición.
	 * @return Si hay una unidad del jugador en la posición recibida.
	 */
	public boolean isPositionOccupedByPlayer(Point position) {
		if (isPositionOccuped(position) && units.get(position).isPlayerUnit())
			return true;
		return false;
	}
	
	/**
	 * Devuelve si en la posición recibida hay una unidad del enemigo.
	 * @param position - La posición.
	 * @return Si hay una unidad del enemigo en la posición recibida.
	 */
	public boolean isPositionOccupedByEnemy(Point position) {
		if (isPositionOccuped(position) && !units.get(position).isPlayerUnit())
			return true;
		return false;
	}

}
