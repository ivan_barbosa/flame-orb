package es.flameorb.units;

import java.util.Random;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

import es.flameorb.game.calculators.RangeCalculator;
import es.flameorb.game.control.BattleLog;
import es.flameorb.game.control.GameController;
import es.flameorb.game.map.Point;
import es.flameorb.game.map.UnitDetailWindow;
import es.flameorb.general.GamePreferences;
import es.flameorb.items.Item;
import es.flameorb.items.Weapon;

/**
 * Esta clase representa a las unidades, cada uno de los personajes
 * a los que el jugador o la IA maneja.
 * @author Iván Barbosa Gutiérrez
 *
 */
public abstract class Unit extends Actor implements Json.Serializable {
	
	protected Texture mapImage;
	protected int id, currentHP;
	protected Point mapPosition;
	protected String unitName, shortDescription, fullDescription, mapImagePath;
	protected int level, internalLevel;
	protected FEClass feclass;
	protected IntArray baseStats, baseGrowing; 
	protected StatsChangesMap statsChanges;
	protected IntArray weaponSkills;
	protected Inventory inventory;
	protected Weapon equipedWeapon;
	protected boolean commander;
	protected Array<Unit> enemies;
	protected boolean endedAction;
	protected UnitDetailWindow unitDetails;
	protected static Random rng = new Random();
	protected static Texture commanderStar = new Texture(Gdx.files.internal("images/units/commander.png"));
	
	/**
	 * Constructor vacío para poder cargar unidades desde json.
	 */
	protected Unit() {
		super();
	}
	
	/**
	 * Realiza el ataque sobre la unidad enemiga con el daño que
	 * va a causar. Devuelve true si la unidad enemiga muere tras el ataque.
	 * @param enemy - La unidad enemiga.
	 * @param damage - El daño que se hace al enemigo.
	 * @return Si la unidad enemiga muere.
	 */
	public boolean attack(Unit enemy, int damage) {
		enemy.loseHP(damage, true);
		unitDetails.inventoryUpdate();
		return enemy.getCurrentHP() == 0;
	}
	
	/**
	 * Si el arma se rompe tras atacar, se retira del inventario y se pone a
	 * nulo el arma equipada para que la unidad no siga atacando.
	 * @param item - El objeto gastado.
	 */
	public void usedUpItem(Item item) {
		inventory.tossItem(item);
		if (item == equipedWeapon)
			equipedWeapon = null;
		unitDetails.inventoryUpdate();
	}
	
	/**
	 * Si el arma se rompe tras atacar, se retira del inventario y se pone a
	 * nulo el arma equipada para que la unidad no siga atacando.
	 */
	public void usedUpItem() {
		usedUpItem(equipedWeapon);
	}
	
	public boolean canUseWeapon(Weapon weapon) {
		return inventory.isInInventory(weapon) && GamePreferences.getWeaponLevelsValues().get(weapon.getLevel()) <=
				getWeaponSkill(weapon.getWeaponId());
	}
	
	public int distanceTo(Unit other) {
		return getMapPosition().distanceTo(other.getMapPosition());
	}
	
	/**
	 * Se equipa la primera arma que la unidad puede usar de su inventario, o
	 * no se equipa nada si no encuentra ningún arma que la unidad pueda usar.
	 */
	public void equipWeapon() {
		Array<Weapon> weapons = inventory.getWeapons();
		boolean equiped = false;
		int i=0;
		while (!equiped && i<weapons.size) {
			if (canUseWeapon(weapons.get(i))) {
				equipedWeapon = weapons.get(i);
				applyStatUpEffect(equipedWeapon.getStatsUp());
				equiped = true;
			}
			else
				i++;
		}
		if (!equiped)
			equipedWeapon = null;
		if (unitDetails != null)
			unitDetails.battleParametersUpdate();
	}
	
	/**
	 * La unidad se equipa el arma recibida si puede usarla.
	 * @param weapon - El arma a equipar.
	 */
	public void equipWeapon(Weapon weapon) {
		if (canUseWeapon(weapon)) {
			equipedWeapon = weapon;
			applyStatUpEffect(equipedWeapon.getStatsUp());
		}
		unitDetails.battleParametersUpdate();
	}
	
	/**
	 * Devuelve la velocidad de ataque de la unidad si se equipa dicha arma.
	 * @param weapon - El arma que usaría.
	 * @return La velocidad de ataque.
	 */
	public int getAttackSpeed(Weapon weapon) {
		int as = getBattleStat(Stats.SPEED);
		if (weapon != null) {
			int burden = weapon.getWeight();
			if (weapon.isPhysical())
				burden -= getBattleStat(Stats.STRENGHT);
			else
				burden -= getBattleStat(Stats.MAGIC);
			if (burden <= 0)
				return as;
			else {
				as -= burden;
				if (as <= 0)
					return 0;
				else
					return as;
			}
		}
		else
			return as;
	}
	
	/**
	 * Devuelve la velocidad de ataque de la unidad con el arma que lleva
	 * equipada actualmente.
	 * @return La velocidad de ataque.
	 */
	public int getCurrentAttackSpeed() {
		return getAttackSpeed(equipedWeapon);
	}
	
	/**
	 * Calcula el poder de ataque de la unidad con el arma dada.
	 * @param weapon - El arma con la que se quiere probar.
	 * @return El poder de ataque.
	 */
	public int getAttackPower(Weapon weapon) {
		if (weapon == null)
			return 0;
		else {
			int power = weapon.getPower();
			if (!weapon.isOnlyPower())
				if (weapon.isPhysical())
					return power + getBattleStat(Stats.STRENGHT);
				else
					return power + getBattleStat(Stats.MAGIC);
			else
				return power;
		}
	}
	
	/**
	 * Calcula el poder de ataque de la unidad con el arma que
	 * lleva equipada en este momento.
	 * @return El poder de ataque.
	 */
	public int getCurrentAttackPower() {
		return getAttackPower(equipedWeapon);
	}
	
	/**
	 * Calcula la probabilidad de golpeo de la unidad con
	 * el arma dada.
	 * @param weapon - El arma con la que se quiere probar.
	 * @return La probabilidad de golpeo.
	 */
	public int getHitRate(Weapon weapon) {
		if (weapon == null)
			return 0;
		else
			return (int) (weapon.getHit() + getBattleStat(Stats.SKILL) * 1.5 + getBattleStat(Stats.LUCK) / 2);
	}
	
	/**
	 * Calcula la probabilidad de golpeo de la unidad con
	 * el arma que lleva equipada en este momento.
	 * @return La probabilidad de golpeo.
	 */
	public int getCurrentHitRate() {
		return getHitRate(equipedWeapon);
	}
	
	/**
	 * Calcula la probabilidad de hacer un golpe crítico de
	 * la unidad con el arma dada.
	 * @param weapon - El arma con la que se quiere probar.
	 * @return La probabilidad de golpe crítico.
	 */
	public int getCriticalRate(Weapon weapon) {
		if (weapon == null)
			return 0;
		else
			return (int) (weapon.getCritical() + getBattleStat(Stats.SKILL) * 0.5);
	}
	
	/**
	 * Calcula la probabilidad de hacer un golpe crítico de
	 * la unidad con el arma que lleva equipada en este
	 * momento.
	 * @return La probabilidad de golpe crítico.
	 */
	public int getCurrentCriticalRate() {
		return getCriticalRate(equipedWeapon);
	}
	
	/**
	 * Calcula la probabilidad de esquivar de la unidad con
	 * el arma dada.
	 * @param weapon - El arma con la que se quiere probar.
	 * @return La probabilidad de esquivar.
	 */
	public int getAvoidRate(Weapon weapon) {
		return (int) (getAttackSpeed(weapon) * 1.5 + getBattleStat(Stats.LUCK) / 2);
	}
	
	/**
	 * Calcula la probabilidad de esquivar de la unidad con
	 * el arma que lleva equipada en este momento.
	 * @return La probabilidad de esquivar.
	 */
	public int getCurrentAvoidRate() {
		return getAvoidRate(equipedWeapon);
	}
	
	/**
	 * Returns the unit name.
	 * @return The unit name.
	 */
	public String getUnitName() {
		return unitName;
	}
	
	/**
	 * Returns the unit short description.
	 * @return The unit short description.
	 */
	public String getShortDescription() {
		return shortDescription;
	}

	/**
	 * Returns the unit full description.
	 * @return The unit full description.
	 */
	public String getFullDescription() {
		return fullDescription;
	}

	/**
	 * Returns the unit level.
	 * @return The unit level.
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * Returns the unit internal level.
	 * @return The unit internal level.
	 */
	public int getInternalLevel() {
		return internalLevel;
	}

	/**
	 * Returns the unit class.
	 * @return The unit class.
	 */
	public FEClass getFeclass() {
		return feclass;
	}
	
	/**
	 * Returns the unit base value of the given stat.
	 * @param stat - The stat.
	 * @return The base value of that stat.
	 */
	public int getBaseStat(Stats stat) {
		return baseStats.get(stat.ordinal());
	}

	/**
	 * Returns the unit and class combined base value of the stat given.
	 * @param stat The stat.
	 * @return The combined base value of that stat.
	 */
	public int getCurrentStat(Stats stat) {
		return baseStats.get(stat.ordinal())+feclass.getBaseStat(stat);
	}

	/**
	 * Returns the unit base growing rate of the stat given.
	 * @param stat - The stat.
	 * @return The base growing rate of that stat.
	 */
	public int getBaseGrowing(Stats stat) {
		return baseGrowing.get(stat.ordinal());
	}

	/**
	 * Returns the unit and class combined growing rate of the stat given.
	 * @param stat - The stat.
	 * @return The combined growing rate of that stat.
	 */
	public int getCurrentGrowing(Stats stat) {
		return baseGrowing.get(stat.ordinal())+feclass.getGrowing(stat);
	}
	
	/**
	 * Returns the movement reduction for the terrain given.
	 * @param terrainId - The terrain Id.
	 * @return The movement reduction for the terrain.
	 */
	public int getMovementReduction(int terrainId) {
		return feclass.getMovementReduction(terrainId);
	}

	/**
	 * Returns the unit weapon level of the general weapon Id given.
	 * @param weaponId - The general weapon Id.
	 * @return The unit weapon level of the weapon given.
	 */
	public int getWeaponSkill(int weaponId) {
		return weaponSkills.get(weaponId);
	}
	
	/**
	 * Returns the unit inventory.
	 * @return The unit inventory.
	 */
	public Inventory getInventory() {
		return inventory;
	}
	
	/**
	 * Returns the unit first type in function of its class.
	 * @return The unit first type.
	 */
	public FEClassType getFirstType() {
		return feclass.getFirstType();
	}
	
	/**
	 * Returns the unit second type in function of its class.
	 * @return The unit second type.
	 */
	public FEClassType getSecondType() {
		return feclass.getSecondType();
	}
	
	/**
	 * Returns the unit movement in function of its class.
	 * @return The unit movement.
	 */
	public int getMovement() {
		return feclass.getMovement();
	}
	
	/**
	 * Returns the battle identifier of the unit.
	 * @return The battle id.
	 */
	public int getBattleId() {
		return id;
	}

	/**
	 * Returns the current HP of the unit.
	 * @return The current HP.
	 */
	public int getCurrentHP() {
		return currentHP;
	}

	/**
	 * Returns the equiped weapon of the unit.
	 * @return The equiped weapon.
	 */
	public Weapon getEquipedWeapon() {
		return equipedWeapon;
	}
	
	/**
	 * Returns the value of the stat given of the unit.
	 * @param stat - The stat which value we want to know.
	 * @return The value of the stat.
	 */
	public int getBattleStat(Stats stat) {
		return getCurrentStat(stat)+statsChanges.getAllStatChanges(stat);
	}
	
	public StatsChangesMap getStatsChanges() {
		return statsChanges;
	}
	
	/**
	 * Returns the unit map position.
	 * @return The unit map position.
	 */
	public Point getMapPosition() {
		return mapPosition;
	}
	
	/**
	 * Returns the unit detail window.
	 * @return The unit detail window.
	 */
	public UnitDetailWindow getUnitDetails() {
		return unitDetails;
	}

	/**
	 * Returns the list of enemy units.
	 * @return The list of enemy units.
	 */
	public Array<Unit> getEnemies() {
		return enemies;
	}
	
	/**
	 * Returns the unit X map position.
	 * @return The X map position.
	 */
	public int getXMap() {
		return mapPosition.x;
	}
	
	/**
	 * Returns the unit Y map position.
	 * @return The Y map position.
	 */
	public int getYMap() {
		return mapPosition.y;
	}
	
	/**
	 * Returns if the unit has ended its action.
	 * @return If the unit has ended its action.
	 */
	public boolean hasEndedAction() {
		return endedAction;
	}

	/**
	 * Devuelve si el enemigo puede responder al ataque
	 * que la unidad lance, al estar dentro del rango de 
	 * ataque del arma enemiga.
	 * @param enemy - La unidad enemiga.
	 * @return Si el enemigo puede contraatacar.
	 */
	public boolean canEnemyCounterattack(Unit enemy) {
		int distance = distanceTo(enemy);
		Weapon enWeapon = enemy.getEquipedWeapon();
		return distance >= enWeapon.getMinRange() &&
				distance <= enWeapon.getMaxRange();
	}
	
	public boolean isEnemyInRange(Point point, Unit enemy) {
		int distance = point.distanceTo(enemy.getMapPosition());
		for (Weapon weapon : inventory.getWeapons()) {
			if (distance >= weapon.getMinRange() && distance <= weapon.getMaxRange())
				return true;
		}
		return false;
	}
	
	public boolean isEnemyInRange(Unit enemy) {
		return isEnemyInRange(getMapPosition(), enemy);
	}
	
	/**
	 * Returns if the unit belongs to the player.
	 * @return If the unit belongs to the player.
	 */
	public boolean isPlayerUnit() {
		return this instanceof PlayerUnit;
	}
	
	/**
	 * Devuelve verdadero si hay una unidad aliada al lado.
	 * @return Si hay una unidad aliada al lado.
	 */
	public boolean isAllyNext() {
		for (Unit unit : GameController.getGameController().getPlayerUnits())
			if (distanceTo(unit) == 1)
				return true;
		return false;
	}
	
	public boolean isCommander() {
		return commander;
	}
	
	public boolean isFlyer() {
		return feclass.getFirstType() == FEClassType.FLYER ||
				feclass.getSecondType() == FEClassType.FLYER;
	}
	
	/**
	 * Devuelve verdadero si el comandante está al lado de la unidad.
	 * @return Si el comandante está al lado.
	 */
	public boolean isNearCommander() {
		if (distanceTo(GameController.getGameController().getPlayerCommander()) == 1)
			return true;
		return false;
	}
	
	/**
	 * Retira de los PS actuales de la unidad el daño sufrido.
	 * @param damage - La cantidad de daño recibido.
	 */
	public void loseHP(int damage, boolean killUnit) {
		currentHP -= damage;
		if (currentHP < 0)
			if (killUnit)
				currentHP = 0;
			else
				currentHP = 1;
		BattleLog.loseHPText(this, damage);
		unitDetails.hpUpdate();
	}
	
	/**
	 * Restaura la cantidad de PS recibida por parámetro.
	 * @param hp - La cantidad de PS restaurada.
	 */
	public void restoreHP(int hp) {
		int beforeHealing = currentHP;
		currentHP += hp;
		if (currentHP > getBattleStat(Stats.HITPOINTS))
			currentHP = getBattleStat(Stats.HITPOINTS);
		BattleLog.restoreHPText(this, currentHP - beforeHealing);
		unitDetails.hpUpdate();
	}
	
	/**
	 * Indica si la unidad ha finalizado su acción o
	 * puede volver a realizarla.
	 * @param state - Si la unidad finaliza o comineza su acción.
	 */
	public void setEndedAction(boolean state) {
		endedAction = state;
	}
	
	/**
	 * Set the enemies list of this unit.
	 * @param enemies - The enemies list.
	 */
	public void setEnemies(Array<Unit> enemies) {
		this.enemies = enemies;
	}
	
	/**
	 * Sets the unit map position.
	 * @param position - The unit map position.
	 */
	public void setMapPosition(Point position) {
		mapPosition = new Point(position);
		setPosition(position.x*32f, position.y*32f);
	}
	
	/**
	 * Sets the unit map position.
	 * @param x - The x map position.
	 * @param y - The y map position.
	 */
	public void setMapPosition(int x, int y) {
		setMapPosition(new Point(x, y));
	}
	
	/**
	 * Si el arma equipada aumenta las estadísticas de la unidad, se aplica
	 * dicho aumento.
	 * @param stat - El stat que se ve aumnentado.
	 */
	private void applyStatUpEffect(IntArray stats) {
		if (stats != null)
			for (Stats stat : Stats.values())
				statsChanges.setPermanentWeaponChanges(stat, stats.get(stat.ordinal()));
		else
			statsChanges.clearPermanentWeaponChanges();
	}
	
	/**
	 * Calcula el nivel interno de la unidad en función de su nivel actual y
	 * la clase a la que pertenezca.
	 */
	private void calculateInternalLevel() {
		if (feclass.isBasic())
			internalLevel = level;
		else if (feclass.isFirstPromotion())
			internalLevel = level+20;
		else
			internalLevel = level+40;
	}
	
	/**
	 * Determina las armas que la unidad puede usar en función de la clase
	 * a la que pertenezca.
	 */
	protected void setWeaponUsage() {
		for (int i=0; i<weaponSkills.size; i++)
			if ((weaponSkills.get(i) < 0 && feclass.getWeaponUsage(i)) ||
					(weaponSkills.get(i) > 0 && !feclass.getWeaponUsage(i)))
				weaponSkills.set(i, weaponSkills.get(i) * (-1));
	}
	
	/**
	 * Actualiza el rango de las armas del inventario.
	 * @param mapLayer - El mapa.  
	 */
	public void updateItemsRange(TiledMapTileLayer mapLayer) {
		for (Item item : inventory.getItems()) {
			if (item instanceof Weapon) {
				Weapon weapon = (Weapon)item;
				weapon.updateAttackRange(RangeCalculator.getAttackRange(mapLayer,
						getMapPosition(), weapon));
			}
			if (item.getUseEffect() != null)
				item.updateUseRange(RangeCalculator.getUseRange(mapLayer, getMapPosition(),
						item, this));
		}
	}
	
	public void initialLevelUp() {
		for (int i=0; i<internalLevel; i++) {
			for (Stats stat : Stats.values()) {
				if (getCurrentGrowing(stat) >= rng.nextInt(101) && getCurrentStat(stat) < feclass.getTopStat(stat))
					baseStats.items[stat.ordinal()]++;
			}
		}
		currentHP = getCurrentStat(Stats.HITPOINTS);
		unitDetails.statsUpdate();
	}
	
	public String toString() {
		return unitName;
	}
	
	/**
	 * Usa el efecto del arma equipada sobre la unidad recibida.
	 * @param receiver - La unidad que recibirá el efecto.
	 */
	public void useItem(Unit receiver, Item item) {
		item.getUseEffect().applyEffect(this, receiver, item);
		unitDetails.inventoryUpdate();
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		batch.draw(mapImage, getX(), getY());
	}
	
	protected static void writeArray(Json json, IntArray array) {
		for (int i=0; i<array.size; i++)
			json.writeValue(array.get(i));
	}

	@Override
	public void write(Json json) {
		json.writeType(getClass());
		json.writeValue("id", id);
		json.writeValue("name", unitName);
		json.writeValue("shortDescription", shortDescription);
		json.writeValue("fullDescription", fullDescription);
		json.writeValue("level", level);
		json.writeValue("feclass", feclass.getId());
		json.writeValue("commander", commander);
		json.writeValue("mapImage", mapImagePath);
		json.writeArrayStart("baseStats");
		writeArray(json, baseStats);
		json.writeArrayEnd();
		json.writeArrayStart("baseGrowing");
		writeArray(json, baseGrowing);
		json.writeArrayEnd();
		json.writeArrayStart("weaponSkills");
		writeArray(json, weaponSkills);
		json.writeArrayEnd();
		json.writeValue("inventory", inventory);
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		id = jsonData.getInt("id");
		unitName = jsonData.getString("name");
		shortDescription = jsonData.getString("shortDescription");
		fullDescription = jsonData.getString("fullDescription", null);
		level = jsonData.getInt("level");
		feclass = Barracks.getFEClass(jsonData.getInt("feclass"));
		commander = jsonData.getBoolean("commander", false);
		baseStats = new IntArray(jsonData.get("baseStats").asIntArray());
		baseGrowing = new IntArray(jsonData.get("baseGrowing").asIntArray());
		weaponSkills = new IntArray(jsonData.get("weaponSkills").asIntArray());
		mapImagePath = jsonData.getString("mapImage", null);
		inventory = json.readValue(Inventory.class, jsonData.get("inventory"));
		if (jsonData.get("position") != null) {
			int [] positionArray = jsonData.get("position").asIntArray();
			setMapPosition(new Point(positionArray[0], positionArray[1]));
		}
		statsChanges = new StatsChangesMap();
		setWeaponUsage();
		calculateInternalLevel();
		equipWeapon();
		if (mapImagePath == null)
			mapImage = new Texture(feclass.getMapImagePath());
		else {
			if (Gdx.app.getType() == ApplicationType.Android)
				mapImage = new Texture(Gdx.files.external("flameorb/"+mapImagePath));
			else
				mapImage = new Texture(Gdx.files.local("data/"+mapImagePath));
		}
	}
	
}
