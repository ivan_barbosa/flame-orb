package es.flameorb.units;

/**
 * Esta enumeración representa las estadísticas de las unidades.
 * Esta pensado para facilitar el uso de arrays para guardar los
 * stats.
 * @author Iván Barbosa Gutiérrez
 *
 */
public enum Stats {
	
	HITPOINTS, STRENGHT,MAGIC, SKILL,
	SPEED, LUCK, DEFENSE, RESISTANCE;

}
