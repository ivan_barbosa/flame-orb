package es.flameorb.units;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

import es.flameorb.game.calculators.BattleCalculator;
import es.flameorb.game.control.BattleLog;
import es.flameorb.game.map.UnitDetailWindow;
import es.flameorb.general.Language;
import es.flameorb.items.Item;

/**
 * Representa a los unidades del usuario o que pueden reclutarse.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class PlayerUnit extends Unit {
	
	private int exp;
	private IntArray topStats;
	
	public PlayerUnit() {
		super();
	}
	
	/**
	 * Añade la experiencia ganada a la unidad.
	 * @param experience - La experiencia ganada.
	 * @return Si la unidad subió de nivel.
	 */
	public boolean addExperience(int experience) {
		BattleLog.addExperienceText(this, experience);
		exp+=experience;
		if (exp >= 100) {
			levelUpSecuence();
			level++;
			if (level == 20)
				exp = 0;
			else
				exp = exp%100;
			if (internalLevel != 60)
				internalLevel++;
			unitDetails.levelUpUpdate();
			return true;
		}
		unitDetails.earnedExperienceUpdate();
		return false;
	}
	
	/**
	 * Añada la experencia a la unidad en función del nivel del enemigo y
	 * si este ha muerto tras nuestro ataque.
	 * @param enemyLevel - El nivel interno del enemigo.
	 * @param result - Si éste ha sido derrotado.
	 */
	public void addExperience(int enemyLevel, boolean result) {
		int experience = (int)((double)enemyLevel/getInternalLevel()*10);
		if (result)
			experience *= 3;
		if (addExperience(experience))
			BattleCalculator.unitNewLevel(this, enemies);
	}
	
	/**
	 * Añade la experiencia al nivel de maestría del arma usada.
	 * @param exp - La experiencia ganada.
	 * @param weaponId - El Id general del arma usada.
	 */
	public void addWeaponExperience(int exp, int weaponId) {
		int experience =  exp + weaponSkills.get(weaponId);
		weaponSkills.set(weaponId, experience);
		unitDetails.weaponExperienceUpdate();
	}
	
	public boolean canPromote() {
		return feclass.isBasic() && level >= 10 && inventory.checkItem("PromotionOrb");
	}
	
	/**
	 * Cambia la clase de la unidad a la dada.
	 * @param id - El identificador de la clase.
	 * @boolean promotion - Si la unidad cambia de clase por
	 * haber promocionado.
	 */
	public void changeClass(FEClass newClass, boolean promotion) {
		if (promotion) {
			inventory.tossItem("PromotionOrb");
			BattleLog.promotionText(unitName, newClass.getName());
			for (Stats stat : Stats.values())
				BattleLog.statUpText(stat, newClass.getBaseStat(stat) - feclass.getBaseStat(stat));
			currentHP += newClass.getBaseStat(Stats.HITPOINTS) - feclass.getBaseStat(Stats.HITPOINTS);
		}
		feclass = newClass;
		level = 1;
		recalculateInternalLevel(promotion);
		setWeaponUsage();
		unitDetails.inventoryUpdate();
		unitDetails.changeClassUpdate();
	}
	
	/**
	 * Returns the unit experience.
	 * @return The unit experience.
	 */
	public int getExp() {
		return exp;
	}
	
	/**
	 * Returns the unit top value of the stat given.
	 * @param stat - The stat.
	 * @return The unit top value of that stat. 
	 */
	public int getTopStat(Stats stat) {
		return topStats.get(stat.ordinal());
	}

	/**
	 * Returns the unit and class combined top value of the stat given.
	 * @param stat - The stat.
	 * @return The combined top value of that stat.
	 */
	public int getCurrentTopStat(Stats stat) {
		return topStats.get(stat.ordinal())+feclass.getTopStat(stat);
	}
	
	/**
	 * Añade los puntos a cada estadística según el crecimiento de la unidad
	 * cuando ésta sube de nivel.
	 */
	public void levelUpSecuence() {
		BattleLog.levelUpText(unitName);
		for (Stats stat : Stats.class.getEnumConstants())
			if (getCurrentGrowing(stat) >= rng.nextInt(101) && getCurrentStat(stat) < getCurrentTopStat(stat)) {
				baseStats.items[stat.ordinal()]++;
				BattleLog.statUpText(stat, 1);
			}
	}
	
	/**
	 * Tira un objeto del inventario.
	 * @param item - El objeto a tirar.
	 */
	public void tossItem(Item item) {
		inventory.tossItem(item);
		if (item == equipedWeapon)
			equipWeapon();
		unitDetails.inventoryUpdate();
	}
	
	/**
	 * Recalcula el nivel interno de la unidad cuando esta cambia de clase.
	 * @param promotion - Si el cambio de clase se debe a una promoción
	 * de la actual.
	 */
	private void recalculateInternalLevel(boolean promotion) {
		if (promotion) {
			if (feclass.isFirstPromotion())
				internalLevel = 21;
			else
				internalLevel = 41;
		}
		else
			internalLevel = (internalLevel-1)/2;
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		if (commander)
			batch.draw(commanderStar, getX(), getY());
		if (endedAction) {
			batch.setColor(0.5f, 0.5f, 0.5f, 1);
			batch.draw(mapImage, getX(), getY());
			batch.setColor(1, 1, 1, 1);
		}
		else {
			batch.setColor(0, 0, 1, 1);
			batch.draw(mapImage, getX(), getY());
			batch.setColor(1, 1, 1, 1);
		}
	}

	@Override
	public void write(Json json) {
		super.write(json);
		json.writeValue("exp", exp);
		json.writeArrayStart("topStats");
		writeArray(json, topStats);
		json.writeArrayEnd();
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		super.read(json, jsonData);
		exp = jsonData.getInt("exp", 0);
		topStats = new IntArray(jsonData.get("topStats").asIntArray());
		currentHP = getCurrentStat(Stats.HITPOINTS);
		unitDetails = new UnitDetailWindow(Language.get("UnitDetail"), this);
	}
	
	

}
