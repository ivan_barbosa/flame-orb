package es.flameorb.units;

import com.badlogic.gdx.utils.IntIntMap;
import com.badlogic.gdx.utils.IntMap;

public class StatsChangesMap {
	
	private static final int PERMANENT_WEAPON_CHANGES = 0;
	private static final int PERMANENT_SKILL_CHANGES = 1;
	private static final int FULLMAP_CHANGES = 2;
	private static final int TEMPORARY_WEAPON_CHANGES = 3;
	private static final int TEMPORARY_ITEM_CHANGES = 4;
	
	private IntMap<IntIntMap> map = new IntMap<IntIntMap>();
	
	public StatsChangesMap() {
		for (Stats stat : Stats.values()) {
			map.put(stat.ordinal(), new IntIntMap());
		}
	}
	
	public void clearPermanentWeaponChanges() {
		for (Stats stat : Stats.values())
			setPermanentWeaponChanges(stat, 0);
	}
	
	public int getPermanentWeaponChanges(Stats stat) {
		return map.get(stat.ordinal()).get(PERMANENT_WEAPON_CHANGES, 0);
	}
	
	public int getPermanentSkillChanges(Stats stat) {
		return map.get(stat.ordinal()).get(PERMANENT_SKILL_CHANGES, 0);
	}
	
	public int getFullMapChanges(Stats stat) {
		return map.get(stat.ordinal()).get(FULLMAP_CHANGES, 0);
	}
	
	public int getTemporaryWeaponChanges(Stats stat) {
		return map.get(stat.ordinal()).get(TEMPORARY_WEAPON_CHANGES, 0);
	}
	
	public int getTemporaryItemChanges(Stats stat) {
		return map.get(stat.ordinal()).get(TEMPORARY_ITEM_CHANGES, 0);
	}
	
	public int getAllStatChanges(Stats stat) {
		return getPermanentWeaponChanges(stat)+getPermanentSkillChanges(stat)+
				getFullMapChanges(stat)+getTemporaryWeaponChanges(stat)+
				getTemporaryItemChanges(stat);
	}
	
	public void setPermanentWeaponChanges(Stats stat, int value) {
		map.get(stat.ordinal()).put(PERMANENT_WEAPON_CHANGES, value);
	}
	
	public void setPermanentSkillChanges(Stats stat, int value) {
		map.get(stat.ordinal()).put(PERMANENT_SKILL_CHANGES, value);
	}
	
	public void setFullMapChanges(Stats stat, int value) {
		map.get(stat.ordinal()).put(FULLMAP_CHANGES, value);
	}
	
	public void setTemporaryWeaponChanges(Stats stat, int value) {
		map.get(stat.ordinal()).put(TEMPORARY_WEAPON_CHANGES, value);
	}
	
	public void setTemporaryItemChanges(Stats stat, int value) {
		map.get(stat.ordinal()).put(TEMPORARY_ITEM_CHANGES, value);
	}
	
	public void updateTemporaryChanges() {
		for (Stats stat : Stats.values()) {
			updateTemporaryWeaponChanges(stat);
			updateTemporaryItemChanges(stat);
		}
	}
	
	private void updateTemporaryWeaponChanges(Stats stat) {
		int value = map.get(stat.ordinal()).get(TEMPORARY_WEAPON_CHANGES, 0);
		if (value > 0)
			value--;
		else if (value < 0)
			value++;
		map.get(stat.ordinal()).put(TEMPORARY_WEAPON_CHANGES, value);
	}
	
	private void updateTemporaryItemChanges(Stats stat) {
		int value = map.get(stat.ordinal()).get(TEMPORARY_ITEM_CHANGES, 0);
		if (value > 0)
			value--;
		else if (value < 0)
			value++;
		map.get(stat.ordinal()).put(TEMPORARY_ITEM_CHANGES, value);
	}

}
