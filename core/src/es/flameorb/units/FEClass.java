package es.flameorb.units;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.BooleanArray;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

import es.flameorb.general.Language;

/**
 * Esta clase representa a cada una de las diferentes classes a las que
 * pueden pertenecer las unidades. Determina las armas que puede usar, los
 * crecimientos, stats iniciales y stats máximos, su FEClassType,
 * que dertermina las debilidades y ciertas capacidades, y el movimiento de
 * la unidad.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class FEClass implements Json.Serializable {
	private int id, movement;
	private String mapImagePath;
	private String name, description;
	private IntArray growing, baseStats, topStats, movReduction;
	private BooleanArray weaponUsage;
	private int promotionState;
	private FEClassType [] types = new FEClassType[2];
	private IntArray promotions;
	
	/**
	 * Returns the class Id.
	 * @return The class Id.
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * Returns the class movement.
	 * @return The class movement.
	 */
	public int getMovement() {
		return movement;
	}
	
	/**
	 * Returns the image on the map.
	 * @return The image on the map.
	 */
	public String getMapImagePath() {
		return mapImagePath;
	}
	
	/**
	 * Returns the class name.
	 * @return The class name.
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the class description.
	 * @return The class description.
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Returns the growing rate of the stat given.
	 * @param stat - The stat.
	 * @return The growing rate of that stat.
	 */
	public int getGrowing(Stats stat) {
		return growing.get(stat.ordinal());
	}
	
	/**
	 * Returns the base value of the stat given.
	 * @param stat - The stat.
	 * @return The base value of that stat.
	 */
	public int getBaseStat(Stats stat) {
		return baseStats.get(stat.ordinal());
	}

	/**
	 * Returns the top value of the stat given.
	 * @param stat - The stat.
	 * @return The top value of that stat.
	 */
	public int getTopStat(Stats stat) {
		return topStats.get(stat.ordinal());
	}
	
	/**
	 * Returns the movement reduction for the terrain given.
	 * @param terrainId - The terrain Id.
	 * @return The movement reduction for the terrain.
	 */
	public int getMovementReduction(int terrainId) {
		return movReduction.get(terrainId);
	}

	/**
	 * Returns the level which the class begins of the given weapon.
	 * @param weaponId - The weapon general Id.
	 * @return The level of maestry in that weapon.
	 */
	public boolean getWeaponUsage(int weaponId) {
		return weaponUsage.get(weaponId);
	}

	/**
	 * Returns if the class is a basic class.
	 * @return If it's basic.
	 */
	public boolean isBasic() {
		return promotionState == 0;
	}
	
	/**
	 * Returns if the class is a first promotion class.
	 * @return If it's a first promotion.
	 */
	public boolean isFirstPromotion() {
		return promotionState == 1;
	}
	
	/**
	 * Returns if the class is a second promotion class.
	 * @return If it's a second promotion.
	 */
	public boolean isSecondPromotion() {
		return promotionState == 2;
	}

	/**
	 * Returns the first class type.
	 * @return The first class type.
	 */
	public FEClassType getFirstType() {
		return types[0];
	}
	
	/**
	 * Returns the second class type.
	 * @return The second class type.
	 */
	public FEClassType getSecondType() {
		if (types.length == 2)
			return types[1];
		else
			return null;
	}
	
	public Array<FEClass> getPromotions() {
		Array<FEClass> promos = new Array<FEClass>();
		if (promotions != null)
			for (int promotion : promotions.items)
				promos.add(Barracks.getFEClass(promotion));
		return promos;
			
	}
	
	@Override
	public String toString() {
		return Language.get(getName());
	}

	@Override
	public void write(Json json) {
		// TODO Auto-generated method stub
		
	}
	
	private void loadTypes(JsonValue jsonData) {
		String [] typesArray = jsonData.asStringArray();
		FEClassType [] feclassTypes = FEClassType.values();
		for (FEClassType type : feclassTypes) {
			if (typesArray[0].equals(type.name()))
				types[0] = type;
			else if (typesArray.length > 1 && typesArray[1].equals(type.name()))
				types[1] = type;
		}
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		id = jsonData.getInt("id");
		name = jsonData.getString("name");
		description = jsonData.getString("description");
		if (Gdx.app.getType() == ApplicationType.Android)
			mapImagePath = "flameorb/" + jsonData.getString("mapImagePath");
		else
			mapImagePath = "data/" + jsonData.getString("mapImagePath");
		movement = jsonData.getInt("movement");
		movReduction = new IntArray(jsonData.get("movReduction").asIntArray());
		baseStats = new IntArray(jsonData.get("baseStats").asIntArray());
		growing = new IntArray(jsonData.get("growing").asIntArray());
		topStats = new IntArray(jsonData.get("topStats").asIntArray());
		weaponUsage = new BooleanArray(jsonData.get("weaponUsage").asBooleanArray());
		promotionState = jsonData.getInt("promotionState");
		if (jsonData.get("promotions") != null)
			promotions = new IntArray(jsonData.get("promotions").asIntArray());
		if (jsonData.get("types") != null)
			loadTypes(jsonData.get("types"));
	}
	
}
