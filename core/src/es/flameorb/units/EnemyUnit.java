package es.flameorb.units;

import java.util.Set;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.ObjectSet;

import es.flameorb.game.calculators.Pair;
import es.flameorb.game.calculators.RangeCalculator;
import es.flameorb.game.control.GameController;
import es.flameorb.game.map.Point;
import es.flameorb.game.map.UnitDetailWindow;
import es.flameorb.general.Language;
import es.flameorb.items.Weapon;
import es.flameorb.units.aistrategy.AIStrategy;

/**
 * Reprensenta a las unidades del enemig.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class EnemyUnit extends Unit {
	
	private boolean reclutable;
	private AIStrategy strategy;
	private int moveType;
	
	public EnemyUnit() {
		super();
	}
	
	private Array<Unit> setToArray(ObjectSet<Unit> set) {
		Array<Unit> units = new Array<Unit>();
		for (Unit unit : set)
			units.add(unit);
		return units;
	}
	
	/**
	 * Elige a la unidad objeto.
	 * @return La unidad objeto.
	 */
	public Unit selectEnemy() {
		ObjectSet<Unit> enemiesSet = getEnemiesInRange();
		Array<Unit> enemiesInRange;
		if (enemiesSet.size == 0)
			enemiesInRange = GameController.getGameController().getPlayerUnits();
		else
			enemiesInRange = setToArray(enemiesSet);
		FloatArray punctuation = strategy.givePunctuation(this, enemiesInRange);
		float maxValue = 0f;
		int selectedUnit = 0;
		for (int i=0; i<punctuation.size; i++)
			if (punctuation.get(i) > maxValue) {
				maxValue = punctuation.get(i);
				selectedUnit = i;
			}
		return enemiesInRange.get(selectedUnit);
	}
	
	/**
	 * La unidad se dirige hacia la unidad marcada como objetivo.
	 * @param enemy - La unidad objetivo.
	 * @return El punto destino del mapa y si puede atacar al enemigo.
	 */
	public Pair<Point, Boolean> moveToEnemy(Unit enemy) {
		if (enemy != null) {
			Set<Point> movementRange = RangeCalculator.getUnitMoveRange(this);
			int maxRange = 1;
			int minRange = 10;
			for (Weapon weapon : inventory.getWeapons()) {
				if (weapon.getMaxRange() > maxRange)
					maxRange = weapon.getMaxRange();
				if (weapon.getMinRange() < minRange)
					minRange = weapon.getMinRange();
			}
			int idealDistance = getIdealDistance(enemy.getEquipedWeapon(), minRange, maxRange);
			int minDistance = Integer.MAX_VALUE;
			Point closest = null, ideal = null;
			for (Point position : movementRange) {
				if (!GameController.getGameController().getUnitsPositions().isPositionOccuped(position)) {
					if (position.distanceTo(enemy.getMapPosition()) < minDistance) {
						minDistance = position.distanceTo(enemy.getMapPosition());
						closest = position;
					}
					if (position.distanceTo(enemy.getMapPosition()) == idealDistance) {
						ideal = position;
					}
				}
			}
			if (ideal != null)
				return new Pair<Point, Boolean>(ideal, true);
			else {
				if (isEnemyInRange(closest, enemy))
					return new Pair<Point, Boolean>(closest, true);
				else
					return new Pair<Point, Boolean>(closest, false);
			}
		}
		else
			return new Pair<Point, Boolean>(getMapPosition(), false);
		
	}
	
	/**
	 * Elige un arma para atacar a la unidad objetivo.
	 * @param enemy - La unidad objetivo.
	 */
	public void chooseWeapon(Unit enemy) {
		int maxDamage = 0;
		Weapon selectedWeapon = getEquipedWeapon();
		for (Weapon weapon : inventory.getWeapons()) {
			equipWeapon(weapon);
			if (enemy.canEnemyCounterattack(this)) {
				if (GameController.getCurrentBattleData(this, enemy).getDamage() > maxDamage) {
					maxDamage = GameController.getCurrentBattleData(this, enemy).getDamage();
					selectedWeapon = weapon;
				}
			}
		}
		equipWeapon(selectedWeapon);
	}
	
	/**
	 * Devuelve todas las unidades a las que puede alcanzar.
	 * @return Las unidades a las que alcanza.
	 */
	private ObjectSet<Unit> getEnemiesInRange() {
		ObjectSet<Unit> reachableEnemies = new ObjectSet<Unit>(enemies.size);
		Set<Point> movementPositions = RangeCalculator.getUnitMoveRange(this);
		switch (moveType) {
		case 0:
			for (Unit enemy : enemies) {
				for (Point point : movementPositions) {
					if (isEnemyInRange(point, enemy))
						reachableEnemies.add(enemy);
				}
			}
			break;
		case 1:
			for (Unit enemy : enemies) {
				for (Point point : movementPositions) {
					if (isEnemyInRange(point, enemy))
						reachableEnemies.add(enemy);
				}
			}
			if (reachableEnemies.size != 0)
				enemySpotted();
			break;
		case 2:
			for (Unit enemy : enemies)
				if (isEnemyInRange(enemy))
					reachableEnemies.add(enemy);
			break;
			
		}
		return reachableEnemies;
	}
	
	/**
	 * Devuelve a que distancia del objetivo debería colocarse para sufrir el menor
	 * daño posible.
	 * @param enemyWeapon - El arma del enemigo.
	 * @param minRange - El rango mínimo de su arma.
	 * @param maxRange - El rango máximo de su arma.
	 * @return La distancia ideal.
	 */
	private int getIdealDistance(Weapon enemyWeapon, int minRange, int maxRange) {
		if (maxRange > enemyWeapon.getMaxRange())
			return maxRange;
		else if (minRange < enemyWeapon.getMinRange())
			return minRange;
		else
			return maxRange;
	}
	
	/**
	 * Cambia el tipo del movimiento del enemigo al entrar una unidad del
	 * jugador en su rango de movimiento-
	 */
	private void enemySpotted() {
		moveType = 0;
	}
	
	/**
	 * Devuelve el tipo de movimiento de la unidad
	 * @return El tipo de movimiento
	 */
	public int getMoveType() {
		return moveType;
	}
	
	public AIStrategy getStrategy() {
		return strategy;
	}
	
	@Override
	public void draw(Batch batch, float parentAlpha) {
		if (commander)
			batch.draw(commanderStar, getX(), getY());
		if (endedAction) {
			batch.setColor(0.5f, 0.5f, 0.5f, 1);
			batch.draw(mapImage, getX(), getY());
			batch.setColor(1, 1, 1, 1);
		}
		else {
			batch.setColor(1, 0, 0, 1);
			batch.draw(mapImage, getX(), getY());
			batch.setColor(1, 1, 1, 1);
		}
	}

	@Override
	public void write(Json json) {
		super.write(json);
		json.writeValue("strategy", strategy, AIStrategy.class);
		json.writeArrayStart("position");
		json.writeValue(mapPosition.x);
		json.writeValue(mapPosition.y);
		json.writeArrayEnd();
		json.writeValue("reclutable", reclutable);
		json.writeValue("move", moveType);
	}
	
	@Override
	public void read(Json json, JsonValue jsonData) {
		super.read(json, jsonData);
		reclutable = jsonData.getBoolean("reclutable");
		moveType = jsonData.getInt("move");
		strategy = json.readValue(AIStrategy.class, jsonData.get("strategy"));
		currentHP = getCurrentStat(Stats.HITPOINTS);
		unitDetails = new UnitDetailWindow(Language.get("UnitDetail"), this);
	}

}
