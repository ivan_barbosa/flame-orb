package es.flameorb.units;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter.OutputType;

import es.flameorb.general.GamePreferences;
import es.flameorb.items.Item;
import es.flameorb.items.ItemFactory;
import es.flameorb.items.Weapon;

/**
 * Reprensenta el inventario de objetos de una unidad.
 * @author Iván Barbosa Gutiérrez
 *
 */
public class Inventory implements Json.Serializable {
	
	private Array<Item> inventory;
	
	public Inventory() {
		inventory = new Array<Item>();
	}
	
	/**
	 * Inserta un objeto en el inventario si hay espacio para ello.
	 * @param item - El objeto a introducir.
	 * @return Si el objeto puede ser introducido
	 */
	public boolean insertItem(Item item) {
		if (inventory.size < GamePreferences.getInventoryMaxCapacity()) {
			inventory.add(item);
			return true;
		}
		else
			return false;
	}
	
	public int getItemsNumber() {
		return inventory.size;
	}
	
	public Item getItem(int index) {
		return inventory.get(index);
	}
	
	public Array<Item> getItems() {
		return inventory;
	}
	
	public Array<Weapon> getWeapons() {
		Array<Weapon> weapons = new Array<Weapon>();
		for (Item item : inventory) {
			if (item instanceof Weapon)
				weapons.add((Weapon) item);
		}
		return weapons;
	}
	
	public boolean isInInventory(Item item) {
		return inventory.contains(item, true);
	}
	
	public boolean checkItem(String name) {
		for (Item item : inventory)
			if (item.getName().equals(name))
				return true;
		return false;
	}
	
	public void tossItem(String name) {
		for (Item item : inventory) {
			if (item.getName().equals(name)) {
				inventory.removeValue(item, true);
				return;
			}
		}
	}
	
	public void tossItem(Item item) {
		inventory.removeValue(item, true);
	}

	@Override
	public void write(Json json) {
		for (int i=0; i<inventory.size; i++)
			json.writeValue(Integer.toString(i), inventory.get(i), inventory.get(i).getClass());
	}

	@Override
	public void read(Json json, JsonValue jsonData) {
		if (jsonData.get("newItems") != null) {
			int [] inventoryArray = jsonData.get("newItems").asIntArray();
			int j=0;
			for (int i=0; i<inventoryArray.length/2; i++) {
				inventory.add(ItemFactory.getItem(inventoryArray[j], inventoryArray[j+1]));
				j += 2;
			}
			if (jsonData.get("dropable") != null) {
				boolean [] dropableArray = jsonData.get("dropable").asBooleanArray();
				for (int i=0; i<dropableArray.length; i++)
					inventory.get(i).setDropable(dropableArray[i]);
			}
		}
		else {
			int index = 0;
			JsonValue weaponJson = jsonData.get(index);
			while (weaponJson != null) {
				inventory.add(json.fromJson(Weapon.class, weaponJson.prettyPrint(OutputType.json, 1)));
				index++;
				weaponJson = jsonData.get(index);
			}
		}
	}

}
