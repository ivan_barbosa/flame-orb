package es.flameorb;

import com.badlogic.gdx.Game;

import es.flameorb.general.Initializer;
import es.flameorb.test.TestScreen;

public class FlameOrbTest extends Game {

	@Override
	public void create() {
		Initializer.initializeGame();
		setScreen(new TestScreen(this));
	}
	
	@Override
	public void render() {
		super.render();
	}

}
