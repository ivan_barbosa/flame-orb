<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Flame_Orb_Tiles" tilewidth="32" tileheight="32" tilecount="10" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <properties>
   <property name="id" type="int" value="0"/>
  </properties>
  <image width="32" height="32" source="floor_32.png"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="id" type="int" value="1"/>
  </properties>
  <image width="32" height="32" source="forest_32.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="id" type="int" value="2"/>
  </properties>
  <image width="32" height="32" source="fortress_32.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="id" type="int" value="3"/>
  </properties>
  <image width="32" height="32" source="river_32.png"/>
 </tile>
 <tile id="5">
  <properties>
   <property name="id" type="int" value="4"/>
  </properties>
  <image width="32" height="32" source="sea_32.png"/>
 </tile>
 <tile id="6">
  <properties>
   <property name="id" type="int" value="5"/>
  </properties>
  <image width="32" height="32" source="sand_32.png"/>
 </tile>
 <tile id="8">
  <properties>
   <property name="id" type="int" value="6"/>
  </properties>
  <image width="32" height="32" source="mount_32.png"/>
 </tile>
 <tile id="9">
  <properties>
   <property name="id" type="int" value="7"/>
  </properties>
  <image width="32" height="32" source="mountain_32.png"/>
 </tile>
 <tile id="10">
  <properties>
   <property name="id" type="int" value="8"/>
  </properties>
  <image width="32" height="32" source="lava_32.png"/>
 </tile>
 <tile id="11">
  <properties>
   <property name="id" type="int" value="9"/>
  </properties>
  <image width="32" height="32" source="wall_32.png"/>
 </tile>
</tileset>
