package es.flameorb.desktop;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

import es.flameorb.FlameOrb;

public class DesktopLauncher {
	public static void main (String[] arg) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		config.setTitle("Flame Orb");
		config.setMaximized(true);
		new Lwjgl3Application(new FlameOrb(), config);
	}
}
