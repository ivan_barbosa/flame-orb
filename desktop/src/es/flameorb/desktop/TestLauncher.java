package es.flameorb.desktop;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;

import es.flameorb.FlameOrbTest;

public class TestLauncher {
	public static void main (String[] arg) {
		Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
		config.setTitle("Flame Orb - Test Mode");
		config.setMaximized(true);
		new Lwjgl3Application(new FlameOrbTest(), config);
	}

}
